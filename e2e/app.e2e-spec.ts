import { AOTEcOibShellSrcPage } from './app.po';

describe('aot-ec-oib-shell-src App', () => {
  let page: AOTEcOibShellSrcPage;

  beforeEach(() => {
    page = new AOTEcOibShellSrcPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
