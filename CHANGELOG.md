## 1.0.0 RC10 (2017-06-27)
### Features:
  - update to eUI 2.0.0 RC101
  - added params to translation to be able to substitute variables
  - router.navigate implementation

### Bugfixes:
  - router.navigateByUrl now returns a promise<boolean>
---
## 1.0.0 RC9 (2017-06-08)
### Features:
  - Real integration of eUI 2.0.0 RC95
---
## 1.0.0 RC8 (2017-05-18)
### Features:
  - Integration of Angular-CLI
---
## 1.0.0 RC7 (2017-05-17)
### Bugfixes:
  - Servicemanager proxy bug
  - defining the homeroute now only takes application plugins into account
---
## 1.0.0 RC6 (2017-05-10)
### Features:
  - Upgrade to Angular _4.1.1_
---
## 1.0.0 RC5 (2017-05-03)
### Features:
  - _sessionKeepAlive_ is a property of the shell config (0 -> disable, n -> do a call every n minutes)
  - _logLevel_ defined the message level you want to see in the console
---
## 1.0.0 RC4 (2017-04-20)
### Features:
  - _SERVICE_HTTP_ in system services now implements the ecas session timeout check
  - _Shellservice_ now implements a session keep alive call (every 5 minutes) to the 'index.html' to keep the ecas session alive
---
## 1.0.0 RC3 (2017-04-10)
### Bugfixes:
  - _watcher$_ in appmodel for the shell
---
## 1.0.0 RC2 (2017-04-06)
### Bugfixes:
  - recursive initialisation of plugins
---
## 1.0.0 RC1 (2017-04-06)
### Breaking changes:
  - _menuContribution$_ and _topBarContribution$_ replaced by watcher$
  - removed all backwardscompatible parameters, oibapp shell and unneccessary style
---
## 0.28.2 (2017-04-10)
###Bugfixes
  - badgecount not updated in menu
---
## 0.28.1 (2017-04-07)
### Bugfixes
  - bugfixes of RC1 merged to this version
---
## 0.28.0 (2017-04-05)
### Features:
  - new _inner sidebar_ layouts added

### Bugfixes:
  - _menuContribution$_ and _topBarContribution$_ inserted index was wrong (-1)
---
## 0.27.2 (2017-04-05)
### Bugfixes:

  - _menuContribution$_ and _topBarContribution$_ called after the onInit (servicemanager was not initialized)
---
## 0.27.1 (2017-04-04)
### Bugfixes:

  - implementation of collapsed menu
  - shell: renaming according to codelyzer rules
---
## 0.27.0 (2017-03-31)
### Features:

  - _menuContribution$_, _topBarContribution$_ added to Pluginannotion: use this if you have the menu built from a service.
---
## 0.26.3 (2017-03-30)
### Bugfixes:

  - optimizing module imports and exports
---
## 0.26.2 (2017-03-29)
### Bugfixes:

  - routes[0] error if no routes are defined
---
## 0.26.1 (2017-03-29)
### Bugfixes:

  - logos where not in the repo...  
---
## 0.26.0 (2017-03-29)
### Features:

  - plugin router as system service
  - _routes_ parameter of the _plugin_ annotation accepts Routes as value.  
    - These are the routes needed for the plugin.
    - function '_rewriteRoutePath_' can be used in the annotation to get the correctvalue of a path 
  > rewriteRoutePath(yourpath,_undefined_,yourpluginID)
  - ROUTER_SERVICE can be injected in the plugin.
    - use _routerservice.getPluginPath('/yourpath')_ to get the correct path insede your plugin to be used for a routerlink
    - use _this.routerservice.navigateByUrl('yourpath')_ to navigate to the correct path inside your plugin
  > DON'T USE THE ANGULAR ROUTER FOR NAVIGATION
---
## 0.25.0 (2017-03-28)
### Features:

  - codelyzer and tslint basic config
  - baseHref webpack plugin to manage the baseHref in the build process, by default this is setup as the homeurl of the shell

---
## 0.24.2 (2017-03-20)
### Bugfixes:

  - given pluginID in the plugin annotation is now used as a UUID for the PluginAwareServiceRegister
  - TranslationService is now loaded at startup as PluginAware

---
## 0.24.1 (2017-03-16)
### Bugfixes:

  - removed dependency of @ec-digit-uxatec
  
---
## 0.24.0 (2017-03-16)
### Features:

  - _shell_ parameter is now a ENUM|euiAppInfo
  - _title_ parameter is now optional
  - _new_ system services: PluginAwareServiceRegister and TranslationService (instead of ng2-translate)
  - shellService can be used to get PAServices with getPAService(service,plugin)
  - _version_, _lastmod_ and _environment_ are now injected through webpack when doing a build, no need to manually change these
  - new layout: Fixed sidebar with clock in middle of header

---
## 0.23.0 (2017-03-02)
### Features:

  - OIB shell has been removed 
  - _shell_ parameter is now a ENUM 

### Bugfixes:

  - some tweaks to the shell behaviour 
  - temporary fix for the UX Language Selector:  component is now a copy without the bugs untill Digit provides a working component 

---
## 0.22.1 (2017-02-28)
### Bugfixes:

  - Recommitted the source (commit failed)

---
## 0.22.0 (2017-02-28)
### Features:

   - First implementation of the EUI layout into the shell
       - calls the titleservice to set the HTML title to the title provied in @Application
       - new optional parameter 'shell' in @Application to control the shelllayout of your application (oib-shell / eui-fixed-sidebar / eui-collapse-sidebar / eui-default-contract / eui-default-contract-fullwidth)
       - new optional parameter 'availableLanguages' in @Application, for each language specified you need a file in /public/i18n/${language}.json
   - Notification service has a title now
    
> Changes needed to be able to run this version:
>
>   - upgrade the @angular packages to 2.4.8
>   - upgrade webpack to version 2
>   - change the webpack config files
>   - change index.html to load the css according to the shell you use

---
## 0.21.4 (2017-02-06)
### Bugfixes:

   - Partially reverted last commits (new components were breaking some plugins) 

---
## 0.21.3 (2017-02-06)
### Bugfixes:

   - Fixed broken component templates (used 'templateUrl' and 'styleUrls' instead of 'template' and 'styles'

---
## 0.21.2 (2017-02-06)
### Features:

   - Changelog is now kept separate in the CHANGELOG.md file
   - Added a PageSubsection component 
   - Used the formatting components in the demo app

### Bugfixes:

   - Fixed exports && eliminated part of the test code in system-services-plugin

---
## 0.21.1 (2017-02-05)
### Features:

   - Added visual components to shell/core
   - Added INotificationService, with a default implementation based in 'alert's
   - Modified IUserPreferencesService to use cookies

---
## 0.21.0 (2017-02-05)
### Bugfixes:

   - Name oibkw was incorrectly left in export/package.json
   - Service methods with several parameters uncorrectly concatenated all parameters 
   