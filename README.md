# Pre-requisite
Check the following page in order to configure your machine:

        https://webgate.ec.europa.eu/CITnet/confluence/display/OIBADM/NPM

# Servers
Start a local server for the web application  

        npm start

Start a local server for the tests  

        npm run tests

> Tests and this section must be updated

# Pre-test the library before publishing it

Once the library is ready, it can be pre-tested with the _ec-oib-plugin-container_ project which is set-up to consume the library.

## Prepare your machine to _publish_ the library

Clone the _ec-oib-shell_ project into a folder on your PC at the same level of _ec-oib-shell-src_ project.  
For example, if _ec-oib-shell-src_ is located at

        C:\PGM\Projects\ec-oib-shell-src

You should clone the lib into

        C:\PGM\Projects\ec-oib-shell
    
>In order for a project consuming the library to be able to use different version of it, your package.json should point to the correct commit-id.  
>        For example to point to version 0.22.0, your package.json should look like:
>        "ec-oib-shell": "git+https://webgate.ec.europa.eu/CITnet/stash/scm/oibadm/ec-oib-shell.git#7a95e78924c"

## Package for publishing 
To transpile Typescript to Javascript and place the generated files in the _dist_ folder:  

        the version number is : major.minor.bugfix

        no change needed in version, execute npm run build
        update the version, execute the correct build script
                - major: npm run build-major
                - minor: npm run build-minor
                - bugfix: npm run build-bugfix
        
> Running the build script, updates the version (if needed) and creates a package.json file in the _dist_ folder which has the depencies as peerDependencies

## Finalise the version

Check the _package.json_ file in _dist_ and make sure that:
- the release version was updated 
- the peerDependencies are up to date  

> At this point, you should already be able to test the library in another project. 
> In order to do this, adjust your package.json to:
>       "ec-oib-shell": "file:../ec-oib-shell-src/dist"

## Publish locally
To copy the files from the _dist_ folder into the _ec-oib-shell_ project. 

        npm run copy

> The content of the _ec-oib-shell_ folder will be cleaned first.  
> However, the .git folder into that project will NOT be deleted.  
> At this point, you should already be able to test the library in another project. 
> In order to do this, adjust your package.json to:
>       "ec-oib-shell": "file:../ec-oib-shell"

## Publish to _CITnet Bitbucket_
 
Simply commit and push your work in both _ec-oib-shell_ and _ec-oib-shell-src_.

> Don't forget to merge back your branches (in both projects) to master.
  
## Publish to _Nexus_

> This part needs to be updated when _Nexus_ will be upgraded and will allow to publish packages starting with "@", like Angular2.
 
  


 
