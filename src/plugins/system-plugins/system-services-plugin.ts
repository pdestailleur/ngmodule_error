import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';
import { NgModule, PlatformRef, Injector } from '@angular/core';
import { Http, HttpModule, ConnectionBackend, RequestOptions, Connection } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { Plugin, IServiceLoader, IPluginContainer, IServiceManager, IService, core_log } from './../core';
import { LoggerService, SERVICE_LOGGER,
         DefaultSystemConfigurationService, SERVICE_SYSTEM_CONFIGURATION,
         NotificationService, SERVICE_NOTIFICATIONS ,
         HttpService, SERVICE_HTTP, IHttpService,
         GuardianMock, SERVICE_GUARDIAN, IGuardian,
         FavouritesMockService, SERVICE_FAVOURITES,
         UserPreferencesMockService, SERVICE_USER_PREFERENCES,
         PluginawareServiceregisterService, SERVICE_PLUGINAWARE_SERVICEREGISTER,
         DefaultTranslationService, SERVICE_TRANSLATIONS, DefaultTranslationsFactory,
         DefaultRouterService, SERVICE_ROUTER, DefaultRouterFactory, IRouter
} from './../../services';
import { IShellInfo, LogLevel } from './../../shell';

/**
 * DON'T USE ONINIT IN THE SYSTEM SERVICES PLUGIN!
 */

@Plugin({
    id: 'system_services',
    name: 'System services',
    services: [
        /* The logger service needs to be first */
        createLoggerService(),
        /* All services that need to be pluginaware */
        createTranslationService(),
        createPluginRouterService(),
        /* The pluginaware service */
        createPluginAwareServiceRegisterService(),
        /* All other services */
        createHttpService(),
        createGuardianService(),
        createFavouritesService(),
        createUserPreferencesService(),
        createSystemConfigurationService(),
        createNotificationsService()
    ],
    modules: [HttpModule, BrowserModule, RouterModule],
})
export class SystemServicesPlugin { }

export function createLoggerService(): IServiceLoader {
    return {
        key: SERVICE_LOGGER,
        name: 'Default ILoggerService',
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            return new LoggerService();
        },
        clazz: LoggerService
    };
}
export function createHttpService(): IServiceLoader {
    return {
        key: SERVICE_HTTP,
        name: 'Default IHttpService',
        instance: (serviceManager: IServiceManager, overwrittenService: IHttpService) => {
            return new HttpService(<Http>serviceManager.injector.get(Http));
        },
        clazz: HttpService
    };
}

export function createGuardianService(): IServiceLoader {
    return {
        key: SERVICE_GUARDIAN,
        name: `Guardian Mock`,
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            return new GuardianMock();
        },
        clazz: GuardianMock
    };
}

export function createNotificationsService(): IServiceLoader {
    return {
        key: SERVICE_NOTIFICATIONS,
        name: `Notifications service`,
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            return new NotificationService();
        },
        clazz: NotificationService
    };
}

export function createUserPreferencesService(): IServiceLoader {
    return {
        key: SERVICE_USER_PREFERENCES,
        name: `User preferences mock service`,
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            return new UserPreferencesMockService();
        },
        clazz: UserPreferencesMockService
    };
}

export function createFavouritesService(): IServiceLoader {
    return {
        key: SERVICE_FAVOURITES,
        name: `Favourites mock service`,
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            return new FavouritesMockService();
        },
        clazz: FavouritesMockService
    };
}

export function createSystemConfigurationService(): IServiceLoader {
    return {
        key: SERVICE_SYSTEM_CONFIGURATION,
        name: `System configuration default service`,
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            return new DefaultSystemConfigurationService();
        },
        clazz: DefaultSystemConfigurationService
    };
}

export function createTranslationService(): IServiceLoader {
    return {
        key: SERVICE_TRANSLATIONS,
        name: `Translations default service`,
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            return new DefaultTranslationService( serviceManager );
        },
        clazz: DefaultTranslationService
    };
}

export function createPluginRouterService(): IServiceLoader {
    return {
        key: SERVICE_ROUTER,
        name: `Service that handles routing for the plugins`,
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            return new DefaultRouterService(<Router>serviceManager.injector.get(Router));
        },
        clazz: DefaultRouterService
    };
}

export function createPluginAwareServiceRegisterService(): IServiceLoader {
    return {
        key: SERVICE_PLUGINAWARE_SERVICEREGISTER,
        name: `Service register for the services that have a plugin aware implementation`,
        instance: (serviceManager: IServiceManager, overwrittenService: any) => {
            const register = new PluginawareServiceregisterService( serviceManager );
            register.registerService(SERVICE_TRANSLATIONS, DefaultTranslationsFactory(<any>serviceManager.service(SERVICE_TRANSLATIONS)));
            register.registerService(SERVICE_ROUTER, DefaultRouterFactory(<any>serviceManager.service(SERVICE_ROUTER)));
            return register;
        },
        clazz: PluginawareServiceregisterService
    };
}
