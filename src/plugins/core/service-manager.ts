import { core_log, IServiceLoader } from './plugin-decorators';
import { Injector, OpaqueToken } from '@angular/core';
import { IShellInfo, LogLevel } from '../../shell';

/**
 * Interface which all services expose once they are loaded by the plugin engine
 */
export interface IService {
    /**
     * key, as defined in the @Plugin
     */
    readonly key: OpaqueToken;

    /**
     * name, as defined in the @Plugin
     */
    readonly name: string;

    /**
     * pluginId of the plugin which loaded this service
     */
    readonly pluginId: string;

    /**
     * Forces the service to be created, in case it is only an empty proxy
     */
    forceLoad(): void;
}

/**
 * Manages the services registered by the plugins
 */
export interface IServiceManager {
    /**
     * Returns all the services already loaded, with the associated metainformation
     */
    readonly services: IService[];
    /**
     * Returns the Angular injector
     */
    injector: Injector;
    /**
     * Returns the service of the given key, if it exists (without the associated metainformation)
     */
    service(key: OpaqueToken): IService;
    /**
     * Needed to provide default logging
     */
    log(message: any, ...additional_arguments: any[]): void;
}

/**
 * Extension of the IServiceManager interface, for internal use
 */
export interface IExtendedServiceManager extends IServiceManager {
    /**
     * Returns all the service loaders which have been registered for a given key, in the order which have been loaded.
     * Therefore, the last one is the active one
     */
    serviceCandidatesForKey(key: OpaqueToken): IServiceLoader[];

    /**
     * Registers the service in the system.
     */
    registerServices(pluginId: string, services: IServiceLoader[]): void;

    /**
     * Initializes the services registered
     */
    bootstrap(): void;
}

/**
 * Implementation of the IServiceManager
 */
export class DefaultServiceManager implements IExtendedServiceManager {
    private _registeredLoaders: IServiceLoader[] = [];
    private _services: IService[] = [];

    /**
     * @param _injector
     * @param _oldServiceManager
     * @param _shell --> only needed for the log function
     */
    constructor(private _injector: Injector, private _oldServiceManager: IExtendedServiceManager, private _shell: IShellInfo) { }

    /**
     * calls the core_log defined in the plugin-decorator
     * @param message
     * @param additional_arguments
     */
    log(message: any, ...additional_arguments: any[]) {
        core_log(message, LogLevel.DEBUG, this._shell, ...additional_arguments);
    }

    get services(): IService[] {
        return this._services;
    }

    get injector() {
        return this._injector;
    }

    get registeredLoaders() {
        return this._registeredLoaders;
    }

    registerServices(pluginId: string, loaders: IServiceLoader[]): void {
        if (!loaders) { return; }
        loaders.forEach(f => this.registerService(pluginId, f));
    }

    registerService(pluginId: string, loader: IServiceLoader): void {
        if (!loader.clazz && loader.instance instanceof Function) {
            throw new Error(`A service needs to either define 'clazz' or to provide a built object as the instance.
                            ${loader.name} provides no clazz but loader function instead of an object`);
        }
        const existing = this.registeredLoaders.filter(f => f.key === loader.key)[0];
        if (existing) {
            this.log(`Overwriting service '${existing.name}' with service '${loader.name}'`);
        } else {
            this.log(`Service '${loader.name}' registered`);
        }
        loader['pluginId'] = pluginId;
        this.registeredLoaders.unshift(loader);
    }

    service(key: OpaqueToken): IService {
        if (!this._services) {
            return undefined;
        }
        return this._services.filter(ls => ls.key === key)[0];
    }

    bootstrap(): void {
        this.log(`Loading services: START`);

        const servicesToLoad = findCurrentLoaders(this.registeredLoaders);

        this._services = servicesToLoad.map(loader => this.createProxyService(loader));

        this._services.forEach(p => {
            p.forceLoad();
        });

        this.log(`Loading service: END`);
    }

    serviceCandidatesForKey(key: OpaqueToken): IServiceLoader[] {
        return this.registeredLoaders.filter(s => s.key === key);
    }

    private createProxyService(loader: IServiceLoader): IService {
        const loaders = this._registeredLoaders.filter(ld => ld.key === loader.key);

        return new LazyServiceProxy( this , loaders, this._oldServiceManager ? this._oldServiceManager.service(loader.key) : undefined);
    }

}

/**
 * An IService which delays the creation of the actual service until any of its methods is called
 */
export class LazyServiceProxy implements IService {
    public key: OpaqueToken;
    public name: string;
    public pluginId: string;
    private loader: IServiceLoader;
    private overwrittenLoaders: IServiceLoader[];
    private implementation: Object = undefined;

    constructor(private serviceManager: IServiceManager, loaders: IServiceLoader[], private oldProxy?: IService) {
        if (!loaders || loaders.length === 0) {
            throw new Error('Attempted to build empty service');
        }

        const [current, ...others] = loaders;
        this.key = current.key;
        this.name = current.name;
        this.pluginId = current['pluginId'];

        this.loader = current;
        this.overwrittenLoaders = others;

        if (current.clazz) {
            this.addClazzMethods(current.clazz);
        } else if ( current.instance instanceof Object) {
            // we should have an object instead of an initializer function, so we can load it right away
            this.addInstanceMethods( current.instance );
        } else {
            this.forceLoad();
        }
    }

    /**
     * Creates and adds to this instance the methods of the passed object instance. The implementation of all those methods
     * will simply trigger the creation of the wrapped service and then pass the call to that service
     * @param instance
     */
    private addInstanceMethods(instance: Object) {
        // FIXME: methods from superclasses are not loaded
        Object.keys(instance['__proto__']).forEach(m => {
            if (instance[m] instanceof Function) {
                this[m] = (...args: any[]) => {
                    if (!this.implementation) {
                        this.forceLoad();
                    }
                    return this.implementation[m](...args);
                };
            } else {
                if (!this.implementation) {
                    this.forceLoad();
                }
                this[m] = this.implementation[m];
            }
        });
    }

    /**
     * Creates and adds to this instance the methods of the passed class clazz. The implementation of all those methods
     * will simply trigger the creation of the wrapped service and then pass the call to that service
     * @param clazz
     */
    private addClazzMethods(clazz: Function) {
        // FIXME: methods from superclasses are not loaded
        Object.getOwnPropertyNames(clazz.prototype).forEach(m => {
            if (clazz.prototype[m] instanceof Function) {
                this[m] = (...args: any[]) => {
                    if (!this.implementation) {
                        this.forceLoad();
                    }
                    return this.implementation[m](...args);
                };
            } else {
                if (!this.implementation) {
                    this.forceLoad();
                }
                this[m] = this.implementation[m];
            }
        });
    }

    /**
     * Builds the service object from the loader that this proxy is using, if any
     */
    private buildService(): Object {
        if (!this.loader) {
            throw new Error('Bug: call to buildService of a service without a loader');
        }

        if (typeof this.loader.instance !== 'function') {
            // The service is an already instanciated object
            return this.loader.instance;
        }

        let overwrittenProxy: IService = undefined;

        if (this.overwrittenLoaders && this.overwrittenLoaders.length > 0) {
            // Here we go: the recursion which makes all this work happens here
            overwrittenProxy = new LazyServiceProxy(this.serviceManager, this.overwrittenLoaders);
        }

        return this.loader.instance(this.serviceManager, overwrittenProxy);
    }

    public forceLoad() {
        if (this.implementation) {
            return;
        }
        this.serviceManager.log('Force loading ' + this.key.toString());
        this.setImplementation(this.buildService());
    }

    /**
     * Changes the internal implementation which this proxy is using.
     */
    public setImplementation(impl: Object) { // FIXME: remove previous methods?
        if (!impl) {
            throw new Error('Bug: call to LazyServiceProxy.setImplementation(undefined)');
        }
        let proto: any = (<any>impl).__proto__; // ugly cast to avoid compiler to complain
        do {
            Object.getOwnPropertyNames(proto).filter(p => proto[p] instanceof Function).forEach((m) => {
                (<any>this).__proto__[m] = (...args: any[]) => {
                    return impl[m].apply(impl, args);
                };
            });
            /*
            Object.getOwnPropertyNames(proto).filter(p => !(proto[p] instanceof Function || p == '__proto__')).forEach((property) => {
                Object.defineProperty( (<any>this).__proto__ , property, {
                        get: function() {
                            return impl[property];
                        },
                        set: function(value:any) {
                            return impl[property] = value;
                        },
                        enumerable: true,
                        configurable: true
                    }
                );
            });
            */
        } while (proto = proto.__proto__);

        this.implementation = impl;

        if (this.oldProxy && this.oldProxy['setImplementation']) {
            this.oldProxy['setImplementation'](impl);
        }
    }
}

/**
 * Returns a list containing only the last loader for every key found on the list
 */
function findCurrentLoaders(loaders: IServiceLoader[]): IServiceLoader[] {
    if (!loaders) {
        return [];
    }

    return loaders.filter(loader => loader === findLastLoaderWithKey(loaders, loader.key));
}

function findLastLoaderWithKey(array: IServiceLoader[], key: OpaqueToken): IServiceLoader {
    if (!array) {
        return undefined;
    }

    return array.filter(loader => loader.key === key)[0];
}


