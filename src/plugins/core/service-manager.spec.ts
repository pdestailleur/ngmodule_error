import { DefaultServiceManager, IServiceManager } from './service-manager';
import { IShellEnvironment, IShellInfo, LogLevel } from '../../shell';

import { IServiceLoader } from './plugin-decorators';
import { OpaqueToken } from '@angular/core';

describe('Test ServiceManager methods', () => {
    interface IDo {
        do(a: number, b: number): number;
    }
    class Adder implements IDo {
        do(a: number, b: number): number {
            return a + 2 * b;
        }
    }
    class Subtractor implements IDo {
        do(a: number, b: number): number {
            return a - 2 * b;
        }
    }

    const token0 = new OpaqueToken('LOADER0');
    const loader0: IServiceLoader = {
        name: 'loader0',
        key: token0,
        instance: () => {
            return {
                do: (a: number, b: number) => a + b
            };
        }
    };

    const token1 = new OpaqueToken('LOADER1');
    const loader1: IServiceLoader = {
        name: 'loader1',
        key: token1,
        instance: () => {
            return {
                do: (a: number, b: number) => a + b
            };
        },
        clazz: Adder
    };

    const token2 = new OpaqueToken('LOADER2');

    const loader2: IServiceLoader = {
        name: 'loader2',
        key: token2,
        instance: () => {
            return {
                do: (a: number, b: number) => a - b
            };
        },
        clazz: Subtractor
    };

    const token3 = new OpaqueToken('LOADER3');

    const loader3: IServiceLoader = {
        name: 'loader3',
        key: token3,
        instance: new Adder()
    };

    const token4 = new OpaqueToken('LOADER4');

    const loader4: IServiceLoader = {
        name: 'loader4',
        key: token4,
        instance: new Subtractor()
    };

    const shell: IShellInfo = {
        log: true,
        logLevel: LogLevel.DEBUG,
        environment: IShellEnvironment.DEVELOPMENT,
        version: 'TEST',
        lastMod: new Date(),
        info: '',
        homeUrl: '/',
        layout: undefined,
        theme: undefined
    };

    it('Should throw error without clazz', () => {
        const sm = new DefaultServiceManager(undefined, undefined, shell);
        expect(() => sm.registerService('plugin1', loader0)).toThrowError(Error);
    });


    it('Should be bootstrapped correctly', () => {
        const sm = new DefaultServiceManager(undefined, undefined, shell);
        sm.registerService('plugin1', loader1);
        expect(sm.bootstrap()).toBeUndefined();
    });

    it('Should sum numbers correctly', () => {
        const sm = new DefaultServiceManager(undefined, undefined, shell);
        sm.registerService('plugin1', loader1);
        sm.bootstrap();
        const instance: IDo = <any>sm.service(token1);
        const res = instance.do(2, 2);
        expect(res).toBe(4);
    });

    it('Should not override same methods with function initializing', () => {
        const sm = new DefaultServiceManager(undefined, undefined, shell);
        sm.registerService('plugin1', loader1);
        sm.registerService('plugin2', loader2);
        sm.bootstrap();
        let instance: IDo = <any>sm.service(token1);
        const res1 = instance.do(2, 2);
        instance = <any>sm.service(token2);
        const res2 = instance.do(2, 2);
        expect(res1 === 4 && res2 === 0).toBeTruthy();
    });

    it('Should not override same methods with class initializing', () => {
        const sm = new DefaultServiceManager(undefined, undefined, shell);
        sm.registerService('plugin3', loader3);
        sm.registerService('plugin4', loader4);
        sm.bootstrap();
        let instance: IDo = <any>sm.service(token3);
        const res1 = instance.do(2, 2);
        instance = <any>sm.service(token4);
        const res2 = instance.do(2, 2);
        expect(res1 === 6 && res2 === -2).toBeTruthy();
    });
});
