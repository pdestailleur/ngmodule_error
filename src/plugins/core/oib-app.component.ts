import {
    Component,
    ApplicationRef,
    PlatformRef, Injector, Inject
} from '@angular/core';
import { Router } from '@angular/router';
import { IExtendedPluginContainer, SERVICE_PLUGIN_CONTAINER } from './plugin-container';
import { SERVICE_LOGGER, LoggerService } from './../../services';

@Component({
    selector: 'oib-app',
    template: `<shell-app-template [appModel]="pluginContainer.applicationModel"></shell-app-template>`,
    styles: [`
    .app {
        display: flex;
        height: 100%;
    }

    .app .menu {
        height: 100%;
        background: #106CC8;
        box-shadow: 0 2px 4px -1px rgba(0,0,0,.2),0 4px 5px 0 rgba(0,0,0,.14),0 1px 10px 0 rgba(0,0,0,.12);
        min-width: 250px;
    }

    .app .menu ul {
        display: flex;
        flex-direction: column;
        padding: 0;
        margin: 0;
        cursor: pointer;
    }

    .app .menu ul li {
        color: #F5F5F5;
        font-size: 17px;
        padding: 15px 0 15px 20px;
        list-style: none;
    }

    .app .menu ul li:hover {
        background: #408EDE;
    }

    .app .menu ul li:focus {
        background: #408EDE;
    }

    .app .menu .logo {
        width: 100%;
        height: 150px;
        display: flex;
        flex-direction: column;
        align-items: center;
        padding-top: 20px;
        padding-bottom: 10px;
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0, #185694), color-stop(1, #106cc8));
    }

    .app .menu .logo .logo-circle {
        width: 150px;
        height: 150px;
        border-radius: 50%;
        background: #DE3641;
        box-shadow: 0px 0px 10px #09294B;
    }

    .app .menu .logo .logo-circle::before {
        content: '';
        display: block;
        margin-left: 50%;
        height: 100%;
        border-radius: 0 100% 100% 0 / 50%;
        background: #BF2E37;
    }

    .app .menu .logo .fa-bolt {
        position: absolute;
        font-size: 100px;
        color: whitesmoke;
        top: 50px;
        left: 102px;
    }

    .app .menu .logo .fa-circle {
        text-shadow: 0px 0px 10px #134273;
    }

    .app .menu .dg {
        text-align: center;
        font-size: 25px;
        color: whitesmoke;
    }

    .app .menu .library {
        text-align: center;
        color: rgba(255, 255, 255, 0.54);
        padding-bottom: 20px;
        border-bottom: solid 1px #267ED5;
    }

    .app .main {
        width: 100%;
        height: 100%;
    }

    .app .top-bar {
        display: flex;
        justify-content: space-between;
        align-items: center;
        font-size: 24px;
        padding: 20px;
        color: #444;
        background: #f6f6f6;
        box-shadow: 0px 0px 1px 1px rgba(0, 0, 0, 0.14), 0px 0px 2px 2px rgba(0, 0, 0, 0.098), 0px 0px 5px 1px rgba(0, 0, 0, 0.084);
    }

    .app .top-bar .version {
        font-size: 15px;
        color: #aaa;
    }
    `]
})
export class OIBAppComponent {
    private title = '';

    constructor(private router: Router, private injector: Injector,
                @Inject(SERVICE_PLUGIN_CONTAINER) private pluginContainer: IExtendedPluginContainer) {
        pluginContainer.bootstrap(injector);
    }
}

