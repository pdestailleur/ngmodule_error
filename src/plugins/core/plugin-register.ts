import { IPluginParams, findPluginByClassName, IApplicationParams } from './plugin-decorators';
import { IExtendedPluginContainer } from './plugin-container';
import { OpaqueToken } from '@angular/core';

/**
 * Internal interface for the object which manages the registering of plugins by the annotations
 */
export interface IPluginRegister {
    /**
     * Returns the list of plugins loaded at the root level (@Application decorator)
     */
    readonly rootPlugins: IPluginParams[];

    /**
     * Registers the application annotation, giving an error if it's called more than once
     */
    registerApplication(params: IApplicationParams): void;

    /**
     * Registers a plugin from the parameters passed to the Plugin annotation. The plugin will be a candidate plugin
     * for loading, but it won't be loaded until it is contained in the "plugins" section of the @Application or @Plugin
     * annotations
     * @param params
     */
    registerPlugin(params: IPluginParams): void;

    /**
     * Marks a plugin as active or inactive. Inactive plugins will not be loaded. Activates or deactivates a given plugin.
     * @param p plugin to be (de)activated
     * @param activate whether to activate or deactivate the plugin p
     */
    activatePlugin(pluginId: string, activate?: boolean): void;

    /**
     * Finds and returs the registered plugin which corresponds to the class of the given name
     */
    findPluginByClassName(className: string): IPluginParams;
}

export interface IExtendedPluginRegister extends IPluginRegister {
    readonly registeredPlugins: IPluginParams[];
    readonly activePlugins: string[];
    readonly registeredApplication: IApplicationParams;
    readonly registeredServiceKeys: OpaqueToken[];
    setPluginContainer(pluginContainer: IExtendedPluginContainer): void;
}

/**
 * Implementation of the register interface
 */
export class DefaultPluginRegister implements IExtendedPluginRegister {
    public registeredPlugins: IPluginParams[] = [];
    public registeredApplication: IApplicationParams = null;
    public rootPlugins: IPluginParams[] = [];
    public activePlugins: string[] = [];
    private pluginContainer: IExtendedPluginContainer;

    constructor() {
    }

    setPluginContainer(pluginContainer: IExtendedPluginContainer): void {
        this.pluginContainer = pluginContainer;
    }

    registerApplication(params: IApplicationParams): void {
        if (!params) {
            throw new Error('Attempt to register and empty application');
        }
        this.registeredApplication = params;
        this.rootPlugins = params.plugins.map(p => this.findPluginByClassName(p.name));
    }

    public findPluginByClassName(className: string): IPluginParams {
        return findPluginByClassName(this.registeredPlugins, className);
    }

    registerPlugin(params: IPluginParams): void {
        this.registeredPlugins.push(params);
        this.activePlugins.push(params.id);

    }

    activatePlugin(pluginId: string, activate: boolean = true): void {
        if ((this.rootPlugins || []).map(p => p.id).filter(id => id === pluginId).length === 0) {
            throw new Error('** Call to activatePlugin of a non existing plugin');
        }

        if (activate) {
            this.activePlugins.push(pluginId);
        } else {
            this.activePlugins = this.activePlugins.filter(id => id !== pluginId);
        }

        this.pluginContainer.reload();
    }

    public get registeredServiceKeys(): OpaqueToken[] {
        let serviceKeys: any[] = [];

        this.registeredPlugins.forEach(plugin => {
            serviceKeys.push(...getServiceKeysForPlugin(this.registeredPlugins, plugin));
        });

        serviceKeys = serviceKeys.filter((item, index, inputArray) => {
            return inputArray.indexOf(item) === index;
        });


        return serviceKeys;
    }
}

function getServiceKeysForPlugin(plugins: IPluginParams[], plugin: IPluginParams): OpaqueToken[] {
    if (!plugin) {
        return [];
    }

    const ret: OpaqueToken[] = [];

    (plugin.plugins || []).forEach(p => {
        ret.push(...getServiceKeysForPlugin(plugins, findPluginByClassName(plugins, p.name)));
    });

    (plugin.services || []).forEach(s => {
        ret.push(s.key);
    });

    return ret;
}

