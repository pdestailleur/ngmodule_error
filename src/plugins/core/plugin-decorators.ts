import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { DefaultPluginContainer, SERVICE_PLUGIN_CONTAINER } from './plugin-container';
import { DefaultPluginRegister } from './plugin-register';
import { DirectivesModule } from '../../directives/directives.module';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {
    IApplicationModel,
    IMenu,
    IMenuEntry,
    IMenuSection,
    IShellInfo,
    ITopBar,
    LogLevel,
    ShellAppModule
    } from '../../shell';
import { IPluginContainer } from './plugin-container';
import { IService, IServiceManager } from './service-manager';
import { Observable } from 'rxjs/Rx';
import { OIBAppComponent } from './oib-app.component';
import { Route, RouterModule, Routes } from '@angular/router';
import {
    Inject,
    Injector,
    NgModule,
    OpaqueToken,
    PlatformRef,
    Type,
} from '@angular/core';


const PLUGIN_REGISTER = new DefaultPluginRegister();
const PLUGIN_CONTAINER = new DefaultPluginContainer(PLUGIN_REGISTER);
const CLASS_NAME_ATTRIBUTE = '__plugin_class_name__';

/**
 * Interface implemented by all the services managed by plugins
 */
export interface IServiceLoader { // API
    /**
     * Unique identifier for the service. If another service has the same key then only the last one will be registered
     */
    key: OpaqueToken;

    /**
     * Descriptive name of the service, to be used by the log and dev tools to visualize it
     */
    name: string;

    /**
     * Implementation of the service, which is given either by an object or by a function that returns an object. In
     * the case that the implementation is a function, it will be called when bootstraping the services. If this
     * factory intends to use the existing factory for the same key (if existing) it can call the previousImplementation
     * function, which will create the last defined factory with the same key
     */
    instance: Object | IServiceObjectLoaderFunction;


    /**
     * Class of the instance object. If the instance is an Object, this is not used. If the instance is a function, then
     * clazz is required
     */
    clazz?: Function;
}

/**
 * Represents a function which can build a service from an injector, a callback to get a an instance of the
 * service which is overwritten by the current service and from the complete list of alrady loaded services
 */
export type IServiceObjectLoaderFunction = (serviceManager: IServiceManager, overwrittenService?: IService) => Object;


/**
 * Represents a function which is executed when bootstrapping the plugin
 */
export type IPluginOnInitFunction = (serviceManager: IServiceManager, shell?: IShellInfo) => void;


/**
 * Parameters for the Plugin annotation
 */
export interface IPluginParams { // API
    /**
     * Unique id identifiying the plugin, to be used as a qualifier to avoid name clashes. It will be used for the
     * routes of the plugin, so this string should be URL friendly (no spaces or special characters, etc)
     */
    id: string;

    /**
     * Name of this plugin, to be used in log and dev tools to visualize it
     */
    name: string; // Name of the plugin

    /**
     * Contribution to the menu of the application.
     */
    menuContribution?: IMenuContribution | ((sm: IServiceManager) => IMenuContribution);

    /**
     * Contribution to the top bar
     */
    topBarContribution?: ITopBarContribution | ((sm: IServiceManager) => ITopBarContribution);

    /**
     * Function which returns the services added by this plugin. The function receives the list of services received
     * so far, which can be useful when wrapping other services or when deciding whether to provide a default
     * implementation of a service if one does not already exist
     */
    services?: IServiceLoader[];

    /**
     * List of other plugins this one depends on.
     */
    plugins?: Array<Function>;

    /**
     * Modules that this plugin depends on, if any
     */
    modules?: any[];

    /**
     * Routes that this plugin depends on, if any
     */
    routes?: Routes;

    /**
     * Executes when bootstrapping the plugin
     */
    onInit?: IPluginOnInitFunction;
    /**
     * Observable to modify pluginparams async
     */
    watcher$?: ((sm: IServiceManager) => Observable<IObservablePluginParams>);
}

/**
 * Interface of the returnvalue for the watcher plugin parameter
 */
export interface IObservablePluginParams {
    menuContribution: IMenuContribution;
    topBarContribution: ITopBarContribution;
}



// We extend these interfaces so we can later on add extra metainformation to the contribution which is not included
// in the model as, for example, information about relative loaction of menu entries, etc

export interface IMenuContribution extends IMenu { // API
    sections: IMenuSectionContribution[];
}

export interface IMenuSectionContribution extends IMenuSection { // API
    entries: IMenuEntryContribution[];
}

export interface IMenuEntryContribution extends IMenuEntry { // API
    // No extension for the moment
}

export interface ITopBarContribution extends ITopBar { // API
    // No extension for the moment
}

/**
 * Parameters to pass to the @Application annotation
 */
export interface IApplicationParams { // API
    /**
     * Shell of the application
     */
    shell?: IShellInfo;
    /**
     * Title of the application
     */
    title?: string;
    /**
     * Array of plugin this application consist of
     */
    plugins: Array<Function>;
    /**
     * the homeplugin of the application
     */
    homePlugin?: Function;
}

////////////////////////////////////////////
// Annotations
/////////////////////////////////////////

/**
 * This is an internal annotation, the general annotation registers some system plugins before calling this one
 */
export function _InternalApplication(params: IApplicationParams) { // Public
    return function (constructor: Function) {
        PLUGIN_REGISTER.registerApplication(params);
        NgModule(_buildNgModuleParamsForApp(params.plugins, params.homePlugin))(constructor); // We apply the NgModule annotation.
    };
}

/**
 * Annotation which defines a plugin
 */
export function Plugin(params: IPluginParams) {
    return function (constructor: Function) {
        params[CLASS_NAME_ATTRIBUTE] = constructor.name;
        PLUGIN_REGISTER.registerPlugin(params);
        NgModule(_buildNgModuleParamsForPlugin(params))(constructor);
        constructor['__pluginParams__'] = params;
    };
}

/**
 * Annotation which allows a class to inject a reference to the PluginContainer object in one of its members
 */
export function PluginContainer(target: Object, propertyKey: string | symbol) {
    target[propertyKey] = PLUGIN_CONTAINER;
}

function _buildNgModuleParamsForPlugin(params: IPluginParams) {
    const modules: any[] = [
        CommonModule,
        DirectivesModule
    ];
    if (params.modules) {
        modules.push(params.modules);
    }

    if (params.plugins) {
        modules.push(params.plugins);
    }

    const ret = {
        modules: modules,
        exports: modules
    };
    return ret;
}

function _buildNgModuleParamsForApp(plugins: Array<Function>, homePlugin: Function) {
    const providers: any[] = [HttpModule, BrowserModule, { provide: SERVICE_PLUGIN_CONTAINER, useValue: PLUGIN_CONTAINER }];

    const serviceKeys = PLUGIN_REGISTER.registeredServiceKeys;

    (serviceKeys || []).forEach(key => {
        providers.push({ provide: key, useFactory: () => PLUGIN_CONTAINER.serviceByKey(key) });
    });
    let routes = [];
    let homeDefined = false;
    let newroute: Route;
    PLUGIN_REGISTER.registeredPlugins.filter( plugin => { return plugin.routes !== undefined; }).forEach( plugin => {
        const isAppPlugin = plugins.findIndex( appPlugin => {
            return appPlugin['__pluginParams__'].id === plugin.id;
        } ) > -1;
        plugin.routes.forEach( route => {
            if ( isAppPlugin && !homeDefined && route.redirectTo === undefined) {
                if (homePlugin === undefined || homePlugin['__pluginParams__'].id === plugin.id) {
                    newroute = {
                        path: '',
                        pathMatch: 'full',
                        redirectTo: rewriteRoutePath(route.path, undefined, plugin.id)
                    };
                    routes = [newroute].concat(routes);
                    homeDefined = true;
                    core_log('defining Home as ', LogLevel.DEBUG, PLUGIN_REGISTER.registeredApplication.shell , newroute.redirectTo);
                }
            }
            newroute = route;
            newroute.path = rewriteRoutePath(route.path, undefined, plugin.id);
            core_log(plugin.id, LogLevel.DEBUG, PLUGIN_REGISTER.registeredApplication.shell , ' rewriting route as ', newroute.path);
            if (route.redirectTo) {
                newroute.redirectTo = rewriteRoutePath(route.redirectTo, undefined, plugin.id);
            }
            routes = routes.concat(newroute);
        });
    });
    if (routes.length > 0) {
        newroute = {
            path: '**',
            redirectTo: routes[0].redirectTo ? routes[0].redirectTo : routes[0].path
        };
        routes = routes.concat([newroute]);
    }

    const appModule: Object = {
        imports: [BrowserModule, ShellAppModule, RouterModule.forRoot(routes), ...plugins],
        declarations: [OIBAppComponent],
        providers: providers,
        exports: [OIBAppComponent]
    };

    core_log('appModule', LogLevel.DEBUG, PLUGIN_REGISTER.registeredApplication.shell, appModule);

    return appModule;
}

export function findPluginByClassName(plugins: IPluginParams[], className: string) {
    return (plugins || []).filter(p => p[CLASS_NAME_ATTRIBUTE] === className)[0];
}

export function rewriteRoutePath(path: string, plugin: Object, pluginID?: string): string {
    if ( plugin ) {
        pluginID = plugin['__pluginParams__'].id;
    }
    pluginID = convertSnakeCase(pluginID);
    let newpath = 'screen/';
    if (path.charAt(0) === '/') {
        newpath = '/' + newpath;
        path = path.slice(1);
    }
    if (path !== '' && path !== '/') {
        newpath = newpath + pluginID + '/' + path;
    } else {
        newpath = newpath + pluginID;
    }
    return newpath;
}

function convertSnakeCase(value: string): string {
    let newstring: string[] = [];
    value.split('').forEach( (letter, index) => {
        if (index === 0) {
            newstring = newstring.concat(letter.toLowerCase());
        } else {
            if (letter === letter.toUpperCase() && !isNumber(letter)) {
                newstring = newstring.concat('-');
            }
            newstring = newstring.concat(letter.toLowerCase());
        }
    });
    return newstring.join('');
}

function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

export function core_log(message: string, logLevel: LogLevel, shell: IShellInfo, ...additional_arguments: any[]) {
    if (logLevel > 0 && logLevel <= shell.logLevel) {
       if (typeof message === 'string') {
            message = LogLevel[logLevel].toUpperCase() + ' : ' + message;
       }
       switch (logLevel) {
           case LogLevel.DEBUG:
                // tslint:disable-next-line:no-console
                console.debug(message, ...additional_arguments);
               break;
           case LogLevel.INFO:
                // tslint:disable-next-line:no-console
                console.info(message, ...additional_arguments);
               break;
           case LogLevel.WARN:
                console.warn(message, ...additional_arguments);
               break;
           case LogLevel.ERROR:
                console.error(message, ...additional_arguments);
               break;
       }
    }
}

