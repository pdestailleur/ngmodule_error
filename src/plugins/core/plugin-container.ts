import { DefaultServiceManager, IExtendedServiceManager, IService } from './service-manager';
import { EuiAppInfo, LogLevel, euiShellLayout } from './../../shell';
import { IApplicationModel, IExtendedApplicationModel, IShellInfo } from './../../shell';
import { IExtendedPluginRegister, IPluginRegister } from './plugin-register';
import { IPluginParams, core_log } from './plugin-decorators';
import { Injector, OpaqueToken } from '@angular/core';

import { PluginContainerAppModel } from './plugin-container-app-model';

/**
 * Token where the IPluginContainer object will be made available to the angular root injector
 */
export const SERVICE_PLUGIN_CONTAINER = new OpaqueToken('IPluginContainer'); // API

/**
 * Interface to the container application (plugin container). It contains hooks for the plugins to
 * programmatically interact with the container
 */
export interface IPluginContainer { // API
    /**
     * Returns the list of all the loaded services (so far) in the application. Note that this list does not
     * include any service defined in the Angular injector
     */
    readonly services: IService[];
    /**
     * Returns the service instance of the given key searching both in the plugin defined services and inside the
     * Angular injector (in this order)
     */
    serviceByKey(key: OpaqueToken | any): Object;
}

/**
 * Internal interface offering added capabilities to the IPluginContainer interface
 */
export interface IExtendedPluginContainer extends IPluginContainer {
    readonly serviceManager: IExtendedServiceManager;
    readonly pluginRegister: IExtendedPluginRegister;
    readonly applicationModel: IExtendedApplicationModel;

    /**
     * Finds and returs the registered plugin which corresponds to the class of the given name
     */
    findPluginByClassName(className: string): IPluginParams;

    /**
     * Reloads the plugins
     */
    reload(): void;

    /**
     * To be called from the bootstraping code in the main component
     */
    bootstrap(injector: Injector): void;
}

/**
 * Default implementation of the IPluginContainer interface
 */
export class DefaultPluginContainer implements IExtendedPluginContainer {
    private injector: Injector;
    private _serviceManager: IExtendedServiceManager;
    public applicationModel: IExtendedApplicationModel = new PluginContainerAppModel();

    constructor(public pluginRegister: IExtendedPluginRegister) {
        pluginRegister.setPluginContainer(this);
    }

    serviceByKey(key: OpaqueToken): Object {
        if (!this._serviceManager) {
            return undefined;
        }

        let ret = this._serviceManager.service(key);
        if (!ret) {
            ret = this.injector.get(key);
        }
        return ret;
    }

    get services(): IService[] {
        return this.serviceManager.services;
    }

    get serviceManager(): IExtendedServiceManager {
        return this._serviceManager;
    }

    get plugins(): IPluginParams[] {
        return this.pluginRegister.rootPlugins;
    }

    bootstrap(injector: Injector) {
        this.injector = injector;
        this.reload();
    }

    reload(): void {
        [this.applicationModel, this._serviceManager] = loadPlugins(this.injector, this.pluginRegister, this.serviceManager);
    }


    findPluginByClassName(className: string): IPluginParams {
        return this.pluginRegister.findPluginByClassName(className);
    }
}

function loadPlugins(injector: Injector,
    pluginRegister: IExtendedPluginRegister,
    oldServiceManager: IExtendedServiceManager): [IExtendedApplicationModel, IExtendedServiceManager] {

    core_log('(Re)loading plugins', LogLevel.DEBUG, pluginRegister.registeredApplication.shell);

    const applicationModel = new PluginContainerAppModel();
    applicationModel.shell = <IShellInfo>new EuiAppInfo();
    if (pluginRegister.registeredApplication.shell) {
        applicationModel.shell = <IShellInfo>pluginRegister.registeredApplication.shell;
    }

    const serviceManager = new DefaultServiceManager(injector, oldServiceManager, pluginRegister.registeredApplication.shell);

    const processedPlugins: IPluginParams[] = [];
    const activePlugins = pluginRegister.rootPlugins
        .filter(plugin => pluginRegister.activePlugins.filter(id => id === plugin.id).length > 0);
    activePlugins.forEach(plugin => loadPlugin(plugin, processedPlugins, serviceManager, applicationModel, pluginRegister));
    serviceManager.bootstrap();
    applicationModel.bootstrap(serviceManager);
    activePlugins.forEach( plugin => initializePlugin(plugin, serviceManager, applicationModel, pluginRegister) );
    return [applicationModel, serviceManager];
}

function initializePlugin(plugin: IPluginParams, serviceManager: IExtendedServiceManager,
                          applicationModel: PluginContainerAppModel, pluginRegister: IExtendedPluginRegister ): void {
    if (plugin.plugins) {
        plugin.plugins.map(subp => pluginRegister.findPluginByClassName(subp.name))
                      .forEach( subplugin =>
                            initializePlugin(subplugin, serviceManager, applicationModel, pluginRegister) );
    }
    if (plugin.onInit) {
        core_log('Calling onInit for \'' + plugin.name + '\'...', LogLevel.DEBUG, applicationModel.shell);
        plugin.onInit(serviceManager, applicationModel.shell);
    }
    if (plugin.watcher$) {
        core_log('Subscribed to watcher for \'' + plugin.name + '\'...', LogLevel.DEBUG, applicationModel.shell);
        plugin.watcher$(serviceManager).subscribe( resp => {
            if (resp.menuContribution) {
                applicationModel.registerMenuContribution(resp.menuContribution,
                                                          applicationModel.shell,
                                                          plugin['__index__']);
                applicationModel.rebuildMenu(serviceManager);
            }
            if (resp.topBarContribution) {
                applicationModel.registerTopbarContribution(resp.topBarContribution,
                                                            applicationModel.shell,
                                                            plugin['__index__']);
                applicationModel.rebuildTopBar(serviceManager);
            }
            applicationModel.watcher$.next(
                {
                    shell: applicationModel.shell,
                    menu: applicationModel.menu,
                    topBar: applicationModel.topBar,
                    help: applicationModel.help
                }
            );
        });
    }
}

function loadPlugin(
    plugin: IPluginParams,
    processedPlugins: IPluginParams[],
    serviceManager: IExtendedServiceManager,
    applicationModel: PluginContainerAppModel,
    pluginRegister: IPluginRegister
) {
    if (processedPlugins.map(p => p.id).indexOf(plugin.id) !== -1) {
        core_log('Skipping plugin \'' + plugin.name + ' because it\'s already loaded', LogLevel.WARN, applicationModel.shell);
        return;
    }

    core_log('Loading plugin \'' + plugin.name + '\'...', LogLevel.DEBUG, applicationModel.shell);

    plugin['__index__'] = processedPlugins.push(plugin) - 1;
    /**
     * Load menu and topbar first before loading other plugins,
     * so the saved index corresponds to the index for menucontributions
     * and topbarcontributions (needed for the watcher)
     */
    applicationModel.registerMenuContribution(plugin.menuContribution, applicationModel.shell);
    applicationModel.registerTopbarContribution(plugin.topBarContribution, applicationModel.shell);

    const subPlugins = plugin.plugins || [];

    subPlugins
        .map(p => pluginRegister.findPluginByClassName(p.name))
        .forEach(p => loadPlugin(p, processedPlugins, serviceManager, applicationModel, pluginRegister));

    serviceManager.registerServices(plugin.id, plugin.services);

    core_log('...finished loading plugin \'' + plugin.name + '\' at index ' + plugin['__index__'] , LogLevel.DEBUG, applicationModel.shell);
}
