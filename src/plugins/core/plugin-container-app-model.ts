import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { core_log, IMenuContribution, ITopBarContribution } from './plugin-decorators';
import {
    IAction,
    IApplicationModel,
    IExtendedApplicationModel,
    IHelp,
    IMenu,
    IMenuEntry,
    IMenuSection,
    IShellEnvironment,
    IShellInfo,
    ITopBar,
    LogLevel
    } from '../../shell';
import { IExtendedServiceManager, IServiceManager } from './service-manager';
import { IPluginContainer } from './plugin-container';

/**
 * Implementation of the IApplicationModel interface which is fed by plugins
 */
export class PluginContainerAppModel implements IExtendedApplicationModel {
    public shell: IShellInfo;
    public menu: IMenu;
    public topBar: ITopBar = new TopBar();
    public help: IHelp;

    private menuContributions: (IMenuContribution | ((sm: IServiceManager) => IMenuContribution))[] = [];
    private topbarContributions: (ITopBarContribution | ((sm: IServiceManager) => ITopBarContribution))[] = [];

    public watcher$: BehaviorSubject<IApplicationModel> =
                            new BehaviorSubject<IApplicationModel>( (this as IApplicationModel));

    constructor() {
        this.reset();
    }

    public addMenu(menu: IMenu): void {
        if (!menu || !menu.sections) {
            return;
        }

        menu.sections.forEach(s => this.addSection(s, this.menu));
    }

    public addTopBar(topBar: ITopBar) {
        if (!topBar) {
            return;
        }
        if (topBar.actions) { this.topBar.actions.push(...topBar.actions.map( action => {
                    if (!action.id) {
                        action.id = this.guid();
                    }
                    return action;
                }));
        };
        if (topBar.sections) { topBar.sections.forEach(s => this.addTopBarSection(s, this.topBar)); }
    }

    private addTopBarSection(section: IMenuSection, m: ITopBar) {
        let existing = m.sections.filter(s => s.label === section.label)[0];

        if (!existing) {
            existing = {
                label: section.label,
                entries: []
            };
            m.sections.push(existing);
        }

        section.entries.forEach(e => this.addMenuEntry(e, existing));
    }
    private guid() {
        const s4 = () => {
            return Math.floor((1 + Math.random()) * 0x10000)
            .toString(16)
            .substring(1);
        }
        return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
            s4() + '-' + s4() + s4() + s4();
    }

    private addSection(section: IMenuSection, m: IMenu) {
        let existing = m.sections.filter(s => s.label === section.label)[0];

        if (!existing) {
            existing = {
                label: section.label,
                entries: []
            };
            m.sections.push(existing);
        }

        section.entries.forEach(e => this.addMenuEntry(e, existing));
    }

    private addMenuEntry(entry: IMenuEntry, section: IMenuSection) {
        if ( !entry.id ) {
            entry.id = this.guid();
        }
        section.entries.push(entry);
    }

    public reset() {
        this.menu = { sections: [] };
        this.topBar.actions = [];
        this.topBar.sections = [];
    }

    rebuildMenu(serviceManager: IExtendedServiceManager) {
        const newMenu: IMenu = { sections: [] };
        this.menuContributions.filter( menucontribution => menucontribution !== undefined).forEach( menucontribution => {
            if (menucontribution instanceof Function) {
                menucontribution = menucontribution(serviceManager);
            }
            menucontribution.sections.forEach(s => this.addSection(s, newMenu));
        });
        this.menu =  newMenu;
    }

    rebuildTopBar(serviceManager: IExtendedServiceManager) {
        const newTopBar: ITopBar = { sections: [], actions: [] };
        this.topbarContributions.filter( topbarcontribution => topbarcontribution !== undefined)
                                .forEach( topbarcontribution => {
            if (topbarcontribution instanceof Function) {
                topbarcontribution = topbarcontribution(serviceManager);
            }
            if (topbarcontribution.sections) {
                topbarcontribution.sections.forEach(s => this.addTopBarSection(s, newTopBar));
            }
            if (topbarcontribution.actions) {
                newTopBar.actions.push( ...(<ITopBarContribution>topbarcontribution).actions );
            }
        });
        this.topBar = newTopBar;
    }

    bootstrap(serviceManager: IExtendedServiceManager) {
        this.menuContributions.forEach(mc => {
            if (mc instanceof Function) {
                this.addMenu(mc(serviceManager));
            } else {
                this.addMenu(mc);
            }
        });

        this.topbarContributions.forEach(tbc => {
            if (tbc instanceof Function) {
                this.addTopBar(tbc(serviceManager));
            } else {
                this.addTopBar(tbc);
            }
        });
    }

    public registerMenuContribution(
                menuContribution: IMenuContribution | ((sm: IServiceManager) => IMenuContribution),
                shell: IShellInfo, index?: number): number {
        if (index) {
            this.menuContributions[index] = menuContribution;
            core_log('Modified menuContribution [' + index + ']', LogLevel.DEBUG, shell);
            return index;
        }
        index = this.menuContributions.push(menuContribution) - 1;
        core_log('Added menuContribution [' + index + ']', LogLevel.DEBUG, shell);
        return index;
    }

    public registerTopbarContribution(
                topbarContribution: ITopBarContribution | ((sm: IServiceManager) => ITopBarContribution),
                shell: IShellInfo, index?: number): number {
        if (index) {
            this.topbarContributions[index] = topbarContribution;
            core_log('Modified topbarContribution [' + index + ']', LogLevel.DEBUG, shell);
            return index;
        }
        index = this.topbarContributions.push(topbarContribution) - 1;
        core_log('Added topbarContribution [' + index + ']', LogLevel.DEBUG, shell);
        return index;
    }
}

class TopBar implements ITopBar {
    public sections: IMenuSection[] = [];
    public actions: IAction[] = [];
}
