import { _InternalApplication, IApplicationParams } from './core/plugin-decorators';
import { SystemServicesPlugin } from './system-plugins/system-services-plugin';

export * from './core';

/**
 * Annotation which defines which is the application being built. There can only be one class class with this annotation in one
 * execution unit
 */
export function Application(params: IApplicationParams) {
    return function (constructor: Function) {
        params.plugins = [SystemServicesPlugin, ...params.plugins];
        _InternalApplication(params)(constructor);
    };
}
