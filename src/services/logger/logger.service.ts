import { ILoggerService } from './logger.api';
import { Injectable } from '@angular/core';
import { LogLevel } from '../../shell';

@Injectable()
export class LoggerService implements ILoggerService {

    private _level: LogLevel;

    setLevel( value: LogLevel ): void {
        this._level = value;
    }

    error(message: any, ...optionalParams: any[]) {
        if (typeof message === 'string') {
            message = 'ERROR : ' + message;
        } else {
            optionalParams.push(message);
            message = 'ERROR : ';
        }
        if (!this.showError()) { return; }
        this.doLog(LogLevel.ERROR, message, ...optionalParams);
    }

    warn(message: any, ...optionalParams: any[]) {
        if (!this.showWarning()) { return; }
        if (typeof message === 'string') {
            message = 'WARNING : ' + message;
        } else {
            optionalParams.push(message);
            message = 'WARNING : ';
        }
        this.doLog(LogLevel.WARN, message, ...optionalParams);
    }

    info(message: any, ...optionalParams: any[]) {
        if (!this.showInfo()) { return; }
        if (typeof message === 'string') {
            message = 'INFO : ' + message;
        } else {
            optionalParams.push(message);
            message = 'INFO : ';
        }
        this.doLog(LogLevel.INFO, message, ...optionalParams);
    }

    debug(message: any, ...optionalParams: any[]) {
        if (!this.showDebug()) { return; }
        if (typeof message === 'string') {
            message = 'DEBUG : ' + message;
        } else {
            optionalParams.push(message);
            message = 'DEBUG : ';
        }
        this.doLog(LogLevel.DEBUG, message, ...optionalParams);
    }

    private doLog(logLevel: LogLevel, message: string, ...args: any[]) {
        switch (logLevel) {
            case LogLevel.DEBUG:
                // tslint:disable-next-line:no-console
                console.debug(message, ...args);
                break;
            case LogLevel.INFO:
                // tslint:disable-next-line:no-console
                console.info(message, ...args);
                break;
            case LogLevel.WARN:
                console.warn(message, ...args);
                break;
            case LogLevel.ERROR:
                console.error(message, ...args);
                break;
        }
    }

    private showError(): boolean { return this._level >= LogLevel.ERROR; }
    private showWarning(): boolean { return this._level >= LogLevel.WARN; }
    private showInfo(): boolean { return this._level >= LogLevel.INFO; }
    private showDebug(): boolean { return this._level >= LogLevel.DEBUG; }

}
