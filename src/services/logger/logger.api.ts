import {Optional, OpaqueToken} from '@angular/core';

/**
 * Endpoint where the service will be registered
 */
export const SERVICE_LOGGER = new OpaqueToken('ILoggerService');

/**
 * Provides support to Log.
 */
export interface ILoggerService {
    error(message?: any, ...optionalParams: any[]);

    warn(message?: any, ...optionalParams: any[]);

    info(message?: any, ...optionalParams: any[]);

    debug(message?: any, ...optionalParams: any[]);
}
