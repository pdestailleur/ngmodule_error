import { PreferenceScope, IUserPreferencesService } from './user-preferences.api';
import { Observable } from 'rxjs/Rx';

/**
 * Mock implementation of IUserPreferences which stores the preference for any
 * scope in an internal object (=> all scopes are the same)
 */
export class CookieUserPreferencesService implements IUserPreferencesService {
    constructor() {
    }

    getPreference(key: string): Observable<any> {
        return Observable.of(readCookie(key));
    }

    setPreference(key: string, value: any, scope: PreferenceScope, onError?: (f: string) => void): void {
        eraseCookie(key);
        createCookie(key, value, 99999);
    }
}

function createCookie(name: any, value: any, days: any) {
    let expires = '';
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toISOString();
    }
    document.cookie = name + '=' + value + expires + '; path=/';
}

function readCookie(name: any) {
    const nameEQ = name + '=';
    const ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1, c.length);
        }
        if (c.indexOf(nameEQ) === 0) {
            return c.substring(nameEQ.length, c.length);
        }
    }
    return null;
}

function eraseCookie(name: any) {
    createCookie(name, '', -1);
}
