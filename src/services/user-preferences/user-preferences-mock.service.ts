import { PreferenceScope, IUserPreferencesService } from './user-preferences.api';
import { Observable } from 'rxjs/Rx';

/**
 * Mock implementation of IUserPreferences which stores the preference for any
 * scope in an internal object (=> all scopes are the same)
 */
export class UserPreferencesMockService implements IUserPreferencesService {
    private preferences = {};

    getPreference(key: string): Observable<any> {
        const ret = this.preferences[key];

        return Observable.of(ret);
    }

    setPreference(key: string, value: any, scope: PreferenceScope, onError?: (f: string) => void): void {
        this.preferences[key] = value;
    }
}

