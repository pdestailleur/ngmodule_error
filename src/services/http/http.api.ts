import { OpaqueToken } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { RequestOptionsArgs, Response } from '@angular/http';

/**
 * Endpoint where the service will be registered
 */
export const SERVICE_HTTP = new OpaqueToken('IHttpService');

export interface ServiceResponse<T> {
    isError: boolean;
    message?: string;
    data?: T;
    raw_data?: T;
}

/**
 * Provides support to HTTP calls.
 */
export interface IHttpService {
    /**
     * Returns the base URL for remote calls.
     */
    getBaseUrl(): string;

    /**
     * Helper function to transform a date into a string manageable in an HTTP url.
     * It receives a date formatted as dd/MM/yyyy and returns a string in format dd-MM-yyyy.
     */
    dateToUrl(date: Date): string;

    /**
     * Converts a 'DD/MM/YYYY'-formatted string into a Date object.
     */
    parseFormattedDate(str: string): Date;

    /**
     * Creates a HTTP url parameter-styled string from a Javascript object.
     */
    objectToUrl(obj: any): string;

    /**
     * Performs an HTTP GET call. By default, the url will be added to the base url of the remote service. If
     * absolute is true, then the url will be considered as absolute and used unmodified to peform the call
     */
    get(url: string, absolute?: boolean): Observable<any>;

    /**
     * Performs an HTTP POST call. By default, the url will be added to the base url of the remote service. If
     * absolute is true, then the url will be considered as absolute and used unmodified to peform the call
     */
    post(url: string, body: string, absolute?: boolean): Observable<any>;

    /**
     * Performs an HTTP PUT call. By default, the url will be added to the base url of the remote service. If
     * absolute is true, then the url will be considered as absolute and used unmodified to peform the call
     */
    put(url: string, body: string, absolute?: boolean): Observable<any>;

    /**
     * Performs an HTTP DELETE call. By default, the url will be added to the base url of the remote service. If
     * absolute is true, then the url will be considered as absolute and used unmodified to peform the call
     */
    delete(url: string, absolute?: boolean): Observable<any>;

    /**
     * Performs an HTTP PATCH call. By default, the url will be added to the base url of the remote service. If
     * absolute is true, then the url will be considered as absolute and used unmodified to peform the call
     */
    patch(url: string, body: string, absolute?: boolean): Observable<any>;

    /**
     * Performs an HTTP HEAD call. By default, the url will be added to the base url of the remote service. If
     * absolute is true, then the url will be considered as absolute and used unmodified to peform the call
     */
    head(url: string, absolute?: boolean): Observable<any>;
}


export interface IHttpCall {
    method: string;
    url: string;
    body: string;
    options: RequestOptionsArgs;
    response: Observable<Response>;
    isCancelled: boolean;
}
