import { environment } from '../../environments/environment';
import { EventEmitter, Inject, Injectable } from '@angular/core';
import { Headers, Http, RequestOptionsArgs } from '@angular/http';
import { IHttpService } from './http.api';
import { Observable } from 'rxjs/Observable';

const GET = 'GET';
const POST = 'POST';
const PUT = 'PUT';
const DELETE = 'DELETE';

@Injectable()
export class HttpService implements IHttpService {
    /**
     * The request initialized event emitter
     */
    requestInit = new EventEmitter();

    /**
     * The request resolved event emitter
     */
    requestResolved = new EventEmitter();

    /**
     * The request failed event emitter
     */
    requestFailed = new EventEmitter();

    constructor( @Inject(Http) private http: Http ) { }

    getBaseUrl(): string {
        if (environment.remoteUrl !== undefined) {
            return environment.remoteUrl
        }
        return '/';
    }

    dateToUrl(date: Date): string {
        if (!date) { return null; }

        const day = date.getDate();
        const month = date.getMonth() + 1;
        const year = date.getFullYear();
        let str = ((day < 10) ? ('0' + day) : day) + '';
        str += ((month < 10) ? ('0' + month) : month) + '';
        str += year + '';
        return str;
    }

    parseFormattedDate(str: string): Date {
        if (!str) { return null; }

        const res = str.split('/');
        const day = Number(res[0]);
        const month = Number(res[1]) - 1;
        const year = Number(res[2]);
        return new Date(year, month, day);
    }

    objectToUrl(obj: any): string {
        let str = '';
        for ( const key in obj ) {
            if (obj.hasOwnProperty(key)) {
                if (str !== '') {
                    str += '&';
                }
                str += key + '=' + encodeURIComponent(obj[key]);
            }
        }
        return str;
    }
    get(uri: string, absolute?: boolean, options?: RequestOptionsArgs): Observable<any> {
        this.emitRequestInitEvent(GET, uri, options);

        options = options || {
            withCredentials: true,
            headers: new Headers({ 'X-Csrf-Request': 'X-Csrf-Request' }),
        };

        uri = this.composeUri( uri, absolute);

        return this.http.get(uri, options)
            .map(res => res.json())
            .catch((error) => {
                this.emitRequestFailedEvent(GET, uri, options);

                return Observable.of({
                    errorCode: 500,
                    error: error,
                });
            });
    }

    post(uri: string, body: any, absolute?: boolean, options?: RequestOptionsArgs): Observable<any> {
        options = options || {
            withCredentials: true,
            headers: new Headers({ 'X-Csrf-Request': 'X-Csrf-Request' }),
        };

        uri = this.composeUri( uri, absolute);

        return this.http.post(uri, body, options)
            .map(res => res.json())
            .catch((error) => {
                this.emitRequestFailedEvent(POST, uri, options);

                return Observable.of({
                    errorCode: 500,
                    error: error,
                });
            });
    }

    put(uri: string, body: any, absolute?: boolean, options?: RequestOptionsArgs): Observable<any> {
        options = options || {
            withCredentials: true,
            headers: new Headers({ 'X-Csrf-Request': 'X-Csrf-Request' }),
        };

        uri = this.composeUri( uri, absolute);

        return this.http.put(uri, body, options)
            .map(res => res.json())
            .catch((error) => {
                this.emitRequestFailedEvent(POST, uri, options);

                return Observable.of({
                    errorCode: 500,
                    error: error,
                });
            });
    }

    delete(uri: string, absolute?: boolean, options?: RequestOptionsArgs): Observable<any> {
        options = options || {
            withCredentials: true,
            headers: new Headers({ 'X-Csrf-Request': 'X-Csrf-Request' }),
        };

        uri = this.composeUri( uri, absolute);

        return this.http.delete(uri, options)
            .map(res => res.json())
            .catch((error) => {
                this.emitRequestFailedEvent(POST, uri, options);

                return Observable.of({
                    errorCode: 500,
                    error: error,
                });
            });
    }

    patch(uri: string, body: any, absolute?: boolean, options?: RequestOptionsArgs): Observable<any> {
        options = options || {
            withCredentials: true,
            headers: new Headers({ 'X-Csrf-Request': 'X-Csrf-Request' }),
        };

        uri = this.composeUri( uri, absolute);

        return this.http.patch(uri, body, options)
            .map(res => res.json())
            .catch((error) => {
                this.emitRequestFailedEvent(POST, uri, options);

                return Observable.of({
                    errorCode: 500,
                    error: error,
                });
            });
    }

    head(uri: string, absolute?: boolean, options?: RequestOptionsArgs): Observable<any> {
        options = options || {
            withCredentials: true,
            headers: new Headers({ 'X-Csrf-Request': 'X-Csrf-Request' }),
        };

        uri = this.composeUri( uri, absolute);

        return this.http.head(uri, options)
            .map(res => res.json())
            .catch((error) => {
                this.emitRequestFailedEvent(POST, uri, options);

                return Observable.of({
                    errorCode: 500,
                    error: error,
                });
            });
    }

    private emitRequestInitEvent(verb: string, uri: string, options: any) {
        this.requestInit.emit({
            verb,
            options,
            uri,
        });
    }

    private emitRequestResolvedEvent(verb: string, uri: string, options: any) {
        this.requestResolved.emit({
            verb,
            options,
            uri,
        });
    }

    private emitRequestFailedEvent(verb: string, uri: string, options: any) {
        this.requestFailed.emit({
            verb,
            options,
            uri,
        });
    }

    private composeUri( uri: string, absolute: boolean): string {
        if (absolute) {
            return uri;
        }
        return environment.remoteUrl + uri;
    }
}
