import { NavigationExtras, Router, Routes } from '@angular/router';

import { OpaqueToken } from '@angular/core';

export const SERVICE_ROUTER = new OpaqueToken('IRouter');

/**
 * Service which allows to get a service that is aware of the plugin it is called from
 */
export interface IRouter {
    getPluginPath( url: string ): string;
    navigateByUrl( url: string ): Promise<boolean>;
    navigate( commands: any[], extras?: NavigationExtras): Promise<boolean>;
}
