import { Inject, Injectable, OpaqueToken } from '@angular/core';
import { NavigationExtras, Route, Routes } from '@angular/router';

import { IPlugin } from './../pluginaware-serviceregister/pluginaware-serviceregister.api';
import { IRouter } from './plugin-router.api';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import { rewriteRoutePath } from '../../plugins/core/plugin-decorators';

/**
 * The system service that holds handles the pluginaware services
 */
@Injectable()
export class DefaultRouterService implements IRouter {

    constructor( @Inject(Router) private router: Router) {
    }

    getPluginPath( path: string): string {
        throw new Error('This method cannot be called!');
    }

    navigateByUrl( url: string ): Promise<boolean> {
        return this.router.navigateByUrl(url);
    }

    navigate( commands: any[], extras?: NavigationExtras): Promise<boolean> {
        return this.router.navigate( commands, extras);
    }


}

/***
 * the default factory for the pluginaware version of the translationservice
 */
export function DefaultRouterFactory(service: IRouter) {
    return (pluginID: string) => {
        return {
            service: service,
            getPluginPath( path: string): string {
               return rewriteRoutePath(path, undefined, pluginID );
            },
            navigateByUrl( url: string ): void {
                service.navigateByUrl(rewriteRoutePath(url, undefined, pluginID));
            }
        };
    };
}
