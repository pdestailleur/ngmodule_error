import { Observable } from 'rxjs/Observable';
import { Inject } from '@angular/core';
import { IHttpService, SERVICE_HTTP } from './../http';
import { IGuardian, IUser } from './guardian.api';

/**
 * Structure of the internal cache.
 */
interface ICache {
    user: IUser;
    roles: string[];
    isLogged: boolean;
    updated: Date;
    valid: boolean;
}

/**
 * Real implementation of the IGuardian service which talks to the REST service
 */
export class GuardianServiceProxy implements IGuardian {
    private static SRV_PREFIX = '/guardian';
    private CACHE: ICache = null;

    constructor( @Inject(SERVICE_HTTP) private http: IHttpService) { }

    userHasRole(role: string): Observable<boolean> {
        const cache: ICache = this.getCacheEntry();
        if (!cache.valid) {
            return Observable.create((observer: any) => {
                this.http.get(GuardianServiceProxy.SRV_PREFIX + '/user/roles').subscribe((rs: string[]) => {
                    const pos = rs.indexOf(role);
                    // update cache
                    cache.roles = rs;
                    this.setCacheEntry(cache);
                    // return value
                    observer.next(pos >= 0);
                    observer.complete();
                });
            });
        } else {
            const pos = cache.roles.indexOf(role);
            return Observable.of(pos >= 0);
        }
    }

    userIsLogged(): Observable<boolean> {
        const cache: ICache = this.getCacheEntry();
        if (!cache.valid) {
            const result = this.http.get(GuardianServiceProxy.SRV_PREFIX + '/user/islogged');
            // update cache
            result.subscribe(x => {
                cache.isLogged = x;
                this.setCacheEntry(cache);
            });
            // return value
            return result;
        } else {
            return Observable.of(cache.isLogged);
        }
    }

    getLoggedUser(): Observable<IUser> {
        const cache: ICache = this.getCacheEntry();
        if (!cache.valid) {
            const result = this.http.get(GuardianServiceProxy.SRV_PREFIX + '/user');
            // update cache
            result.subscribe(u => {
                cache.user = u;
                this.setCacheEntry(cache);
            });
            this.setCacheEntry(cache);
            // return value
            return result;
        } else {
            return Observable.of(cache.user);
        }
    }

    private getCacheEntry(): ICache {
        const max_cache_timeout = 5 * 60 * 1000;  // 5 minutes in milliseconds

        if (this.CACHE === null) {
            this.CACHE = {
                user: null,
                roles: [],
                isLogged: false,
                updated: new Date(),
                valid: false
            };
        } else {
            const time = new Date().getTime() - this.CACHE.updated.getTime();
            this.CACHE.valid = (time <= max_cache_timeout);
        }
        return this.CACHE;
    }

    private setCacheEntry(cache: ICache) {
        cache.updated = new Date();
        cache.valid = true;
        this.CACHE = cache;
    }

}
