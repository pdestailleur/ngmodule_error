import { Observable } from 'rxjs/Observable';
import { IGuardian, IUser } from './guardian.api';

/** Mock implementation for the IGuardian interface */
export class GuardianMock implements IGuardian {
    private user: IUser = {
        id: '0',
        fullName: 'Guardian Mock Service User',
        name: 'Guardian Mock',
        surname: 'User',
        roles: []
    };

    userHasRole(role: string): Observable<boolean> {
        return Observable.of(this.user.roles.indexOf(role) !== -1);
    }

    userIsLogged(): Observable<boolean> {
        return Observable.of(true);
    }

    getLoggedUser(): Observable<IUser> {
        return Observable.of(Object.freeze(this.user));
    }

    addRole(role: string) {
        this.user.roles.push(role);
    }

    removeRole(role: string) {
        const index: number = this.user.roles.indexOf(role);
        if (index !== -1) {
            this.user.roles.splice(index, 1);
        }
    }
}
