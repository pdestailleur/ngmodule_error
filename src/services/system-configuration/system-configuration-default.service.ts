import { Observable } from 'rxjs/Rx';
import { ISystemConfiguration } from './system-configuration.api';

/**
 * Default implementation of ISystemConfiguration which stores
 * the variables for any plugin in an internal object
 */
export class DefaultSystemConfigurationService implements ISystemConfiguration {
    private variables = {};

    getEnvVar(key: string): Observable<any> {
        const ret = this.variables[key];
        return Observable.of(ret);
    }

    setEnvVar(key: string, value: any) {
        this.variables[key] = value;
    }

}
