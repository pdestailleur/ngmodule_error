import { OpaqueToken } from '@angular/core';
import { Observable } from 'rxjs/Rx';

export const SERVICE_SYSTEM_CONFIGURATION = new OpaqueToken('ISystemConfiguration');

/**
 * Service which allows to retrieve and set environment variables.
 */
export interface ISystemConfiguration {
    /**
     * Retrieves the environment variable with the given key. The promise resolves
     * to undefined if the variable has not been set by any of the plugins.
     */
    getEnvVar(key: string): Observable<any>;

    /**
     * Sets a environment variable with a given value.
     */
    setEnvVar(key: string, value: any);

}
