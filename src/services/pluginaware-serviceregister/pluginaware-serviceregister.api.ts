import { OpaqueToken } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { IPluginParams, IServiceManager } from './../../plugins';

export const SERVICE_PLUGINAWARE_SERVICEREGISTER = new OpaqueToken('IPluginawareServiceregister');

/**
 * Service which allows to get a service that is aware of the plugin it is called from
 */
export interface IPluginawareServiceregister {

    getService(service: OpaqueToken, plugin: IPlugin): any;
    registerService(service: OpaqueToken, factory: IPluginServiceFactory): void;
}

/**
 * Represents a function which can build a service
 */
export type IPluginServiceFactory = (pluginID: string) => Object;

/**
 * Interface for a plugin
 */
export interface IPlugin { }

