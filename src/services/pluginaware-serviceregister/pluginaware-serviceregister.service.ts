import { OpaqueToken, Inject } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { IPluginParams, IServiceManager } from './../../plugins';
import { IPluginawareServiceregister, IPluginServiceFactory, IPlugin } from './pluginaware-serviceregister.api';

/**
 * The system service that holds handles the pluginaware services
 */
export class PluginawareServiceregisterService implements IPluginawareServiceregister {
     /**
     * key/value pair of pluginID and IPluginServiceFactory.
     */
    private registeredServices: { [plugin: string]: Function }  = {};

    constructor( private sm: IServiceManager ) {
    }

    /**
     * returns a pluginaware object, returned as a result of the factory provided by the pluginaware service
     */
    getService(service: OpaqueToken, plugin: IPlugin): any {
        if (!(service.toString() in this.registeredServices)) {
            throw(new Error('Service ' + service.toString() + ' is not registered as PluginAware!'));
        }
        this.sm.log('Returned PAService ' + service.toString() + ' for ' + plugin['__pluginParams__'].id);
        return this.registeredServices[service.toString()](plugin['__pluginParams__'].id);
    };

    /**
     * registers a service as a pluginaware service
     * the factory provieded implements all exposed methods needed by a pluginaware version of the service
     */
    registerService(service: OpaqueToken, serviceFactory: IPluginServiceFactory) {
        if (service.toString() in this.registeredServices) {
            this.sm.log('Service ' + service.toString() + ' is already registered as Pluginaware!');
            return;
        }
        this.registeredServices[service.toString()] = serviceFactory;
        this.sm.log('Service ' + service.toString() + ' is registered as Pluginaware');
    };
}
