import { IFavouritesService, IFavourite } from './favourites.api';

export class FavouritesMockService implements IFavouritesService {
    public addFavourite(fav: IFavourite): void {
        // Noop
    }

    retrieveFavourites(type?: string): void {
        // Noop
    }
}
