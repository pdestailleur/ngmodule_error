import { EventEmitter, OpaqueToken } from '@angular/core';

export const SERVICE_TRANSLATIONS = new OpaqueToken('ITranslations');

/**
 * Service which allows to handle the translations
 */
export interface ITranslations {

    onLangChange(): EventEmitter<LangChangeEvent>;
    onTranslationsAdded(): EventEmitter<boolean>;
    getTranslation(key: string, language?: string, ...args: any[]): string;
    getAllTranslations(language?: string): Object;
    addTranslations(translations: ILanguageTranslation[]): void;
    getCurrentLang(): string;
    getAvailableLanguages(): string[];
}

/**
 * interface for a language containing all the translations
 */
export interface ILanguageTranslation {
    language: string;
    translations: Object;
}

/**
 * the langchange event emitted by the ShellService whenever the language of the interface changes
 */
export interface LangChangeEvent {
    lang: string;
}

