import { EventEmitter, Inject } from '@angular/core';
import { Http } from '@angular/http';
import { ILanguageTranslation, ITranslations, LangChangeEvent } from './translations.api';
import { IServiceManager } from '../../plugins';
import { IShellInfo, LogLevel } from './../../shell';


/**
 * Service which allows to handle the translations
 */
export class DefaultTranslationService implements ITranslations {
    private _translations: ILanguageTranslation[]= [];
    private _currentLang = '';
    private _onLangChange: EventEmitter<LangChangeEvent> = new EventEmitter<LangChangeEvent>();
    private _onTranslationsAdded: EventEmitter<boolean> = new EventEmitter<boolean>();

    constructor( private sm: IServiceManager ) {
    }

    /**
     * private function to return an ILanguageTranslation object for the given language
     */
    private getLangTranslation(language: string): ILanguageTranslation {
        const langTrans: ILanguageTranslation[] = this._translations.filter( currentLangTrans => currentLangTrans.language === language );
        if (langTrans.length > 0) {
            return langTrans[0];
        }
        return { language: language, translations: {} };
    }

    private parseTranslation( translation: string, ...params: any[]): string {
        params.forEach( param => {
            const paramKey = Object.keys(param)[0];
            translation = translation.replace( '{' + paramKey + '}', param[paramKey]);
        });
        return translation;
    }

    private isDefined(value: any): boolean {
        return typeof value !== 'undefined' && value !== null;
    }

    /**
     * function to change the language used by the translation service
     * triggers the onLangChange eventemitter
     */
    public use(lang: string) {
        this._currentLang = lang;
        this.onLangChange().emit({lang: lang});
    }

    /**
     * returns the current language of the translation service
     */
    public getCurrentLang(): string {
        return this._currentLang;
    }

    /**
     * returns an array containing all the available languages in the application.
     * this doesn't mean that all languages contain all translations
     */
    public getAvailableLanguages(): string[] {
        return this._translations.map( langTrans => langTrans.language );
    }

    /**
     * returns the translation of the given key
     * if not found returns the given key
     */
    public getTranslation(key: string, language?: string, ...args: any[]): string {
        let trans: string;
        if (language) {
            trans =  this.getAllTranslations(language)[key];
        } else {
            trans = this.getAllTranslations(this._currentLang)[key];
        }
        if (trans === undefined) { return key; }
        if ( args.length ) {
            let interpolateParams: Object;
            if (this.isDefined(args[0]) && args.length) {
                if (typeof args[0] === 'string' && args[0].length) {
                    // we accept objects written in the template such as {n:1}, {'n':1}, {n:'v'}
                    // which is why we might need to change it to real JSON objects such as {"n":1} or {"n":"v"}
                    const validArgs: string = args[0]
                        .replace(/(\')?([a-zA-Z0-9_]+)(\')?(\s)?:/g, '"$2":')
                        .replace(/:(\s)?(\')(.*?)(\')/g, ':"$3"');
                    try {
                        interpolateParams = JSON.parse(validArgs);
                    } catch (e) {
                        throw new SyntaxError(`Wrong parameter in TranslatePipe. Expected a valid Object, received: ${args[0]}`);
                    }
                } else if (typeof args[0] === 'object' && !Array.isArray(args[0])) {
                    interpolateParams = args[0];
                }
            }
            return this.parseTranslation(trans, ...args);
        }
        return trans;
    }

    /**
     * returns all the translations for the given language
     */
    public getAllTranslations(language?: string): Object {
        if (language) {
            return this.getLangTranslation(language).translations;
        }
        return this.getLangTranslation(this._currentLang ).translations;
    }

    /**
     * add translations to the translationservice
     */
    public addTranslations(translations: ILanguageTranslation[]): void {
        translations.forEach( langTrans => {
            const existing: ILanguageTranslation = this.getLangTranslation(langTrans.language);
            existing.translations = Object.assign(existing.translations, langTrans.translations);
            this._translations = this._translations.filter( currentLangTrans => currentLangTrans.language !== existing.language );
            this._translations.push(existing);
        });
        this._onTranslationsAdded.emit(true);
        this.sm.log('Added ' + translations.length +
                    ' language translations for ' +  Object.keys(translations[0].translations)[0].split('_')[0]);
    }

    /**
     * notifies when the language of the interface changes
     * USE THE ONLANGCHANGE PROPERTY OF THE SHELLSERVICE IN YOUR PLUGINS TO GET NOTIFIED OF LANGUAGECHANGES
     */
    public onLangChange(): EventEmitter<LangChangeEvent> {
        return this._onLangChange;
    }

    public onTranslationsAdded(): EventEmitter<boolean> {
        return this._onTranslationsAdded;
    }
}

/***
 * the default factory for the pluginaware version of the translationservice
 */
export function DefaultTranslationsFactory(service: ITranslations) {
    return (pluginID: string) => {
        return {
            service: service,
            getNewKey(key: string) {
                return pluginID + '_' + key;
            },
            getCurrentLang(): string {
                return service.getCurrentLang();
            },
            getTranslation(key: string, language?: string, ...args: any[]): String {
                const trans: string = service.getTranslation(this.getNewKey(key), language, ...args);
                if (this.getNewKey(key) === trans) { return key; }
                return trans;
            },
            getAllTranslations(language: string): Object {
                const translations: Object = service.getAllTranslations(language);
                const pluginTranslations: Object = {};
                if (translations) {
                    for (const key in translations) {
                        if (key.split('_')[0] === pluginID) {
                            pluginTranslations[key.split('_').slice(1).join('_')] = translations[key];
                        }
                    }
                }
                return pluginTranslations;
            },
            addTranslations: function(translations: ILanguageTranslation[]): void {
                const newtranslations: ILanguageTranslation[] = [];
                translations.forEach( transLang => {
                    newtranslations.push({ language: transLang.language, translations: {} } );
                    for (const key in transLang.translations) {
                        if ( transLang.translations.hasOwnProperty(key) ) {
                            newtranslations[newtranslations.length - 1].translations[this.getNewKey(key)] = transLang.translations[key];
                        }
                    }
                });
                service.addTranslations(newtranslations);
            }
        };
    };
}
