import { EventEmitter } from '@angular/core';
import { INotification, INotificationService, NotificationType } from './notifications-api';
import { Observable } from 'rxjs/Rx';

export class NotificationService implements INotificationService {

    private onDebugEmitter = new EventEmitter<INotification>();
    private onInfoEmitter = new EventEmitter<INotification>();
    private onWarnEmitter = new EventEmitter<INotification>();
    private onErrorEmitter = new EventEmitter<INotification>();

    debug(msg: string, autoCloseDelay?: number, title?: string): void {
        const notif: INotification = this.createNotification(title, msg, NotificationType.Debug, autoCloseDelay);
        this.onDebugEmitter.emit(notif);
    }

    info(msg: string, autoCloseDelay?: number, title?: string): void {
        const notif: INotification = this.createNotification(title, msg, NotificationType.Info, autoCloseDelay);
        this.onInfoEmitter.emit(notif);
    }

    warn(msg: string, autoCloseDelay?: number, title?: string): void {
        const notif: INotification = this.createNotification(title, msg, NotificationType.Warn, autoCloseDelay);
        this.onWarnEmitter.emit(notif);
    }

    error(msg: string, autoCloseDelay?: number, title?: string): void {
        const notif: INotification = this.createNotification(title, msg, NotificationType.Error, autoCloseDelay);
        this.onErrorEmitter.emit(notif);
    }

    onDebug(): Observable<INotification> {
        return this.onDebugEmitter.asObservable();
    }

    onInfo(): Observable<INotification> {
        return this.onInfoEmitter.asObservable();
    }

    onWarn(): Observable<INotification> {
        return this.onWarnEmitter.asObservable();
    }

    onError(): Observable<INotification> {
        return this.onErrorEmitter.asObservable();
    }

    private createNotification(title: string, msg: string, type: NotificationType, delay: number) {
        return {
            id: '' + Date.now(),
            title: title,
            message: msg,
            type: type,
            autoCloseDelay: delay ? delay : 0
        };
    }
}
