import {OpaqueToken} from '@angular/core';
import {Observable} from 'rxjs/Rx';

export const SERVICE_NOTIFICATIONS = new OpaqueToken('INotificationService');

export enum NotificationType {
    Debug,
    Info,
    Warn,
    Error,
}

export interface INotification {
    id: string;
    title?: string;
    message: string;
    type: NotificationType;
    autoCloseDelay?: number;
}

export interface INotificationService {
    debug(msg: string, autoCloseDelay?: number, title?: string): void;
    info(msg: string, autoCloseDelay?: number, title?: string): void;
    warn(msg: string, autoCloseDelay?: number, title?: string): void;
    error(msg: string, autoCloseDelay?: number, title?: string): void;
    onDebug(): Observable<INotification>;
    onInfo(): Observable<INotification>;
    onWarn(): Observable<INotification>;
    onError(): Observable<INotification>;
}
