import { ShellService, OIBCoreModule } from './core';
import { ShellAppTemplateComponent } from './shell-app.component';
import { EuiShellModule } from './euiApp';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';


@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        FormsModule,
        RouterModule,
        OIBCoreModule,
        EuiShellModule
    ],
    providers: [ShellService],
    declarations: [ShellAppTemplateComponent],
    exports: [ShellAppTemplateComponent]
})
export class ShellAppModule { }
