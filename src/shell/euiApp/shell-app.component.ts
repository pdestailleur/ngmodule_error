import { BehaviorSubject, Observable } from 'rxjs/Rx';
import {
    Component,
    Inject,
    Input,
    OnChanges,
    OnInit
    } from '@angular/core';
import { DefaultTranslationService, SERVICE_TRANSLATIONS } from './../../services';
import { EuiAppInfo, euiShellLayout } from './shell-app.api';
import { IApplicationModel, IMenuSection, ShellService } from './../core';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { UxService } from './../../@eui/ux-commons';


@Component({
    selector: 'eui-shell',
    template: `
    <inner-sidebar-top-menu-layout [appInfo]="appInfo" *ngIf="isInnerSideBarLayout">
    </inner-sidebar-top-menu-layout>
    <overlay-sidebar-layout [appInfo]="appInfo" *ngIf="isOverlaySideBarLayout">
    </overlay-sidebar-layout>
    <fixed-sidebar-layout [appInfo]="appInfo" *ngIf="isFixedSideBarLayout">
    </fixed-sidebar-layout>
    <default-contract-layout [appInfo]="appInfo" *ngIf="isDefaultContractLayout">
    </default-contract-layout>
  `,
    styles: [`
        .app-infos { float:right !important; }
    `]
})
export class EuiShellTemplateComponent implements OnInit, OnChanges {
    @Input() appModel: IApplicationModel;

    public appInfo: EuiAppInfo;

    constructor(public shellService: ShellService, private uxService: UxService, private titleService: Title) {
        Observable.fromEvent (window, 'resize')
            .debounceTime(200)
            .subscribe((event: Event) => {
                const window: Window = <Window> event.target;
                this.uxService.setActiveBreakpoint(window.innerWidth);
                this.uxService.setWindowHeight(window.innerHeight);

            });
    }

    ngOnInit() {
        this.uxService.setActiveBreakpoint(window.innerWidth);
        this.uxService.setWindowHeight(window.innerHeight);
    }

    ngOnChanges(changes: any) {
        if (changes.appModel) {
            this.shellService.initApp(changes.appModel.currentValue);
            this.appInfo = <EuiAppInfo>this.shellService.appInfo;
            this.titleService.setTitle(this.appInfo.appFullName);
        }
    }

    get isInnerSideBarLayout(): boolean {
        return this.appInfo.layout === euiShellLayout.INNERSIDEBAR_COLLAPSE ||
               this.appInfo.layout === euiShellLayout.INNERSIDEBAR_COLLAPSE_CLOCK;
    }
    get isOverlaySideBarLayout(): boolean {
        return this.appInfo.layout === euiShellLayout.SIDEBAR_COLLAPSE ||
               this.appInfo.layout === euiShellLayout.SIDEBAR_COLLAPSE_CLOCK;
    }

    get isFixedSideBarLayout(): boolean {
        return this.appInfo.layout === euiShellLayout.SIDEBAR_FIXED ||
               this.appInfo.layout === euiShellLayout.SIDEBAR_FIXED_CLOCK;
    }
    get isDefaultContractLayout(): boolean {
        return this.appInfo.layout === euiShellLayout.DEFAULT_CONTRACT ||
               this.appInfo.layout === euiShellLayout.DEFAULT_CONTRACT_FULLWIDTH;
    }
}
