import { Component, Input } from '@angular/core';
import { EuiAppInfo } from './../shell-app.api';
import { euiShellLayout } from '../shell-app.api';
import { EuiShellTemplateComponent } from './../shell-app.component';
import { ShellService } from './../../core/shell.service';
import { UxService } from './../../../@eui/ux-commons';


@Component({
    selector: 'default-contract-layout',
    template: `
    <div id="app-wrapper" [class]="mainClass" uxTrackScroll>
        <div id="main">
            <ux-layout-header   [homeUrl]="appInfo.homeUrl"
                                [appFullName]="appInfo.appFullName"
                                [appShortName]="appInfo.appShortName"
                                [userInfos]="shellService.getUserInfo()"
                                [envLabel]="shellService.envLabel"
                                [isShowLanguageSelector]="appInfo.availableLanguages != 'none'"
                                [isHideLogo]="!shellService.isHeaderShowLogo"
                                [languageCodes]="appInfo.availableLanguages"
                                (languageChanged)="shellService.changeLanguage($event)">
            </ux-layout-header>
            <oib-clock *ngIf="shellService.isShowClock"></oib-clock>
            <ux-layout-nav-bar>
                <ux-layout-nav-bar-top-menu *ngIf="shellService.allMenu.length"
                                            [links]="shellService.allMenu"
                                            [homeUrl]="appInfo.homeUrl"
                                            (menuItemClicked)="shellService.onAllMenuClicked($event)">
                </ux-layout-nav-bar-top-menu>
                <ux-layout-nav-bar-actions>
                    <ux-layout-nav-bar-action-item *ngFor="let action of shellService.appModel.topBar.actions"
                        iconClass="{{action.icon}}"
                        tagCount="{{action.badgeCount}}"
                        [isToggleContent]="false"
                        (clicked)="shellService.onActionClicked($event)">
                    </ux-layout-nav-bar-action-item>
                    <ux-layout-nav-bar-action-item iconClass="fa-bars"
                                                itemClass="app-menu"
                                                [isOverlayPanel]="true"
                                                [isOverlayPanelCustomContent]="true">
                        <uxLayoutNavBarOverlayPanelContent>
                            <eui-menu-overlay-panel [appInfo]="appInfo"
                                                    [links]="shellService.allMenu"
                                                    (clicked)="shellService.onAllMenuClicked($event)"
                                                            (languageChanged)="shellService.changeLanguage($event)">
                            </eui-menu-overlay-panel>
                        </uxLayoutNavBarOverlayPanelContent>
                    </ux-layout-nav-bar-action-item>
                </ux-layout-nav-bar-actions>
            </ux-layout-nav-bar>
            <div id="main-content">
                <router-outlet></router-outlet>
            </div>
            <ux-layout-footer isCompact="true">
                <div class="app-infos">
                    {{shellService.getFooterAppInfo()}}
                </div>
                <div class="links">
                    {{shellService.getFooterLinks()}}
                </div>
            </ux-layout-footer>
        </div>
    </div>
`,
styles: [`
    .app-infos {
        float:right !important;
    }

`]
})
export class DefaultContractLayoutComponent {
    @Input() public appInfo: EuiAppInfo;

    public mainClass: String = 'contract';

    constructor(public shellService: ShellService, private uxService: UxService) {
        if (shellService.appInfo.layout === euiShellLayout.DEFAULT_CONTRACT_FULLWIDTH) {
            this.mainClass = 'contract-full-width';
        }
    }
}
