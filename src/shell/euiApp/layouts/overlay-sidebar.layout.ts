import { Component, Input } from '@angular/core';
import { EuiAppInfo } from './../shell-app.api';
import { EuiShellTemplateComponent } from './../shell-app.component';
import { ShellService } from './../../core/shell.service';
import { UxService } from './../../../@eui/ux-commons';


@Component({
    selector: 'overlay-sidebar-layout',
    template: `
    <ux-layout-app uxTrackScroll>
        <uxLayoutAppSidebarContainer>
            <ux-layout-sidebar [theme]="shellService.shellTheme"
                               [isLargeItems]="false"
                               [isStateOpen]="true">
                <uxLayoutSidebarContent>
                    <eui-layout-sidebar-items
                        [links]="shellService.sideBarMenu"
                        (clicked)="shellService.onSideBarMenuClicked($event)">
                    </eui-layout-sidebar-items>
                </uxLayoutSidebarContent>
            </ux-layout-sidebar>
        </uxLayoutAppSidebarContainer>
        <uxLayoutAppMainContainer>
            <ux-layout-app-main>
                <uxLayoutAppMainHeaderContainer>
                    <ux-layout-header   [homeUrl]="appInfo.homeUrl"
                                        [appFullName]="appInfo.appFullName"
                                        [appShortName]="appInfo.appShortName"
                                        [envLabel]="shellService.envLabel"
                                        [userInfos]="shellService.getUserInfo()"
                                        [isShowLanguageSelector]="appInfo.availableLanguages != 'none'"
                                        [isHideLogo]="!shellService.isHeaderShowLogo"
                                        [languageCodes]="appInfo.availableLanguages"
                                        (languageChanged)="shellService.changeLanguage($event)">
                    </ux-layout-header>
                    <oib-clock *ngIf="shellService.isShowClock"></oib-clock>
                </uxLayoutAppMainHeaderContainer>
                <uxLayoutAppMainNavBarContainer>
                    <ux-layout-nav-bar>
                        <ux-layout-nav-bar-top-menu *ngIf="shellService.topBarMenu.length"
                                                    [links]="shellService.topBarMenu"
                                                    [homeUrl]="appInfo.homeUrl"
                                                    (menuItemClicked)="shellService.onTopMenuClicked($event)">
                        </ux-layout-nav-bar-top-menu>
                        <ux-layout-nav-bar-actions>
                            <ux-layout-nav-bar-action-item *ngFor="let action of shellService.appModel.topBar.actions"
                                iconClass="{{action.icon}}"
                                tagCount="{{action.badgeCount}}"
                                [isToggleContent]="false"
                                (clicked)="shellService.onActionClicked($event)">
                            </ux-layout-nav-bar-action-item>
                            <ux-layout-nav-bar-action-item iconClass="fa-bars"
                                                        itemClass="app-menu"
                                                        [isOverlayPanel]="true"
                                                        [isOverlayPanelCustomContent]="true">
                                <uxLayoutNavBarOverlayPanelContent>
                                    <eui-menu-overlay-panel [appInfo]="appInfo"
                                                            [links]="shellService.allMenu"
                                                            (clicked)="shellService.onAllMenuClicked($event)"
                                                            (languageChanged)="shellService.changeLanguage($event)">
                                    </eui-menu-overlay-panel>
                                </uxLayoutNavBarOverlayPanelContent>
                            </ux-layout-nav-bar-action-item>
                        </ux-layout-nav-bar-actions>
                    </ux-layout-nav-bar>
                </uxLayoutAppMainNavBarContainer>
                <uxLayoutAppMainContentContainer>
                    <router-outlet></router-outlet>
                </uxLayoutAppMainContentContainer>
                <uxLayoutAppMainFooterContainer>
                    <ux-layout-footer isCompact="true">
                        <div class="app-infos">
                            {{shellService.getFooterAppInfo()}}
                        </div>
                        <div class="links">
                            {{shellService.getFooterLinks()}}
                        </div>
                    </ux-layout-footer>
                </uxLayoutAppMainFooterContainer>
            </ux-layout-app-main>
        </uxLayoutAppMainContainer>
    </ux-layout-app>
`,
styles: [`
    .app-infos {
        float:right !important;
    }
`]
})
export class OverlaySideBarLayoutComponent {
    @Input() public appInfo: EuiAppInfo;

    constructor(public shellService: ShellService, private uxService: UxService) {
    }
}
