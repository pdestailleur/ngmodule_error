import { CustomComponentModule } from '../components/custom-components.module';
import { DefaultContractLayoutComponent } from './default-contract.layout';
import { FixedSideBarLayoutComponent } from './fixed-sidebar.layout';
import { InnerSideBarTopMenuLayoutComponent } from './inner-sidebar-topmenu.layout';
import { NgModule } from '@angular/core';
import { OverlaySideBarLayoutComponent } from './overlay-sidebar.layout';
import { Title } from '@angular/platform-browser';
import { UxModule, UxService } from './../../../@eui/ux-commons';


const layouts: any[] = [
    InnerSideBarTopMenuLayoutComponent,
    OverlaySideBarLayoutComponent,
    FixedSideBarLayoutComponent,
    DefaultContractLayoutComponent
];

@NgModule({
    imports: [
        UxModule,
        CustomComponentModule
    ],
    declarations: [
        ...layouts
    ],
    providers: [
        UxService,
        Title
    ],
    exports: [
        ...layouts
    ]
})
export class EuiShellLayoutModule { }
