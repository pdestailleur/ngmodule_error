import { IShellEnvironment, IShellInfo, LogLevel } from './../core';

export class EuiAppInfo implements IShellInfo {
    layout: euiShellLayout;
    appFullName: string;
    appShortName: string;
    defaultLanguage: string;
    version: string;
    lastMod: Date;
    availableLanguages: string;
    environment: IShellEnvironment;
    info: string;
    homeUrl: string;
    log: boolean;
    logLevel: LogLevel;
    sessionKeepAlive: number;
    theme: euiTheme;

    constructor() {
        this.layout = euiShellLayout.DEFAULT_CONTRACT;
        this.appFullName = '';
        this.appShortName = '';
        this.defaultLanguage = 'none';
        this.version = '';
        this.availableLanguages = 'none';
        this.environment = IShellEnvironment.DEVELOPMENT;
        this.info = '';
        this.homeUrl = '/';
        this.log = false;
        this.logLevel = LogLevel.OFF;
        this.sessionKeepAlive = -1;
        this.theme = euiTheme.LIGHT;
    }
}

export enum euiShellLayout {
    SIDEBAR_FIXED,
    SIDEBAR_FIXED_CLOCK,
    SIDEBAR_COLLAPSE,
    SIDEBAR_COLLAPSE_CLOCK,
    INNERSIDEBAR_COLLAPSE,
    INNERSIDEBAR_COLLAPSE_CLOCK,
    DEFAULT_CONTRACT,
    DEFAULT_CONTRACT_FULLWIDTH
}

export enum euiTheme {
    LIGHT,
    DARK
}
