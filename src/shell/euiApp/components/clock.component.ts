import { Component } from '@angular/core';
import { ShellService } from '../../core/shell.service';

@Component({
    selector: 'oib-clock',
    template: `
	  <div class="clock"
           [class.clock-mobile]="shellService.isMobile()"
           [class.clock-desktop]="!shellService.isMobile()"
           *ngIf="hour">
		<span class="btn">
		  	<i class="fa fa-clock-o fa-lg" aria-hidden="true"></i>
			<b>{{hour}}:{{min}}</b>
		</span>
	  </div>
	  `,
    styles: [`
		.clock{
			position: absolute;
			display: flex;
			justify-content: center;
			align-items: center;
		}
		.btn {
			background-color: white !important;
			border: 1px solid lightgrey;
			cursor: default;
            z-index: 10000;
		}
		.btn:hover {
			cursor: default;
		}
		.btn:active {
			-moz-box-shadow:    inset 0 0 0px white;
			-webkit-box-shadow: inset 0 0 0px white;
			box-shadow:         inset 0 0 0px white;
		}
        .clock-desktop {
			left: 0; right: 0; top: 2rem;
        }
        .clock-mobile {
            left: 0; right: 0; top: 0.6rem;
        }
	`]
})
export class OIBClockComponent {
    min: string;
    hour: string;

    constructor( private shellService: ShellService) {
        setInterval(() => {
            const _date = new Date();
            this.min = (_date.getMinutes() < 10 ? '0' : '') + _date.getMinutes();
            this.hour = (_date.getHours() < 10 ? '0' : '') + _date.getHours();
        }, 1000);
    }
}
