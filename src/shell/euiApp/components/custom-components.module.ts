import { CommonModule } from '@angular/common';
import { EuiLayoutSidebarItemComponent } from './sidebar-menuitem.component';
import { EuiLayoutSidebarItemsComponent } from './sidebar-menuitems.component';
import { MenuOverlayPanelComponent } from './menu-overlay-panel.component';
import { NgModule } from '@angular/core';
import { OIBClockComponent } from './clock.component';
import { RouterModule } from '@angular/router';
import { UxModule, UxService, UxTagComponentModule } from './../../../@eui/ux-commons';


@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        UxModule,
        UxTagComponentModule
    ],
    declarations: [
        EuiLayoutSidebarItemComponent,
        EuiLayoutSidebarItemsComponent,
        MenuOverlayPanelComponent,
        OIBClockComponent
    ],
    providers: [
        UxService
    ],
    exports: [
        EuiLayoutSidebarItemsComponent,
        MenuOverlayPanelComponent,
        OIBClockComponent
    ]
})
export class CustomComponentModule {
}

