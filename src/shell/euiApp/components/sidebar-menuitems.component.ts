import {
    AfterContentInit,
    Component,
    ContentChildren,
    EventEmitter,
    forwardRef,
    Input,
    Output,
    QueryList
    } from '@angular/core';
import { EuiLayoutSidebarItemComponent } from './sidebar-menuitem.component';
import { UxLink } from './../../../@eui/ux-commons';


@Component({
    selector: 'eui-layout-sidebar-items',
    template: `
        <div class="sidebar-items"
             [class.sidebar-items--inner-navigation-sidebar]="isInnerNavigationSidebar">

            <template [ngIf]="!hasLinks">
                <ng-content></ng-content>
            </template>

            <template [ngIf]="hasLinks">
                <eui-layout-sidebar-item *ngFor="let link of links"
                                        id="{{link.id}}"
                                        label="{{link.label}}"
                                        subLabel="{{link.subLabel}}"
                                        url="{{link.url}}"
                                        [isActive]="link.active"
                                        [isLarge]="isLargeItems"
                                        iconClass="{{link.iconClass}}"
                                        [isHome]="link.isHome"
                                        [hasTag]="link.hasTag"
                                        tagCount="{{link.tagCount}}"
                                        (clicked)="onClick($event)">
                    <eui-layout-sidebar-item *ngFor="let childLink of link.children"
                                            id="{{childLink.id}}"
                                            label="{{childLink.label}}"
                                            subLabel="{{childLink.subLabel}}"
                                            url="{{childLink.url}}"
                                            [isActive]="childLink.active"
                                            [isLarge]="isLargeItems"
                                            iconClass="{{childLink.iconClass}}"
                                            [hasTag]="childLink.hasTag"
                                            tagCount="{{childLink.tagCount}}"
                                            (clicked)="onClick($event)">
                        <eui-layout-sidebar-item *ngFor="let childLinkSub of childLink.children"
                                                id="{{childLinkSub.id}}"
                                                label="{{childLinkSub.label}}"
                                                subLabel="{{childLinkSub.subLabel}}"
                                                url="{{childLinkSub.url}}"
                                                [isActive]="childLinkSub.active"
                                                [isLarge]="isLargeItems"
                                                iconClass="{{childLinkSub.iconClass}}"
                                                [hasTag]="childLinkSub.hasTag"
                                                tagCount="{{childLinkSub.tagCount}}"
                                                (clicked)="onClick($event)">
                        </eui-layout-sidebar-item>
                    </eui-layout-sidebar-item>
                </eui-layout-sidebar-item>
            </template>
        </div>
    `
})
export class EuiLayoutSidebarItemsComponent implements AfterContentInit {
    @Input() links: UxLink[] = [];
    @Input() isLargeItems = false;
    @Input() isInnerNavigationSidebar = false;

    @Output() clicked: EventEmitter<EuiLayoutSidebarItemComponent> = new EventEmitter<EuiLayoutSidebarItemComponent>();

    @ContentChildren(forwardRef(() => EuiLayoutSidebarItemComponent)) items: QueryList<EuiLayoutSidebarItemComponent>;

    constructor() {}

    ngAfterContentInit() {
        if (this.items.length !== 0) {
            this.items.forEach((item) => {
                if (this.isLargeItems) {
                    item.isLarge = this.isLargeItems;
                }
            });
        }
    }

    get hasLinks(): boolean {
        if (this.links) {
            return this.links.length !== 0;
        }
        return false;
    }

    onClick( item: EuiLayoutSidebarItemComponent) {
        this.clicked.emit( item );
    }
}
