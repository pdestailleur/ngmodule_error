import { ActivatedRoute, Router, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {
    Component,
    ContentChildren,
    EventEmitter,
    forwardRef,
    Input,
    Output,
    QueryList
    } from '@angular/core';


@Component({
    selector: 'eui-layout-sidebar-item',
    template: `
    <div class="sidebar-item" (keydown)="onKeyDown($event)"
                              (click)="onClick($event)">

        <div class="sidebar-item__header" [class.sidebar-item__header--title]="!(url || id)"
                                          [class.sidebar-item__header--large]="isLarge"
                                          [class.sidebar-item__header--active]="isCurrentRouteActive" tabindex="0">
            <div *ngIf="iconClass" class="sidebar-item__header-icon-wrapper">
                <span class="sidebar-item__header-icon-wrapper-icon fa fa-fw {{iconClass}}"></span>
            </div>
            <div class="sidebar-item__header-label-wrapper">
                <div class="sidebar-item__header-label-wrapper-label">
                    <span class="pull-left">{{label}}</span>
                    <span class="sidebar-tag pull-right">
                        <ux-a-tag   *ngIf="hasTag"
                                    label="{{tagCount}}"
                                    isRounded="true" isSmall="true"></ux-a-tag>
                    </span>
                </div>
                <div *ngIf="subLabel" class="sidebar-item__header-label-wrapper-sub-label">
                    {{subLabel}}
                </div>
            </div>
            <span *ngIf="hasSub" class="sidebar-menu-item__header-expand-toggle">
                <span class="fa fa-angle-down"></span>
            </span>
        </div>
        <div class="sidebar-item__sub">
            <ng-content></ng-content>
        </div>
    </div>
  `,
  styles: [`
      .sidebar-tag {
        width:auto;
      }
      .sidebar-menu-item__header-expand-toggle{
        font-size: 1.42857rem;
        padding: 0.25rem;
        text-align: center;
        width: 35px;
      }

  `]
})
export class EuiLayoutSidebarItemComponent {
    @Input() id: string;
    @Input() label: string;
    @Input() subLabel: string;
    @Input() url: string;
    @Input() iconClass: string;
    @Input() isLarge = false;
    @Input() isActive = false;
    @Input() isHome = false;
    @Input() hasTag = false;
    @Input() tagCount = '';

    @Output() clicked: EventEmitter<EuiLayoutSidebarItemComponent> = new EventEmitter<EuiLayoutSidebarItemComponent>();

    @ContentChildren(forwardRef(() => EuiLayoutSidebarItemComponent)) subItems: QueryList<EuiLayoutSidebarItemComponent>;

    constructor(private router: Router, private activatedRoute: ActivatedRoute) {

    }

    get hasSub(): boolean {
        //  to prevent self inclusion : https://github.com/angular/angular/issues/10098#issuecomment-235157642
        if (this.subItems.length > 1) {
            return true;
        }
        return false;
    }

    get isCurrentRouteActive(): boolean {
        if (this.url) {
            if (this.router.isActive(this.router.createUrlTree([this.url], { relativeTo: this.activatedRoute }), this.isHome)) {
                return true;
            }
        } else if (this.id && this.isActive) {
            return true;
        }
        return false;
    }

    // as routerLink is defined on the main <div> we need to trap the ENTER key for keyboard navigation
    onKeyDown(event: any) {
        if (event.keyCode === 13) {
            this.navigateTo();
        }
    }

    onClick(event: any) {
        this.clicked.emit(this);
        this.navigateTo();
        event.preventDefault();
        event.stopPropagation();
    }

    private navigateTo() {
        if (this.url) {
            this.router.navigate([this.url], { relativeTo: this.activatedRoute });
            window.scrollTo(0, 0);
        }
    }

}
