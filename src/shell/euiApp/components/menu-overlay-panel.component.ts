import {
    Component,
    EventEmitter,
    Input,
    Output
    } from '@angular/core';
import { EuiAppInfo } from './../shell-app.api';
import { ShellService } from './../../core/shell.service';
import { UxLanguage, UxLink, UxService } from './../../../@eui/ux-commons';


@Component({
    selector: 'eui-menu-overlay-panel',
    template: `
        <div class="overlay-panel__header">
            <div class="overlay-panel__header-profile">
                <span (click)="onLanguageSelectorClick($event)"><ux-language-selector
                            (languageChanged)="onLanguageChanged($event)"
                            [languageCodes]="appInfo.availableLanguages"
                            [selectedLanguage]="uxService.activeLanguage">
                </ux-language-selector></span>
                <span class="overlay-panel__header-profile-infos">
                    {{shellService.getUserInfo()}}
                </span>
            </div>
        </div>
        <div class="overlay-panel__inner">
            <eui-layout-sidebar-items
                [links]="links"
                (clicked)="onClick($event)">
            </eui-layout-sidebar-items>
        </div>
    `
})
export class MenuOverlayPanelComponent {
    @Input() public appInfo: EuiAppInfo;
    @Input() public links: UxLink[];
    @Output() public languageChanged: EventEmitter<UxLanguage> = new EventEmitter<UxLanguage>();
    @Output() public clicked: EventEmitter<UxLink> = new EventEmitter();

    constructor( private shellService: ShellService, private uxService: UxService) {

    }

    onLanguageSelectorClick(event: Event) {
        event.stopImmediatePropagation();
        event.preventDefault();
    }

    onClick( event: UxLink ) {
        this.clicked.emit( event );
    }

    onLanguageChanged(language: UxLanguage) {
        this.uxService.activeLanguage = language;
        this.languageChanged.emit(language);
    }

}

