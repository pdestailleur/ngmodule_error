import { EuiShellLayoutModule } from './layouts/eui-shell-layout.module';
import { EuiShellTemplateComponent } from './shell-app.component';
import { NgModule } from '@angular/core';
import { OIBCoreModule } from './../core/oib-core.module';

@NgModule({
    imports: [
        OIBCoreModule,
        EuiShellLayoutModule
    ],
    declarations: [
        EuiShellTemplateComponent
    ],
    exports: [
        EuiShellTemplateComponent,
        OIBCoreModule
    ]
})
export class EuiShellModule { }
