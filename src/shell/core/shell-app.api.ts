import { EuiAppInfo, euiShellLayout, euiTheme } from './../euiApp';
import { Route, Router } from '@angular/router';

import { BehaviorSubject } from 'rxjs/Rx';

export interface IApplicationModel {
    shell: IShellInfo;
    menu: IMenu;
    topBar?: ITopBar;
    help?: IHelp;
}

export interface IExtendedApplicationModel extends IApplicationModel {
    watcher$: BehaviorSubject<IApplicationModel>;
}

export enum IShellEnvironment {
    DEVELOPMENT,
    TEST,
    ACCEPTANCE,
    PRODUCTION
}

export interface IShellInfo {
    log: boolean;
    logLevel: LogLevel;
    environment: IShellEnvironment;
    version: string;
    lastMod: Date;
    info: string;
    homeUrl: string;
    layout: euiShellLayout;
    theme: euiTheme;
}

export interface IShellUser {
    firstname: string;
    lastname: string;
    email: string;
    groups?: string[];
    roles?: string[];
}

/**
 * The log level.
 */
export enum LogLevel {
    OFF = 0,
    ERROR = 1,
    WARN = 2,
    INFO = 3,
    DEBUG = 4
}
/** PluginContainer menu */
export interface IMenu {
    sections: IMenuSection[];
}

export interface IMenuSection {
    label: string;
    entries: IMenuEntry[];
}

export interface IMenuEntry {
    id?: string;
    icon?: string;
    label: string;
    badgeCount?: number;
    onAction: ((router: Router) => void) | string;
}
/** End of application menu */

/** PluginContainer top bar */
export interface ITopBar {
    actions?: IAction[];
    sections?: IMenuSection[];
}

export interface IAction {
    id?: string;
    icon: string;
    badgeCount?: number;
    tooltip?: string;
    onAction?: ((router: Router) => void) | string;
    priority?: ActionPriority;
}

export enum ActionPriority {
    LOWEST = 1,
    LOW = 2,
    MEDIUM = 3,
    HIGH = 4,
    HIGHEST = 5,
    SYSTEM = 6
}
/** End of application top bar */

/** PluginContainer help menu */
export interface IHelp {
    title: string;
    sections: IHelpSection[];
}

export interface IHelpSection {
    title: string;
    entries: IHelpEntry[];
}

export interface IHelpEntry {
    text: string;
    url?: string;
    checkable?: boolean;
}

/** End of application help menu */
