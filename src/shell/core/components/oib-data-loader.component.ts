import { Component, Inject, Input, SimpleChanges, OnInit, OnChanges } from '@angular/core';
import { Observable } from 'rxjs/Rx';

@Component({
    selector: 'oib-data-loader',
    template: `
        <div *ngIf="isLoading()" class="oeh-loading">
            <div *ngIf="showSpinner()" class="spinner"><i class="fa fa-spinner fa-pulse"></i></div>
        </div>
        <div *ngIf="hasError() && !isLoading()" class="oeh-error">
            <i class="fa fa-2x fa-exclamation-triangle"></i> Error loading data
            <a *ngIf="allowRetry" href="#" (click)="onRetry()">(try again)</a>

        </div>
        <div *ngIf="!hasError() && !isLoading()">
            <ng-content></ng-content>
        </div>
    `,
    styles: [`
        .oeh-error {
            text-align: center;
            font-weight: bold;
            font-style: italic;
            color: darkred;
        }

        .oeh-loading {
            text-align: center;
            font-weight: bold;
            font-style: italic;
            font-size: small;
            color: dimgray;
        }
    `]
})
export class OIBDataLoaderComponent implements OnInit, OnChanges {

    @Input() private loader: () => Observable<any>;
    @Input() private delay = 500; // By default delay to show the spinner is half a second

    @Input() public allowRetry = true;

    private _error = false;
    private _loading = false;
    private _showSpinner = false;

    ngOnInit() {
        this.startLoading();
    }

    ngOnChanges(changes: SimpleChanges) {
        this.startLoading();
    }

    public isLoading(): boolean {
        return this._loading;
    }

    public showSpinner(): boolean {
        return this._showSpinner;
    }

    public hasError(): boolean {
        return this._error;
    }

    private startLoading() {
        this._error = false;
        this._loading = true;
        this._showSpinner = false;

        this.loader().subscribe(
            next => this.stopLoading(false),
            err => this.stopLoading(true)
        );

        setTimeout(
            () => {
                if (this._loading) {
                    this._showSpinner = true;
                } else {
                    this._showSpinner = false;
                }
            }
            , this.delay
        );
    }

    private stopLoading(error: boolean) {
        this._loading = false;
        this._showSpinner = false;
        this._error = error;
    }

    public onRetry() {
        this.startLoading();
        return false; // Stop propagating event
    }
}
