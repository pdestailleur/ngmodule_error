import { Component, OnChanges, Input, SimpleChange } from '@angular/core';

@Component({
    selector: 'oib-badge',
    template: `
        <span role="separator" class="divider"></span>
        <span class="navbar-text">
            <i [ngClass]="fontClass" class="fa navbar-icon icon-positioning" aria-hidden="true"></i>
            <span animate-change="badgeValue" [class.glow]="isAnimating" ng-bind="badgeValue" class="badge oib-badge">{{badgeValue}}</span>
            <!--<span [class.glow]="isAnimating" class="badge oib-badge">{{badgeValue}}</span>-->
        </span>
    `,
    styles: [`
        .oib-badge {
            position: relative;
            background-color: #f00;
            color: whitesmoke;
            top: 0px;
            left: -6px;
            font-size: 0.7em;
        }

        .navbar-text {
            color: #fff;
        }

        .icon-positioning {
            position: relative;
            font-size: 1.5em;
            top: -2px;
            left: 3px;
            vertical-align: bottom;
        }

        .glow {
          animation: glow-keys ease-in-out 3s;
          animation-iteration-count: 1;
        }

        @keyframes glow-keys {
          33% {
            background: whitesmoke;
            color:red;
          }
          84% {
            background: red;
            color:whitesmoke;
          }
          60% {
            background: whitesmoke;
            color:red;
          }
        }
    `]
})

export class OIBBadgeComponent implements OnChanges {

    @Input() badgeValue: number;
    @Input() fontClass: string;

    isAnimating = false;
    idBadge: number;

    ngOnChanges(changes: { [propName: string]: SimpleChange }) {
        if (this.badgeValue > 0) {
            this.bounceBadge();
        }
    }

    bounceBadge() {
        this.isAnimating = true;
        /*this.idBadge =*/
        setInterval(() => {
            this.isAnimating = false;
            if (this.idBadge) {
                clearInterval(this.idBadge);
            }
        }, 2000);
    }
}
