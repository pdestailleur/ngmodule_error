import {
    Component,
    EventEmitter,
    Inject,
    Input,
    Output
    } from '@angular/core';

@Component({
    selector: 'shell-page-subsection',
    template: `
    <div class="oib-ps">
        <h5 class="oib-ps-title">{{title}}</h5>
        <div class="oib-ps-contents">
            <ng-content></ng-content>
        </div>
    </div>
  `,
    styles: [`
        .oib-ps {
            margin-bottom: 10px;
        }
        .oib-ps-contents{
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 20px;
        }
  `],
})
export class ShellPageSubsectionComponent {
    @Input() title: string;
}
