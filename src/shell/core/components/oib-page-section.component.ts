import {Component, EventEmitter, Inject, Input, Output} from '@angular/core';

@Component({
  selector: 'oib-page-section',
  template: `
    <div class="oib-ps">
        <h3 class="oib-ps-title">{{title}}</h3>
        <div class="oib-ps-contents">
            <ng-content></ng-content>
        </div>
    </div>
  `,
  styles: [`
        .oib-ps {
            margin-bottom: 10px;
        }
        .oib-ps-contents{
            padding-top: 5px;
            padding-bottom: 5px;
            padding-left: 20px;
        }
  `],
})
export class OIBPageSectionComponent {
    @Input() title: string;
    @Input() isError: boolean;
}
