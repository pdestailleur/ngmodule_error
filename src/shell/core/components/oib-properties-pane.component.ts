import { Component } from '@angular/core';

@Component({
    selector: 'oib-properties-pane',
    template: `
        <div class="oib-pp-container">
            <ng-content></ng-content>
        </div>
    `,
    styles: [`
        .oib-pp-container {
            display: flex;
            flex-direction: column;
            flex-wrap: nowrap;
            justify-content: flex-start;
        }
    `],
})
export class OIBPropertiesPaneComponent {
}
