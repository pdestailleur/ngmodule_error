import {
    AfterContentInit,
    Component,
    ContentChildren,
    Input,
    QueryList
    } from '@angular/core';
import { OIBTableFormEntryFormComponent } from './oib-table-form-entry-form.component';

@Component({
    selector: 'oib-table-form-entry',
    template: `
<div class="oib-tfe" [class.oib-tfe-expanded]="isExpanded">
    <div class="oib-tfe-row">
        <div class="oib-tfe-en">
            {{entryName}}
        </div>
        <div class="oib-tfe-ev">
            <div *ngIf="!isExpanded">
                {{entryValue}}
            </div>
            <div *ngIf="isExpanded">
                <ng-content></ng-content>
            </div>
        </div>
        <div class="oib-tfe-ac">
            <a *ngIf="!isExpanded" class="oib-tfe-m" href="#" (click)="onExpandClicked($event)">Modify</a>
        </div>
    </div>
</div>
    `,
    styles: [`
.oib-tfe {
    border-bottom: solid thin;
    border-color: #E8E8E8;
    padding-bottom: 2px;
    padding-top: 2px;
    margin-left: 0px;
}

.oib-tfe-row {
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
}

.oib-tfe-expanded {
    background-color: #eee;
    padding-left: 15px;
    padding-right: 15px;
    padding-bottom: 5px;
    padding-top: 10px;
    margin-bottom: 5px;
}

.oib-tfe-en {
    flex-basis: 20%;
    text-align: left;
    font-weight: bold;
    font-size: small;
}

.oib-tfe-ev {
    flex-basis: 70%;
    text-align: left;
    text-align: left;
    font-weight: normal;
    font-size: small;
    color: #999;
}

.oib-tfe-ac {
    flex-basis: 10%;
}

.oib-tfe-m {
    text-align: center;
    font-size: x-small;
    text-decoration: none;
    color: #006d91;
    vertical-align: middle;
    padding-bottom: 2px; /*FIXME: It should be enough with previous line */
}
	`]
})
export class OIBTableFormEntryComponent implements AfterContentInit {
    @Input() entryName: string;
    @Input() entryValue: string;
    @Input() isExpanded: boolean;

    @ContentChildren(OIBTableFormEntryFormComponent) formQuery: QueryList<OIBTableFormEntryFormComponent>;

    ngAfterContentInit() {
        // If we have forms as children, we will subscribe to the cancel even to close the entry
        this.formQuery.forEach(
            f => f.onCancel.subscribe(
                (ev: any) => this.isExpanded = false
            )
        );
    }

    onExpandClicked(event: any) {
        this.isExpanded = !this.isExpanded;
        event.stopPropagation();
        return false;
    }
}
