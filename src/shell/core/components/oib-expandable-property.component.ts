import { Component, Input } from '@angular/core';

export interface IAppAction {
    label: string;
    onActionFired: () => void;
    isDisabled: boolean;
}

@Component({
  selector: 'oib-expandable-property-item',
  template: `
<div class="oib-ei">
    <div class="oib-ei-header">
        <div (click)="onExpandClicked($event)" class="oib-ei-header-text">
            <div *ngIf="!isExpanded" class="glyphicon glyphicon-play oib-expander" aria-hidden="true"></div>
            <div *ngIf="isExpanded" class="glyphicon glyphicon-triangle-bottom oib-expander" aria-hidden="true"></div>
            <div class="oib-property-name"> {{propertyName}}:</div>
            <div class="oib-property-value"> {{propertyValue}}</div>
        </div>
        <div class="oib-ei-header-actions">
            <a *ngFor="let action of actions" href="#"
                    class="oib-property-action"
                    [class.oib-action-disabled]="action.isDisabled"
                    (click)="!action.isDisabled && action.onActionFired()">
                {{action.label}}
            </a>
        </div>
    </div>
    <div *ngIf="isExpanded" class="oib-ei-contents">
        <ng-content></ng-content>
    </div>
</div>
  `,
  styles: [`
.oib-ei {
    display: flex;
    flex-flow: column nowrap;
    justify-content: flex-start;
    margin-bottom: 5px;
}

.oib-ei-header {
    display: flex;
    flex-flow: row nowrap;
    flex-basis: auto;
    justify-content: flex-start;
    padding-bottom: 5px;
}

.oib-ei-header-text {
    display: flex;
    flex-flow: row nowrap;
    flex-basis: auto;
    flex-grow: 0;
    justify-content: flex-start;
    align-items: center;
}

.oib-ei-header-text:hover {
    text-decoration: underline;
    text-decoration-style: dotted;
    text-decoration-color: #bbb;
    cursor: pointer;
    cursor: hand;
}

.oib-ei-header-actions {
    flex-basis: auto;
    flex-grow: 1;
    margin-left: 5px;
}

.oib-ei-contents {
    margin-left: 2px;
    margin-top: 12px;
    margin-bottom: 33px;
    padding-left: 25px;
    padding-top: 0px;
    padding-bottom: 0px;
    border-left: solid #FFF thick;
}

.oib-expander {
    flex-basis: auto;
    font-size: x-small;
    color: #bbb;
}

.oib-property-name {
    margin-left: 5px;
    font-weight: bold;
    flex-basis: auto;
}

.oib-property-value {
    flex-basis: auto;
    margin-left: 5px;
    color: #777;
    margin-right: 10px;
}

.oib-property-action {
    margin-left: 10px;
    /*background-color: #e7e7e7;*/
    /*color: #888;*/
    font-size: smaller;
}

.oib-action-disabled {
    color: gray;
    text-decoration: line-through;
    cursor: default;
}
  `]
})
export class OIBExpandablePropertyComponent {
    @Input() propertyName: string;
    @Input() propertyValue: string;

    @Input() actions: IAppAction[];
    @Input() isExpanded: boolean;

    onExpandClicked() {
        this.isExpanded = !this.isExpanded;
    }
}
