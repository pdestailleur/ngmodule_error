import { Component, Input } from '@angular/core';

@Component({
    selector: 'oib-properties-pane-item',
    template: `
        <div class="oib-ppi">
            <div class="oib-ppi-label">{{label}}</div>
            <div class="oib-ppi-content">
                <ng-content></ng-content>
            </div>
        </div>
    `,
    styles: [`
        .oib-ppi {
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            justify-content: flex-start;
        }
        .oib-ppi-label {
            flex: 1 1 30%;
            font-weight: bold;
        }
        .oib-ppi-content {
            flex: 1 1 70%;
        }
    `]
})
export class OIBPropertiesPaneItemComponent {
    @Input() label: string;
}
