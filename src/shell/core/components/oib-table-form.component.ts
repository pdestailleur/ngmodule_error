import { Component } from '@angular/core';

@Component({
    selector: 'oib-table-form',
    template: `
<div class="oib-tf">
    <span *ngIf="title != null" class="oib-tf-title">{{title}}</span>
    <div class="oib-tf-body">
        <ng-content></ng-content>
    </div>
</div>`,
    styles: [`
.oib-tf {
    padding-top: 1px;
    padding-bottom:1px;
}

.oib-tf-title {
    text-transform: uppercase;
    color: #aaa;
    border-color: #888;
    padding-bottom: 1px;
}

.oib-tf-body {
    padding-top: 3px;
    padding-bottom: 3px;
    border-top: solid thin;
    border-color: #E8E8E8;
}
	`]
})
export class OIBTableFormComponent {
}
