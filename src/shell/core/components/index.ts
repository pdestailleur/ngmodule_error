import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { OIBBadgeComponent } from './oib-badge.component';
import { OIBDataLoaderComponent } from './oib-data-loader.component';
import { OIBPageComponent } from './oib-page.component';
import { OIBPageSectionComponent } from './oib-page-section.component';
import { OIBPropertiesPaneComponent } from './oib-properties-pane.component';
import { OIBPropertiesPaneItemComponent } from './oib-properties-pane-item.component';
import { OIBTableFormComponent } from './oib-table-form.component';
import { OIBTableFormEntryComponent } from './oib-table-form-entry.component';
import { OIBTableFormEntryFormComponent } from './oib-table-form-entry-form.component';
import { OIBTableFormEntryFormPropertyComponent } from './oib-table-form-entry-form-property.component';
import { ShellPageComponent } from './shell-page.component';
import { ShellPageSectionComponent } from './shell-page-section';
import { ShellPageSubsectionComponent } from './shell-page-subsection';


const components: any[] = [
    OIBBadgeComponent,
    OIBDataLoaderComponent,
    OIBPageSectionComponent,
    OIBPageComponent,
    OIBPropertiesPaneComponent ,
    OIBPropertiesPaneItemComponent,
    OIBTableFormComponent,
    OIBTableFormEntryComponent,
    OIBTableFormEntryFormComponent,
    OIBTableFormEntryFormPropertyComponent,
    ShellPageComponent,
    ShellPageSectionComponent,
    ShellPageSubsectionComponent
];

@NgModule({
    imports: [ CommonModule ],
    exports: [ ...components ],
    declarations: [ ...components ]
})
export class CoreComponentsModule {}
