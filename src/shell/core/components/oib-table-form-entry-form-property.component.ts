import { Component, Input } from '@angular/core';

@Component({
    selector: 'oib-table-form-entry-form-property',
    template: `
<div class="oib-tfef-row">
    <div class="oib-tfef-ename">
        {{propertyName}}
    </div>
    <div class="oib-tfef-evalue">
        <ng-content></ng-content>
    </div>
</div>`,
    styles: [`
.oib-tfef {
    margin-top: 4px;
    margin-bottom: 2px;
    margin-left: 0;
    padding-left: 0;
}
.oib-tfef-row {
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
    align-items: center;
    margin-top: 4px;
    margin-bottom: 2px;
}

.oib-tfef-ename {
    flex-basis: 30%;
    color: #777;
    font-weight: bold;
    text-align: right;
    margin-right: 8px;
}

.oib-tfef-evalue {
    flex-basis: 70%;
    color: #555
}

	`]
})
export class OIBTableFormEntryFormPropertyComponent {
    @Input() propertyName: string;
    @Input() isMandatory: boolean;
    @Input() isError: boolean;
}
