import {
    Component,
    EventEmitter,
    Input,
    Output
    } from '@angular/core';

@Component({
    selector: 'oib-table-form-entry-form',
    template: `
<div class="oib-tfef">
    <ng-content></ng-content>

    <div class="oib-tfef-row oib-tfef-buttons">
        <div class="oib-tfef-btleft">
            <button type="submit" class="btn btn-primary btn-sm oib-tfef-bsave" (click)="onSaveClicked($ev)">{{textSave}}</button>
        </div>
        <div class="oib-tfef-btright">
            <button type="submit" class="btn btn-default btn-sm" (click)="onCancelClicked($ev)">{{textCancel}}</button>
        </div>
    </div>
</div>`,
    styles: [`
.oib-tfef {
    margin-top: 4px;
    margin-bottom: 2px;
    margin-left: 0;
    padding-left: 0;
}
.oib-tfef-row {
    display: flex;
    flex-flow: row nowrap;
    justify-content: flex-start;
    align-items: center;
    margin-top: 4px;
    margin-bottom: 2px;
}

.oib-tfef-comment {
    color: #555
}

.oib-tfef-buttons {
    display: flex;
    /*justify-content: ;*/
    margin-top: 15px;
    margin-bottom: 5px;
}

.oib-tfef-btleft {
    flex-basis: 30%;
    text-align: right;
    margin-right: 8px;
}
.oib-tfef-btright {
    flex-basis: 70%;
}

.oib-tfef-bsave {
    background-color: #325D88;
    color: #fff;
    font-weight: bold;
    margin-right: 6px;
}

.oib-tfef-bcancel {
    background-color: #ffffff;
    color: #555500;
    font-weight: bold;
    margin-right: 4px;
}
	`]
})
export class OIBTableFormEntryFormComponent {
    @Input() entryName: string;
    @Input() entryValue: string;
    @Input() textSave = 'Save';
    @Input() textCancel = 'Cancel';
    @Output() onCancel = new EventEmitter();
    @Output() onSave = new EventEmitter();

    onCancelClicked(ev: any) {
        this.onCancel.emit(ev);
    }

    onSaveClicked(ev: any) {
        this.onSave.emit(ev);
    }
}
