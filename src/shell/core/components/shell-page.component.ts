import { Component, Inject, Input } from '@angular/core';

@Component({
    selector: 'shell-page',
    template: `
        <div class="oib-page-wrapper">
            <div class="oib-page-main">
                <!--
                <div class="oib-page-header">
                    <div class="oib-page-header-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Household management</a></li>
                            <li class="active">Household information</li>
                        </ol>
                    </div>
                    <div class="oib-page-header-options">
                        <div class="oib-page-header-option">
                            <button type="button" class="btn btn-xs btn-default">
                                Favourite
                                <span class="glyphicon glyphicon-star-empty" aria-hidden="true"></span>
                            </button>
                        </div>
                        <div class="oib-page-header-option">
                            <button type="button" class="btn btn-xs btn-default">
                                Actions
                                <span class="glyphicon glyphicon-option-horizontal" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                </div>
                -->

                <h1>{{title}}</h1>

                <ng-content></ng-content>

            </div>
        </div>
    `,
    styles: [`
        .oib-page-wrapper {
            display: flex;
            flex-direction: row;
            flex-wrap: nowrap;
            justify-content: space-between;
        }

        .oib-page-main {
            min-width: 50em;
        }

        h1 {
            margin-top: 20px;
            border-bottom: 1px solid #DDDDDD;
            margin-bottom: 1em;
        }

        .oib-page-header {
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-start;
            align-items: center;

        }

        .breadcrumb {
            margin-bottom: 0px;
            margin-left: 0px;
            padding-left: 0px;
            font-size: x-small;
        }
        .oib-page-header-breadcrumb {
            display: flex;
            justify-content: flex-start;
            flex-basis: 70%;
        }

        .oib-page-header-options {
            display: flex;
            flex-flow: row nowrap;
            justify-content: flex-end;
            align-items: center;
            flex-basis: 30%;
            flex-grow: 1;
        }

        .oib-page-header-option {
            flex-basis: auto;
            margin-left: 5px;
            margin-right: 5px;
        }
        .oib-page-header-option button {
            /*margin: 2px;*/
            /*padding: 2px;*/
        }

        .oib-page-footer {
            display: flex;
            flex-flow: row nowrap;
            justify-content: center;
            margin-top: 60px;
            margin-bottom: 20px;
            padding-top: 10px;
            border-top: 1px solid #DDDDDD;
            color: #aaa;
            font-size: x-small;
        }
        .oib-page-footer a {
            color: #999;
            font-size: x-small;
        }
        .oib-page-footer-element {
            flex-basis: auto;
            margin-left: 5px;
            margin-right: 5px;
        }

        .oib-page-right {
            max-width: 300px;
            min-width: 10px;
            padding-right: 0px;
            margin-right: -22px; /* FIXME: temporal hack */
            align-self: stretch;
        }
    `]
})
export class ShellPageComponent {
    @Input() title: string;
}
