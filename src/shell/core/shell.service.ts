import { BehaviorSubject, Observable, Subscription } from 'rxjs/Rx';
import { DatePipe } from '@angular/common';
import { EuiAppInfo, euiShellLayout, euiTheme } from './../euiApp';
import { EuiLayoutSidebarItemComponent } from '../euiApp/components/sidebar-menuitem.component';
import {
    EventEmitter,
    Inject,
    Injectable,
    OpaqueToken
    } from '@angular/core';
import { Message } from 'primeng/primeng';
import { NavigationEnd, NavigationStart, Router } from '@angular/router';
import {
    UxEuLanguages,
    UxLanguage,
    UxLayoutNavBarActionItemComponent,
    UxLink,
    UxService
    } from './../../@eui/ux-commons';
import {
         DefaultTranslationService,
         IGuardian,
         IHttpService,
         INotification,
         INotificationService,
         IPlugin,
         IUser,
         LangChangeEvent,
         LoggerService,
         NotificationType,
         PluginawareServiceregisterService,
         SERVICE_GUARDIAN,
         SERVICE_HTTP,
         SERVICE_LOGGER,
         SERVICE_NOTIFICATIONS,
         SERVICE_PLUGINAWARE_SERVICEREGISTER,
         SERVICE_TRANSLATIONS,
} from './../../services';
import {
    IAction,
    IApplicationModel,
    IMenuEntry,
    IMenuSection,
    IShellEnvironment,
    IShellInfo,
    IShellUser,
    LogLevel,
} from './shell-app.api';


/**
 * This service controls all the behaviour of the Shell, it digest all the changes to the IApplicationModel and the sh
 */
@Injectable()
export class ShellService {

    onLangChange: EventEmitter<LangChangeEvent> = new EventEmitter<LangChangeEvent>();
    appModel: IApplicationModel;
    appInfo: IShellInfo;
    user: IShellUser;
    isNavigationBlockDocumentActive = false;
    isLoggingActive = false;
    /**
     * Setter and getter used to set/get the uxService.activelanguage;
     */
    get activeLanguage(): UxLanguage{
        return this.uxService.activeLanguage;
    }
    set activeLanguage( lang: UxLanguage ){
        this.uxService.activeLanguage = lang;
    }

    envLabel: string;

    isContractLayout = false;
    isSidebarLayout = false;
    isInnerSidebarLayout = false;

    get shellTheme(): string {
        if ( this.appModel.shell.theme === euiTheme.LIGHT) {
            return 'light';
        }
        return null;
    }

    isShowTopHome = false;
    isShowSideHome = false;

    isHeaderShowLogo = false;
    isShowClock = false;

    // sidebarmenu
    private _sideBarMenu: UxLink[];
    get sideBarMenu(): UxLink[] {
        if ( !this._sideBarMenu ) {
            this.sideBarMenu = this.createMenu(this.appModel.menu.sections);
        }
        return this._sideBarMenu;
    }
    set sideBarMenu( menu: UxLink[]) {
        this._sideBarMenu = menu;
    }
    // topbarmenu
    private _topBarMenu: UxLink[];
    get topBarMenu(): UxLink[] {
        if ( !this._topBarMenu ) {
            this.topBarMenu = this.createMenu(this.appModel.topBar.sections);
        }
        return this._topBarMenu;
    }
    set topBarMenu( menu: UxLink[]) {
        this._topBarMenu = menu;
    }
    get allMenu (): UxLink[]{
        return this.topBarMenu.concat(this.sideBarMenu);
    }

    private keepAliveSubscription: Subscription;

    constructor(@Inject(SERVICE_HTTP) private http: IHttpService, private router: Router,
                @Inject(SERVICE_TRANSLATIONS) private translate: DefaultTranslationService,
                @Inject(SERVICE_NOTIFICATIONS) private notifier: INotificationService,
                @Inject(SERVICE_PLUGINAWARE_SERVICEREGISTER) private pas: PluginawareServiceregisterService,
                @Inject(SERVICE_LOGGER) private logger: LoggerService,
                @Inject(SERVICE_GUARDIAN) private guardian: IGuardian,
                private uxService: UxService ) {
        router.events
            .subscribe((event) => {
                if (event instanceof NavigationStart) {
                    if (this.isNavigationBlockDocumentActive) {
                        this.blockDocument();
                    }
                    this.log(LogLevel.DEBUG, 'shellService : navigationStart', event.url);
                }
                if (event instanceof NavigationEnd) {
                    if (this.isNavigationBlockDocumentActive) {
                        this.unblockDocument();
                    }
                    this.log(LogLevel.DEBUG, 'shellService : navigationEnd', this.router.url);
                }
            });

        notifier.onInfo().subscribe( notification => this.growlNotification(notification));
        notifier.onWarn().subscribe( notification => this.growlNotification(notification));
        notifier.onError().subscribe( notification => this.growlNotification(notification));
        notifier.onDebug().subscribe( notification => this.growlNotification(notification));

        guardian.getLoggedUser().subscribe( (user: IUser) => this.setUser(user) );

        this.changeLanguageByCode(window.navigator.language.split('-')[0].split('_')[0]);
    }

    initApp(appModel: IApplicationModel) {
        this.isNavigationBlockDocumentActive = true;
        this.appModel = appModel;
        this.appInfo = this.appModel.shell;
        this.isLoggingActive = this.appInfo.log;
        this.logger.setLevel(this.isLoggingActive ? this.appInfo.logLevel : LogLevel.OFF);
        if (this.appInfo.environment === undefined) {
            throw(new Error('The build script doesn\'t set the environment variable'));
        }
        if (this.appInfo.environment !== IShellEnvironment.PRODUCTION) {
            this.envLabel = IShellEnvironment[this.appInfo.environment].substring(0, 3);
        }
        if (this.appInfo.version === undefined) {
            this.appInfo.version = '';
        }
        this.log(LogLevel.DEBUG, 'shellService : initApp', appModel);
        this.isShowTopHome = (this.appModel.topBar.sections.length > 0) &&
                             (this.appModel.menu.sections.length > 0 &&
                                (this.isSidebarLayout || this.isInnerSidebarLayout));
        this.isShowSideHome = !this.isShowTopHome;

        if ( this.appInfo.constructor === EuiAppInfo) { this.initEuiApp(); }
    }

    getPAService(service: OpaqueToken, plugin: IPlugin) {
        return this.pas.getService(service, plugin);
    }

/* used in EUI */
    getTopMenu(): IMenuSection[] {
        if (this.isSidebarLayout || this.isInnerSidebarLayout) {
            return this.appModel.topBar.sections.concat(this.appModel.menu.sections);
        }
        return this.appModel.topBar.sections;
    }

    updateTopMenuLinks(update: boolean = false): void {
        if ( !this.topBarMenu ) {
            this.topBarMenu = this.createMenu(this.getTopMenu());
        } else {
            this.topBarMenu = this.updateMenu( this.createMenu(this.getTopMenu()) , this.topBarMenu );
        }
    }

    updateSideBarMenuLinks(): void {
        if ( !this.sideBarMenu ) {
            this.sideBarMenu = this.createMenu(this.appModel.menu.sections);
        } else {
            this.sideBarMenu = this.updateMenu( this.createMenu(this.appModel.menu.sections) , this.sideBarMenu );
        }
    }

    private flattenMenu ( menu: UxLink[] ): UxLink[] {
        let flatMenu: UxLink[] = [];
        menu.forEach ( item => {
            flatMenu = flatMenu.concat(item);
            if (item.children) {
                flatMenu = flatMenu.concat(...this.flattenMenu(item.children));
            }
        });
        return flatMenu;
    }

    private createMenu( source: IMenuSection[]): UxLink[] {
        let menu: UxLink[] = [];
        source.forEach( section => {
            menu = [...menu,
                new UxLink({
                    label: section.label,
                    children: section.entries.map( entry => {
                        const link = new UxLink({ id: entry.id,
                                                label: entry.label,
                                                iconClass: entry.icon});
                        if ( typeof(entry.onAction) === 'string') {
                            link.url = entry.onAction;
                        }
                        if ( entry.badgeCount !== undefined ) {
                            link.hasTag = true;
                            link.tagCount = entry.badgeCount.toString();
                        }
                        return link;
                    })
                })];
        });
        return menu;
    }

    private updateMenu( newMenu: UxLink[], oldMenu: UxLink[]) {
        const flatMenu = this.flattenMenu( oldMenu );
        const modifiedMenu = [...newMenu];
        modifiedMenu.forEach ( main => {
            main = this.findAndMerge( main, flatMenu );
            if ( main.children ) {
                main.children.forEach( sub => {
                    sub = this.findAndMerge( sub, flatMenu );
                    if ( sub.children ) {
                        sub.children.forEach ( subsub => {
                            subsub = this.findAndMerge( subsub, flatMenu );
                        });
                    }
                });
            }
        });
        return modifiedMenu;
    }

    private findAndMerge( menuitem, flatMenu ): UxLink {
        const templateObject = { active: false, expanded: false };
        let found: UxLink;
        if (menuitem.id) {
            found = flatMenu.filter( link => link.id === menuitem.id)[0];
        } else {
            found = flatMenu.filter( link => link.label === menuitem.label)[0];
        }
        if ( found ) {
            for ( const p in templateObject ) {
                if ( found[p] ) {
                    templateObject[p] = found[p];
                }
            }
        }
        return Object.assign( menuitem, templateObject );
    }

    onSideBarMenuClicked( link: EuiLayoutSidebarItemComponent ): void {
        if (link.id) {
            this.sideBarMenu.forEach( menulink => {
                menulink.active = (menulink.id === link.id);
                if (menulink.children) {
                    menulink.children.forEach( subLink => {
                        subLink.active = (subLink.id === link.id);
                        if (subLink.children) {
                            subLink.children.forEach( subsubLink => {
                            subsubLink.active = (subsubLink.id === link.id);
                        });
                    }
                    });
                }
            });
            this.appModel.menu.sections.map( section => section.entries)
                .reduce( (a, b) => a.concat(b) )
                .filter ( entry => {
                    return entry.id === link.id;
                }).forEach( selectedLink => {
                        if ( typeof(selectedLink.onAction) === 'function') {
                            selectedLink.onAction(this.router);
                        }
                    }

                );
        } else {
            this.sideBarMenu.filter( menulink => menulink.label === link.label)
                            .forEach( menulink => {
                                menulink.active = true;
                            });
        }
    }

    onTopMenuClicked( link: UxLink|string ): void {
        const selectedLink =
            this.appModel.topBar.sections.map( section => section.entries)
                .reduce( (a, b) => a.concat(b), [] )
                .filter ( entry =>
                    entry.id === ( typeof link === 'string' ? link : link.id ) ) [0];
        if (!selectedLink || !selectedLink.onAction) {
            return;
        }
        if ( typeof(selectedLink.onAction) === 'string') {
            this.router.navigateByUrl(selectedLink.onAction);
        } else {
            selectedLink.onAction(this.router);
        }
    }

    onAllMenuClicked( link: UxLink|string ): void {
        const selectedLink =
            this.appModel.menu.sections.map( section => section.entries)
                .concat( ...this.appModel.topBar.sections.map( section => section.entries) )
                .reduce( (a, b) => a.concat(b), [] )
                .filter ( entry =>
                    entry.id === ( typeof link === 'string' ? link : link.id ) ) [0];
        if (!selectedLink || !selectedLink.onAction) {
            return;
        }
        if ( typeof(selectedLink.onAction) === 'string') {
            this.router.navigateByUrl(selectedLink.onAction);
        } else {
            selectedLink.onAction(this.router);
        }
    }
/* used in EUI */

    onActionClicked( action: UxLayoutNavBarActionItemComponent ): void {
        const selectedAction =
            this.appModel.topBar.actions.filter ( existingAction =>
                    existingAction.id === action.id ) [0];
        if (!selectedAction || !selectedAction.onAction) {
            return;
        }
        if (selectedAction.onAction instanceof Function) {
            selectedAction.onAction(this.router);
        } else {
            this.router.navigateByUrl(selectedAction.onAction);
        }
    }

    getTopBarActions(): IAction[] {
        return this.appModel.topBar.actions;
    }

    getUserInfo(): String {
        if (this.user) {
            return this.user.firstname + ' ' + this.user.lastname.toUpperCase();
        }
        return undefined;
    }

    getFooterLinks(): String {
        return '';
    }

    getFooterAppInfo(): String {
        let info = '';
        if (this.appInfo.version !== '') {
            info = 'version ' + this.appInfo.version;
        }
        if (this.appInfo.environment !== IShellEnvironment.PRODUCTION) {
            if (info !== '') {
                info = info + ' - ' + IShellEnvironment[this.appInfo.environment];
            } else {
                info = IShellEnvironment[this.appInfo.environment];
            }
        }
        if (this.appInfo.lastMod) {
            info = new DatePipe(this.activeLanguage.code).transform(this.appInfo.lastMod, 'dd MMMM yyyy')
                    + ' - ' + info;
        }
        if (!this.appInfo.info) {
            return info;
        }
        return this.appInfo.info + ' - ' + info;
    }

    log(level: LogLevel, message: any, ...optionalParams: any[]): void {
      switch ( level ) {
           case LogLevel.DEBUG:
               this.logger.debug(message, ...optionalParams);
               break;
           case LogLevel.INFO:
               this.logger.info(message, ...optionalParams);
               break;
           case LogLevel.WARN:
               this.logger.warn(message, ...optionalParams);
               break;
           case LogLevel.ERROR:
               this.logger.error(message, ...optionalParams);
               break;
       }
    }

    changeLanguageByCode(code: string) {
        this.activeLanguage = UxEuLanguages.languagesByCode[code];
        this.changeLanguage(this.activeLanguage);
    }


    changeLanguage(lang: UxLanguage) {
        this.translate.use(lang.code);
        this.onLangChange.next({lang: lang.code});
        this.log(LogLevel.DEBUG, 'shellService : changeLanguage', lang);
        this.uxService.activeLanguage = lang;
    }

    get appRouter(): Router {
        return this.router;
    }


    private initEuiApp() {
        switch (this.appModel.shell.layout) {
            case euiShellLayout.SIDEBAR_COLLAPSE:
                this.isSidebarLayout = true;
                this.uxService.isSidebarActive = true;
                this.uxService.isSidebarCollapsible = true;
                this.uxService.isSidebarStateOpen = true;
                this.isHeaderShowLogo = !this.uxService.isSidebarStateOpen;
                break;
            case euiShellLayout.SIDEBAR_COLLAPSE_CLOCK:
                this.isShowClock = true;
                this.isSidebarLayout = true;
                this.uxService.isSidebarActive = true;
                this.uxService.isSidebarCollapsible = true;
                this.uxService.isSidebarStateOpen = true;
                this.isHeaderShowLogo = !this.uxService.isSidebarStateOpen;
                break;
            case euiShellLayout.SIDEBAR_FIXED:
                this.isSidebarLayout = true;
                this.uxService.isSidebarActive = true;
                this.uxService.isSidebarStateOpen = true;
                this.isHeaderShowLogo = !this.uxService.isSidebarStateOpen;
                break;
            case euiShellLayout.SIDEBAR_FIXED_CLOCK:
                this.isShowClock = true;
                this.isSidebarLayout = true;
                this.uxService.isSidebarActive = true;
                this.uxService.isSidebarStateOpen = true;
                this.isHeaderShowLogo = !this.uxService.isSidebarStateOpen;
                break;
            case euiShellLayout.INNERSIDEBAR_COLLAPSE:
                this.isInnerSidebarLayout = true;
                this.uxService.isSidebarActive = true;
                this.uxService.isSidebarCollapsible = true;
                this.uxService.isSidebarStateOpen = true;
                this.isHeaderShowLogo = true;
                break;
             case euiShellLayout.INNERSIDEBAR_COLLAPSE_CLOCK:
                this.isShowClock = true;
                this.isInnerSidebarLayout = true;
                this.uxService.isSidebarActive = true;
                this.uxService.isSidebarCollapsible = true;
                this.uxService.isSidebarStateOpen = true;
                this.isHeaderShowLogo = true;
                break;
            case euiShellLayout.DEFAULT_CONTRACT:
                this.isContractLayout = true;
                this.isHeaderShowLogo = true;
                break;
            case euiShellLayout.DEFAULT_CONTRACT_FULLWIDTH:
                this.isContractLayout = true;
                this.isHeaderShowLogo = true;
                break;
        }
        const euiappinfo = <EuiAppInfo>this.appInfo;
        const missing_lang = euiappinfo.availableLanguages.split(',')
                                                          .filter( l => l !== 'none')
                                                          .filter( l =>
                                    this.translate.getAvailableLanguages().filter( t_l => t_l === l ).length === 0);
        if (missing_lang.length) {
            throw(new Error('The following languages are not available: ' + missing_lang.toString()));
        }
        if (euiappinfo.availableLanguages !== 'none') {
            euiappinfo.defaultLanguage = euiappinfo.availableLanguages.split(',')[0];
            if (euiappinfo.availableLanguages.indexOf(this.activeLanguage.code) < 0) {
                this.log(LogLevel.DEBUG, 'shellService : default language/activelanguage', euiappinfo.defaultLanguage, this.activeLanguage);
                this.changeLanguageByCode(euiappinfo.defaultLanguage);
            }
        }
        if (euiappinfo.sessionKeepAlive > 0) {
            if (this.keepAliveSubscription !== undefined) {
                this.keepAliveSubscription.unsubscribe();
            }
            this.keepAliveSubscription = Observable.interval(euiappinfo.sessionKeepAlive * 60 * 1000)
                .flatMap( () => { return this.http.get('index.html'); })
                .subscribe(
                        response => {
                            this.logger.debug('session keepalive OK', );
                        },
                        error => {
                            this.logger.debug('session keepalive ERROR', error);
                        }
                    );
        }
        this.updateSideBarMenuLinks();
        this.uxService.setSidebarLinks(this.sideBarMenu);
    }

    private setUser( user: IUser ) {
        this.user = {
            firstname : user.name,
            lastname : user.surname,
            email : undefined,
            groups: undefined,
            roles : user.roles
        };
    }

    // Growl helpers
    // -------------
    private growlNotification(notification: INotification) {
        let sev: string = NotificationType[notification.type].toString().toLowerCase();
        if (sev === 'debug') { sev = 'info'; }
        const message: Message = { severity: sev, detail: notification.message, summary: notification.title };
        if (notification.autoCloseDelay === 0) {
            this.growl(message, true );
            return;
        }
        this.growl(message, null, notification.autoCloseDelay * 1000);
    }
    growl(msg: Message, isSticky?: boolean, life?: number) {
        switch (msg.severity) {
            case 'error': {
                msg.severity = 'danger';
                break;
            }
            case 'warn': {
                msg.severity = 'warning';
                break;
            }
        }
        this.uxService.growl(msg, isSticky, true, life);
    }
    // BlockUI helpers
    // ---------------
    blockDocument() {
        this.uxService.blockDocument();
    }
    unblockDocument() {
        this.uxService.unblockDocument();
    }
    // Responsive helpers
    // ------------------

    isMobile(): boolean {
        return this.uxService.isMobile();
    }
    isTablet(): boolean {
        return this.uxService.isTablet();
    }
    isLtDesktop(): boolean {
        return this.uxService.isLtDesktop();
    }
    isDesktop(): boolean {
        return this.uxService.isDesktop();
    }
    isLargeDesktop(): boolean {
        return this.uxService.isLargeDesktop();
    }
}
