import { CommonModule } from '@angular/common';
import { CoreComponentsModule } from './components';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslatePipe } from './utils';


@NgModule({
    declarations: [
        TranslatePipe
    ],
    imports: [
        CommonModule,
        CoreComponentsModule,
        FormsModule,
        RouterModule
    ],
    exports: [
        CoreComponentsModule,
        TranslatePipe,
        RouterModule,
        CommonModule,
        FormsModule
    ]
})
export class OIBCoreModule {

}
