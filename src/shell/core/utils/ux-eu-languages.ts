import { UxLanguage } from './ux-language';
export class UxEuLanguages {
    public static languagesByCode: {[code: string]: UxLanguage} = {
        bg: {
            code: 'bg',
            label: 'български'
        },
        cs: {
            code: 'cs',
            label: 'čeština'
        },
        da: {
            code: 'da',
            label: 'dansk'
        },
        de: {
            code: 'de',
            label: 'Deutsch'
        },
        et: {
            code: 'et',
            label: 'eesti keel'
        },
        el: {
            code: 'el',
            label: 'ελληνικά'
        },
        en: {
            code: 'en',
            label: 'English'
        },
        es: {
            code: 'es',
            label: 'español'
        },
        fr: {
            code: 'fr',
            label: 'français'
        },
        ga: {
            code: 'ga',
            label: 'Gaeilge'
        },
        it: {
            code: 'it',
            label: 'italiano'
        },
        lv: {
            code: 'lv',
            label: 'latviešu valoda'
        },
        lt: {
            code: 'lt',
            label: 'lietuvių kalba'
        },
        hu: {
            code: 'hu',
            label: 'magyar'
        },
        mt: {
            code: 'mt',
            label: 'Malti'
        },
        nl: {
            code: 'nl',
            label: 'Nederlands'
        },
        pl: {
            code: 'pl',
            label: 'polski'
        },
        pt: {
            code: 'pt',
            label: 'português'
        },
        ro: {
            code: 'ro',
            label: 'română'
        },
        sk: {
            code: 'sk',
            label: 'slovenčina'
        },
        sl: {
            code: 'sl',
            label: 'slovenščina'
        },
        fi: {
            code: 'fi',
            label: 'suomi'
        },
        sv: {
            code: 'sv',
            label: 'svenska'
        },
        hr: {
            code: 'hr',
            label: 'hrvatski'
        },
        is: {
            code: 'is',
            label: 'íslenska'
        },
        mk: {
            code: 'mk',
            label: 'македонски'
        },
        no: {
            code: 'no',
            label: 'norsk'
        },
        tr: {
            code: 'tr',
            label: 'türkçe'
        }
    };
    private static defaultCodes: string [] =
                ['bg', 'cs', 'da', 'de', 'et', 'el', 'en', 'es', 'fr', 'ga', 'it', 'lv', 'lt', 'hu',
                 'mt', 'nl', 'pl', 'pt', 'ro', 'sk', 'sl', 'fi', 'sv', 'hr', 'is', 'mk', 'no', 'tr'];

    static getLanguages(codes?: string []): UxLanguage [] {
        if (codes == null || codes.length <= 0) {
            codes = this.defaultCodes;
        }

        const languages: UxLanguage [] = [];
        for (let c = 0; c < codes.length; c++) {
            const language = this.languagesByCode [codes [c]];
            if (language != null) {
                languages.push(language);
            }
        }

        return languages;
    }
}
