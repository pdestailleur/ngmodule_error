import {
    ChangeDetectorRef,
    EventEmitter,
    Inject,
    Injectable,
    OnDestroy,
    Pipe,
    PipeTransform
    } from '@angular/core';
import {
        DefaultTranslationService,
        IPlugin,
        LangChangeEvent,
        PluginawareServiceregisterService,
        SERVICE_PLUGINAWARE_SERVICEREGISTER,
        SERVICE_TRANSLATIONS,
} from './../../../services';

@Injectable()
@Pipe({
    name: 'translate',
    pure: false
})
export class TranslatePipe implements PipeTransform, OnDestroy {
    private translate: DefaultTranslationService;
    private lastKey: string;
    value = '';
    lastParams: any[];
    onLangChange: EventEmitter<LangChangeEvent>;

    constructor(@Inject(SERVICE_PLUGINAWARE_SERVICEREGISTER)private pas: PluginawareServiceregisterService,
                private _ref: ChangeDetectorRef,
                @Inject(SERVICE_TRANSLATIONS) private gl_translate: DefaultTranslationService) {
    }

    private updateValue(key: string, plugin: IPlugin, ...args: any[]): void {

        if (!plugin) {
            this.translate = this.gl_translate;
        } else if (!this.translate) {
            this.translate = this.pas.getService(SERVICE_TRANSLATIONS, plugin);
        }
        this.lastKey = key;
        this.lastParams = args;
        this.value = this.translate.getTranslation(this.lastKey);
        this.lastParams.forEach( param => {
            if ( param === undefined ) {
                return;
            }
            const paramKey = Object.keys(param)[0];
            this.value = this.value.replace( '{' + paramKey + '}', param[paramKey]);
        });
        this._ref.markForCheck();
    }

    transform(query: string, plugin: IPlugin, ...args: any[]): any {
        if (this.equals(query, this.lastKey) && this.equals(args, this.lastParams)) {
            return this.value;
        }

        let interpolateParams: Object;
        if (this.isDefined(args[0]) && args.length) {
            if (typeof args[0] === 'string' && args[0].length) {
                // we accept objects written in the template such as {n:1}, {'n':1}, {n:'v'}
                // which is why we might need to change it to real JSON objects such as {"n":1} or {"n":"v"}
                const validArgs: string = args[0]
                    .replace(/(\')?([a-zA-Z0-9_]+)(\')?(\s)?:/g, '"$2":')
                    .replace(/:(\s)?(\')(.*?)(\')/g, ':"$3"');
                try {
                    interpolateParams = JSON.parse(validArgs);
                } catch (e) {
                    throw new SyntaxError(`Wrong parameter in TranslatePipe. Expected a valid Object, received: ${args[0]}`);
                }
            } else if (typeof args[0] === 'object' && !Array.isArray(args[0])) {
                interpolateParams = args[0];
            }
        }
        this.updateValue(query, plugin, interpolateParams);
        this.dispose();
        if (!this.onLangChange) {
            this.onLangChange = this.gl_translate.onLangChange().subscribe((event: LangChangeEvent) => {
                if (this.lastKey) {
                    this.updateValue(query, plugin, interpolateParams);
                }
            });
        }
        return this.value;
    }

    private dispose(): void {
        if (typeof this.onLangChange !== 'undefined') {
            this.onLangChange.unsubscribe();
            this.onLangChange = undefined;
        }
    }

    ngOnDestroy(): void {
        this.dispose();
    }

    private equals(o1: any, o2: any): boolean {
        if (o1 === o2) { return true; }
        if (o1 === null || o2 === null) { return false; }
        if (o1 !== o1 && o2 !== o2) { return true; } // NaN === NaN
        const t1 = typeof o1, t2 = typeof o2;
        let length: number, key: any, keySet: any;
        if (t1 === t2 && t1 === 'object') {
            if (Array.isArray(o1)) {
                if (!Array.isArray(o2)) { return false; }
                if ((length = o1.length) === o2.length) {
                    for (key = 0; key < length; key++) {
                        if (!this.equals(o1[key], o2[key])) { return false; }
                    }
                    return true;
                }
            } else {
                if (Array.isArray(o2)) {
                    return false;
                }
                keySet = Object.create(null);
                for (key in o1) {
                    if (o1.hasOwnProperty(key)) {
                        if (!this.equals(o1[key], o2[key])) {
                            return false;
                        }
                        keySet[key] = true;
                    }
                }
                for (key in o2) {
                    if (o2.hasOwnProperty(key)) {
                        if (!(key in keySet) && typeof o2[key] !== 'undefined') {
                            return false;
                        }
                    }
                }
                return true;
            }
        }
        return false;
    }

    private isDefined(value: any): boolean {
        return typeof value !== 'undefined' && value !== null;
    }
}
