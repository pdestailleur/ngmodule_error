import {
    Component,
    Input,
    NgZone,
    OnInit
    } from '@angular/core';
import { euiTheme } from './euiApp/shell-app.api';
import { IApplicationModel, IExtendedApplicationModel } from './core';


/**
 * This is the base component that creates the required shell for the application
 * For now only 1 shell is implemented, this could change in the future when other shells are needed.
 * In that case add the logic here based on the the type of the appModel.shell variable
 *
 *            eUi shell --> typeof appModel.shell === euiAppInfo
 *
 */
@Component({
  selector: 'shell-app-template',
  template: `<eui-shell [appModel]="shellModel"></eui-shell>`
})
export class ShellAppTemplateComponent implements OnInit {
  @Input() appModel: IExtendedApplicationModel;
  /**
   * The application model contains only the properties the shell needs to function
   */
  public shellModel: IApplicationModel;

  /**
   * NgZone is needed to call the change detection when the shellModel changes
   */
  constructor(private zone: NgZone) { }

  ngOnInit() {
    this.shellModel = {
      shell: this.appModel.shell,
      menu: this.appModel.menu,
      topBar: this.appModel.topBar,
      help: this.appModel.help
    };
    this.appModel.watcher$.subscribe( (resp: IApplicationModel) => {
        this.zone.run( () => { this.shellModel = resp; });
    });
  }
}
