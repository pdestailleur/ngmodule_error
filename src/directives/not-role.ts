import { ViewContainerRef, TemplateRef, OnInit, OnDestroy, Directive, Input, Inject} from '@angular/core';
import { IGuardian, SERVICE_GUARDIAN } from './../services';
import { IPluginContainer, SERVICE_PLUGIN_CONTAINER } from './../plugins/core/plugin-container';
import { Subscription } from 'rxjs/Rx';

@Directive({ selector: '[notRole]' })
export class NotRoleDirective implements OnInit, OnDestroy {
    @Input('notRole') private notRole: string;

    private subscription: Subscription;

    constructor( private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef,
                 @Inject(SERVICE_PLUGIN_CONTAINER) private pluginContainer: IPluginContainer) {
    }

    ngOnInit() {
        const guardian: IGuardian = <IGuardian>this.pluginContainer.serviceByKey(SERVICE_GUARDIAN);
        if (!guardian || this.subscription) { return; }
        this.subscription = guardian.userHasRole(this.notRole).subscribe(hasIt => {
            this.viewContainer.clear();
            if (!hasIt) {
                this.viewContainer.createEmbeddedView(this.templateRef);
            } else {
                this.viewContainer.clear();
            }
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}
