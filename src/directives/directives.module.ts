import { NgModule } from '@angular/core';
import { RequireRoleDirective } from './require-role';
import { NotRoleDirective } from './not-role';

@NgModule({
    declarations: [RequireRoleDirective, NotRoleDirective],
    exports: [RequireRoleDirective, NotRoleDirective]
})
export class DirectivesModule { }

