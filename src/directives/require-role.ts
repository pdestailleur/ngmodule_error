import { ViewContainerRef, TemplateRef, OnInit, OnDestroy, Directive, Input, Inject} from '@angular/core';
import { IGuardian, SERVICE_GUARDIAN } from './../services';
import { IPluginContainer, SERVICE_PLUGIN_CONTAINER } from './../plugins/core/plugin-container';
import { Subscription } from 'rxjs/Rx';

@Directive({ selector: '[requireRole]' })
export class RequireRoleDirective implements OnInit, OnDestroy {
    @Input('requireRole') private requireRole: string;

    private subscription: Subscription;

    constructor( private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef,
                 @Inject(SERVICE_PLUGIN_CONTAINER) private pluginContainer: IPluginContainer) { }

    ngOnInit() {
        const guardian: IGuardian = <IGuardian>this.pluginContainer.serviceByKey(SERVICE_GUARDIAN);
        if (!guardian || this.subscription) { return; }
        this.subscription = guardian.userHasRole(this.requireRole).subscribe(hasIt => {
            this.viewContainer.clear();
            if (hasIt) {
                this.viewContainer.createEmbeddedView(this.templateRef);
            } else {
                this.viewContainer.clear();
            }
        });
    }

    ngOnDestroy() {
        if (this.subscription) {
            this.subscription.unsubscribe();
        }
    }
}

