import { environment as mainEnvironment } from './environment';

export const environment = {
  production: true,
  environment : 'PRODUCTION',
  version : mainEnvironment.version,
  lastMod : mainEnvironment.lastMod,
  homeUrl : mainEnvironment.homeUrl
};
