import { Component, Inject } from '@angular/core';
import { HomePlugin } from './home.plugin';
import { LogLevel, ShellService } from './../../../shell';
import {
         IGuardian,
         IHttpService,
         INotificationService,
         IRouter,
         ITranslations,
         LangChangeEvent,
         PluginawareServiceregisterService,
         SERVICE_GUARDIAN,
         SERVICE_HTTP,
         SERVICE_NOTIFICATIONS,
         SERVICE_PLUGINAWARE_SERVICEREGISTER,
         SERVICE_ROUTER,
         SERVICE_TRANSLATIONS,
} from './../../../services';


@Component({
    selector: 'home-component',
    template: `<h1>HOME COMPONENT</h1>
    <div class="btn-group">
        <button class="btn btn-secondary btn-info" (click)="onInfo()">Info</button>
        <button class="btn btn-secondary btn-danger" (click)="onError()">Error</button>
        <button class="btn btn-secondary btn-warning" (click)="onWarn()">Warn</button>
        <button class="btn btn-secondary" (click)="navigate()">Navigate by function</button>
        <button class="btn btn-secondary" (click)="httpCall()">do success HttpCall</button>
        <button class="btn btn-secondary" (click)="httpCall(true)">do failed HttpCall</button>
    </div>
    <div>
        <br />
        <a [routerLink]="routerservice.getPluginPath('/another-route-from-home')">Link with directive</a>
    </div>
    <h2>Demo of translation (own translation system)</h2>
    <p>with function: {{hometranslation}}</p>
    <p>with pipe: {{ 'hometranslation' | translate:getPlugin():{test:'param with pipe'} }}</p>
    `
})
export class HomeComponent {
    private translateservice: ITranslations;
    public routerservice: IRouter;

    public hometranslation: String;
    public getPlugin(): any {
        return HomePlugin;
    }

    constructor(private shellService: ShellService, @Inject(SERVICE_GUARDIAN) private guardian: IGuardian,
                @Inject(SERVICE_NOTIFICATIONS) private notifier: INotificationService,
                @Inject(SERVICE_HTTP) private http: IHttpService) {
        this.translateservice = <ITranslations>shellService.getPAService(SERVICE_TRANSLATIONS, HomePlugin);
        this.routerservice = <IRouter>shellService.getPAService(SERVICE_ROUTER, HomePlugin);
        this.shellService.onLangChange.subscribe( (event: LangChangeEvent) => {
            this.hometranslation = this.translateservice.getTranslation('hometranslation',
                        this.translateservice.getCurrentLang() , {test: 'param with function'});
        });
        this.hometranslation = this.translateservice.getTranslation('hometranslation', null , {test: 'param with function'});
    }

    onInfo() {
        this.notifier.info('info message ' + Date.now(), null, 'GENERAL INFO');
    }

    onError() {
        this.notifier.error('error message', 3 , 'ERROR ERROR ERROR');
    }

    onWarn() {
        this.notifier.warn('warn message', 5);
    }

    navigate() {
        this.routerservice.navigateByUrl('another-route-from-home');
    }

    httpCall(fail: boolean = false) {
        let file = 'index.html';
        if (fail) {
            file = 'index2.html';
        }
        this.http.get(file).subscribe(
            response => {
                this.shellService.log (LogLevel.DEBUG, 'http response' );
            },
            error => {
                this.shellService.log( LogLevel.ERROR, 'http error' );
            }
        );
    }

}
