
import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { OIBCoreModule } from './../../../shell';


@NgModule({
    imports: [OIBCoreModule],
    declarations: [HomeComponent]
})
export class HomeModule {

}

