import {
         HttpService,
         IHttpService,
         ILoggerService,
         IRouter,
         PluginawareServiceregisterService,
         SERVICE_LOGGER,
         SERVICE_PLUGINAWARE_SERVICEREGISTER,
         SERVICE_ROUTER,
         SERVICE_TRANSLATIONS,
} from '../../../services';

import { HomeComponent } from './home.component';
import { HomeModule } from './home.module';
import { Http } from '@angular/http';
import { IMenuContribution } from '../../../plugins/core/plugin-decorators';
import { IServiceManager } from '../../../plugins/core/service-manager';
import { OpaqueToken } from '@angular/core';
import { Plugin } from '../../../plugins';
import { Routes } from '@angular/router';

export const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'another-route-from-home', component: HomeComponent },
];

@Plugin({
    id: 'home',
    name: 'HomePlugin',
    modules: [HomeModule],
    routes: appRoutes,
    services: [{
        key: new OpaqueToken('SERVICE_HOME'),
        name: 'Default HomeService',
        instance: getHomeService,
        clazz: HttpService
    }],
    onInit: homePluginOnInit
})
export class HomePlugin {

}

export function getHomeService(serviceManager: IServiceManager, overwrittenService: IHttpService): HttpService {
    const logger: ILoggerService = (<any>serviceManager.service(SERVICE_LOGGER));
    const register: PluginawareServiceregisterService = (<any>serviceManager.service(SERVICE_PLUGINAWARE_SERVICEREGISTER));
    const routerservice: IRouter = register.getService(SERVICE_ROUTER, HomePlugin);
    logger.debug('getting paRouterService', routerservice);
    return new HttpService(<Http>serviceManager.injector.get(Http));
}

export function homePluginOnInit(sm: IServiceManager) {
    const register: PluginawareServiceregisterService = (<any>sm.service(SERVICE_PLUGINAWARE_SERVICEREGISTER));
    register.getService( SERVICE_TRANSLATIONS, HomePlugin ).addTranslations(
        [
            {
                language: 'en',
                translations : {
                    'hometranslation' : 'Current language is English {test}'
                }

            },
            {
                language: 'fr',
                translations : {
                    'hometranslation' : 'La langue actuelle est le français {test}'
                }
            },
            {
                language: 'de',
                translations : {
                    'hometranslation' : 'Aktuelle sprache ist Deutsch {test}'
                }
            },
            {
                language: 'nl',
                translations : {
                    'hometranslation' : 'En we hebben zelfs Nederlands {test}'
                }
            }

        ]
    );
}
