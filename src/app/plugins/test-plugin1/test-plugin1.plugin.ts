import {
         DefaultTranslationsFactory,
         IPlugin,
         PluginawareServiceregisterService,
         SERVICE_PLUGINAWARE_SERVICEREGISTER,
         SERVICE_ROUTER,
         SERVICE_TRANSLATIONS,
} from './../../../services';
import {
         IMenuContribution,
         IObservablePluginParams,
         IServiceManager,
         ITopBarContribution,
         Plugin,
         rewriteRoutePath,
} from './../../../plugins';
import { Observable, Observer } from 'rxjs/Rx';
import { TESTPLUGIN1SERVICE, TestPlugin1Service } from './test-plugin1.service';

import { IMenuSection } from './../../../shell';
import { OpaqueToken } from '@angular/core';
import { Routes } from '@angular/router';
import { TestModule1 } from './test-plugin1.module';
import { TestPlugin1Component } from './test-plugin1.component';

export function getTestPlugin1HomeRoute(): string {
    return rewriteRoutePath('home', undefined, 'TestPlugin1');
}

@Plugin({
    id: 'TestPlugin1',
    name: 'TestPlugin1',
    modules: [TestModule1],
    routes: [
        { path: '', redirectTo: 'home' },
        { path: 'home', component: TestPlugin1Component }
    ],
    services: [
        {
            key: TESTPLUGIN1SERVICE,
            name: 'Testplugin1 Service',
            instance: new TestPlugin1Service()
        }
    ],
    onInit: testPlugin1OnInit,
    watcher$: buildWatcher
})
export class TestPlugin1 {

}

export function testPlugin1OnInit(sm: IServiceManager) {
    const translations = (<any>sm.service(SERVICE_TRANSLATIONS));
    const register: PluginawareServiceregisterService = (<any>sm.service(SERVICE_PLUGINAWARE_SERVICEREGISTER));
    register.getService( SERVICE_TRANSLATIONS, TestPlugin1 ).addTranslations(
        [
            {
                language: 'en',
                translations : {
                    'pluginname' : 'English pluginname',
                    'pluginbody' : 'English pluginbody'
                }

            },
            {
                language: 'fr',
                translations : {
                    'pluginname' : 'Nom du plugin en Français',
                    'pluginbody' : 'Body du plugin en Français'
                }
            },
            {
                language: 'de',
                translations : {
                    'pluginname' : 'Plugin name auf Deutsch',
                    'pluginbody' : 'Plugin body auf Deutsch'
                }
            }
        ]
    );
}

export function buildWatcher(sm: IServiceManager): Observable<IObservablePluginParams> {
    return Observable.create((observer: any) => {
            observer.next(new TestPlugin1PluginParams(sm, observer));
    });
}


export class TestPlugin1PluginParams implements IObservablePluginParams {
    public menuContribution: IMenuContribution;
    public topBarContribution: ITopBarContribution;

    constructor(sm: IServiceManager, parentObserver: any) {
        let mySection: IMenuSection;
        this.getMyMenu().subscribe( resp => {
            mySection = {
                label: 'Test plugins',
                entries: []
            };
            resp.map(
                g => {
                    return {
                        icon: 'fa-group',
                        label: g.name,
                        onAction: getTestPlugin1HomeRoute(),
                    };
                }
            ).forEach( me => {
                mySection.entries.push(me);
            });

            this.menuContribution = {
                sections: [mySection]
            };
            this.topBarContribution = undefined;
            parentObserver.next(this);
        });

        this.getMyBagdeCount().subscribe( resp => {
            mySection.entries.forEach( entry => {
                entry.badgeCount = resp;
            });
            this.menuContribution = {
                sections: [mySection]
            };
            this.topBarContribution = {
                actions: [
                            {
                                badgeCount: resp,
                                onAction: getTestPlugin1HomeRoute(),
                                icon: 'fa-envelope'
                            }
                        ]
            };
            parentObserver.next(this);
        });
    }

    getMyMenu(): Observable<{name: string}[]> {

        return Observable.create((observer: any) => {
            setTimeout(() => {
                observer.next([
                    {name: 'Inbox'},
                    {name: 'Inbox2'},
                    {name: 'Inbox3'}
                ]);
             }, 5000);
            observer.next([
                {name: 'Inbox'}
            ]);
        });
    }

    getMyBagdeCount(): Observable<number> {

        return Observable.create((observer: any) => {
            let i = 0;
            const interval = setInterval(() => {
                if (i === 5) {
                    clearInterval(interval);
                }
                observer.next(i++);
             }, 5000);

        });
    }
}
