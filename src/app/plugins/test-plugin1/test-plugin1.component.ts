import { Component, Inject } from '@angular/core';
import { IGuardian, SERVICE_GUARDIAN, SERVICE_TRANSLATIONS } from './../../../services';
import { Router } from '@angular/router';
import { ShellService } from './../../../shell';
import { TestPlugin1 } from './test-plugin1.plugin';

@Component({
    selector: 'test-plugin1-component',
    template: `
        **** Biribiribir ***
    `
})
export class TestPlugin1Component {

    private pluginTranslationService: any;

    constructor(private router: Router, private shellService: ShellService) {
        this.pluginTranslationService = this.shellService.getPAService(SERVICE_TRANSLATIONS, TestPlugin1);
        if (this.pluginTranslationService) {
            // tslint:disable-next-line:no-console
            console.debug(this.pluginTranslationService.getAllTranslations('en'),
                          this.pluginTranslationService.getAllTranslations('fr'),
                          this.pluginTranslationService.getAllTranslations('de'));
        } else {
            // tslint:disable-next-line:no-console
            console.debug('ERROR: ' + SERVICE_TRANSLATIONS.toString() + ' is not registered as pluginAware!');
        }
    }
}
