import { OIBCoreModule } from './../../../shell';
import { NgModule } from '@angular/core';
import { TestPlugin1Component } from './test-plugin1.component';


@NgModule({
    imports: [OIBCoreModule],
    declarations: [TestPlugin1Component]
})
export class TestModule1 {

}

