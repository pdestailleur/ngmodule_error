import { EventEmitter, OpaqueToken } from '@angular/core';
import { Observable } from 'rxjs/Rx';

export const TESTPLUGIN1SERVICE: OpaqueToken = new OpaqueToken('testplugin1service');

export class TestPlugin1Service  {

    shout(msg: String) {
        // tslint:disable-next-line:no-console
        console.debug(msg);
    }

}
