import { Component, Inject } from '@angular/core';
import {
    IGuardian,
    IGuardianFactory,
    SERVICE_GUARDIAN,
    SERVICE_TRANSLATIONS
    } from '../../../services';
import { LogLevel, ShellService } from './../../../shell';
import { Router } from '@angular/router';
import { TestPlugin2 } from './test-plugin2.plugin';

@Component({
    selector: 'test-plugin2-component',
    template: `**** Plugin 2 *** `
})
export class TestPlugin2Component {

    private pluginTranslationService: any;

    constructor(private router: Router, private shellService: ShellService) {
        this.pluginTranslationService = this.shellService.getPAService(SERVICE_TRANSLATIONS, TestPlugin2);
        if (this.pluginTranslationService) {
            this.shellService.log( LogLevel.DEBUG, this.pluginTranslationService.getAllTranslations('en'),
                                                   this.pluginTranslationService.getAllTranslations('fr'),
                                                   this.pluginTranslationService.getAllTranslations('de'));
        } else {
            this.shellService.log( LogLevel.ERROR, SERVICE_TRANSLATIONS.toString() + ' is not registered as pluginAware!');
        }

    }

}
