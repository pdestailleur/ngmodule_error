import { SERVICE_PLUGINAWARE_SERVICEREGISTER, PluginawareServiceregisterService,
         SERVICE_TRANSLATIONS, SERVICE_ROUTER, DefaultTranslationsFactory, IPlugin } from './../../../services';
import { Routes } from '@angular/router';
import { TestModule2 } from './test-plugin2.module';
import { TestPlugin2Component } from './test-plugin2.component';
import { Plugin } from '../../../plugins';
import { OpaqueToken } from '@angular/core';
import { IMenuContribution, IServiceManager, rewriteRoutePath } from '../../../plugins';


export function getTestPlugin2HomeRoute(): string {
    return rewriteRoutePath('home', undefined, 'TestPlugin2');
}

@Plugin({
    id: 'TestPlugin2',
    name: 'TestPlugin2',
    menuContribution: buildMenu,
    topBarContribution: {
        actions: [
            {
                badgeCount: 1,
                onAction: getTestPlugin2HomeRoute(),
                icon: 'fa-info'
            }
        ],
        sections: [
            {
                label: 'Plugins',
                entries: [
                    { label: 'Outbox', icon: 'fa-envelope', onAction: getTestPlugin2HomeRoute(), badgeCount: 1 }
                ]
            }
        ]
    },
    modules: [TestModule2],
    routes: [
        { path: '', redirectTo: 'home' },
        { path: 'home', component: TestPlugin2Component }
    ],
    services: [
        {
            key: new OpaqueToken('service'),
            name: 'My test service',
            instance: 'Service instance'
        }
    ],
    onInit: testPlugin2OnInit
})
export class TestPlugin2 {

}

export function testPlugin2OnInit(sm: IServiceManager) {
    const translations = (<any>sm.service(SERVICE_TRANSLATIONS));
    const register: PluginawareServiceregisterService = (<any>sm.service(SERVICE_PLUGINAWARE_SERVICEREGISTER));
    register.getService( SERVICE_TRANSLATIONS, TestPlugin2 ).addTranslations(
        [
            {
                language: 'en',
                translations : {
                    'pluginname' : 'English pluginname 2',
                    'pluginbody' : 'English pluginbody 2'
                }

            },
            {
                language: 'fr',
                translations : {
                    'pluginname' : 'Nom du plugin en Français 2',
                    'pluginbody' : 'Body du plugin en Français 2'
                }
            }
        ]
    );
}


export function buildMenu(sm: IServiceManager): IMenuContribution {
    return {
        sections: [
            {
                label: 'Test plugins',
                entries: [
                    { label: 'Outbox', icon: 'fa-envelope', onAction: getTestPlugin2HomeRoute(), badgeCount: 1 }
                ]
            }
        ]
    };
}

