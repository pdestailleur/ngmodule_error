import { OIBCoreModule } from './../../../shell';
import { NgModule } from '@angular/core';
import { TestPlugin2Component } from './test-plugin2.component';


@NgModule({
    imports: [OIBCoreModule],
    declarations: [TestPlugin2Component]
})
export class TestModule2 {

}

