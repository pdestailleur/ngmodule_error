import { Application, OIBAppComponent } from './../plugins';
import { environment } from '../environments/environment';
import {
    EuiAppInfo,
    euiShellLayout,
    IShellEnvironment,
    LogLevel
    } from '../shell';
import { euiTheme } from '../shell/euiApp/shell-app.api';
import { HomePlugin, TestPlugin1, TestPlugin2 } from './plugins';
import { ModuleWithProviders, NgModule } from '@angular/core';


const config: EuiAppInfo = new EuiAppInfo();

config.appFullName = 'Demo Shell Application';
config.appShortName = 'Demo App';
config.availableLanguages = 'fr,en,de';
config.defaultLanguage = 'fr';
config.layout = euiShellLayout.INNERSIDEBAR_COLLAPSE;
config.version = environment.version;
config.lastMod = environment.lastMod;
config.environment = IShellEnvironment[<string>environment.environment];
config.homeUrl = environment.homeUrl;
config.log = true;
config.logLevel = LogLevel.DEBUG;

export function buildPlugins(): Function[] {
    const plugins: Function[] =  [
      HomePlugin, TestPlugin1, TestPlugin2
    ];
    return plugins;
}

@Application({
  shell: config,
  plugins: buildPlugins()
})
export class AppApplication {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppApplication
    };
  }
 }

@NgModule({
  imports: [
    AppApplication.forRoot()
  ],
  bootstrap: [OIBAppComponent]
})
export class AppModule { }

