import { PipeTransform } from '@angular/core';
export declare class UxFilterPipe implements PipeTransform {
    transform(value: any, args: any): any;
}
export declare class UxFilterPipeModule {
}
