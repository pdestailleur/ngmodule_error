import { PipeTransform } from '@angular/core';
export declare class UxCurrencyPipe implements PipeTransform {
    protected decimalSeparator: string;
    protected thousandsSeparator: string;
    transform(value: number | string, fractionSize?: number): string;
}
export declare class UxCurrencyPipeModule {
}
