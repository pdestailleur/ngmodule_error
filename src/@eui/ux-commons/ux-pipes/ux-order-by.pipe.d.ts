import { PipeTransform } from '@angular/core';
export declare class UxOrderByPipe implements PipeTransform {
    static _orderByComparator(a: any, b: any): number;
    transform(input: any, [config]: [string]): any;
}
export declare class UxOrderByPipeModule {
}
