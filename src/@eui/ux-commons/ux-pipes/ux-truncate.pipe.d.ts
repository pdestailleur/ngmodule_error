import { PipeTransform } from '@angular/core';
export declare class UxTruncatePipe implements PipeTransform {
    transform(value: string, limit?: number, trail?: String): string;
}
export declare class UxTruncatePipeModule {
}
