import { AfterContentInit, QueryList } from '@angular/core';
import { UxTreeListComponent } from './ux-tree-list.component';
import { UxLink } from '../../shared/models/ux-link.model';
import { UxService } from '../../shared/ux.service';
export declare class UxTreeListItemComponent implements AfterContentInit {
    private uxService;
    styleClass: string;
    headerStyleClass: string;
    id: string;
    label: string;
    linkUrl: string;
    subLabel: string;
    iconClass: string;
    active: boolean;
    expanded: boolean;
    alwaysExpanded: boolean;
    url: string;
    isDisplaySubLinksOnHover: boolean;
    subLinks: UxLink[];
    typeClass: string;
    markerTypeClass: string;
    isVisible: boolean;
    isHovered: boolean;
    treeListComponent: UxTreeListComponent;
    hasSub: boolean;
    hasCustomContent: boolean;
    subTreeList: QueryList<UxTreeListComponent>;
    customSubLabel: QueryList<UxTreeListItemSubLabelTagDirective>;
    customContent: QueryList<UxTreeListItemCustomContentTagDirective>;
    customDetailContent: QueryList<UxTreeListItemDetailsContentTagDirective>;
    customSubContainerContent: QueryList<UxTreeListItemSubContainerContentTagDirective>;
    constructor(treeListComponent: UxTreeListComponent, uxService: UxService);
    ngAfterContentInit(): void;
    toggle(event: Event): void;
    navigateToLink(event: any): void;
    setVisibleState(state: boolean): void;
    setExpandedState(state: boolean): void;
}
export declare class UxTreeListItemCustomContentTagDirective {
}
export declare class UxTreeListItemRightContentTagDirective {
}
export declare class UxTreeListItemSubLabelTagDirective {
}
export declare class UxTreeListItemDetailsContentTagDirective {
}
export declare class UxTreeListItemSubContainerContentTagDirective {
}
export declare class UxTreeListItemComponentModule {
}
