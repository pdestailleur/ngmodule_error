import { EventEmitter, QueryList } from '@angular/core';
import { UxTreeListItemComponent } from './ux-tree-list-item.component';
export declare class UxTreeListComponent {
    styleClass: string;
    isShowToolbar: boolean;
    isShowToolbarToggle: boolean;
    filterLabel: string;
    expandAllLabel: string;
    collapseAllLabel: string;
    hasItemsUrl: boolean;
    hasItemsBullet: boolean;
    itemSelected: EventEmitter<string>;
    items: QueryList<UxTreeListItemComponent>;
    onExpandAll(event: any): void;
    onCollapseAll(event: any): void;
    onFilter(filterValue: string): void;
    setVisibleState(state: boolean): void;
    setExpandedState(state: boolean): void;
    private filterMatched(label, filterValue);
}
export declare class UxTreeListToolbarContentTagDirective {
}
export declare class UxTreeListComponentModule {
}
