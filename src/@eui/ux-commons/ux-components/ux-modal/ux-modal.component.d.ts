import { EventEmitter, QueryList, AfterContentInit, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { UxService } from '../../shared/ux.service';
export declare class UxModalComponent implements AfterContentInit {
    uxService: UxService;
    id: string;
    styleClass: string;
    title: string;
    dismissActionLabel: string;
    dismissActionDisabled: boolean;
    acceptActionLabel: string;
    acceptActionDisabled: boolean;
    isFooterVisible: boolean;
    isVisible: boolean;
    isKeepBodyScroll: boolean;
    isSizeSmall: boolean;
    isSizeLarge: boolean;
    isSizeFullScreen: boolean;
    isSizeFullHeight: boolean;
    isSizeMediumHeight: boolean;
    isShowActionIcons: boolean;
    acceptIconClass: string;
    dismissIconClass: string;
    hasNoBodyPadding: boolean;
    customWidth: string;
    isFooterCustomAlignment: boolean;
    onDismiss: EventEmitter<any>;
    onAccept: EventEmitter<any>;
    vcCloseButton: ElementRef;
    subscription: Subscription;
    customFooterContent: QueryList<UxModalFooterTagDirective>;
    stateClasses: string;
    constructor(uxService: UxService);
    readonly isModalOpen: boolean;
    ngAfterContentInit(): void;
    close(event: any): void;
    closeModalOnEscape(event: any): void;
    onAcceptActionClick(event: any): void;
    onDismissActionClick(event: any): void;
}
export declare class UxModalBodyTagDirective {
}
export declare class UxModalFooterTagDirective {
}
export declare class UxModalComponentModule {
}
