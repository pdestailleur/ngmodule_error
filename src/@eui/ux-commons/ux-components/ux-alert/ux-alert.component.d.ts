import { AfterContentInit } from '@angular/core';
export declare class UxAlertComponent implements AfterContentInit {
    styleClass: string;
    typeClass: string;
    iconClass: string;
    isMuted: boolean;
    contentClass: string;
    ngAfterContentInit(): void;
}
export declare class UxAlertComponentModule {
}
