export declare class UxBadgeGroupComponent {
    styleClass: string;
    typeClass: string;
    isSmall: boolean;
    isLarge: boolean;
    isPill: boolean;
    label: string;
    subLabel: string;
    constructor();
}
export declare class UxBadgeGroupComponentModule {
}
