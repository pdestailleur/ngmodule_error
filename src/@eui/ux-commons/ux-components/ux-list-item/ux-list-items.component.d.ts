import { EventEmitter } from '@angular/core';
import { UxLink } from '../../shared/models/ux-link.model';
export declare class UxListItemsComponent {
    title: string;
    titleClass: string;
    expanded: boolean;
    links: Array<UxLink>;
    isSmall: boolean;
    isLarge: boolean;
    listItemClick: EventEmitter<UxLink>;
    onListItemClick(item: UxLink): void;
}
export declare class UxListItemsComponentModule {
}
