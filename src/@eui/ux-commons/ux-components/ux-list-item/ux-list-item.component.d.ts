import { EventEmitter, OnInit, QueryList } from '@angular/core';
import { UxLink } from '../../shared/models/ux-link.model';
export declare class UxListItemComponent implements OnInit {
    item: UxLink;
    styleClass: string;
    id: string;
    label: string;
    subLabel: string;
    iconClass: string;
    iconTypeClass: string;
    hasIconBg: boolean;
    hasMarker: boolean;
    markerTypeClass: string;
    tagTypeClass: string;
    tagLabel: string;
    tagCount: string;
    isTagRounded: boolean;
    isLarge: boolean;
    isSmall: boolean;
    typeClass: string;
    isExpanded: boolean;
    listItemClick: EventEmitter<UxLink>;
    customContent: QueryList<UxListItemContentTagDirective>;
    customSubLabel: QueryList<UxListItemSubLabelTagDirective>;
    isHovered: boolean;
    ngOnInit(): void;
    readonly hasTag: boolean;
    onClick(): void;
    onMouseenter(): void;
    onMouseleave(): void;
}
export declare class UxListItemSubLabelTagDirective {
}
export declare class UxListItemContentTagDirective {
}
export declare class UxListItemComponentModule {
}
