import { OnInit } from '@angular/core';
import { UxService } from '../../shared/ux.service';
export declare class UxDashboardCardComponent implements OnInit {
    private uxService;
    styleClass: string;
    typeClass: string;
    iconClass: string;
    imageUrl: string;
    imageWidth: string;
    imageHeight: string;
    isIconCompact: boolean;
    label: string;
    subLabel: string;
    urlExternal: string;
    url: string;
    buttonLinkLabel: string;
    buttonStyleClass: string;
    isClickable: boolean;
    isHorizontalLayout: boolean;
    constructor(uxService: UxService);
    ngOnInit(): void;
    readonly stateTypeClass: string;
    onKeyDown(keyCode: number): void;
    onClick(event: any): void;
    openLink(): void;
}
export declare class UxDashboardCardComponentModule {
}
