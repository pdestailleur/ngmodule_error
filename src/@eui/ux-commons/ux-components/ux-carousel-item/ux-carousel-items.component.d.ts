import { QueryList, AfterContentInit } from '@angular/core';
import { UxCarouselItemComponent } from './ux-carousel-item.component';
export declare class UxCarouselItemsComponent implements AfterContentInit {
    isShowSummaryIndicators: boolean;
    items: QueryList<UxCarouselItemComponent>;
    itemsArray: Array<UxCarouselItemComponent>;
    itemsLength: number;
    currentItem: number;
    ngAfterContentInit(): void;
    selectItem(item: UxCarouselItemComponent): void;
    gotoItem(itemIndex: number): void;
    readonly isFirstItemActive: boolean;
    readonly isLastItemActive: boolean;
    goPrevious(event: any): void;
    goNext(event: any): void;
}
export declare class UxCarouselItemsComponentModule {
}
