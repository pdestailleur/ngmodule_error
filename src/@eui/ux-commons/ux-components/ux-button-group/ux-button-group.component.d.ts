import { EventEmitter, QueryList, AfterContentInit } from '@angular/core';
import { UxLink } from '../../shared/models/ux-link.model';
import { UxButtonGroupItemComponent } from './ux-button-group-item.component';
export declare class UxButtonGroupComponent implements AfterContentInit {
    styleClass: string;
    typeClass: string;
    links: UxLink[];
    isCheckboxButtons: boolean;
    isRadioButtons: boolean;
    hasPairedIcon: boolean;
    clicked: EventEmitter<UxLink>;
    items: QueryList<UxButtonGroupItemComponent>;
    constructor();
    ngAfterContentInit(): void;
    onClick(link: UxLink, event: Event): void;
}
export declare class UxButtonGroupComponentModule {
}
