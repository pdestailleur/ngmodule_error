export declare class UxButtonGroupItemComponent {
    id: string;
    label: string;
    iconClass: string;
    isActive: boolean;
    typeClass: string;
}
export declare class UxButtonGroupItemComponentModule {
}
