import { QueryList, AfterContentInit } from '@angular/core';
import { UxPieChartComponent } from '../ux-pie-chart/ux-pie-chart.component';
import { UxService } from '../../shared/ux.service';
export declare class UxPieChartGroupComponent implements AfterContentInit {
    private uxService;
    itemsArray: UxPieChartComponent[];
    isMobile: boolean;
    items: QueryList<UxPieChartComponent>;
    constructor(uxService: UxService);
    ngAfterContentInit(): void;
}
export declare class UxPieChartGroupComponentModule {
}
