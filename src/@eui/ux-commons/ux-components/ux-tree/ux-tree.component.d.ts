import { EventEmitter, TemplateRef, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { UxLink } from '../../shared/models/ux-link.model';
import { UxService } from '../../shared/ux.service';
export declare class UxTreeNodeComponent {
    private router;
    private uxService;
    node: UxLink;
    isRootNode: boolean;
    expandedIconClass: string;
    collapsedIconClass: string;
    templateVariable: TemplateRef<any>;
    nodeClick: EventEmitter<UxLink>;
    constructor(router: Router, uxService: UxService);
    onNodeKeydown(event: any, uxLink: UxLink): void;
    onNodeClick(event: Event, uxLink: UxLink): void;
    onMetadataNodeToggle(event: Event, uxLink: UxLink): void;
    onSubNodeClick(uxLink: UxLink): void;
}
export declare class UxTreeComponent implements AfterContentInit {
    nodes: UxLink[];
    isShowToolbar: boolean;
    collapseAllLabel: string;
    expandAllLabel: string;
    filterLabel: string;
    isExpanded: boolean;
    expandedIconClass: string;
    collapsedIconClass: string;
    nodeClick: EventEmitter<UxLink>;
    templateVariable: TemplateRef<any>;
    ngAfterContentInit(): void;
    onNodeClick(uxLink: UxLink): void;
    onExpandAll(event: Event): void;
    onCollapseAll(event: Event): void;
    onFilter(filterValue: string): void;
    private isFilterMatched(node, filterValue);
    private setNodesExpandedState(expanded);
    private setNodesVisibleState(visible);
}
export declare class UxTreeComponentModule {
}
