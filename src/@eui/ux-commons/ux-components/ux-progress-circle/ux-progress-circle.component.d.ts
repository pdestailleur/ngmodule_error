import { AfterContentInit } from '@angular/core';
export declare class UxProgressCircleComponent implements AfterContentInit {
    styleClass: string;
    value: number;
    emptyLabel: string;
    isDefaultColorSteps: boolean;
    colorSteps: string;
    colorType: string;
    isSmall: boolean;
    isLarge: boolean;
    iconLabelClass: string;
    iconLabelStyleClass: string;
    stateColorNumberClass: string;
    labelValue: string;
    roundedValue: number;
    ngAfterContentInit(): void;
}
export declare class UxProgressCircleComponentModule {
}
