import { UxLink } from '../../shared/models/ux-link.model';
export declare class UxActionBoxItem {
    label: string;
    actions: UxLink[];
    constructor(values?: Object);
}
