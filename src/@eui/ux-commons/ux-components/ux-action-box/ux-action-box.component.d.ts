import { UxLink } from '../../shared/models/ux-link.model';
import { UxActionBoxItem } from './ux-action-box-item';
export declare class UxActionBoxComponent {
    title: string;
    items: UxActionBoxItem[];
    isCollapsible: boolean;
    isExpanded: boolean;
    constructor();
    readonly stateCollapsedClass: string;
    onItemClick(link: UxLink): void;
    onToggle(): void;
}
export declare class UxActionBoxComponentModule {
}
