import { AfterContentInit, QueryList } from '@angular/core';
import { UxPanelComponent } from './ux-panel.component';
import { UxLink } from '../../shared/models/ux-link.model';
export declare class UxPanelsComponent implements AfterContentInit {
    isShowToolbar: boolean;
    isShowToolbarExtendedPanelsToggle: boolean;
    openAllPanelsLabel: string;
    closeAllPanelsLabel: string;
    filterLabel: string;
    goToPanelsLabel: string;
    isMultipleExpanded: boolean;
    isAllPanelsExpandable: boolean;
    isDebug: boolean;
    panelsFilter: string;
    panels: QueryList<UxPanelComponent>;
    extendedPanelsLinks: UxLink[];
    ngAfterContentInit(): void;
    selectPanel(index: number): void;
    collapseAll(item: UxPanelComponent): void;
    openAllPanels(): void;
    closeAllPanels(): void;
    filterPanels(): void;
}
export declare class UxPanelsComponentModule {
}
