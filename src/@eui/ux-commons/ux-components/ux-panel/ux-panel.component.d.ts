import { AfterContentInit, QueryList, ElementRef } from '@angular/core';
import { UxPanelsComponent } from './ux-panels.component';
export declare class UxPanelComponent implements AfterContentInit {
    id: string;
    styleClass: string;
    contentStyleClass: string;
    headerStyleClass: string;
    label: string;
    subLabel: string;
    labelInfos: string;
    iconClass: string;
    iconTypeClass: string;
    isIconRounded: boolean;
    isExpandable: boolean;
    isExpanded: boolean;
    isVisible: boolean;
    isSelected: boolean;
    tagLabel: string;
    tagTypeClass: string;
    tagCount: string;
    badgeLabel: string;
    badgeTypeClass: string;
    hasTabs: boolean;
    hasNoHeader: boolean;
    hasNoContentPadding: boolean;
    isBlocked: boolean;
    maxHeight: string;
    contentHeight: string;
    isFlat: boolean;
    isLargeHeader: boolean;
    isLargeFooter: boolean;
    hasStatusIndicator: boolean;
    statusIndicatorTypeClass: string;
    statusIndicatorMessageLabel: string;
    isTogglableStatusIndicatorContent: boolean;
    isStatusIndicatorContentExpanded: boolean;
    hasStatusIndicatorHeaderBg: boolean;
    hasStatusIndicatorHeaderIcon: boolean;
    isEditModeActive: boolean;
    isContainerOnly: boolean;
    isClickable: boolean;
    actualMaxHeight: string;
    uxPanelsComponent: UxPanelsComponent;
    statusIndicatorIconClass: string;
    customPanelHeader: QueryList<UxPanelHeaderTagDirective>;
    customPanelFooter: QueryList<UxPanelFooterTagDirective>;
    customPanelHeaderRightContent: QueryList<UxPanelHeaderRightContentTagDirective>;
    customPanelHeaderWithDescendants: QueryList<UxPanelHeaderWithDescendantsTagDirective>;
    customPanelFooterWithDescendants: QueryList<UxPanelFooterWithDescendantsTagDirective>;
    customPanelHeaderRightContentWithDescendants: QueryList<UxPanelHeaderRightContentWithDescendantsTagDirective>;
    customStatusIndicatorContent: QueryList<UxPanelHeaderStatusIndicatorContentTagDirective>;
    HTMLElement: HTMLElement;
    hasMaxHeight: boolean;
    stateExpandableClass: string;
    stateTabsClass: string;
    statePanelItemClasses: string;
    statePanelHeaderClasses: string;
    statePanelContentClasses: string;
    constructor(uxPanelsComponent: UxPanelsComponent, el: ElementRef);
    ngAfterContentInit(): void;
    readonly statePanelItemClassesDynamic: string;
    readonly statePanelContentClassesDynamic: string;
    readonly hasCustomPanelHeader: boolean;
    readonly hasCustomPanelFooter: boolean;
    readonly hasCustomPanelHeaderRightContent: boolean;
    readonly hasCustomPanelHeaderWithDescedants: boolean;
    readonly hasCustomPanelFooterWithDescedants: boolean;
    readonly hasCustomPanelHeaderRightContentWithDescedants: boolean;
    toggle(event: Event): void;
    toggleMaxHeight(event: Event): void;
    readonly isMaxHeightExpanded: boolean;
    onToggleStatusIndicatorContent(): void;
}
export declare class UxPanelHeaderTagDirective {
}
export declare class UxPanelFooterTagDirective {
}
export declare class UxPanelHeaderRightContentTagDirective {
}
export declare class UxPanelHeaderWithDescendantsTagDirective {
}
export declare class UxPanelFooterWithDescendantsTagDirective {
}
export declare class UxPanelHeaderRightContentWithDescendantsTagDirective {
}
export declare class UxPanelHeaderStatusIndicatorContentTagDirective {
}
export declare class UxPanelComponentModule {
}
