export declare class UxPanel {
    id: string;
    label: string;
    subLabel: string;
    iconClass: string;
    iconTypeClass: string;
    isExpandable: boolean;
    isExpanded: boolean;
    hasTag: boolean;
    tagTypeClass: string;
    tagLabel: string;
    tagCount: string;
    constructor(values?: Object);
}
