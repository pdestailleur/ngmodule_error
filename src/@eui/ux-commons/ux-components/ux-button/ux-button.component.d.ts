import { AfterContentInit } from '@angular/core';
export declare class UxButtonComponent implements AfterContentInit {
    type: string;
    styleClass: string;
    typeClass: string;
    iconClass: string;
    isOutline: boolean;
    isSecondary: boolean;
    isSmall: boolean;
    isLarge: boolean;
    isBlock: boolean;
    isDisabled: boolean;
    btnTypeClass: string;
    btnSizeClass: string;
    ngAfterContentInit(): void;
}
export declare class UxButtonComponentModule {
}
