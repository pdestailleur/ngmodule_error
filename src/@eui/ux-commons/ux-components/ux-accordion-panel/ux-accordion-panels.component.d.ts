import { QueryList, EventEmitter } from '@angular/core';
import { UxAccordionPanelComponent } from './ux-accordion-panel.component';
export declare class UxAccordionPanelsComponent {
    items: QueryList<UxAccordionPanelComponent>;
    selectPanel: EventEmitter<any>;
    collapseAll(item: UxAccordionPanelComponent): void;
}
export declare class UxAccordionPanelsComponentModule {
}
