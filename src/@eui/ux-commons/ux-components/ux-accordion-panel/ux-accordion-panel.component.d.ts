import { UxAccordionPanelsComponent } from './ux-accordion-panels.component';
export declare class UxAccordionPanelComponent {
    id: string;
    label: string;
    isExpanded: boolean;
    private uxAccordionPanelsComponent;
    constructor(uxAccordionPanelsComponent: UxAccordionPanelsComponent);
    toggle(): void;
    readonly stateAccordionExpandedClass: string;
}
export declare class UxAccordionPanelComponentModule {
}
