import { AfterContentInit, AfterViewInit, ElementRef, Renderer2 } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { UxTimebarItem } from './ux-timebar-item.model';
import { UxService } from '../../shared/ux.service';
export declare class UxTimebarItemUI {
    perc: number;
    stepTypeClass: string;
    tooltipColor: string;
    isGrouped?: boolean;
    groupIndex?: number;
    groupLabel?: string;
    groupLabelMobile?: string;
    groupEndDate?: Date;
    item: UxTimebarItem;
}
export declare class UxTimebarComponent implements AfterContentInit, AfterViewInit {
    private uxService;
    private renderer2;
    items: UxTimebarItem[];
    startLabel: string;
    endLabel: string;
    dateFormat: string;
    isShowLegend: boolean;
    isShowLegendAsIndex: boolean;
    isShowCurrentDateMarker: boolean;
    isGroupOverlappingLabels: boolean;
    container: ElementRef;
    itemsUI: UxTimebarItemUI[];
    currentDate: Date;
    currentPerc: number;
    timebarColumnClass: string;
    subscription: Subscription;
    isShowLegendGenerated: boolean;
    isShowLegendAsIndexGenerated: boolean;
    isGroupOverlappingLabelsGenerated: boolean;
    isMobile: boolean;
    isSomeStepsAreGrouped: boolean;
    extraTimelineLabelSpace: number;
    protected maxStepWidth: number;
    constructor(uxService: UxService, renderer2: Renderer2);
    ngAfterContentInit(): void;
    ngAfterViewInit(): void;
    onBreakpointChange(bkp: string): void;
    protected removeNullItems(): void;
    protected sortItems(): void;
    protected groupOverlappingLabels(): void;
    protected clearGrouping(): void;
    protected calculateExtraTimelineLabelSpace(): void;
}
export declare class UxTimebarComponentModule {
}
