export declare class UxTimebarItem {
    date: Date;
    label: string;
    stepType: string;
    constructor(values?: Object);
}
