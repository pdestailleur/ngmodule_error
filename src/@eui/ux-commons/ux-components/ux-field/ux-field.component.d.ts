import { ElementRef, AfterContentInit } from '@angular/core';
import { FormControl, ControlValueAccessor } from '@angular/forms';
import { UxFieldsetComponent } from '../ux-fieldset/ux-fieldset.component';
export declare class UxFieldComponent implements ControlValueAccessor, AfterContentInit {
    uxContentPlaceholder: ElementRef;
    label: string;
    isVertical: boolean;
    isRequired: boolean;
    feedbackTypeClass: string;
    labelWidthClass: string;
    inputWidthClass: string;
    isReadOnly: boolean;
    styleClass: string;
    formControl: FormControl;
    validationErrorMessage: string;
    uxFieldsetComponent: UxFieldsetComponent;
    stateClass: string;
    private _inputValue;
    constructor(uxFieldsetComponent: UxFieldsetComponent);
    hasCustomContent(): boolean;
    ngAfterContentInit(): void;
    readonly isFieldInvalid: boolean;
    propagateChange: any;
    validateFn: any;
    onModelChange: Function;
    onModelTouched: Function;
    inputValue: string;
    onChange(value: any): void;
    registerOnChange(fn: Function): void;
    registerOnTouched(fn: Function): void;
    writeValue(value: any): void;
    validate(c: FormControl): any;
}
export declare class UxFieldComponentModule {
}
