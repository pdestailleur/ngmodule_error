import { ElementRef, AfterContentInit } from '@angular/core';
import { UxFieldComponent } from './ux-field.component';
import { UxFieldsetComponent } from '../ux-fieldset/ux-fieldset.component';
import { FormControl } from '@angular/forms';
export declare class UxFieldCalendarComponent extends UxFieldComponent implements AfterContentInit {
    private el;
    label: string;
    isVertical: boolean;
    isRequired: boolean;
    feedbackType: string;
    labelWidthClass: string;
    inputWidthClass: string;
    isReadOnly: boolean;
    styleClass: string;
    formControl: FormControl;
    validationErrorMessage: string;
    constructor(uxFieldsetComponent: UxFieldsetComponent, el: ElementRef);
    ngAfterContentInit(): void;
    onCalendarClick(event: Event): void;
}
export declare class UxFieldCalendarComponentModule {
}
