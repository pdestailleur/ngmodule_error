import { AfterContentInit, QueryList, EventEmitter } from '@angular/core';
import { UxService } from '../../shared/ux.service';
import { UxLink } from '../../shared/models/ux-link.model';
import { UxLanguage } from '../ux-language-selector/ux-language';
export declare class UxLayoutAppShellComponent implements AfterContentInit {
    uxService: UxService;
    styleClass: string;
    appHomeUrl: string;
    themeClass: string;
    isContract: boolean;
    isContractMedium: boolean;
    isContractSmall: boolean;
    hasHeader: boolean;
    headerAppFullName: string;
    headerAppShortName: string;
    headerAppSubtitle: string;
    headerUserInfos: string;
    hasHeaderUserProfile: boolean;
    hasHeaderProfileAvatar: boolean;
    headerUserProfileLinks: UxLink[];
    isHeaderHideLogo: boolean;
    languageCodes: string;
    headerEnvLabel: string;
    headerLanguageCodes: string;
    headerLanguageChanged: EventEmitter<UxLanguage>;
    navBarTopMenuLinks: UxLink[];
    navBarNotificationLinks: UxLink[];
    isNavBarMuted: boolean;
    navBarSidebarToggleLabel: string;
    navBarTopMenuItemClicked: EventEmitter<UxLink>;
    navbarNotificationsRefreshClick: EventEmitter<any>;
    navbarNotificationsViewAllClick: EventEmitter<any>;
    navbarNotificationsItemClick: EventEmitter<UxLink>;
    navbarNotificationsItemMarkAsRead: EventEmitter<UxLink>;
    navbarNotificationsItemMarkAsUnRead: EventEmitter<UxLink>;
    footerAppVersion: string;
    footerAppReleaseDate: string;
    hasSidebar: boolean;
    isSidebarInner: boolean;
    sidebarLinks: UxLink[];
    sidebarTitle: string;
    isSidebarShowLogo: boolean;
    isSidebarAlwaysVisible: boolean;
    isSidebarCollapsible: boolean;
    isSidebarStateOpen: boolean;
    isSidebarLargeItems: boolean;
    isSidebarStateCloseWithIcons: boolean;
    isSidebarMobileVisible: boolean;
    sidebarTheme: string;
    appClasses: string;
    customSidebarContent: QueryList<UxAppShellSidebarContentTagDirective>;
    customSidebarHeaderContent: QueryList<UxAppShellSidebarHeaderContentTagDirective>;
    customHeaderRightContent: QueryList<UxAppShellHeaderRightContentTagDirective>;
    customHeaderTitleContent: QueryList<UxAppShellHeaderTitleContentTagDirective>;
    customMainContent: QueryList<UxAppShellMainContentTagDirective>;
    customNavBarItemsContent: QueryList<UxAppShellNavBarItemsContentTagDirective>;
    constructor(uxService: UxService);
    ngAfterContentInit(): void;
    onHeaderLanguageChanged(uxLanguage: UxLanguage): void;
    onTopMenuItemClicked(uxLink: UxLink): void;
}
export declare class UxAppShellSidebarContentTagDirective {
}
export declare class UxAppShellSidebarHeaderContentTagDirective {
}
export declare class UxAppShellHeaderRightContentTagDirective {
}
export declare class UxAppShellHeaderTitleContentTagDirective {
}
export declare class UxAppShellMainContentTagDirective {
}
export declare class UxAppShellNavBarItemsContentTagDirective {
}
export declare class UxLayoutAppShellComponentModule {
}
