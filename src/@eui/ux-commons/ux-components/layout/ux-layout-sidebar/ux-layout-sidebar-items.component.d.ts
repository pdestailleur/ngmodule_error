import { EventEmitter, AfterContentInit, QueryList } from '@angular/core';
import { UxService } from '../../../shared/ux.service';
import { UxLink } from '../../../shared/models/ux-link.model';
import { UxLayoutSidebarItemComponent } from './ux-layout-sidebar-item.component';
export declare class UxLayoutSidebarItemsComponent implements AfterContentInit {
    uxService: UxService;
    links: UxLink[];
    isLargeItems: boolean;
    isInnerNavigationSidebar: boolean;
    clicked: EventEmitter<string>;
    items: QueryList<UxLayoutSidebarItemComponent>;
    constructor(uxService: UxService);
    ngAfterContentInit(): void;
    readonly hasLinks: boolean;
    onClick(id: string): void;
}
export declare class UxLayoutSidebarItemsComponentModule {
}
