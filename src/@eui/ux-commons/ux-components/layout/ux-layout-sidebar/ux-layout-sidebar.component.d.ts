import { EventEmitter, AfterContentInit, QueryList } from '@angular/core';
import { UxService } from '../../../shared/ux.service';
import { UxLink } from '../../../shared/models/ux-link.model';
import { UxLayoutSidebarItemComponent } from './ux-layout-sidebar-item.component';
export declare class UxLayoutSidebarComponent implements AfterContentInit {
    uxService: UxService;
    private document;
    homeUrl: string;
    title: string;
    isShowLogo: boolean;
    isStateOpen: boolean;
    isAlwaysVisible: boolean;
    isCollapsible: boolean;
    links: UxLink[];
    isStateCloseWithIcons: boolean;
    isLargeItems: boolean;
    toggleOpenIconClass: string;
    toggleCloseIconClass: string;
    hasFilter: boolean;
    filterPlaceholderLabel: string;
    isInnerSidebar: boolean;
    isMobileVisible: boolean;
    theme: string;
    clicked: EventEmitter<string>;
    customHeaderContent: QueryList<UxLayoutSidebarHeaderTagDirective>;
    customContent: QueryList<UxLayoutSidebarContentTagDirective>;
    items: QueryList<UxLayoutSidebarItemComponent>;
    themeClass: string;
    private topPosition;
    constructor(uxService: UxService, document: any);
    onWindowScroll(): void;
    readonly innerSidebarTopPosition: number;
    readonly hasLinks: boolean;
    ngAfterContentInit(): void;
    toggleSidebar(): void;
    onClick(id: string): void;
}
export declare class UxLayoutSidebarHeaderTagDirective {
}
export declare class UxLayoutSidebarContentTagDirective {
}
export declare class UxLayoutSidebarComponentModule {
}
