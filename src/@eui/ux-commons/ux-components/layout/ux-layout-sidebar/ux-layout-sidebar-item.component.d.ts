import { EventEmitter, QueryList } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UxService } from '../../../shared/ux.service';
export declare class UxLayoutSidebarItemComponent {
    private uxService;
    private router;
    private activatedRoute;
    id: string;
    label: string;
    subLabel: string;
    url: string;
    iconClass: string;
    isLarge: boolean;
    isActive: boolean;
    isExpanded: boolean;
    isHome: boolean;
    clicked: EventEmitter<string>;
    subItems: QueryList<UxLayoutSidebarItemComponent>;
    constructor(uxService: UxService, router: Router, activatedRoute: ActivatedRoute);
    readonly hasSub: boolean;
    readonly isCurrentRouteActive: boolean;
    onKeyDown(event: any): void;
    toggle(event: Event): void;
    onClick(event: any): void;
    private navigateTo();
}
export declare class UxLayoutSidebarItemComponentModule {
}
