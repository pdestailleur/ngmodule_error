export declare class UxLayoutOverlayPanelComponent {
    isActive: boolean;
    onClick(event: Event): void;
}
export declare class UxLayoutOverlayPanelHeaderTagDirective {
}
export declare class UxLayoutOverlayPanelInnerTagDirective {
}
export declare class UxLayoutOverlayPanelFooterTagDirective {
}
export declare class UxLayoutOverlayPanelComponentModule {
}
