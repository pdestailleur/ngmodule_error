export declare class UxLayoutAppMainComponent {
    styleClass: string;
}
export declare class UxLayoutAppMainHeaderContainerTagDirective {
}
export declare class UxLayoutAppMainBannerTopContainerTagDirective {
}
export declare class UxLayoutAppMainNavBarContainerTagDirective {
}
export declare class UxLayoutAppMainTopMessageContainerTagDirective {
}
export declare class UxLayoutAppMainBreadcrumbsContainerTagDirective {
}
export declare class UxLayoutAppMainContentContainerTagDirective {
}
export declare class UxLayoutAppMainBannerBottomContainerTagDirective {
}
export declare class UxLayoutAppMainFooterContainerTagDirective {
}
export declare class UxLayoutAppMainComponentModule {
}
