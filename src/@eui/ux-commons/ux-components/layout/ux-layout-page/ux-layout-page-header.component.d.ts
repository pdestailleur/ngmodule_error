import { QueryList } from '@angular/core';
export declare class UxLayoutPageHeaderComponent {
    styleClass: string;
    pageTitle: string;
    pageSubtitle: string;
    customActionsContent: QueryList<UxLayoutPageHeaderActionsContentTagDirective>;
    customActionIconsContent: QueryList<UxLayoutPageHeaderActionIconsContentTagDirective>;
}
export declare class UxLayoutPageHeaderActionsContentTagDirective {
}
export declare class UxLayoutPageHeaderActionIconsContentTagDirective {
}
export declare class UxLayoutPageHeaderComponentModule {
}
