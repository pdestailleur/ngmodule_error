import { OnInit } from '@angular/core';
import { UxService } from '../../shared/ux.service';
export declare class UxLayoutAppComponent implements OnInit {
    uxService: UxService;
    styleClass: string;
    constructor(uxService: UxService);
    ngOnInit(): void;
}
export declare class UxLayoutAppSidebarContainerTagDirective {
}
export declare class UxLayoutAppMainContainerTagDirective {
}
export declare class UxLayoutAppComponentModule {
}
