import { QueryList } from '@angular/core';
import { UxLayoutNavBarActionItemComponent } from './ux-layout-nav-bar-action-item.component';
import { UxService } from '../../../shared/ux.service';
export declare class UxLayoutNavBarActionsComponent {
    private uxService;
    items: QueryList<UxLayoutNavBarActionItemComponent>;
    constructor(uxService: UxService);
    unToggleAll(): void;
    hideAll(item: UxLayoutNavBarActionItemComponent): void;
}
export declare class UxLayoutNavBarActionsComponentModule {
}
