import { QueryList } from '@angular/core';
import { UxLayoutNavBarTopMenuComponent } from './ux-layout-nav-bar-top-menu.component';
import { UxLayoutNavBarLeftActionItemSidebarToggleComponent } from './ux-layout-nav-bar-left-action-item-sidebar-toggle.component';
export declare class UxLayoutNavBarComponent {
    isMuted: boolean;
    styleClass: string;
    topMenu: QueryList<UxLayoutNavBarTopMenuComponent>;
    sidebarToggle: QueryList<UxLayoutNavBarLeftActionItemSidebarToggleComponent>;
}
export declare class UxLayoutNavBarElementsContentTagDirective {
}
export declare class UxLayoutNavBarLeftActionsContentTagDirective {
}
export declare class UxLayoutNavBarComponentModule {
}
