import { EventEmitter } from '@angular/core';
import { UxLink } from '../../../shared/models/ux-link.model';
export declare class UxLayoutNavBarTopMenuComponent {
    links: Array<UxLink>;
    homeUrl: string;
    menuItemClicked: EventEmitter<UxLink>;
    readonly hasLinks: boolean;
    onHomeClick(): void;
    onLinkClick(link: UxLink): void;
}
export declare class UxLayoutNavBarTopMenuComponentModule {
}
