import { EventEmitter, AfterContentInit } from '@angular/core';
import { UxLanguage } from '../../ux-language-selector/ux-language';
import { UxLayoutNavBarActionsComponent } from './ux-layout-nav-bar-actions.component';
import { UxLink } from '../../../shared/models/ux-link.model';
import { UxService } from '../../../shared/ux.service';
export declare class UxLayoutNavBarActionItemComponent implements AfterContentInit {
    uxService: UxService;
    id: string;
    isToggleContent: boolean;
    isOverlayPanel: boolean;
    isOverlayPanelCustomContent: boolean;
    isOverlayPanelBg: boolean;
    links: Array<UxLink>;
    isShowHome: boolean;
    homeUrl: string;
    iconClass: string;
    itemClass: string;
    contentClass: string;
    tagCount: number;
    userInfos: string;
    isHiddenMobile: boolean;
    isMobileOnly: boolean;
    isUseSidebarTemplateAsLinks: boolean;
    isActive: boolean;
    isContentFixedHeight: boolean;
    languageChanged: EventEmitter<UxLanguage>;
    clicked: EventEmitter<UxLayoutNavBarActionItemComponent>;
    uxLayoutNavBarActionsComponent: UxLayoutNavBarActionsComponent;
    constructor(uxLayoutNavBarActionsComponent: UxLayoutNavBarActionsComponent, uxService: UxService);
    ngAfterContentInit(): void;
    onLanguageChanged(language: UxLanguage): void;
    toggle(event: Event): void;
    onClick(event: Event): void;
    onSidebarItemClick(event: Event): void;
}
export declare class UxLayoutNavBarOverlayPanelContentTagDirective {
}
export declare class UxLayoutNavBarActionItemComponentModule {
}
