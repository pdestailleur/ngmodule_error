import { UxService } from '../../../shared/ux.service';
export declare class UxLayoutNavBarLeftActionItemSidebarToggleComponent {
    uxService: UxService;
    label: string;
    constructor(uxService: UxService);
    onToggleSidebar(event: Event): void;
}
export declare class UxLayoutNavBarLeftActionItemSidebarToggleComponentModule {
}
