import { EventEmitter, QueryList, OnInit } from '@angular/core';
import { UxLanguage } from '../../ux-language-selector/ux-language';
import { UxService } from '../../../shared/ux.service';
export declare class UxLayoutHeaderProfileComponent implements OnInit {
    uxService: UxService;
    userInfos: string;
    languageCodes: string;
    isConnected: boolean;
    isOnline: boolean;
    isShowTimeWrapper: boolean;
    isUserLoggedIn: boolean;
    selectedLanguage: UxLanguage;
    languageChanged: EventEmitter<UxLanguage>;
    customProfileMenuContent: QueryList<UxLayoutHeaderProfileMenuContentTagDirective>;
    isProfileDropdownVisible: boolean;
    localTime: Date;
    utcTime: any;
    utcOffset: any;
    closeProfileDropdown(): void;
    constructor(uxService: UxService);
    ngOnInit(): void;
    readonly activeLanguage: UxLanguage;
    onLanguageChanged(language: UxLanguage): void;
    onToggleProfileDropdown(event: Event): void;
    readonly localTimeString: string;
    readonly utcTimeString: string;
    private formatDate(d);
    private refreshUtcTime();
}
export declare class UxLayoutHeaderProfileMenuContentTagDirective {
}
export declare class UxLayoutHeaderProfileNotLoggedInContentTagDirective {
}
export declare class UxLayoutHeaderProfileComponentModule {
}
