import { EventEmitter } from '@angular/core';
import { UxLanguage } from '../../ux-language-selector/ux-language';
import { UxService } from '../../../shared/ux.service';
import { UxLink } from '../../../shared/models/ux-link.model';
export declare class UxLayoutHeaderComponent {
    uxService: UxService;
    appFullName: string;
    appFullNameStyleClass: string;
    appShortName: string;
    appSubtitle: string;
    envLabel: string;
    userInfos: string;
    languageCodes: string;
    isCustomRightContent: boolean;
    isCustomTitleContent: boolean;
    isShowExtraButtonAction: boolean;
    isShowLanguageSelector: boolean;
    homeUrl: string;
    isHideLogo: boolean;
    userProfileLinks: UxLink[];
    selectedLanguage: UxLanguage;
    languageChanged: EventEmitter<UxLanguage>;
    readonly hasSubtitle: boolean;
    constructor(uxService: UxService);
    readonly activeLanguage: UxLanguage;
    readonly appShortNameGen: string;
    onLanguageChanged(language: UxLanguage): void;
}
export declare class UxLayoutHeaderRightContentTagDirective {
}
export declare class UxLayoutHeaderUserProfileLinksTagDirective {
}
export declare class UxLayoutHeaderTitleContentTagDirective {
}
export declare class UxLayoutHeaderComponentModule {
}
