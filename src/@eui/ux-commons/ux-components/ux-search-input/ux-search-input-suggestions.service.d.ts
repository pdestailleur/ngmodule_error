import { Observable } from 'rxjs/Observable';
export interface UxSearchInputSuggestionsService {
    getSuggestions(query: string): Observable<string[]>;
}
