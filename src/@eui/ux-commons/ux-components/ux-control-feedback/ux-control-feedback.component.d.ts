import { AfterContentInit } from '@angular/core';
export declare class UxControlFeedbackComponent implements AfterContentInit {
    typeClass: string;
    stateClass: string;
    ngAfterContentInit(): void;
}
export declare class UxControlFeedbackComponentModule {
}
