import { QueryList, AfterContentInit, EventEmitter } from '@angular/core';
import { UxWizardStepComponent } from './ux-wizard-step.component';
export declare class UxWizardStepsComponent implements AfterContentInit {
    isCustomContent: boolean;
    isShowStepTitle: boolean;
    selectStep: EventEmitter<any>;
    steps: QueryList<UxWizardStepComponent>;
    ngAfterContentInit(): void;
    onSelectStep(step: UxWizardStepComponent): void;
}
export declare class UxWizardStepsComponentModule {
}
