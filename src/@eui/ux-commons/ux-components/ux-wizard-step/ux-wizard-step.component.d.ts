import { QueryList } from '@angular/core';
export declare class UxWizardStepComponent {
    label: string;
    subLabel: string;
    isCompleted: boolean;
    isActive: boolean;
    isShowStepTitle: boolean;
    isDisabled: boolean;
    customContent: QueryList<UxWizardStepContentTagDirective>;
}
export declare class UxWizardStepContentTagDirective {
}
export declare class UxWizardStepComponentModule {
}
