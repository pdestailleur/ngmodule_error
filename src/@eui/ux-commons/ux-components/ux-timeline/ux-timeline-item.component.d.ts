import { AfterContentInit } from '@angular/core';
import { UxTimelineItemsComponent } from './ux-timeline-items.component';
export declare class UxTimelineItemComponent implements AfterContentInit {
    label: string;
    subLabel: string;
    styleClass: string;
    date: string;
    uxTimelineItemsComponent: UxTimelineItemsComponent;
    isLeftAligned: boolean;
    constructor(uxTimelineItemsComponent: UxTimelineItemsComponent);
    ngAfterContentInit(): void;
}
export declare class UxTimelineItemComponentModule {
}
