import { AfterContentInit } from '@angular/core';
export declare class UxTimelineItemsComponent implements AfterContentInit {
    isLeftAligned: boolean;
    ngAfterContentInit(): void;
}
export declare class UxTimelineItemsComponentModule {
}
