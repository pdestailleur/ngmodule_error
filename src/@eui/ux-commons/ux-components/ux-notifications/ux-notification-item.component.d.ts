import { EventEmitter } from '@angular/core';
import { UxLink } from '../../shared/models/ux-link.model';
import { UxService } from '../../shared/ux.service';
export declare class UxNotificationItemComponent {
    private uxService;
    notificationLink: UxLink;
    dateFormat: string;
    itemClick: EventEmitter<UxLink>;
    itemMarkAsRead: EventEmitter<UxLink>;
    itemMarkAsUnRead: EventEmitter<UxLink>;
    markAsReadLabel: string;
    markAsUnReadLabel: string;
    constructor(uxService: UxService);
    onItemClick(event: Event): void;
    onItemMarkAsRead(event: Event): void;
    onItemMarkAsUnRead(event: Event): void;
    readonly notificationTypeClass: string;
}
export declare class UxNotificationItemComponentModule {
}
