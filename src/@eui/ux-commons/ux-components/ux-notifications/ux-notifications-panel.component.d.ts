import { EventEmitter, OnInit, OnChanges, SimpleChanges } from '@angular/core';
import { UxLink } from '../../shared/models/ux-link.model';
import { UxService } from '../../shared/ux.service';
import { UxLayoutNavBarActionsComponent } from '../layout/ux-layout-nav-bar/ux-layout-nav-bar-actions.component';
import { UxLayoutOverlayPanelComponent } from '../layout/ux-layout-overlay-panel.component';
export declare class UxNotificationsPanelComponent implements OnInit, OnChanges {
    private uxService;
    notificationLinks: UxLink[];
    viewAllNotificationsLabel: string;
    headerTitleLabel: string;
    noNotificationFoundLabel: string;
    todaysSeparatorLabel: string;
    oldestSeparatorLabel: string;
    isShowViewAllAction: boolean;
    isSeparateCurrentDayNotifications: boolean;
    isHidePanelOnViewAllAction: boolean;
    refreshClick: EventEmitter<any>;
    viewAllClick: EventEmitter<any>;
    itemClick: EventEmitter<UxLink>;
    itemMarkAsRead: EventEmitter<UxLink>;
    itemMarkAsUnRead: EventEmitter<UxLink>;
    currentDayNotifications: UxLink[];
    oldestNotifications: UxLink[];
    unreadNotifications: UxLink[];
    today: Date;
    unreadLabel: string;
    uxLayoutNavBarActionsComponent: UxLayoutNavBarActionsComponent;
    uxLayoutOverlayPanelComponent: UxLayoutOverlayPanelComponent;
    constructor(uxLayoutNavBarActionsComponent: UxLayoutNavBarActionsComponent, uxLayoutOverlayPanelComponent: UxLayoutOverlayPanelComponent, uxService: UxService);
    readonly unreadCount: number;
    ngOnChanges(changes: SimpleChanges): void;
    ngOnInit(): void;
    onRefresh(event: Event): void;
    onItemClick(link: UxLink): void;
    onViewAllClick(event: Event): void;
    onItemMarkAsRead(link: UxLink): void;
    onItemMarkAsUnRead(link: UxLink): void;
    getNotificationStateClass(link: UxLink): "" | "ux-list-item--muted";
}
export declare class UxNotificationsPanelComponentModule {
}
