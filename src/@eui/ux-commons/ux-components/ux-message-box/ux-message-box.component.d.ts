import { EventEmitter, AfterContentInit, QueryList } from '@angular/core';
import { UxService } from '../../shared/ux.service';
export declare class UxMessageBoxComponent implements AfterContentInit {
    private uxService;
    id: string;
    titleLabel: string;
    message: string;
    typeClass: string;
    messageBoxType: string;
    acceptLabel: string;
    dismissLabel: string;
    isFooterCustomAlignment: boolean;
    clicked: EventEmitter<boolean>;
    customFooterContent: QueryList<UxMessageBoxFooterTagDirective>;
    yesLabel: string;
    noLabel: string;
    stateClasses: string;
    constructor(uxService: UxService);
    ngAfterContentInit(): void;
    onClick(value: boolean): void;
}
export declare class UxMessageBoxFooterTagDirective {
}
export declare class UxMessageBoxComponentModule {
}
