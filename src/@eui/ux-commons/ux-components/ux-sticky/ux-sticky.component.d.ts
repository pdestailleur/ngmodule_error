import { ElementRef, EventEmitter, OnInit, OnDestroy, AfterViewInit } from '@angular/core';
export declare class UxStickyComponent implements OnInit, OnDestroy, AfterViewInit {
    private element;
    zIndex: number;
    width: string;
    offsetTop: number;
    offsetBottom: number;
    start: number;
    stickClass: string;
    endStickClass: string;
    mediaQuery: string;
    parentMode: boolean;
    activated: EventEmitter<{}>;
    deactivated: EventEmitter<{}>;
    private onScrollBind;
    private onResizeBind;
    private isStuck;
    private elem;
    private container;
    private originalCss;
    private windowHeight;
    private containerHeight;
    private elemHeight;
    private containerStart;
    private scrollFinish;
    constructor(element: ElementRef);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    onScroll(): void;
    onResize(): void;
    defineDimensions(): void;
    resetElement(): void;
    stuckElement(): void;
    unstuckElement(): void;
    matchMediaQuery(): any;
    sticker(): void;
    private scrollbarYPos();
    private getBoundingClientRectValue(element, property);
    private getCssValue(element, property);
    private getCssNumber(element, property);
}
export declare class UxStickyComponentModule {
}
