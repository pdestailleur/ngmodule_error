import { OnInit } from '@angular/core';
export declare class UxPieChartComponent implements OnInit {
    data: any;
    options: any;
    chartTitle: string;
    chartType: string;
    isShowLabel: boolean;
    pieOptions: any;
    innerData: any;
    pieData: any;
    constructor();
    ngOnInit(): void;
}
export declare class UxPieChartComponentModule {
}
