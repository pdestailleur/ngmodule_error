export declare class UxIconBoxComponent {
    styleClass: string;
    typeClass: string;
    iconClass: string;
    label: string;
}
export declare class UxIconBoxComponentModule {
}
