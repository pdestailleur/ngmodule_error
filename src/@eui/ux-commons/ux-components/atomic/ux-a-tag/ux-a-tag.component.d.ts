import { AfterContentInit } from '@angular/core';
export declare class UxTagComponent implements AfterContentInit {
    styleClass: string;
    typeClass: string;
    label: string;
    subLabel: string;
    isRounded: boolean;
    isSmall: boolean;
    stateClasses: string;
    ngAfterContentInit(): void;
}
export declare class UxTagComponentModule {
}
