import { AfterContentInit } from '@angular/core';
import { UxService } from '../../../shared/ux.service';
export declare class UxDateTagComponent implements AfterContentInit {
    private uxService;
    styleClass: string;
    typeClass: string;
    date: Date;
    dateFormat: string;
    isVerticalLayout: boolean;
    isHandleOverdue: boolean;
    isDisplayDaysLeft: boolean;
    languageCode: string;
    isMuted: boolean;
    stateClass: string;
    constructor(uxService: UxService);
    ngAfterContentInit(): void;
    readonly daysLeftValue: string;
    private getOverdueState();
}
export declare class UxDateTagComponentModule {
}
