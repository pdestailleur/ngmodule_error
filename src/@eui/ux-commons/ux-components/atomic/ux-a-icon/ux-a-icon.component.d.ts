import { AfterContentInit } from '@angular/core';
export declare class UxIconComponent implements AfterContentInit {
    styleClass: string;
    typeClass: string;
    iconClass: string;
    isRounded: boolean;
    isLarge: boolean;
    iconStateClasses: string;
    ngAfterContentInit(): void;
}
export declare class UxIconComponentModule {
}
