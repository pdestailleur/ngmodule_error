import { EventEmitter, OnInit } from '@angular/core';
import { UxService } from '../../../shared/ux.service';
export declare class UxToolbarFilterComponent implements OnInit {
    private uxService;
    isVisible: boolean;
    filterLabel: string;
    expandAllLabel: string;
    collapseAllLabel: string;
    isToggleVisible: boolean;
    filter: EventEmitter<string>;
    expandAll: EventEmitter<any>;
    collapseAll: EventEmitter<any>;
    filterValue: string;
    isToggleExpanded: boolean;
    translatedFilterLabel: string;
    translatedExpandAllLabel: string;
    translatedCollapseAllLabel: string;
    constructor(uxService: UxService);
    ngOnInit(): void;
    onFilter(event: any): void;
    onExpandAll(event: any): void;
    onCollapseAll(event: any): void;
}
export declare class UxToolbarFilterComponentModule {
}
