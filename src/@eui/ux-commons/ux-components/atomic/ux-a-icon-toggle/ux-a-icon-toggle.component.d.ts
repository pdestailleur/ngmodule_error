import { EventEmitter, AfterContentInit } from '@angular/core';
export declare class UxIconToggleComponent implements AfterContentInit {
    iconClassOff: string;
    iconClassOn: string;
    isChecked: boolean;
    isReadOnly: boolean;
    toggle: EventEmitter<boolean>;
    iconClass: string;
    stateClass: string;
    ngAfterContentInit(): void;
    onToggle(): void;
    private setIconClass();
}
export declare class UxIconToggleComponentModule {
}
