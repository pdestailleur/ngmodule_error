import { QueryList, SimpleChanges, OnChanges } from '@angular/core';
export declare class UxLabelComponent implements OnChanges {
    styleClass: string;
    label: string;
    subLabel: string;
    labelIconClass: string;
    hasMarker: boolean;
    markerTypeClass: string;
    isClickable: boolean;
    badgeLabel: string;
    badgeTypeClass: string;
    infos: string;
    customSubLabel: QueryList<UxLabelSubLabelTagDirective>;
    ngOnChanges(changes: SimpleChanges): void;
}
export declare class UxLabelSubLabelTagDirective {
}
export declare class UxLabelComponentModule {
}
