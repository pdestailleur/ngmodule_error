export declare class UxFieldsetComponent {
    label: string;
    labelWidthClass: string;
    inputWidthClass: string;
    styleClass: string;
    isFirst: boolean;
    readonly stateClass: string;
}
export declare class UxFieldsetComponentModule {
}
