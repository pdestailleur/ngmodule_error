export declare class UxBadgeComponent {
    styleClass: string;
    typeClass: string;
    isSmall: boolean;
    isLarge: boolean;
    isTiny: boolean;
    isPill: boolean;
    isOutline: boolean;
    constructor();
}
export declare class UxBadgeComponentModule {
}
