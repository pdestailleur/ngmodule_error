import { OnInit, EventEmitter, ElementRef } from '@angular/core';
import { UxLanguage } from './ux-language';
import { UxService } from '../../shared/ux.service';
export declare class UxLanguageSelectorComponent implements OnInit {
    private shadowDomRoot;
    private uxService;
    languageCodes: string;
    selectedLanguage: UxLanguage;
    isShowLabel: boolean;
    languageChanged: EventEmitter<UxLanguage>;
    modalId: string;
    languages: UxLanguage[];
    languageRows: UxLanguage[][];
    modalIsOpen: boolean;
    dropDownIsOpen: boolean;
    readonly isShowDropDown: boolean;
    readonly isShowModal: boolean;
    constructor(shadowDomRoot: ElementRef, uxService: UxService);
    ngOnInit(): void;
    shadowDomQuery(querySelector: string): HTMLElement;
    prepareLanguageRows(): UxLanguage[][];
    selectLanguage(languageCode: string): void;
    openKeyDown(keyCode: number): void;
    open(): void;
    close(): void;
    closeModalOnEscape(keyCode: number): void;
    consumeEvent(event: Event): boolean;
    toggleDropDownKeyDown(keyCode: number): void;
    toggleDropDown(): void;
    selectDropdownLanguage(languageCode: string): void;
    closeDropDownOnEscape(keyCode: number): void;
    closeOnBlur(): void;
    navigateDropDownSelection(event: KeyboardEvent): void;
}
export declare class UxLanguageSelectorComponentModule {
}
