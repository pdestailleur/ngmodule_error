import { UxLanguage } from './ux-language';
export declare class UxEuLanguages {
    static languagesByCode: {
        [code: string]: UxLanguage;
    };
    private static defaultCodes;
    static getLanguages(codes?: string[]): UxLanguage[];
}
