import { EventEmitter, AfterContentInit } from '@angular/core';
import { UxAutocompleteTagItem } from './ux-autocomplete-tag-item.model';
import { UxService } from '../../shared/ux.service';
export declare class UxAutocompleteTagComponent implements AfterContentInit {
    private uxService;
    isReadOnly: boolean;
    items: UxAutocompleteTagItem[];
    selectedItems: UxAutocompleteTagItem[];
    placeholder: string;
    isFreeTagsAllowed: boolean;
    selectionChanged: EventEmitter<UxAutocompleteTagItem[]>;
    itemAdded: EventEmitter<UxAutocompleteTagItem>;
    itemRemoved: EventEmitter<UxAutocompleteTagItem>;
    query: string;
    filteredList: UxAutocompleteTagItem[];
    elementRef: any;
    selectedIdx: number;
    handleBodyClick(event: Event): void;
    constructor(uxService: UxService);
    ngAfterContentInit(): void;
    onInputKeyup(event: any): void;
    onInputClick(event: any): void;
    onToggle(event: any): void;
    onInputBlur(event: any): void;
    selectItem(item: UxAutocompleteTagItem): void;
    removeItem(item: UxAutocompleteTagItem): void;
    private reset();
    private updateFilteredList();
}
export declare class UxAutocompleteTagComponentModule {
}
