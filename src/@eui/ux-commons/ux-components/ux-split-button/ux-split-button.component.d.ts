import { EventEmitter, AfterContentInit } from '@angular/core';
import { UxLink } from '../../shared/models/ux-link.model';
export declare class UxSplitButtonComponent implements AfterContentInit {
    typeClass: string;
    isOutline: boolean;
    label: string;
    links: UxLink[];
    isDropDownRightAligned: boolean;
    linkSelected: EventEmitter<UxLink>;
    buttonClicked: EventEmitter<any>;
    btnTypeClass: string;
    constructor();
    ngAfterContentInit(): void;
    onLinkSelected(link: UxLink): void;
    onButtonClicked(event: Event): void;
}
export declare class UxSplitButtonComponentModule {
}
