import { ElementRef, QueryList, EventEmitter, OnInit, AfterContentInit } from '@angular/core';
import { UxLink } from '../../shared/models/ux-link.model';
import { UxService } from '../../shared/ux.service';
import { UxDropdownButtonItemComponent } from './ux-dropdown-button-item.component';
export declare class UxDropdownButtonComponent implements OnInit, AfterContentInit {
    private uxService;
    private shadowDomRoot;
    styleClass: string;
    label: string;
    iconClass: string;
    links: UxLink[];
    activeLink: UxLink;
    typeClass: string;
    isSplitButtonToggle: boolean;
    isDropDownRightAligned: boolean;
    isLinkToggle: boolean;
    isUpdateLabelFromSelectedItem: boolean;
    isOutline: boolean;
    isShowDropdownToggle: boolean;
    linkSelected: EventEmitter<UxLink>;
    items: QueryList<UxDropdownButtonItemComponent>;
    isShown: boolean;
    dropDownIsOpen: boolean;
    ngOnInit(): void;
    ngAfterContentInit(): void;
    readonly toggleClass: string;
    toggleOutside(): void;
    constructor(uxService: UxService, shadowDomRoot: ElementRef);
    shadowDomQuery(querySelector: string): HTMLElement;
    toggleDropDownKeyDown(event: any): void;
    toggleDropDown(event: any): void;
    selectDropdownItem(event: Event, link: UxLink): void;
    closeDropDownOnEscape(keyCode: number): void;
    closeOnBlur(): void;
    navigateDropDownSelection(event: KeyboardEvent): void;
}
export declare class UxDropdownButtonComponentModule {
}
