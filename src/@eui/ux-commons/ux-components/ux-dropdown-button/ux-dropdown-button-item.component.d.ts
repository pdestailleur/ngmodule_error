export declare class UxDropdownButtonItemComponent {
    id: string;
    label: string;
    iconClass: string;
}
export declare class UxDropdownButtonItemComponentModule {
}
