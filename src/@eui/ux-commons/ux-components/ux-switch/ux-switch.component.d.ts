import { EventEmitter } from '@angular/core';
export declare class UxSwitchComponent {
    styleClass: string;
    isChecked: boolean;
    isDisabled: boolean;
    isSmall: boolean;
    isLarge: boolean;
    label: string;
    toggle: EventEmitter<boolean>;
    handleClicked(): void;
}
export declare class UxSwitchComponentModule {
}
