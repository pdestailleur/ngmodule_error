import { QueryList, AfterContentInit, OnDestroy, EventEmitter, ElementRef } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { UxTabComponent } from './ux-tab.component';
import { UxTab } from './ux-tab';
import { UxService } from '../../shared/ux.service';
export declare class UxTabsComponent implements AfterContentInit, OnDestroy {
    private uxService;
    private el;
    styleClass: string;
    tabs: Array<UxTab>;
    isMainNavigation: boolean;
    isSubTabs: boolean;
    tabSelected: EventEmitter<UxTabComponent>;
    tabClosed: EventEmitter<UxTabComponent>;
    subscription: Subscription;
    calculatedWidth: string;
    childrenTabs: QueryList<UxTabComponent>;
    constructor(uxService: UxService, el: ElementRef);
    ngAfterContentInit(): void;
    ngOnDestroy(): void;
    checkTabsWidthHandler(): void;
    selectTab(tab: UxTab, init?: boolean): void;
    onCloseTab(event: Event, tab: UxTabComponent): void;
}
export declare class UxTabsComponentModule {
}
