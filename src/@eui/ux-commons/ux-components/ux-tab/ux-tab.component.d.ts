export declare class UxTabComponent {
    id: string;
    label: string;
    subLabel: string;
    iconClass: string;
    iconTypeClass: string;
    isClosable: boolean;
    isDisabled: boolean;
    tag: string;
    tagTypeClass: string;
    hasMarker: boolean;
    markerTypeClass: string;
    isActive: boolean;
    url: string;
    isVisible: boolean;
    isVisibleOnScreen: boolean;
}
export declare class UxTabComponentModule {
}
