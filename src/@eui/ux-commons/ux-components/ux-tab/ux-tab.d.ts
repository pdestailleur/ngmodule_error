export declare class UxTab {
    id: string;
    label: string;
    subLabel: string;
    iconClass: string;
    iconTypeClass: string;
    isClosable: boolean;
    isDisabled: boolean;
    tag: string;
    tagTypeClass: string;
    hasMarker: boolean;
    markerTypeClass: string;
    isActive: boolean;
    isVisible: boolean;
    url: string;
    isVisibleOnScreen: boolean;
    constructor(values?: Object);
}
