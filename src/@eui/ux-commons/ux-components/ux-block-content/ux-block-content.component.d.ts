import { ElementRef, OnChanges, Renderer, SimpleChanges } from '@angular/core';
export declare class UxBlockContentComponent implements OnChanges {
    private elRef;
    private renderer;
    isBlocked: boolean;
    constructor(elRef: ElementRef, renderer: Renderer);
    ngOnChanges(changes: SimpleChanges): void;
}
export declare class UxBlockContentComponentModule {
}
