import { QueryList, AfterContentInit } from '@angular/core';
import { UxDashboardButtonComponent } from './ux-dashboard-button.component';
export declare class UxDashboardButtonsComponent implements AfterContentInit {
    isRounded: boolean;
    isLarge: boolean;
    styleClass: string;
    buttons: QueryList<UxDashboardButtonComponent>;
    isExtendedPanelsViewActive: boolean;
    ngAfterContentInit(): void;
}
export declare class UxDashboardButtonsComponentModule {
}
