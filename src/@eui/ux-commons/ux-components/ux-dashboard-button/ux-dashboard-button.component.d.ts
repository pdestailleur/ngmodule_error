export declare class UxDashboardButtonComponent {
    mainLabel: string;
    typeClass: string;
    iconClass: string;
    label: string;
    subLabel: string;
    url: string;
    isRounded: boolean;
    isLarge: boolean;
    isFullWidth: boolean;
    cmpPrefix: string;
    readonly stateRoundedClass: string;
    readonly stateTypeClass: string;
    readonly stateLargeClass: string;
    readonly stateFullWidthClass: string;
}
export declare class UxDashboardButtonComponentModule {
}
