import { ElementRef, AfterViewInit, DoCheck, OnDestroy, IterableDiffers } from '@angular/core';
import { UxMessage } from '../../shared/models/ux-message.model';
export declare class UxGrowlComponent implements AfterViewInit, DoCheck, OnDestroy {
    el: ElementRef;
    sticky: boolean;
    life: number;
    value: UxMessage[];
    style: any;
    styleClass: string;
    containerViewChild: ElementRef;
    differ: any;
    zIndex: number;
    container: HTMLDivElement;
    stopDoCheckPropagation: boolean;
    timeout: any;
    constructor(el: ElementRef, differs: IterableDiffers);
    ngAfterViewInit(): void;
    ngDoCheck(): void;
    remove(msg: UxMessage, msgel: any): void;
    removeAll(): void;
    findMessageIndex(msg: UxMessage): number;
    ngOnDestroy(): void;
}
export declare class UxGrowlComponentModule {
}
