import { ElementRef, OnInit, OnChanges, SimpleChanges } from '@angular/core';
export declare class UxTooltip implements OnInit, OnChanges {
    text: string;
    position: string;
    color: string;
    size: string;
    rounded: boolean;
    always: boolean;
    private _el;
    constructor(el: ElementRef);
    ngOnInit(): void;
    ngOnChanges(changes: SimpleChanges): void;
    show(): void;
    hide(): void;
}
export declare class UxTooltipModule {
}
