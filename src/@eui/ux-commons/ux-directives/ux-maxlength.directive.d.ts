import { ElementRef, AfterContentInit } from '@angular/core';
export declare class UxMaxlengthDirective implements AfterContentInit {
    private el;
    length: number;
    private _element;
    constructor(el: ElementRef);
    ngAfterContentInit(): void;
    onKeyUp(event: KeyboardEvent): void;
}
export declare class UxMaxlengthModule {
}
