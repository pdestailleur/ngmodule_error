import { OnInit, ElementRef, AfterViewInit } from '@angular/core';
export declare class UxAutoResizeDirective implements OnInit, AfterViewInit {
    private el;
    private defaultRows;
    minRows: number;
    onInput(textArea: HTMLTextAreaElement): void;
    constructor(el: ElementRef);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    resize(): void;
}
export declare class UxAutoResizeModule {
}
