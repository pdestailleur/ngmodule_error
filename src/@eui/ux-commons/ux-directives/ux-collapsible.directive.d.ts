import { ElementRef, AfterContentInit, OnChanges, SimpleChange } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/concat';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMapTo';
export declare class UxCollapsibleDirective implements OnChanges, AfterContentInit {
    private el;
    isFinishedMoving: boolean;
    isExpanded: boolean;
    isMoving: boolean;
    uxExpanded: boolean;
    uxCollapsed: boolean;
    container: HTMLElement;
    containerStyle: CSSStyleDeclaration;
    transitionDuration: string;
    animation: Subscription;
    constructor(el: ElementRef);
    ngOnChanges(changes: {
        [propertyName: string]: SimpleChange;
    }): void;
    ngAfterContentInit(): void;
    collapse(): void;
    expand(): void;
    disableFocus(): void;
    enableFocus(): void;
}
export declare class UxCollapsibleDirectiveModule {
}
