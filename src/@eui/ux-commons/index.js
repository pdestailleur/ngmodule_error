import { Component, ContentChild, ContentChildren, Directive, ElementRef, EventEmitter, Host, HostBinding, HostListener, Inject, Injectable, Input, IterableDiffers, NgModule, Optional, Output, Pipe, Renderer, Renderer2, TemplateRef, ViewChild, forwardRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs/Rx';
import { Observable as Observable$1 } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/concat';
import 'rxjs/add/observable/timer';
import 'rxjs/add/operator/switchMapTo';
import { ActivatedRoute, NavigationEnd, NavigationStart, Router, RouterModule } from '@angular/router';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { FormControl, FormsModule, NG_VALIDATORS, NG_VALUE_ACCESSOR, ReactiveFormsModule } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { DOCUMENT } from '@angular/platform-browser';
import { ChartModule } from 'primeng/primeng';
import { ConnectionBackend, Headers, Http, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

/*
 * Example use
 *		Basic Array of single type: *ngFor="#todo of todoService.todos | orderBy : '-'"
 *		Multidimensional Array Sort on single column: *ngFor="#todo of todoService.todos | orderBy : ['-status']"
 *		Multidimensional Array Sort on multiple columns: *ngFor="#todo of todoService.todos | orderBy : ['status', '-title']"
 */
var UxOrderByPipe = (function () {
    function UxOrderByPipe() {
    }
    /**
     * @param {?} a
     * @param {?} b
     * @return {?}
     */
    UxOrderByPipe._orderByComparator = function (a, b) {
        if ((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))) {
            // Isn't a number so lowercase the string to properly compare
            if (a.toLowerCase() < b.toLowerCase()) {
                return -1;
            }
            if (a.toLowerCase() > b.toLowerCase()) {
                return 1;
            }
        }
        else {
            // Parse strings as numbers to compare properly
            if (parseFloat(a) < parseFloat(b)) {
                return -1;
            }
            if (parseFloat(a) > parseFloat(b)) {
                return 1;
            }
        }
        return 0; // equal each other
    };
    /**
     * @param {?} input
     * @param {?} __1
     * @return {?}
     */
    UxOrderByPipe.prototype.transform = function (input, _a) {
        var _b = _a[0], config = _b === void 0 ? '+' : _b;
        if (!Array.isArray(input)) {
            return input;
        }
        if (!Array.isArray(config) || (Array.isArray(config) && config.length === 1)) {
            var /** @type {?} */ propertyToCheck = !Array.isArray(config) ? config : config[0];
            var /** @type {?} */ desc_1 = propertyToCheck.substr(0, 1) === '-';
            // Basic array
            if (!propertyToCheck || propertyToCheck === '-' || propertyToCheck === '+') {
                return !desc_1 ? input.sort() : input.sort().reverse();
            }
            else {
                var /** @type {?} */ property_1 = propertyToCheck.substr(0, 1) === '+' || propertyToCheck.substr(0, 1) === '-'
                    ? propertyToCheck.substr(1)
                    : propertyToCheck;
                return input.sort(function (a, b) {
                    return !desc_1
                        ? UxOrderByPipe._orderByComparator(a[property_1], b[property_1])
                        : -UxOrderByPipe._orderByComparator(a[property_1], b[property_1]);
                });
            }
        }
        else {
            // Loop over property of the array in order and sort
            return input.sort(function (a, b) {
                for (var /** @type {?} */ i = 0; i < config.length; i++) {
                    var /** @type {?} */ desc = config[i].substr(0, 1) === '-';
                    var /** @type {?} */ property = config[i].substr(0, 1) === '+' || config[i].substr(0, 1) === '-'
                        ? config[i].substr(1)
                        : config[i];
                    var /** @type {?} */ comparison = !desc
                        ? UxOrderByPipe._orderByComparator(a[property], b[property])
                        : -UxOrderByPipe._orderByComparator(a[property], b[property]);
                    // Don't return 0 yet in case of needing to sort by next property
                    if (comparison !== 0) {
                        return comparison;
                    }
                }
                return 0; // equal each other
            });
        }
    };
    return UxOrderByPipe;
}());
UxOrderByPipe.decorators = [
    { type: Pipe, args: [{ name: 'uxOrderBy', pure: false },] },
];
/**
 * @nocollapse
 */
UxOrderByPipe.ctorParameters = function () { return []; };
var UxOrderByPipeModule = (function () {
    function UxOrderByPipeModule() {
    }
    return UxOrderByPipeModule;
}());
UxOrderByPipeModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxOrderByPipe],
                declarations: [UxOrderByPipe]
            },] },
];
/**
 * @nocollapse
 */
UxOrderByPipeModule.ctorParameters = function () { return []; };

var UxFilterPipe = (function () {
    function UxFilterPipe() {
    }
    /**
     * @param {?} value
     * @param {?} args
     * @return {?}
     */
    UxFilterPipe.prototype.transform = function (value, args) {
        if (!args[0]) {
            return value;
        }
        else if (value) {
            return value.filter(function (item) {
                for (var /** @type {?} */ key in item) {
                    if ((typeof item[key] === 'string' || item[key] instanceof String)) {
                        if (item[key].indexOf(args) !== -1) {
                            return true;
                        }
                    }
                }
            });
        }
    };
    return UxFilterPipe;
}());
UxFilterPipe.decorators = [
    { type: Pipe, args: [{
                name: 'uxFilter'
            },] },
];
/**
 * @nocollapse
 */
UxFilterPipe.ctorParameters = function () { return []; };
var UxFilterPipeModule = (function () {
    function UxFilterPipeModule() {
    }
    return UxFilterPipeModule;
}());
UxFilterPipeModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxFilterPipe],
                declarations: [UxFilterPipe]
            },] },
];
/**
 * @nocollapse
 */
UxFilterPipeModule.ctorParameters = function () { return []; };

var UxTruncatePipe = (function () {
    function UxTruncatePipe() {
    }
    /**
     * @param {?} value
     * @param {?=} limit
     * @param {?=} trail
     * @return {?}
     */
    UxTruncatePipe.prototype.transform = function (value, limit, trail) {
        if (limit === void 0) { limit = 40; }
        if (trail === void 0) { trail = '\u2026'; }
        if (value) {
            if (limit < 0) {
                limit *= -1;
                return value.length > limit ? trail + value.substring(value.length - limit, value.length) : value;
            }
            else {
                return value.length > limit ? value.substring(0, limit) + trail : value;
            }
        }
        return;
    };
    return UxTruncatePipe;
}());
UxTruncatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'uxTruncate'
            },] },
];
/**
 * @nocollapse
 */
UxTruncatePipe.ctorParameters = function () { return []; };
var UxTruncatePipeModule = (function () {
    function UxTruncatePipeModule() {
    }
    return UxTruncatePipeModule;
}());
UxTruncatePipeModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxTruncatePipe],
                declarations: [UxTruncatePipe]
            },] },
];
/**
 * @nocollapse
 */
UxTruncatePipeModule.ctorParameters = function () { return []; };

var UxCurrencyPipe = (function () {
    function UxCurrencyPipe() {
        this.decimalSeparator = '.';
        this.thousandsSeparator = ',';
    }
    /**
     * @param {?} value
     * @param {?=} fractionSize
     * @return {?}
     */
    UxCurrencyPipe.prototype.transform = function (value, fractionSize) {
        if (fractionSize === void 0) { fractionSize = 2; }
        var /** @type {?} */ maxFractions = [];
        for (var /** @type {?} */ i = 0; i < fractionSize; i++) {
            maxFractions.push('0');
        }
        var /** @type {?} */ maxFraction = maxFractions.join('');
        var _a = (value || '0').toString().split(this.decimalSeparator), integer = _a[0], _b = _a[1], fraction = _b === void 0 ? maxFraction : _b;
        fraction = fractionSize > 0 ? (fraction + maxFraction).substring(0, fractionSize) : '';
        if (fraction === maxFraction) {
            fraction = '';
        }
        integer = integer.replace(/\B(?=(\d{3})+(?!\d))/g, this.thousandsSeparator);
        return '€ ' + integer + (fraction.length > 0 ? this.decimalSeparator + fraction : '');
    };
    return UxCurrencyPipe;
}());
UxCurrencyPipe.decorators = [
    { type: Pipe, args: [{ name: 'uxCurrency' },] },
];
/**
 * @nocollapse
 */
UxCurrencyPipe.ctorParameters = function () { return []; };
var UxCurrencyPipeModule = (function () {
    function UxCurrencyPipeModule() {
    }
    return UxCurrencyPipeModule;
}());
UxCurrencyPipeModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxCurrencyPipe],
                declarations: [UxCurrencyPipe]
            },] },
];
/**
 * @nocollapse
 */
UxCurrencyPipeModule.ctorParameters = function () { return []; };

var UxTrackScrollDirective = (function () {
    function UxTrackScrollDirective() {
    }
    /**
     * @param {?} $event
     * @return {?}
     */
    UxTrackScrollDirective.prototype.track = function ($event) {
        var /** @type {?} */ scrollTop = Math.max(window.pageYOffset, document.body.scrollTop);
        var /** @type {?} */ app = document.getElementById("app-wrapper");
        if (app != null && app.classList != null) {
            if (!app.classList.contains('sm')) {
                if (scrollTop > 75 && (document.body.scrollHeight - window.screen.height - 100) > 75) {
                    app.classList.add("shrink-header-active");
                }
                else {
                    app.classList.remove("shrink-header-active");
                }
            }
            else {
                app.classList.remove("shrink-header-active");
            }
        }
    };
    return UxTrackScrollDirective;
}());
UxTrackScrollDirective.decorators = [
    { type: Directive, args: [{
                selector: '[uxTrackScroll]',
                host: { '(window:scroll)': 'track($event)' }
            },] },
];
/**
 * @nocollapse
 */
UxTrackScrollDirective.ctorParameters = function () { return []; };
var UxTrackScrollDirectiveModule = (function () {
    function UxTrackScrollDirectiveModule() {
    }
    return UxTrackScrollDirectiveModule;
}());
UxTrackScrollDirectiveModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxTrackScrollDirective],
                declarations: [UxTrackScrollDirective]
            },] },
];
/**
 * @nocollapse
 */
UxTrackScrollDirectiveModule.ctorParameters = function () { return []; };

var UxMaxlengthDirective = (function () {
    /**
     * @param {?} el
     */
    function UxMaxlengthDirective(el) {
        this.el = el;
    }
    /**
     * @return {?}
     */
    UxMaxlengthDirective.prototype.ngAfterContentInit = function () {
        this._element = document.createElement('span');
        this._element.classList.add('input-maxlength');
        this._element.innerHTML = this.length.toString();
        var /** @type {?} */ nativeElement = this.el.nativeElement;
        if (nativeElement != null) {
            var /** @type {?} */ parent_1 = nativeElement.parentNode;
            var /** @type {?} */ wrapper = document.createElement('div');
            if (parent_1 != null) {
                parent_1.replaceChild(wrapper, nativeElement);
                wrapper.appendChild(nativeElement);
                if (nativeElement.parentElement != null) {
                    nativeElement.parentElement.appendChild(this._element);
                    nativeElement.parentElement.classList.add('input-maxlength-wrapper');
                }
            }
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxMaxlengthDirective.prototype.onKeyUp = function (event) {
        var /** @type {?} */ input = (this.el.nativeElement);
        var /** @type {?} */ remainingLength = this.length - input.value.length;
        if (remainingLength <= 0) {
            this._element.classList.add('error');
            if (remainingLength < 0) {
                input.value = input.value.substr(0, input.value.length - 1);
            }
        }
        if (remainingLength >= 0) {
            this._element.innerHTML = (this.length - input.value.length).toString();
        }
        if (remainingLength > 0) {
            this._element.classList.remove('error');
        }
    };
    return UxMaxlengthDirective;
}());
UxMaxlengthDirective.decorators = [
    { type: Directive, args: [{
                selector: '[uxMaxlength]',
                host: { '(keyup)': 'onKeyUp($event)' }
            },] },
];
/**
 * @nocollapse
 */
UxMaxlengthDirective.ctorParameters = function () { return [
    { type: ElementRef, },
]; };
UxMaxlengthDirective.propDecorators = {
    'length': [{ type: Input, args: ['uxMaxlength',] },],
};
var UxMaxlengthModule = (function () {
    function UxMaxlengthModule() {
    }
    return UxMaxlengthModule;
}());
UxMaxlengthModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxMaxlengthDirective],
                declarations: [UxMaxlengthDirective]
            },] },
];
/**
 * @nocollapse
 */
UxMaxlengthModule.ctorParameters = function () { return []; };

var UxTooltip = (function () {
    /**
     * @param {?} el
     */
    function UxTooltip(el) {
        this.text = '';
        this.position = 'top';
        this.color = 'none';
        this.size = 'auto';
        this.rounded = false;
        this.always = false;
        this._el = el.nativeElement;
    }
    /**
     * @return {?}
     */
    UxTooltip.prototype.ngOnInit = function () {
        if (this.always) {
            this._el.classList.add("hint--always");
            this.show();
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    UxTooltip.prototype.ngOnChanges = function (changes) {
        for (var /** @type {?} */ i = 0; i < this._el.classList.length; i++) {
            var /** @type {?} */ currentClass = this._el.classList[i];
            if (currentClass.indexOf("hint--") !== -1) {
                this._el.classList.remove(currentClass);
            }
        }
        if (this.always) {
            this._el.classList.add("hint--always");
            this.show();
        }
    };
    /**
     * @return {?}
     */
    UxTooltip.prototype.show = function () {
        if (!this.text || this.text.length == 0)
            return;
        this.hide();
        this._el.setAttribute("data-hint", this.text);
        for (var /** @type {?} */ i = 0; i < this._el.classList.length; i++) {
            var /** @type {?} */ currentClass = this._el.classList[i];
            if (currentClass.indexOf("hint") !== -1)
                this._el.classList.remove(currentClass);
        }
        if (this.always) {
            this._el.classList.add("hint--always");
        }
        this._el.classList.add("hint--" + this.position);
        switch (this.color) {
            case "danger":
                this._el.classList.add("hint--error");
                break;
            case "warning":
                this._el.classList.add("hint--warning");
                break;
            case "info":
                this._el.classList.add("hint--info");
                break;
            case "success":
                this._el.classList.add("hint--success");
                break;
            default:
        }
        switch (this.size) {
            case "small":
                this._el.classList.add("hint--small");
                break;
            case "medium":
                this._el.classList.add("hint--medium");
                break;
            case "large":
                this._el.classList.add("hint--large");
                break;
            default:
        }
        if (this.rounded)
            this._el.classList.add("hint--rounded");
    };
    /**
     * @return {?}
     */
    UxTooltip.prototype.hide = function () {
        if (this.always)
            return;
        this._el.removeAttribute("data-hint");
    };
    return UxTooltip;
}());
UxTooltip.decorators = [
    { type: Directive, args: [{
                selector: '[uxTooltip]',
                host: {
                    '(mouseover)': 'show()',
                    '(mouseout)': 'hide()',
                    '(focus)': 'show()',
                    '(unfocus)': 'hide()'
                }
            },] },
];
/**
 * @nocollapse
 */
UxTooltip.ctorParameters = function () { return [
    { type: ElementRef, },
]; };
UxTooltip.propDecorators = {
    'text': [{ type: Input, args: ['uxTooltip',] },],
    'position': [{ type: Input },],
    'color': [{ type: Input },],
    'size': [{ type: Input },],
    'rounded': [{ type: Input },],
    'always': [{ type: Input },],
};
var UxTooltipModule = (function () {
    function UxTooltipModule() {
    }
    return UxTooltipModule;
}());
UxTooltipModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxTooltip],
                declarations: [UxTooltip]
            },] },
];
/**
 * @nocollapse
 */
UxTooltipModule.ctorParameters = function () { return []; };

var UxAutoResizeDirective = (function () {
    /**
     * @param {?} el
     */
    function UxAutoResizeDirective(el) {
        var _this = this;
        this.el = el;
        this.defaultRows = 1;
        Observable.fromEvent(window, 'resize')
            .debounceTime(300)
            .subscribe(function (event) {
            _this.resize();
        });
    }
    Object.defineProperty(UxAutoResizeDirective.prototype, "minRows", {
        /**
         * @param {?} minRows
         * @return {?}
         */
        set: function (minRows) {
            this.defaultRows = minRows;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} textArea
     * @return {?}
     */
    UxAutoResizeDirective.prototype.onInput = function (textArea) {
        this.resize();
    };
    /**
     * @return {?}
     */
    UxAutoResizeDirective.prototype.ngOnInit = function () {
        this.el.nativeElement.rows = this.defaultRows;
        this.el.nativeElement.style.overflow = 'hidden';
        this.el.nativeElement.style.resize = 'none';
    };
    /**
     * @return {?}
     */
    UxAutoResizeDirective.prototype.ngAfterViewInit = function () {
        this.resize();
    };
    /**
     * @return {?}
     */
    UxAutoResizeDirective.prototype.resize = function () {
        this.el.nativeElement.style.height = 'auto';
        var /** @type {?} */ offset = this.el.nativeElement.offsetHeight, /** @type {?} */ scroll = this.el.nativeElement.scrollHeight;
        this.el.nativeElement.style.height = (offset > scroll ? offset : scroll) + 'px';
    };
    return UxAutoResizeDirective;
}());
UxAutoResizeDirective.decorators = [
    { type: Directive, args: [{
                selector: 'textarea[uxAutoResize]'
            },] },
];
/**
 * @nocollapse
 */
UxAutoResizeDirective.ctorParameters = function () { return [
    { type: ElementRef, },
]; };
UxAutoResizeDirective.propDecorators = {
    'minRows': [{ type: Input },],
    'onInput': [{ type: HostListener, args: ['input', ['$event.target'],] },],
};
var UxAutoResizeModule = (function () {
    function UxAutoResizeModule() {
    }
    return UxAutoResizeModule;
}());
UxAutoResizeModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxAutoResizeDirective],
                declarations: [UxAutoResizeDirective]
            },] },
];
/**
 * @nocollapse
 */
UxAutoResizeModule.ctorParameters = function () { return []; };

var UxCollapsibleDirective = (function () {
    /**
     * @param {?} el
     */
    function UxCollapsibleDirective(el) {
        this.el = el;
        this.isFinishedMoving = true;
        this.isExpanded = true;
        this.isMoving = false;
        this.uxExpanded = true;
        this.uxCollapsed = false;
        this.transitionDuration = '0.35s';
        this.container = el.nativeElement;
        this.containerStyle = this.container.style;
        this.containerStyle.overflow = 'hidden';
        this.containerStyle.transform = 'translate3d(0, 0, 0)';
        var display = this.containerStyle.display;
        if (display == null || display.length <= 0) {
            this.containerStyle.display = 'block';
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    UxCollapsibleDirective.prototype.ngOnChanges = function (changes) {
        var /** @type {?} */ change = changes['uxCollapsed'];
        if (change != null && !change.isFirstChange()) {
            if (this.uxCollapsed) {
                this.collapse();
            }
            else {
                this.expand();
            }
        }
        change = changes['uxExpanded'];
        if (change != null && !change.isFirstChange()) {
            if (this.uxExpanded) {
                this.expand();
            }
            else {
                this.collapse();
            }
        }
    };
    /**
     * @return {?}
     */
    UxCollapsibleDirective.prototype.ngAfterContentInit = function () {
        this.containerStyle.transitionDuration = '0';
        if (this.uxCollapsed || !this.uxExpanded || this.container.getAttribute('aria-expanded') === 'false') {
            this.isExpanded = false;
            this.containerStyle.height = '0';
        }
        else {
            this.containerStyle.height = 'auto'; // Set it to auto to ensure a working resizing.
        }
    };
    /**
     * @return {?}
     */
    UxCollapsibleDirective.prototype.collapse = function () {
        var _this = this;
        if (this.isExpanded) {
            if (this.animation != null) {
                this.animation.unsubscribe();
            }
            this.animation = Observable$1.concat(Observable$1.of(function () {
                _this.disableFocus();
                _this.isMoving = true;
                _this.isFinishedMoving = false;
                // Temporarily disable the transition to ensure a transition from and actual height value.
                _this.containerStyle.transitionDuration = '0';
            }), Observable$1.timer(20).switchMapTo(Observable$1.of(function () {
                // Set it to the current height first to ensure the animation on the 'height' property.
                _this.containerStyle.height = _this.container.offsetHeight + 'px';
                _this.containerStyle.transitionDuration = _this.transitionDuration;
            })), Observable$1.timer(20).switchMapTo(Observable$1.of(function () {
                _this.containerStyle.opacity = '1.0';
                _this.containerStyle.height = '0';
            })), Observable$1.timer(350).switchMapTo(Observable$1.of(function () {
                _this.isExpanded = false;
                _this.isFinishedMoving = true;
                _this.isMoving = false;
                _this.containerStyle.transitionDuration = '0';
            }))).subscribe(function (f) { return f(); });
        }
    };
    /**
     * @return {?}
     */
    UxCollapsibleDirective.prototype.expand = function () {
        var _this = this;
        if (!this.isExpanded) {
            if (this.animation != null) {
                this.animation.unsubscribe();
            }
            var /** @type {?} */ height_1 = 0;
            this.animation = Observable$1.concat(Observable$1.of(function () {
                _this.enableFocus();
                _this.isMoving = false;
                _this.isFinishedMoving = false;
                // Temporarily disable the transition to ensure a transition from an actual height value.
                _this.containerStyle.transitionDuration = '0';
                // Also make the content invisible to know the actual current height.
                _this.containerStyle.opacity = '0';
            }), Observable$1.timer(0).switchMapTo(Observable$1.of(function () {
                _this.containerStyle.height = 'auto';
                height_1 = _this.container.offsetHeight;
                _this.containerStyle.height = '0'; // Set it first back to 0 to ensure the animation on the 'height' property.
            })), Observable$1.timer(20).switchMapTo(Observable$1.of(function () {
                _this.containerStyle.transitionDuration = _this.transitionDuration;
                _this.containerStyle.opacity = '1.0';
            })), Observable$1.timer(20).switchMapTo(Observable$1.of(function () {
                _this.containerStyle.height = height_1 + 'px';
            })), Observable$1.timer(350).switchMapTo(Observable$1.of(function () {
                _this.isExpanded = true;
                _this.isFinishedMoving = true;
                _this.isMoving = false;
                _this.containerStyle.height = 'auto'; // Set it to auto to ensure a working resizing.
                _this.containerStyle.transitionDuration = '0';
            }))).subscribe(function (f) { return f(); });
        }
    };
    /**
     * @return {?}
     */
    UxCollapsibleDirective.prototype.disableFocus = function () {
        var /** @type {?} */ focusedElement = this.container.querySelector(':focus');
        if (focusedElement != null) {
            focusedElement.blur();
        }
        var /** @type {?} */ elements = this.container.querySelectorAll('*');
        for (var /** @type {?} */ i = 0; i < elements.length; i++) {
            var /** @type {?} */ element = elements[i];
            var /** @type {?} */ tabIndex = element.getAttribute('tabindex');
            if (tabIndex != null) {
                element.setAttribute('data-prev-tabindex', tabIndex);
            }
            element.setAttribute('tabindex', '-1');
        }
    };
    /**
     * @return {?}
     */
    UxCollapsibleDirective.prototype.enableFocus = function () {
        var /** @type {?} */ elements = this.container.querySelectorAll('*');
        for (var /** @type {?} */ i = 0; i < elements.length; i++) {
            var /** @type {?} */ element = elements[i];
            var /** @type {?} */ prevTabIndex = element.getAttribute('data-prev-tabindex');
            if (prevTabIndex != null) {
                element.setAttribute('tabindex', prevTabIndex);
            }
            else {
                element.removeAttribute('tabindex');
            }
        }
    };
    return UxCollapsibleDirective;
}());
UxCollapsibleDirective.decorators = [
    { type: Directive, args: [{
                selector: '[uxCollapsible]',
                exportAs: 'uxCollapsible'
            },] },
];
/**
 * @nocollapse
 */
UxCollapsibleDirective.ctorParameters = function () { return [
    { type: ElementRef, },
]; };
UxCollapsibleDirective.propDecorators = {
    'isFinishedMoving': [{ type: HostBinding, args: ['class.collapse',] },],
    'isExpanded': [{ type: HostBinding, args: ['class.in',] }, { type: HostBinding, args: ['attr.aria-expanded',] },],
    'isMoving': [{ type: HostBinding, args: ['class.collapsing',] },],
    'uxExpanded': [{ type: Input },],
    'uxCollapsed': [{ type: Input },],
};
var UxCollapsibleDirectiveModule = (function () {
    function UxCollapsibleDirectiveModule() {
    }
    return UxCollapsibleDirectiveModule;
}());
UxCollapsibleDirectiveModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxCollapsibleDirective],
                declarations: [UxCollapsibleDirective]
            },] },
];
/**
 * @nocollapse
 */
UxCollapsibleDirectiveModule.ctorParameters = function () { return []; };

var UxEuLanguages = (function () {
    function UxEuLanguages() {
    }
    /**
     * @param {?=} codes
     * @return {?}
     */
    UxEuLanguages.getLanguages = function (codes) {
        if (codes == null || codes.length <= 0) {
            codes = this.defaultCodes;
        }
        var /** @type {?} */ languages = [];
        for (var /** @type {?} */ c = 0; c < codes.length; c++) {
            var /** @type {?} */ language = this.languagesByCode[codes[c]];
            if (language != null) {
                languages.push(language);
            }
        }
        return languages;
    };
    return UxEuLanguages;
}());
UxEuLanguages.languagesByCode = {
    bg: {
        code: 'bg',
        label: 'български'
    },
    cs: {
        code: 'cs',
        label: 'čeština'
    },
    da: {
        code: 'da',
        label: 'dansk'
    },
    de: {
        code: 'de',
        label: 'Deutsch'
    },
    et: {
        code: 'et',
        label: 'eesti keel'
    },
    el: {
        code: 'el',
        label: 'ελληνικά'
    },
    en: {
        code: 'en',
        label: 'English'
    },
    es: {
        code: 'es',
        label: 'español'
    },
    fr: {
        code: 'fr',
        label: 'français'
    },
    ga: {
        code: 'ga',
        label: 'Gaeilge'
    },
    it: {
        code: 'it',
        label: 'italiano'
    },
    lv: {
        code: 'lv',
        label: 'latviešu valoda'
    },
    lt: {
        code: 'lt',
        label: 'lietuvių kalba'
    },
    hu: {
        code: 'hu',
        label: 'magyar'
    },
    mt: {
        code: 'mt',
        label: 'Malti'
    },
    nl: {
        code: 'nl',
        label: 'Nederlands'
    },
    pl: {
        code: 'pl',
        label: 'polski'
    },
    pt: {
        code: 'pt',
        label: 'português'
    },
    ro: {
        code: 'ro',
        label: 'română'
    },
    sk: {
        code: 'sk',
        label: 'slovenčina'
    },
    sl: {
        code: 'sl',
        label: 'slovenščina'
    },
    fi: {
        code: 'fi',
        label: 'suomi'
    },
    sv: {
        code: 'sv',
        label: 'svenska'
    },
    hr: {
        code: 'hr',
        label: 'hrvatski'
    },
    is: {
        code: 'is',
        label: 'íslenska'
    },
    mk: {
        code: 'mk',
        label: 'македонски'
    },
    no: {
        code: 'no',
        label: 'norsk'
    },
    tr: {
        code: 'tr',
        label: 'türkçe'
    }
};
UxEuLanguages.defaultCodes = [
    'bg', 'cs', 'da', 'de', 'et', 'el', 'en',
    'es', 'fr', 'ga', 'it', 'lv', 'lt', 'hu',
    'mt', 'nl', 'pl', 'pt', 'ro', 'sk', 'sl',
    'fi', 'sv', 'hr', 'is', 'mk', 'no', 'tr'
];

var UX_COLORS = {
    primary: '#004494',
    primaryDark: '#003675',
    primaryDarker: '#002857',
    primaryDarkest: '#001a38',
    primaryDarkester: '#000c1a',
    primaryLight: '#0052b3',
    primaryLighter: '#0060d1',
    primaryLightest: '#1a83ff',
    primaryLightester: '#52a1ff',
    primaryLightester2: '#9ecbff',
    accent: '#ffeb3b',
    accentDark: '#fdd835',
    accentDarker: '#fbc02d',
    accentDarkest: '#f9a825',
    accentDarkester: '#f57f17',
    accentLight: '#ffee58',
    accentLighter: '#fff176',
    accentLightest: '#fff59d',
    accentLightester: '#fff9c4',
    accentLightester2: '#fffde7',
    info: '#03a9f4',
    infoDark: '#039be5',
    infoDarker: '#0288d1',
    infoDarkest: '#0277bd',
    infoDarkester: '#01579b',
    infoLight: '#29b6f6',
    infoLighter: '#4fc3f7',
    infoLightest: '#81d4fa',
    infoLightester: '#b3e5fc',
    infoLightester2: '#e1f5fe',
    success: '#4caf50',
    successDark: '#43a047',
    successDarker: '#388e3c',
    successDarkest: '#2e7d32',
    successDarkester: '#1b5e20',
    successLight: '#66bb6a',
    successLighter: '#81c784',
    successLightest: '#a5d6a7',
    successLightester: '#c8e6c9',
    successLightester2: '#e8f5e9',
    warning: '#ff9800',
    warningDark: '#fb8c00',
    warningDarker: '#f57c00',
    warningDarkest: '#ef6c00',
    warningDarkester: '#e65100',
    warningLight: '#ffa726',
    warningLighter: '#ffb74d',
    warningLightest: '#ffcc80',
    warningLightester: '#fff3e0',
    warningLightester2: '#fff3e0',
    danger: '#f44336',
    dangerDark: '#e53935',
    dangerDarker: '#d32f2f',
    dangerDarkest: '#c62828',
    dangerDarkester: '#b71c1c',
    dangerLight: '#ef5350',
    dangerLighter: '#e57373',
    dangerLightest: '#ef9a9a',
    dangerLightester: '#ffcdd2',
    dangerLightester2: '#ffebee',
    grey: '#9e9e9e',
    greyDark: '#757575',
    greyDarker: '#616161',
    greyDarkest: '#424242',
    greyDarkester: '#212121',
    greyLight: '#bdbdbd',
    greyLighter: '#e0e0e0',
    greyLightest: '#eeeeee',
    greyLightester: '#f5f5f5',
    greyLightester2: '#fafafa'
};
var UX_I18N = {
    fr: {
        expandAll: 'Etendre tout',
        collapseAll: 'Fermer tout',
        filter: 'Filtrer',
        today: 'aujourd\'hui',
        oneDayAgo: 'il y a un jour',
        inOneDay: 'dans un jour',
        xDaysAgo: 'il y a %PARAM% jours',
        inXDays: 'dans %PARAM% jours',
        YES: 'OUI',
        NO: 'NON',
        viewAllNotifications: 'Voir toutes les notifications',
        myNotifications: 'Mes notifications',
        noNotificationFound: 'Pas de notification trouvée',
        notificationMarkAsRead: 'Marquer comme lu',
        notificationMarkAsUnRead: 'Marquer comme non-lu',
        notificationsToday: 'Notification du jour',
        notificationsOldest: 'Notifications précédentes',
        notificationsUnread: 'non lues'
    },
    en: {
        expandAll: 'Expand all',
        collapseAll: 'Collapse all',
        filter: 'Filter',
        today: 'today',
        oneDayAgo: 'one day ago',
        inOneDay: 'in one day',
        xDaysAgo: '%PARAM% days ago',
        inXDays: 'in %PARAM% days',
        YES: 'YES',
        NO: 'NO',
        viewAllNotifications: 'View all notifications',
        myNotifications: 'My notifications',
        noNotificationFound: 'No notification found',
        notificationMarkAsRead: 'Mark as read',
        notificationMarkAsUnRead: 'Mark as unread',
        notificationsToday: 'Today\'s notifications',
        notificationsOldest: 'Oldest notifications',
        notificationsUnread: 'unread'
    }
};

var UxModal = (function () {
    /**
     * @param {?=} values
     */
    function UxModal(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return UxModal;
}());
var UxService = (function () {
    /**
     * @param {?} router
     */
    function UxService(router) {
        var _this = this;
        this.router = router;
        this.isNavigationBlockDocumentActive = false;
        this.isLoggingActive = false;
        this.activeLanguage = UxEuLanguages.languagesByCode['en'];
        this.breadcrumbLinks = [];
        this.growlMessages = [];
        this.isGrowlSticky = false;
        this.growlLife = 3000;
        this.isBlockDocumentActive = false;
        this.isSidebarActive = false;
        this.isSidebarStateOpen = false;
        this.isSidebarAlwaysVisible = false;
        this.isSidebarCollapsible = false;
        this.isSidebarStateCloseWithIcons = false;
        this.isSidebarInnerActive = false;
        this.isSidebarMobileVisible = false;
        this.activeBreakpoint = new BehaviorSubject('');
        this.windowHeight = new BehaviorSubject(0);
        this.activeModals = [];
        this.sidebarLinks = [];
        router.events
            .subscribe(function (event) {
            if (event instanceof NavigationStart) {
                _this.navigationStartHandler(event);
            }
            if (event instanceof NavigationEnd) {
                _this.navigationEndHandler(event);
            }
        });
        console.log('UxService : Initiated');
    }
    Object.defineProperty(UxService.prototype, "appRouter", {
        /**
         * @return {?}
         */
        get: function () {
            return this.router;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} querySelector
     * @return {?}
     */
    UxService.prototype.shadowDomQuery = function (querySelector) {
        return this.shadowDomRoot.nativeElement.querySelector(querySelector);
    };
    /**
     * @param {?} links
     * @return {?}
     */
    UxService.prototype.setSidebarLinks = function (links) {
        this.sidebarLinks = links;
    };
    Object.defineProperty(UxService.prototype, "hasBreadcrumbLinks", {
        /**
         * @return {?}
         */
        get: function () {
            return this.breadcrumbLinks.length !== 0;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxService.prototype.clearBreadcrumbLinks = function () {
        this.breadcrumbLinks = [];
    };
    /**
     * @param {?} link
     * @return {?}
     */
    UxService.prototype.addBreadcrumbLink = function (link) {
        var /** @type {?} */ linkFound = false;
        var /** @type {?} */ indexFound = -1;
        this.breadcrumbLinks.forEach(function (linkItem, i) {
            if (linkItem.label === linkItem.label) {
                linkFound = true;
                indexFound = i;
            }
        });
        if (!linkFound) {
            this.breadcrumbLinks.push(link);
        }
    };
    /**
     * @param {?} msg
     * @param {?=} isSticky
     * @param {?=} isMultiple
     * @param {?=} life
     * @return {?}
     */
    UxService.prototype.growl = function (msg, isSticky, isMultiple, life) {
        if (msg.severity !== 'info' && msg.severity !== 'warning' && msg.severity !== 'success' && msg.severity !== 'danger') {
            throw new Error('UxService.growl() ERROR : message severity must be either : success,warning,info,danger');
        }
        else {
            if (isMultiple === undefined || !isMultiple) {
                this.growlMessages = [];
            }
            this.growlMessages = this.growlMessages.concat(msg);
            if (life === undefined) {
                this.growlLife = 3000;
            }
            else {
                this.growlLife = life;
            }
            if (isSticky) {
                this.isGrowlSticky = isSticky;
            }
            else {
                this.isGrowlSticky = false;
            }
        }
    };
    /**
     * @return {?}
     */
    UxService.prototype.clearGrowl = function () {
        this.growlMessages = [];
    };
    /**
     * @return {?}
     */
    UxService.prototype.blockDocument = function () {
        this.isBlockDocumentActive = true;
    };
    /**
     * @return {?}
     */
    UxService.prototype.unblockDocument = function () {
        this.isBlockDocumentActive = false;
    };
    /**
     * @param {?} width
     * @return {?}
     */
    UxService.prototype.setActiveBreakpoint = function (width) {
        this.windowWidth = width;
        switch (true) {
            case (width >= 0 && width < 576):
                this.activeBreakpoint.next('xs');
                break;
            case (width >= 576 && width < 768):
                this.activeBreakpoint.next('sm');
                break;
            case (width >= 768 && width < 992):
                this.activeBreakpoint.next('md');
                break;
            case (width >= 992 && width < 1200):
                this.activeBreakpoint.next('lg');
                break;
            case (width >= 1200):
                this.activeBreakpoint.next('xl');
                break;
        }
        console.log('UxService : activeBreakpoint : ' + this.activeBreakpoint.value + ' -- window width: ' + this.windowWidth);
    };
    /**
     * @return {?}
     */
    UxService.prototype.isMobile = function () {
        return (this.activeBreakpoint.value === 'xs' || this.activeBreakpoint.value === 'sm');
    };
    /**
     * @return {?}
     */
    UxService.prototype.isTablet = function () {
        return this.activeBreakpoint.value === 'md';
    };
    /**
     * @return {?}
     */
    UxService.prototype.isLtDesktop = function () {
        return (this.activeBreakpoint.value === 'xs'
            || this.activeBreakpoint.value === 'sm'
            || this.activeBreakpoint.value === 'md');
    };
    /**
     * @return {?}
     */
    UxService.prototype.isDesktop = function () {
        return this.activeBreakpoint.value === 'lg';
    };
    /**
     * @return {?}
     */
    UxService.prototype.isLargeDesktop = function () {
        return (this.activeBreakpoint.value === 'xl');
    };
    /**
     * @param {?} height
     * @return {?}
     */
    UxService.prototype.setWindowHeight = function (height) {
        this.windowHeight.next(height);
        console.log('UxService : windowHeight : ' + this.windowHeight.value);
    };
    /**
     * @param {?} key
     * @param {?=} languageCode
     * @return {?}
     */
    UxService.prototype.translate = function (key, languageCode) {
        var /** @type {?} */ code = 'en';
        if (languageCode) {
            code = languageCode;
        }
        else {
            if (this.activeLanguage) {
                code = this.activeLanguage.code;
            }
        }
        return UX_I18N[code][key];
    };
    /**
     * @param {?} dateStart
     * @param {?} dateEnd
     * @return {?}
     */
    UxService.prototype.diffDays = function (dateStart, dateEnd) {
        return Math.round((dateEnd.getTime() - dateStart.getTime()) / 1000 / 60 / 60 / 24);
    };
    /**
     * @param {?} date
     * @return {?}
     */
    UxService.prototype.diffDaysFromToday = function (date) {
        return this.diffDays(new Date(), date);
    };
    /**
     * @return {?}
     */
    UxService.prototype.resetSidebarClasses = function () {
        this.isSidebarActive = false;
        this.isSidebarStateOpen = false;
        this.isSidebarAlwaysVisible = false;
        this.isSidebarCollapsible = false;
        this.isSidebarStateCloseWithIcons = false;
        this.isSidebarInnerActive = false;
        this.isSidebarMobileVisible = false;
    };
    /**
     * @return {?}
     */
    UxService.prototype.toggleInnerSidebar = function () {
        this.isSidebarStateOpen = !this.isSidebarStateOpen;
    };
    /**
     * @param {?} error
     * @return {?}
     */
    UxService.prototype.handleError = function (error) {
        var /** @type {?} */ errMsg = (error.message) ? error.message :
            error.status ? error.status + " - " + error.statusText : 'Server error';
        console.error(errMsg);
        return Observable$1.throw(errMsg);
    };
    /**
     * @param {?=} modalId
     * @return {?}
     */
    UxService.prototype.openModal = function (modalId) {
        document.body.classList.add('modal-open');
        if (!modalId) {
            modalId = 'single_modal';
        }
        this.activeModals.push(new UxModal({
            id: modalId,
            isOpen: true
        }));
        setTimeout(function () {
            var /** @type {?} */ el = document.getElementById(modalId + '-close-button');
            if (el) {
                el.focus();
            }
        });
    };
    /**
     * @param {?=} messageBoxId
     * @return {?}
     */
    UxService.prototype.openMessageBox = function (messageBoxId) {
        if (!messageBoxId) {
            messageBoxId = 'messagebox_modal';
        }
        this.openModal(messageBoxId);
    };
    /**
     * @param {?=} messageBoxId
     * @return {?}
     */
    UxService.prototype.closeMessageBox = function (messageBoxId) {
        if (!messageBoxId) {
            messageBoxId = 'messagebox_modal';
        }
        this.closeModal(messageBoxId);
    };
    /**
     * @param {?=} modalId
     * @return {?}
     */
    UxService.prototype.isModalOpen = function (modalId) {
        if (!modalId) {
            modalId = 'single_modal';
        }
        if (this.activeModals.length) {
            var /** @type {?} */ modalIndex = this.findModalIndex(modalId);
            if (modalIndex !== -1) {
                return this.activeModals[modalIndex].isOpen;
            }
        }
        return false;
    };
    /**
     * @param {?=} modalId
     * @return {?}
     */
    UxService.prototype.closeModal = function (modalId) {
        if (!modalId) {
            modalId = 'single_modal';
        }
        this.activeModals.splice(this.findModalIndex(modalId), 1);
        document.body.classList.remove('modal-open');
    };
    /**
     * @param {?} modalId
     * @return {?}
     */
    UxService.prototype.findModalIndex = function (modalId) {
        var /** @type {?} */ index = -1;
        if (this.activeModals.length) {
            for (var /** @type {?} */ i = 0; i < this.activeModals.length; i++) {
                if (this.activeModals[i].id === modalId) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    };
    /**
     * Provides read-only equivalent of jQuery's position function:
    http://api.jquery.com/position/
     * @param {?} nativeEl
     * @return {?}
     */
    UxService.prototype.position = function (nativeEl) {
        var /** @type {?} */ elBCR = this.offset(nativeEl);
        var /** @type {?} */ offsetParentBCR = { top: 0, left: 0 };
        var /** @type {?} */ offsetParentEl = this.parentOffsetEl(nativeEl);
        if (offsetParentEl !== this.document) {
            offsetParentBCR = this.offset(offsetParentEl);
            offsetParentBCR.top += offsetParentEl.clientTop - offsetParentEl.scrollTop;
            offsetParentBCR.left += offsetParentEl.clientLeft - offsetParentEl.scrollLeft;
        }
        var /** @type {?} */ boundingClientRect = nativeEl.getBoundingClientRect();
        return {
            width: boundingClientRect.width || nativeEl.offsetWidth,
            height: boundingClientRect.height || nativeEl.offsetHeight,
            top: elBCR.top - offsetParentBCR.top,
            left: elBCR.left - offsetParentBCR.left
        };
    };
    /**
     * Provides read-only equivalent of jQuery's offset function:
    http://api.jquery.com/offset/
     * @param {?} nativeEl
     * @return {?}
     */
    UxService.prototype.offset = function (nativeEl) {
        var /** @type {?} */ boundingClientRect = nativeEl.getBoundingClientRect();
        return {
            width: boundingClientRect.width || nativeEl.offsetWidth,
            height: boundingClientRect.height || nativeEl.offsetHeight,
            top: boundingClientRect.top + (this.window.pageYOffset || this.document.documentElement.scrollTop),
            left: boundingClientRect.left + (this.window.pageXOffset || this.document.documentElement.scrollLeft)
        };
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxService.prototype.consumeEvent = function (event) {
        event.preventDefault();
        event.stopPropagation();
        event.cancelBubble = true;
        return false;
    };
    /**
     * @param {?} c
     * @return {?}
     */
    UxService.prototype.validateEmail = function (c) {
        /* tslint:disable */
        var /** @type {?} */ EMAIL_REGEXP = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        /* tslint:enable */
        return EMAIL_REGEXP.test(c.value) ? null : {
            validateEmail: {
                valid: false
            }
        };
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxService.prototype.navigationStartHandler = function (event) {
        /*
        if (this.isNavigationBlockDocumentActive) {
            this.blockDocument();
        }
        */
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxService.prototype.navigationEndHandler = function (event) {
        /*
        if (this.isNavigationBlockDocumentActive) {
            this.unblockDocument();
        }
        */
        window.scrollTo(0, 0);
    };
    Object.defineProperty(UxService.prototype, "window", {
        /**
         * @return {?}
         */
        get: function () {
            return window;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxService.prototype, "document", {
        /**
         * @return {?}
         */
        get: function () {
            return window.document;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} nativeEl
     * @param {?} cssProp
     * @return {?}
     */
    UxService.prototype.getStyle = function (nativeEl, cssProp) {
        // IE
        if (nativeEl.currentStyle) {
            return nativeEl.currentStyle[cssProp];
        }
        if (this.window.getComputedStyle) {
            return this.window.getComputedStyle(nativeEl)[cssProp];
        }
        // finally try and get inline style
        return nativeEl.style[cssProp];
    };
    /**
     * Checks if a given element is statically positioned
     * @param {?} nativeEl
     * @return {?}
     */
    UxService.prototype.isStaticPositioned = function (nativeEl) {
        return (this.getStyle(nativeEl, 'position') || 'static') === 'static';
    };
    /**
     * returns the closest, non-statically positioned parentOffset of a given element
    \@param nativeEl
     * @param {?} nativeEl
     * @return {?}
     */
    UxService.prototype.parentOffsetEl = function (nativeEl) {
        var /** @type {?} */ offsetParent = nativeEl.offsetParent || this.document;
        while (offsetParent && offsetParent !== this.document &&
            this.isStaticPositioned(offsetParent)) {
            offsetParent = offsetParent.offsetParent;
        }
        return offsetParent || this.document;
    };
    
    return UxService;
}());
UxService.decorators = [
    { type: Injectable },
];
/**
 * @nocollapse
 */
UxService.ctorParameters = function () { return [
    { type: Router, },
]; };

var UxModalComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxModalComponent(uxService) {
        this.uxService = uxService;
        this.dismissActionLabel = 'Cancel';
        this.dismissActionDisabled = false;
        this.acceptActionLabel = 'OK';
        this.acceptActionDisabled = false;
        this.isFooterVisible = true;
        this.isVisible = false;
        this.isKeepBodyScroll = false;
        this.isSizeSmall = false;
        this.isSizeLarge = false;
        this.isSizeFullScreen = false;
        this.isSizeFullHeight = false;
        this.isSizeMediumHeight = false;
        this.isShowActionIcons = false;
        this.acceptIconClass = 'fa-check';
        this.dismissIconClass = 'fa-close';
        this.hasNoBodyPadding = false;
        this.isFooterCustomAlignment = false;
        this.onDismiss = new EventEmitter();
        this.onAccept = new EventEmitter();
        // modal states
        this.stateClasses = '';
    }
    Object.defineProperty(UxModalComponent.prototype, "isModalOpen", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.uxService.isModalOpen(this.id)) {
                return true;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxModalComponent.prototype.ngAfterContentInit = function () {
        if (this.isVisible) {
            this.uxService.openModal(this.id);
        }
        // setting modal states
        if (this.isSizeSmall) {
            this.stateClasses = 'ux-modal--small';
        }
        if (this.isSizeLarge) {
            this.stateClasses = 'ux-modal--large';
        }
        if (this.isSizeFullScreen) {
            this.stateClasses = 'ux-modal--full-screen';
        }
        this.vcCloseButton.nativeElement.focus();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxModalComponent.prototype.close = function (event) {
        this.uxService.closeModal(this.id);
        this.onDismiss.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxModalComponent.prototype.closeModalOnEscape = function (event) {
        if (event.keyCode === 27) {
            this.close(event);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxModalComponent.prototype.onAcceptActionClick = function (event) {
        this.uxService.closeModal();
        this.onAccept.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxModalComponent.prototype.onDismissActionClick = function (event) {
        this.uxService.closeModal();
        this.close(event);
    };
    return UxModalComponent;
}());
UxModalComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-modal',
                template: "\n        <div id=\"{{id}}\" class=\"modal fade ux-modal {{styleClass}} {{stateClasses}}\" [class.show]=\"isModalOpen\"\n                                         [ngClass]=\"{'in': isModalOpen}\" tabindex=\"-1\"\n                                         role=\"dialog\"\n                                         [class.ux-modal--full-height]=\"isSizeFullHeight\"\n                                         [class.ux-modal--medium-height]=\"isSizeMediumHeight\"\n                                         [attr.aria-hidden]=\"!isModalOpen\"\n                                         [style.display]=\"isModalOpen ? 'block' : 'none'\"\n                                         (keydown)=\"closeModalOnEscape($event)\" tabIndex=\"-1\">\n            <div class=\"modal-dialog ux-modal__dialog\" role=\"document\" [style.max-width]=\"customWidth\">\n              <div class=\"modal-content ux-modal__content\">\n                <div class=\"modal-header ux-modal__header\">\n                  <h5 class=\"modal-title ux-modal__header-title\">{{title}}</h5>\n                  <button #closeButton type=\"button\" id=\"{{id}}-close-button\"\n                          class=\"ux-modal__header-close close\" data-dismiss=\"modal\" aria-label=\"Close\" (click)=\"close($event)\">\n                    <span aria-hidden=\"true\">&times;</span>\n                  </button>\n                </div>\n                <div class=\"modal-body ux-modal__body\" [class.p-0]=\"hasNoBodyPadding\">\n                  <ng-content select=\"uxModalBody\"></ng-content>\n                </div>\n                <div *ngIf=\"isFooterVisible\" class=\"modal-footer ux-modal__footer\"\n                                             [class.ux-modal__footer--custom-alignment]=\"isFooterCustomAlignment\">\n                  <template [ngIf]=\"customFooterContent\">\n                      <ng-content select=\"uxModalFooter\"></ng-content>\n                  </template>\n                  <template [ngIf]=\"!customFooterContent\">\n                      <button class=\"btn btn-secondary\" [disabled]=\"dismissActionDisabled\" (click)=\"onDismissActionClick($event)\" >\n                        <i *ngIf=\"isShowActionIcons\" class=\"fa {{dismissIconClass}}\"></i>\n                        {{dismissActionLabel}}\n                      </button>\n                      <button class=\"btn btn-primary\" [disabled]=\"acceptActionDisabled\" (click)=\"onAcceptActionClick($event)\">\n                        <i *ngIf=\"isShowActionIcons\" class=\"fa {{acceptIconClass}}\"></i>\n                        {{acceptActionLabel}}\n                      </button>\n                  </template>\n                </div>\n              </div>\n            </div>\n        </div>\n        <div class=\"modal-backdrop\" [class.show]=\"isModalOpen\"\n                                    [ngClass]=\"{fade: isModalOpen, in: isModalOpen}\"\n                                    [style.display]=\"isModalOpen ? 'block' : 'none'\" (focus)=\"close($event)\"></div>\n        "
            },] },
];
/**
 * @nocollapse
 */
UxModalComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxModalComponent.propDecorators = {
    'id': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'title': [{ type: Input },],
    'dismissActionLabel': [{ type: Input },],
    'dismissActionDisabled': [{ type: Input },],
    'acceptActionLabel': [{ type: Input },],
    'acceptActionDisabled': [{ type: Input },],
    'isFooterVisible': [{ type: Input },],
    'isVisible': [{ type: Input },],
    'isKeepBodyScroll': [{ type: Input },],
    'isSizeSmall': [{ type: Input },],
    'isSizeLarge': [{ type: Input },],
    'isSizeFullScreen': [{ type: Input },],
    'isSizeFullHeight': [{ type: Input },],
    'isSizeMediumHeight': [{ type: Input },],
    'isShowActionIcons': [{ type: Input },],
    'acceptIconClass': [{ type: Input },],
    'dismissIconClass': [{ type: Input },],
    'hasNoBodyPadding': [{ type: Input },],
    'customWidth': [{ type: Input },],
    'isFooterCustomAlignment': [{ type: Input },],
    'onDismiss': [{ type: Output },],
    'onAccept': [{ type: Output },],
    'vcCloseButton': [{ type: ViewChild, args: ['closeButton',] },],
    'customFooterContent': [{ type: ContentChild, args: [forwardRef(function () { return UxModalFooterTagDirective; }),] },],
};
var UxModalBodyTagDirective = (function () {
    function UxModalBodyTagDirective() {
    }
    return UxModalBodyTagDirective;
}());
UxModalBodyTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxModalBody' },] },
];
/**
 * @nocollapse
 */
UxModalBodyTagDirective.ctorParameters = function () { return []; };
var UxModalFooterTagDirective = (function () {
    function UxModalFooterTagDirective() {
    }
    return UxModalFooterTagDirective;
}());
UxModalFooterTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxModalFooter' },] },
];
/**
 * @nocollapse
 */
UxModalFooterTagDirective.ctorParameters = function () { return []; };
var UxModalComponentModule = (function () {
    function UxModalComponentModule() {
    }
    return UxModalComponentModule;
}());
UxModalComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxModalComponent, UxModalBodyTagDirective, UxModalFooterTagDirective],
                declarations: [UxModalComponent, UxModalBodyTagDirective, UxModalFooterTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxModalComponentModule.ctorParameters = function () { return []; };

var UxLanguageSelectorComponent = (function () {
    /**
     * @param {?} shadowDomRoot
     * @param {?} uxService
     */
    function UxLanguageSelectorComponent(shadowDomRoot, uxService) {
        this.shadowDomRoot = shadowDomRoot;
        this.uxService = uxService;
        this.selectedLanguage = UxEuLanguages.languagesByCode['en'];
        this.isShowLabel = true;
        this.languageChanged = new EventEmitter();
        this.modalId = 'ux-language-selector-modal';
        this.modalIsOpen = false;
        this.dropDownIsOpen = false;
    }
    Object.defineProperty(UxLanguageSelectorComponent.prototype, "isShowDropDown", {
        /**
         * @return {?}
         */
        get: function () {
            return this.languages.length > 1 && this.languages.length < 4;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxLanguageSelectorComponent.prototype, "isShowModal", {
        /**
         * @return {?}
         */
        get: function () {
            return this.languages.length >= 4;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.ngOnInit = function () {
        if (this.languageCodes != null) {
            var /** @type {?} */ codes = this.languageCodes.split(/[ ,]+/g);
            this.languages = UxEuLanguages.getLanguages(codes);
        }
        else {
            this.languages = UxEuLanguages.getLanguages();
        }
        this.languageRows = this.prepareLanguageRows();
    };
    /**
     * @param {?} querySelector
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.shadowDomQuery = function (querySelector) {
        return this.shadowDomRoot.nativeElement.querySelector(querySelector);
    };
    /**
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.prepareLanguageRows = function () {
        var /** @type {?} */ rows = [];
        var /** @type {?} */ row = [];
        for (var /** @type {?} */ i = 0; i < this.languages.length; i++) {
            if (i % 4 === 0) {
                if (row.length > 0) {
                    rows.push(row);
                    row = [];
                }
            }
            row.push(this.languages[i]);
        }
        if (row.length > 0) {
            rows.push(row);
        }
        return rows;
    };
    /**
     * @param {?} languageCode
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.selectLanguage = function (languageCode) {
        if (this.selectedLanguage != null && this.selectedLanguage.code !== languageCode) {
            this.selectedLanguage = UxEuLanguages.languagesByCode[languageCode];
            this.languageChanged.emit(this.selectedLanguage);
        }
        this.close();
    };
    /**
     * @param {?} keyCode
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.openKeyDown = function (keyCode) {
        if (keyCode === 13) {
            this.open();
        }
    };
    /**
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.open = function () {
        var _this = this;
        this.uxService.openModal(this.modalId);
        setTimeout(function () {
            _this.shadowDomQuery('.btn-primary').focus();
        }, 0);
    };
    /**
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.close = function () {
        this.uxService.closeModal(this.modalId);
    };
    /**
     * @param {?} keyCode
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.closeModalOnEscape = function (keyCode) {
        if (keyCode === 27) {
            this.close();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.consumeEvent = function (event) {
        event.preventDefault();
        event.stopPropagation();
        event.cancelBubble = true;
        return false;
    };
    /**
     * @param {?} keyCode
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.toggleDropDownKeyDown = function (keyCode) {
        if (keyCode === 13) {
            this.toggleDropDown();
        }
    };
    /**
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.toggleDropDown = function () {
        this.dropDownIsOpen = !this.dropDownIsOpen;
        if (this.dropDownIsOpen) {
            var /** @type {?} */ selectedItem_1 = this.shadowDomQuery('.dropdown-item[data-language-code=' + this.selectedLanguage.code + ']');
            if (selectedItem_1 != null) {
                setTimeout(function () {
                    selectedItem_1.focus();
                }, 100);
            }
        }
    };
    /**
     * @param {?} languageCode
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.selectDropdownLanguage = function (languageCode) {
        this.selectLanguage(languageCode);
        this.dropDownIsOpen = false;
    };
    /**
     * @param {?} keyCode
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.closeDropDownOnEscape = function (keyCode) {
        if (keyCode === 27) {
            this.dropDownIsOpen = false;
        }
    };
    /**
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.closeOnBlur = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.shadowDomQuery('.dropdown-item:focus') == null) {
                _this.dropDownIsOpen = false;
            }
        }, 0);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxLanguageSelectorComponent.prototype.navigateDropDownSelection = function (event) {
        var /** @type {?} */ keyCode = event.keyCode;
        /*
         38 = arrow up
         40 = arrow down
         */
        var /** @type {?} */ focusedItem = this.shadowDomQuery('.dropdown-item:focus');
        if (focusedItem != null) {
            var /** @type {?} */ next = void 0;
            if (keyCode === 40) {
                next = ((focusedItem)).nextElementSibling;
                if (!next) {
                    next = ((((focusedItem)).parentNode)).firstElementChild;
                }
            }
            else if (keyCode === 38) {
                next = ((focusedItem)).previousElementSibling;
                if (!next) {
                    next = ((((focusedItem)).parentNode)).lastElementChild;
                }
            }
            if (next) {
                ((next)).focus();
                this.consumeEvent(event);
            }
        }
    };
    return UxLanguageSelectorComponent;
}());
UxLanguageSelectorComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-language-selector',
                template: "\n        <div class=\"ux-language-selector\" *ngIf=\"isShowDropDown\">\n            <div class=\"btn-group\">\n                <a class=\"language-toggle\" tabindex=\"1\" aria-haspopup=\"true\"\n                         [attr.aria-expanded]=\"!dropDownIsOpen\"\n                         (click)=\"toggleDropDown()\" (keydown)=\"toggleDropDownKeyDown($event.keyCode)\">\n                    <span *ngIf=\"isShowLabel\" class=\"language-label\">{{selectedLanguage.label}}</span>\n                    <span class=\"language-code\">{{selectedLanguage.code | uppercase}}</span>\n                </a>\n                <div class=\"dropdown-menu\" [style.display]=\"dropDownIsOpen ? 'block' : 'none'\"\n                     (keydown)=\"closeDropDownOnEscape($event.keyCode)\">\n                    <a *ngFor=\"let language of languages\" href=\"javascript:void(0);\"\n                       class=\"dropdown-item\" [attr.data-language-code]=\"language.code\"\n                       (click)=\"selectDropdownLanguage(language.code)\"\n                       (blur)=\"closeOnBlur()\"\n                       (keydown)=\"navigateDropDownSelection($event)\" >\n                        <label>{{language.label}} ({{language.code}})</label>\n                    </a>\n                </div>\n            </div>\n        </div>\n\n\n        <div class=\"ux-language-selector\" *ngIf=\"isShowModal\">\n            <a class=\"language-toggle\" tabindex=\"1\" (click)=\"open()\" (keydown)=\"openKeyDown($event.keyCode)\">\n                <span *ngIf=\"isShowLabel\" class=\"language-label\">{{selectedLanguage.label}}</span>\n                <span class=\"language-code\">{{selectedLanguage.code | uppercase}}</span>\n            </a>\n\n            <ux-modal id=\"{{modalId}}\" title=\"Select language\" [isFooterVisible]=\"false\" styleClass=\"ux-language-selector-modal\">\n              <uxModalBody>\n                <div *ngFor=\"let row of languageRows\" class=\"row\">\n                    <div *ngFor=\"let language of row\" class=\"col-md-3 col-sm-6\">\n                        <button class=\"btn\"\n                                [class.btn-secondary]=\"language.code != selectedLanguage.code\"\n                                [class.btn-primary]=\"language.code == selectedLanguage.code\"\n                                (click)=\"selectLanguage(language.code)\"\n                                (keydown)=\"navigateModalSelection($event)\">\n                            <label>{{language.label}} ({{language.code}})</label>\n                        </button>\n                    </div>\n                </div>\n              </uxModalBody>\n            </ux-modal>\n\n        </div>\n\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLanguageSelectorComponent.ctorParameters = function () { return [
    { type: ElementRef, },
    { type: UxService, },
]; };
UxLanguageSelectorComponent.propDecorators = {
    'languageCodes': [{ type: Input },],
    'selectedLanguage': [{ type: Input },],
    'isShowLabel': [{ type: Input },],
    'languageChanged': [{ type: Output },],
};
var UxLanguageSelectorComponentModule = (function () {
    function UxLanguageSelectorComponentModule() {
    }
    return UxLanguageSelectorComponentModule;
}());
UxLanguageSelectorComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxModalComponentModule],
                exports: [UxLanguageSelectorComponent],
                declarations: [UxLanguageSelectorComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLanguageSelectorComponentModule.ctorParameters = function () { return []; };

var UxLanguage = (function () {
    function UxLanguage() {
    }
    return UxLanguage;
}());

var UxSearchInputComponent = (function () {
    /**
     * @param {?} shadowDomRoot
     */
    function UxSearchInputComponent(shadowDomRoot) {
        var _this = this;
        this.shadowDomRoot = shadowDomRoot;
        this.minCharacters = 3;
        this.maxHistoryEntries = 3;
        this.keyboardDebounceTime = 200;
        this.historicalSearches = [];
        this.suggestionsService = {
            /**
             * @param {?} query
             * @return {?}
             */
            getSuggestions: function (query) {
                return new Observable$1(function (observer) {
                    observer.next([]);
                    observer.complete();
                });
            }
        };
        this.search = new EventEmitter();
        this.query = '';
        this.autocompleteLoading = false;
        this.historicalSearchesVisible = true;
        this.suggestionsVisible = false;
        this.noSuggestionsFound = false;
        this.ignoreInputBlur = false;
        this.control = new FormControl();
        this.control.valueChanges.debounceTime(this.keyboardDebounceTime).subscribe(function (query) {
            _this.query = query;
            _this.getSuggestions();
        });
    }
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.ngAfterContentInit = function () {
        this.searchInput = (this.shadowDomQuery('input'));
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.getQuery = function () {
        return this.query;
    };
    /**
     * @param {?} query
     * @return {?}
     */
    UxSearchInputComponent.prototype.setQuery = function (query) {
        this.query = query;
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.focus = function () {
        this.searchInput.focus();
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.focusAndSelect = function () {
        this.focus();
        this.searchInput.select();
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.hideSuggestions = function () {
        this.suggestionsVisible = false;
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.showSuggestions = function () {
        var /** @type {?} */ show = true;
        // The suggestions should not be shown when:
        // 1. There are no visible suggestions.
        var /** @type {?} */ firstVisibleSuggestion = this.shadowDomQuery('.dropdown-item:not([hidden])');
        show = show && (firstVisibleSuggestion != null);
        // 2. The focus is no longer on the input field.
        show = show && (this.searchInput === this.shadowDomQuery(':focus'));
        if (show) {
            this.suggestionsVisible = true;
        }
        this.inactivateActiveSuggestion();
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.startSearch = function () {
        this.storeHistoricalSearch();
        this.search.emit(this.query);
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.showHistoricalSearches = function () {
        this.suggestions = [];
        this.historicalSearchesVisible = true;
        this.noSuggestionsFound = this.historicalSearches.length <= 0;
        this.showSuggestions();
    };
    /**
     * @param {?} querySelector
     * @return {?}
     */
    UxSearchInputComponent.prototype.shadowDomQuery = function (querySelector) {
        return this.shadowDomRoot.nativeElement.querySelector(querySelector);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxSearchInputComponent.prototype.consumeEvent = function (event) {
        event.preventDefault();
        event.stopPropagation();
        event.cancelBubble = true;
        return false;
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.ignoreBlur = function () {
        this.ignoreInputBlur = true;
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.allowBlur = function () {
        var _this = this;
        // IE issue: let the focus() on an element first be completed before the blur() can be triggered.
        setTimeout(function () {
            _this.ignoreInputBlur = false;
        }, 0);
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.searchInputBlur = function () {
        if (!this.ignoreInputBlur) {
            this.hideSuggestions();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxSearchInputComponent.prototype.hideSuggestionOnEscape = function (event) {
        var /** @type {?} */ keyCode = event.keyCode;
        if (keyCode === 27) {
            this.hideSuggestions();
            this.focus();
            this.consumeEvent(event);
        }
    };
    /**
     * @param {?} menuItem
     * @return {?}
     */
    UxSearchInputComponent.prototype.focusOnMenuItem = function (menuItem) {
        this.ignoreBlur();
        menuItem.focus();
        this.allowBlur();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxSearchInputComponent.prototype.searchInputKeydown = function (event) {
        var _this = this;
        var /** @type {?} */ keyCode = event.keyCode;
        if (keyCode === 40 || keyCode === 38) {
            this.focusNextSuggestion(event);
        }
        else if (keyCode === 13) {
            // Wait for field value to update:
            setTimeout(function () {
                _this.startSearch();
            }, this.keyboardDebounceTime);
        }
        else {
            this.hideSuggestionOnEscape(event);
        }
    };
    /**
     * @param {?} activeSuggestion
     * @return {?}
     */
    UxSearchInputComponent.prototype.focusFirstSuggestion = function (activeSuggestion) {
        var _this = this;
        if (!this.noSuggestionsFound) {
            if (activeSuggestion == null) {
                var /** @type {?} */ firstSuggestion = this.shadowDomQuery('.dropdown-item:not([hidden])');
                if (firstSuggestion != null && !firstSuggestion.classList.contains('empty')) {
                    firstSuggestion.classList.add('active');
                    this.focusOnMenuItem(firstSuggestion);
                }
            }
            else {
                setTimeout(function () {
                    _this.focusOnMenuItem(activeSuggestion);
                }, 0);
            }
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxSearchInputComponent.prototype.focusNextSuggestion = function (event) {
        var /** @type {?} */ activeSuggestion = this.getActiveSuggestion();
        if (!this.suggestionsVisible) {
            this.showSuggestions();
            this.focusFirstSuggestion(activeSuggestion);
        }
        else {
            if (activeSuggestion != null) {
                // focus next suggestion:
                var /** @type {?} */ nextSuggestion = (activeSuggestion.nextElementSibling);
                if (nextSuggestion != null && !nextSuggestion.classList.contains('empty')) {
                    activeSuggestion.classList.remove('active');
                    nextSuggestion.classList.add('active');
                    this.ignoreBlur();
                    nextSuggestion.focus();
                    this.allowBlur();
                }
            }
            else {
                this.focusFirstSuggestion(activeSuggestion);
            }
        }
        this.consumeEvent(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxSearchInputComponent.prototype.focusPreviousSuggestion = function (event) {
        var /** @type {?} */ activeSuggestion = this.getActiveSuggestion();
        if (!this.suggestionsVisible) {
            this.showSuggestions();
            this.focusFirstSuggestion(activeSuggestion);
        }
        else {
            if (activeSuggestion != null) {
                // focus next suggestion:
                var /** @type {?} */ previousSuggestion = (activeSuggestion.previousElementSibling);
                if (previousSuggestion != null && !previousSuggestion.classList.contains('empty')) {
                    activeSuggestion.classList.remove('active');
                    previousSuggestion.classList.add('active');
                    this.ignoreBlur();
                    previousSuggestion.focus();
                    this.allowBlur();
                }
            }
            else {
                this.focusFirstSuggestion(activeSuggestion);
            }
        }
        this.consumeEvent(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxSearchInputComponent.prototype.suggestionKeydown = function (event) {
        var /** @type {?} */ keyCode = event.keyCode;
        if (keyCode === 40) {
            this.focusNextSuggestion(event);
        }
        else if (keyCode === 38) {
            this.focusPreviousSuggestion(event);
        }
        else if (keyCode === 13) {
            this.selectActiveSuggestion();
        }
        else if (keyCode === 8) {
            this.consumeEvent(event);
        }
        this.hideSuggestionOnEscape(event);
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.suggestionBlur = function () {
        if (!this.ignoreInputBlur) {
            this.hideSuggestions();
        }
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.selectActiveSuggestion = function () {
        var _this = this;
        var /** @type {?} */ activeSuggestion = this.getActiveSuggestion();
        if (activeSuggestion != null) {
            var /** @type {?} */ suggestion_1 = activeSuggestion.textContent;
            // Wait for input event to finish:
            setTimeout(function () {
                _this.hideSuggestions();
                _this.focus();
                _this.query = (suggestion_1);
                _this.startSearch();
            }, 0);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxSearchInputComponent.prototype.suggestionClicked = function (event) {
        var _this = this;
        var /** @type {?} */ clickedSuggestion = (event.currentTarget);
        if (clickedSuggestion != null) {
            this.inactivateActiveSuggestion();
            clickedSuggestion.classList.add('active');
            // Wait for input event to finish:
            setTimeout(function () {
                _this.hideSuggestions();
                _this.focus();
                _this.query = (clickedSuggestion.textContent);
                _this.startSearch();
            }, 0);
        }
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.getActiveSuggestion = function () {
        return this.shadowDomQuery('.dropdown-item.active');
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.inactivateActiveSuggestion = function () {
        var /** @type {?} */ activeSuggestion = this.getActiveSuggestion();
        if (activeSuggestion != null) {
            activeSuggestion.classList.remove('active');
        }
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.storeHistoricalSearch = function () {
        // Remove existing entry:
        var /** @type {?} */ search = this.query;
        if (search !== null && search.trim().length > 0) {
            var /** @type {?} */ index = this.historicalSearches.indexOf(search, 0);
            if (index > -1) {
                this.historicalSearches.splice(index, 1);
            }
            this.historicalSearches.unshift(search);
            if (this.historicalSearches.length > this.maxHistoryEntries) {
                this.historicalSearches.splice(this.maxHistoryEntries);
            }
        }
    };
    /**
     * @return {?}
     */
    UxSearchInputComponent.prototype.getSuggestions = function () {
        var _this = this;
        if (this.query.length >= this.minCharacters) {
            this.hideSuggestions();
            this.noSuggestionsFound = false;
            this.autocompleteLoading = true;
            this.suggestions = [];
            this.suggestionsService.getSuggestions(this.query).subscribe(function (suggestions) {
                _this.suggestions = suggestions;
                _this.noSuggestionsFound = (_this.suggestions == null || _this.suggestions.length <= 0);
                _this.autocompleteLoading = false;
                _this.historicalSearchesVisible = false;
                setTimeout(function () {
                    _this.showSuggestions();
                }, 0);
            });
        }
        else if (this.query.trim().length <= 0) {
            this.showHistoricalSearches();
        }
    };
    return UxSearchInputComponent;
}());
UxSearchInputComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-search-input',
                template: "\n\n        <div class=\"ux-search-input-container\">\n            <input type=\"search\" class=\"form-control\" [value]=\"query\" \n                   [formControl]=\"control\" (blur)=\"searchInputBlur()\" \n                   (keydown)=\"searchInputKeydown($event)\" />\n            <div class=\"dropdown-menu-container\">\n                <div class=\"dropdown-menu\" [hidden]=\"! suggestionsVisible\">\n                    <div *ngFor=\"let historicalSearch of historicalSearches\" \n                         class=\"dropdown-item historical\" tabindex=\"0\" \n                         [hidden]=\"! historicalSearchesVisible\" \n                         (keydown)=\"suggestionKeydown($event)\" \n                         (mousedown)=\"suggestionClicked($event)\" \n                         (blur)=\"suggestionBlur()\">\n                         {{historicalSearch}}\n                   </div>\n                    <div *ngFor=\"let suggestion of suggestions\" class=\"dropdown-item\" tabindex=\"0\" \n                         (keydown)=\"suggestionKeydown($event)\" \n                         (mousedown)=\"suggestionClicked($event)\" \n                         (blur)=\"suggestionBlur()\">\n                         {{suggestion}}\n                    </div>\n                    <div class=\"dropdown-item empty\" [hidden]=\"! noSuggestionsFound\">\n                        No suggestions found.\n                    </div>\n                </div>\n            </div>\n            <div class=\"spinner\" [hidden]=\"! autocompleteLoading\">\n                <div class=\"spinner-item1\"></div>\n                <div class=\"spinner-item2\"></div>\n                <div class=\"spinner-item3\"></div>\n            </div>\n        </div>\n\n\n    "
            },] },
];
/**
 * @nocollapse
 */
UxSearchInputComponent.ctorParameters = function () { return [
    { type: ElementRef, },
]; };
UxSearchInputComponent.propDecorators = {
    'minCharacters': [{ type: Input },],
    'maxHistoryEntries': [{ type: Input },],
    'keyboardDebounceTime': [{ type: Input },],
    'historicalSearches': [{ type: Input },],
    'suggestionsService': [{ type: Input },],
    'search': [{ type: Output },],
};
var UxSearchInputComponentModule = (function () {
    function UxSearchInputComponentModule() {
    }
    return UxSearchInputComponentModule;
}());
UxSearchInputComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, ReactiveFormsModule],
                exports: [UxSearchInputComponent],
                declarations: [UxSearchInputComponent]
            },] },
];
/**
 * @nocollapse
 */
UxSearchInputComponentModule.ctorParameters = function () { return []; };

var UxTagComponent = (function () {
    function UxTagComponent() {
        this.isRounded = false;
        this.isSmall = false;
    }
    /**
     * @return {?}
     */
    UxTagComponent.prototype.ngAfterContentInit = function () {
        this.stateClasses = '';
        if (this.typeClass) {
            this.stateClasses += 'ux-a-tag--' + this.typeClass + ' ';
        }
        if (!this.subLabel && this.isRounded) {
            this.stateClasses += 'ux-a-tag--rounded ';
        }
        if (this.isSmall) {
            this.stateClasses += 'ux-a-tag--small';
        }
    };
    return UxTagComponent;
}());
UxTagComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-a-tag',
                template: "\n        <div *ngIf=\"label\" class=\"ux-a-tag {{stateClasses}} {{styleClass}}\">\n            <span class=\"ux-a-tag__label\">\n                {{label}}\n            </span>\n            <span *ngIf=\"subLabel\" class=\"ux-a-tag__sub-label\">\n                {{subLabel}}\n            </span>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTagComponent.ctorParameters = function () { return []; };
UxTagComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'isRounded': [{ type: Input },],
    'isSmall': [{ type: Input },],
};
var UxTagComponentModule = (function () {
    function UxTagComponentModule() {
    }
    return UxTagComponentModule;
}());
UxTagComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxTagComponent],
                declarations: [UxTagComponent]
            },] },
];
/**
 * @nocollapse
 */
UxTagComponentModule.ctorParameters = function () { return []; };

var UxIconComponent = (function () {
    function UxIconComponent() {
        this.isRounded = true;
        this.isLarge = false;
    }
    /**
     * @return {?}
     */
    UxIconComponent.prototype.ngAfterContentInit = function () {
        this.iconStateClasses = '';
        if (this.iconClass) {
            if (this.isRounded) {
                if (this.isLarge) {
                    this.iconStateClasses += 'ux-a-icon--rounded-large ';
                }
                else {
                    this.iconStateClasses += 'ux-a-icon--rounded ';
                }
            }
            else {
                if (this.isLarge) {
                    this.iconStateClasses += 'ux-a-icon--large ';
                }
            }
            if (this.typeClass) {
                this.iconStateClasses += 'ux-a-icon--' + this.typeClass;
            }
        }
    };
    return UxIconComponent;
}());
UxIconComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-a-icon',
                template: "\n        <div *ngIf=\"iconClass\" class=\"ux-a-icon {{iconStateClasses}} {{styleClass}}\">\n           <span class=\"fa {{iconClass}} ux-a-icon__icon\"></span>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxIconComponent.ctorParameters = function () { return []; };
UxIconComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'isRounded': [{ type: Input },],
    'isLarge': [{ type: Input },],
};
var UxIconComponentModule = (function () {
    function UxIconComponentModule() {
    }
    return UxIconComponentModule;
}());
UxIconComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxIconComponent],
                declarations: [UxIconComponent]
            },] },
];
/**
 * @nocollapse
 */
UxIconComponentModule.ctorParameters = function () { return []; };

var UxIconToggleComponent = (function () {
    function UxIconToggleComponent() {
        this.isChecked = false;
        this.isReadOnly = false;
        this.toggle = new EventEmitter();
    }
    /**
     * @return {?}
     */
    UxIconToggleComponent.prototype.ngAfterContentInit = function () {
        this.setIconClass();
    };
    /**
     * @return {?}
     */
    UxIconToggleComponent.prototype.onToggle = function () {
        if (!this.isReadOnly) {
            this.isChecked = !this.isChecked;
            this.setIconClass();
            this.toggle.emit(this.isChecked);
        }
    };
    /**
     * @return {?}
     */
    UxIconToggleComponent.prototype.setIconClass = function () {
        if (this.isChecked) {
            this.iconClass = this.iconClassOn;
            this.stateClass = 'ux-u-color-accent-dark';
        }
        else {
            this.iconClass = this.iconClassOff;
            this.stateClass = 'ux-u-color-grey-dark';
        }
    };
    return UxIconToggleComponent;
}());
UxIconToggleComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-a-icon-toggle',
                template: "\n        <span class=\"ux-a-icon-toggle {{stateClass}}\">\n           <span class=\"fa {{iconClass}} ux-a-icon__icon\" [class.ux-u-cursor-pointer]=\"!isReadOnly\" (click)=\"onToggle()\"></span>\n        </span>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxIconToggleComponent.ctorParameters = function () { return []; };
UxIconToggleComponent.propDecorators = {
    'iconClassOff': [{ type: Input },],
    'iconClassOn': [{ type: Input },],
    'isChecked': [{ type: Input },],
    'isReadOnly': [{ type: Input },],
    'toggle': [{ type: Output },],
};
var UxIconToggleComponentModule = (function () {
    function UxIconToggleComponentModule() {
    }
    return UxIconToggleComponentModule;
}());
UxIconToggleComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxIconToggleComponent],
                declarations: [UxIconToggleComponent]
            },] },
];
/**
 * @nocollapse
 */
UxIconToggleComponentModule.ctorParameters = function () { return []; };

var UxIconBoxComponent = (function () {
    function UxIconBoxComponent() {
    }
    return UxIconBoxComponent;
}());
UxIconBoxComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-a-icon-box',
                template: "\n        <div class=\"ux-a-icon-box {{styleClass}}\">\n            <div class=\"ux-a-icon-box__icon\">\n                <span class=\"fa {{iconClass}}\"></span>\n            </div>\n            <div class=\"ux-a-icon-box__label\">\n                {{label}}\n            </div>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxIconBoxComponent.ctorParameters = function () { return []; };
UxIconBoxComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'label': [{ type: Input },],
};
var UxIconBoxComponentModule = (function () {
    function UxIconBoxComponentModule() {
    }
    return UxIconBoxComponentModule;
}());
UxIconBoxComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxIconBoxComponent],
                declarations: [UxIconBoxComponent]
            },] },
];
/**
 * @nocollapse
 */
UxIconBoxComponentModule.ctorParameters = function () { return []; };

var UxMarkerComponent = (function () {
    function UxMarkerComponent() {
    }
    return UxMarkerComponent;
}());
UxMarkerComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-a-marker',
                template: "\n        <div class=\"ux-a-marker ux-a-marker--{{typeClass}} {{styleClass}}\">\n            <span class=\"fa fa-circle ux-a-marker__icon\"></span>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxMarkerComponent.ctorParameters = function () { return []; };
UxMarkerComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
};
var UxMarkerComponentModule = (function () {
    function UxMarkerComponentModule() {
    }
    return UxMarkerComponentModule;
}());
UxMarkerComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxMarkerComponent],
                declarations: [UxMarkerComponent]
            },] },
];
/**
 * @nocollapse
 */
UxMarkerComponentModule.ctorParameters = function () { return []; };

var UxBadgeComponent = (function () {
    function UxBadgeComponent() {
        this.typeClass = 'default';
        this.isSmall = false;
        this.isLarge = false;
        this.isTiny = false;
        this.isPill = false;
        this.isOutline = false;
    }
    return UxBadgeComponent;
}());
UxBadgeComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-badge',
                template: "\n        <span class=\"badge ux-c-badge badge-{{typeClass}} ux-c-badge--{{typeClass}} {{styleClass}}\"\n              [class.ux-c-badge--outline]=\"isOutline\"\n              [class.badge-pill]=\"isPill\"\n              [class.badge--small]=\"isSmall\"\n              [class.badge--large]=\"isLarge\"\n              [class.badge--tiny]=\"isTiny\">\n              <ng-content></ng-content>\n        </span>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxBadgeComponent.ctorParameters = function () { return []; };
UxBadgeComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'isSmall': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'isTiny': [{ type: Input },],
    'isPill': [{ type: Input },],
    'isOutline': [{ type: Input },],
};
var UxBadgeComponentModule = (function () {
    function UxBadgeComponentModule() {
    }
    return UxBadgeComponentModule;
}());
UxBadgeComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxBadgeComponent],
                declarations: [UxBadgeComponent]
            },] },
];
/**
 * @nocollapse
 */
UxBadgeComponentModule.ctorParameters = function () { return []; };

var UxLabelComponent = (function () {
    function UxLabelComponent() {
        this.hasMarker = false;
        this.isClickable = false;
        this.badgeTypeClass = 'primary';
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    UxLabelComponent.prototype.ngOnChanges = function (changes) {
        var /** @type {?} */ subLabelChange = changes && changes['subLabel'];
        if (subLabelChange) {
            var /** @type {?} */ subLabel = subLabelChange.currentValue;
            if (subLabel && subLabel.length > 500) {
                this.subLabel = subLabel.substr(0, 500) + '...';
            }
            else {
                this.subLabel = subLabel;
            }
        }
    };
    return UxLabelComponent;
}());
UxLabelComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-a-label',
                template: "\n        <div class=\"ux-a-label {{styleClass}}\">\n            <div *ngIf=\"hasMarker\" class=\"ux-a-label__marker ux-a-marker ux-a-marker--{{markerTypeClass}}\">\n                <span class=\"fa fa-circle ux-a-marker__icon\"></span>\n            </div>\n            <div class=\"ux-a-label__content\">\n                <div class=\"ux-a-label__label\">\n                    <template [ngIf]=\"isClickable\">\n                        <a class=\"ux-a-label__label--clickable\" tabindex=\"1\">{{label}}</a>\n                    </template>\n                    <template [ngIf]=\"!isClickable\">\n                        {{label}}\n                    </template>\n                </div>\n                <template [ngIf]=\"!customSubLabel\">\n                    <div *ngIf=\"subLabel\" class=\"ux-a-label__sub-label\">\n                        {{subLabel}}\n                    </div>\n                </template>\n                <template [ngIf]=\"customSubLabel\">\n                    <div class=\"ux-a-label__sub-label\">\n                        <ng-content select=\"uxLabelSubLabel\"></ng-content>\n                    </div>\n                </template>\n            </div>\n            <div *ngIf=\"labelIconClass\" class=\"ux-a-label__label-icon fa fa-fw {{labelIconClass}}\"></div>\n            <ux-badge *ngIf=\"badgeLabel\" typeClass=\"{{badgeTypeClass}}\" [isPill]=\"true\">{{badgeLabel}}</ux-badge>\n            <span *ngIf=\"infos\" class=\"ux-a-label__label-infos\">\n                {{infos}}\n            </span>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxLabelComponent.ctorParameters = function () { return []; };
UxLabelComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'labelIconClass': [{ type: Input },],
    'hasMarker': [{ type: Input },],
    'markerTypeClass': [{ type: Input },],
    'isClickable': [{ type: Input },],
    'badgeLabel': [{ type: Input },],
    'badgeTypeClass': [{ type: Input },],
    'infos': [{ type: Input },],
    'customSubLabel': [{ type: ContentChild, args: [forwardRef(function () { return UxLabelSubLabelTagDirective; }),] },],
};
var UxLabelSubLabelTagDirective = (function () {
    function UxLabelSubLabelTagDirective() {
    }
    return UxLabelSubLabelTagDirective;
}());
UxLabelSubLabelTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLabelSubLabel' },] },
];
/**
 * @nocollapse
 */
UxLabelSubLabelTagDirective.ctorParameters = function () { return []; };
var UxLabelComponentModule = (function () {
    function UxLabelComponentModule() {
    }
    return UxLabelComponentModule;
}());
UxLabelComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxBadgeComponentModule],
                exports: [UxLabelComponent, UxLabelSubLabelTagDirective],
                declarations: [UxLabelComponent, UxLabelSubLabelTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxLabelComponentModule.ctorParameters = function () { return []; };

var UxDateTagComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxDateTagComponent(uxService) {
        this.uxService = uxService;
        this.dateFormat = 'dd/MM/yyyy';
        this.isVerticalLayout = true;
        this.isHandleOverdue = false;
        this.isDisplayDaysLeft = false;
        this.isMuted = false;
        this.stateClass = '';
    }
    /**
     * @return {?}
     */
    UxDateTagComponent.prototype.ngAfterContentInit = function () {
        var /** @type {?} */ prefix = 'ux-a-date-tag';
        if (this.isMuted) {
            this.isVerticalLayout = false;
        }
        if (this.isVerticalLayout) {
            this.stateClass += prefix + '--vertical ';
            if (this.typeClass) {
                this.stateClass += prefix + '--vertical--' + this.typeClass + ' ';
            }
            else {
                if (this.isHandleOverdue) {
                    this.stateClass += prefix + '--vertical--' + this.getOverdueState();
                }
            }
        }
        else {
            if (this.typeClass) {
                this.stateClass += prefix + '--' + this.typeClass;
            }
            else {
                if (this.isHandleOverdue) {
                    this.stateClass += prefix + '--' + this.getOverdueState();
                }
            }
            if (this.isMuted) {
                this.stateClass += ' ' + prefix + '--muted';
            }
        }
    };
    Object.defineProperty(UxDateTagComponent.prototype, "daysLeftValue", {
        /**
         * @return {?}
         */
        get: function () {
            var /** @type {?} */ diffDays = this.uxService.diffDaysFromToday(this.date);
            var /** @type {?} */ label = '';
            switch (true) {
                case (diffDays < -1):
                    label = this.uxService.translate('xDaysAgo', this.languageCode).replace('%PARAM%', Math.abs(diffDays).toString());
                    break;
                case (diffDays === -1):
                    label = this.uxService.translate('oneDayAgo', this.languageCode);
                    break;
                case (diffDays === 0):
                    label = this.uxService.translate('today', this.languageCode);
                    break;
                case (diffDays === 1):
                    label = this.uxService.translate('inOneDay', this.languageCode);
                    break;
                case (diffDays > 1):
                    label = this.uxService.translate('inXDays', this.languageCode).replace('%PARAM%', diffDays.toString());
                    break;
            }
            return label;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxDateTagComponent.prototype.getOverdueState = function () {
        var /** @type {?} */ now = new Date();
        if (this.date.toDateString() === now.toDateString()) {
            return 'warning';
        }
        else if (now > this.date) {
            return 'danger';
        }
        return 'primary';
    };
    return UxDateTagComponent;
}());
UxDateTagComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-a-date-tag',
                template: "\n        <div *ngIf=\"isVerticalLayout\" class=\"ux-a-date-tag {{stateClass}} {{styleClass}}\">\n            <div class=\"ux-a-date-tag__d\">{{date | date:'dd'}}</div>\n            <div class=\"ux-a-date-tag__m\">{{date | date:'MMM'}}</div>\n            <div class=\"ux-a-date-tag__y\">{{date | date:'yyyy'}}</div>\n        </div>\n\n        <div *ngIf=\"!isVerticalLayout\" class=\"ux-a-date-tag {{stateClass}} {{styleClass}}\">\n            {{date | date: dateFormat}}\n        </div>\n\n        <div *ngIf=\"isDisplayDaysLeft\" class=\"ux-a-date-tag__sub-label\">\n            {{daysLeftValue}}\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxDateTagComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxDateTagComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'date': [{ type: Input },],
    'dateFormat': [{ type: Input },],
    'isVerticalLayout': [{ type: Input },],
    'isHandleOverdue': [{ type: Input },],
    'isDisplayDaysLeft': [{ type: Input },],
    'languageCode': [{ type: Input },],
    'isMuted': [{ type: Input },],
};
var UxDateTagComponentModule = (function () {
    function UxDateTagComponentModule() {
    }
    return UxDateTagComponentModule;
}());
UxDateTagComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxDateTagComponent],
                declarations: [UxDateTagComponent]
            },] },
];
/**
 * @nocollapse
 */
UxDateTagComponentModule.ctorParameters = function () { return []; };

var UxToolbarFilterComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxToolbarFilterComponent(uxService) {
        this.uxService = uxService;
        this.isVisible = false;
        this.isToggleVisible = true;
        this.filter = new EventEmitter();
        this.expandAll = new EventEmitter();
        this.collapseAll = new EventEmitter();
        this.filterValue = '';
        this.isToggleExpanded = false;
    }
    /**
     * @return {?}
     */
    UxToolbarFilterComponent.prototype.ngOnInit = function () {
        if (!this.filterLabel) {
            this.translatedFilterLabel = this.uxService.translate('filter');
        }
        else {
            this.translatedFilterLabel = this.filterLabel;
        }
        if (!this.expandAllLabel) {
            this.translatedExpandAllLabel = this.uxService.translate('expandAll');
        }
        else {
            this.translatedExpandAllLabel = this.expandAllLabel;
        }
        if (!this.collapseAllLabel) {
            this.translatedCollapseAllLabel = this.uxService.translate('collapseAll');
        }
        else {
            this.translatedCollapseAllLabel = this.collapseAllLabel;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxToolbarFilterComponent.prototype.onFilter = function (event) {
        if (this.filterValue === '') {
            if (!this.isToggleExpanded) {
                this.collapseAll.emit(null);
            }
        }
        this.filter.emit(this.filterValue);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxToolbarFilterComponent.prototype.onExpandAll = function (event) {
        this.isToggleExpanded = !this.isToggleExpanded;
        this.expandAll.emit(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxToolbarFilterComponent.prototype.onCollapseAll = function (event) {
        this.isToggleExpanded = !this.isToggleExpanded;
        this.collapseAll.emit(event);
    };
    return UxToolbarFilterComponent;
}());
UxToolbarFilterComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-a-toolbar-filter',
                template: "\n    <div *ngIf=\"isVisible\" class=\"ux-a-toolbar-filter\">\n        <div class=\"ux-a-toolbar-filter-content\">\n            <div class=\"ux-a-toolbar-filter-filter\">\n                <input type=\"text\" class=\"ux-a-toolbar-filter-filter-input\"\n                        placeholder=\"{{translatedFilterLabel}}\" [(ngModel)]=\"filterValue\"\n                        (keyup)=\"onFilter($event)\"/>\n                <div class=\"ux-a-toolbar-filter-filter-input-search-icon\">\n                    <span class=\"fa fa-search\"></span>\n                </div>\n            </div>\n            <div *ngIf=\"isToggleVisible\" class=\"ux-a-toolbar-filter-trigger-wrapper\">\n                <button *ngIf=\"!isToggleExpanded\" class=\"ux-a-toolbar-filter-trigger btn btn-secondary\" (click)=\"onExpandAll($event)\">\n                    <span class=\"fa fa-level-down ux-a-toolbar-filter-trigger-icon\"></span>\n                    {{translatedExpandAllLabel}}\n                </button>\n                <button *ngIf=\"isToggleExpanded\" class=\"ux-a-toolbar-filter-trigger btn btn-secondary\" (click)=\"onCollapseAll($event)\">\n                    <span class=\"fa fa-level-up ux-a-toolbar-filter-trigger-icon\"></span>\n                    {{translatedCollapseAllLabel}}\n                </button>\n            </div>\n\n            <div class=\"ml-auto d-flex-none\">\n                <ng-content></ng-content>\n            </div>\n        </div>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxToolbarFilterComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxToolbarFilterComponent.propDecorators = {
    'isVisible': [{ type: Input },],
    'filterLabel': [{ type: Input },],
    'expandAllLabel': [{ type: Input },],
    'collapseAllLabel': [{ type: Input },],
    'isToggleVisible': [{ type: Input },],
    'filter': [{ type: Output },],
    'expandAll': [{ type: Output },],
    'collapseAll': [{ type: Output },],
};
var UxToolbarFilterComponentModule = (function () {
    function UxToolbarFilterComponentModule() {
    }
    return UxToolbarFilterComponentModule;
}());
UxToolbarFilterComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule],
                exports: [UxToolbarFilterComponent],
                declarations: [UxToolbarFilterComponent]
            },] },
];
/**
 * @nocollapse
 */
UxToolbarFilterComponentModule.ctorParameters = function () { return []; };

var UxBlockDocumentComponent = (function () {
    function UxBlockDocumentComponent() {
    }
    return UxBlockDocumentComponent;
}());
UxBlockDocumentComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-block-document',
                template: "\n        <div class=\"ux-block-document\" [ngStyle]=\"{display: isBlocked ? 'block' : 'none'}\">\n            <div class=\"loading\"></div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxBlockDocumentComponent.ctorParameters = function () { return []; };
UxBlockDocumentComponent.propDecorators = {
    'isBlocked': [{ type: Input },],
};
var UxBlockDocumentComponentModule = (function () {
    function UxBlockDocumentComponentModule() {
    }
    return UxBlockDocumentComponentModule;
}());
UxBlockDocumentComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxBlockDocumentComponent],
                declarations: [UxBlockDocumentComponent]
            },] },
];
/**
 * @nocollapse
 */
UxBlockDocumentComponentModule.ctorParameters = function () { return []; };

var UxBlockContentComponent = (function () {
    /**
     * @param {?} elRef
     * @param {?} renderer
     */
    function UxBlockContentComponent(elRef, renderer) {
        this.elRef = elRef;
        this.renderer = renderer;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    UxBlockContentComponent.prototype.ngOnChanges = function (changes) {
        var /** @type {?} */ isBlockedChange = changes['isBlocked'];
        if (isBlockedChange && isBlockedChange.currentValue && !isBlockedChange.previousValue) {
            var /** @type {?} */ activeElement = this.elRef.nativeElement.querySelector(':focus');
            if (activeElement) {
                this.renderer.invokeElementMethod(activeElement, 'blur');
            }
        }
    };
    return UxBlockContentComponent;
}());
UxBlockContentComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-block-content',
                template: "\n        <div class=\"ux-c-block-content\" [class.ux-is-active]=\"isBlocked\">\n            <ng-content></ng-content>\n        </div>\n        "
            },] },
];
/**
 * @nocollapse
 */
UxBlockContentComponent.ctorParameters = function () { return [
    { type: ElementRef, },
    { type: Renderer, },
]; };
UxBlockContentComponent.propDecorators = {
    'isBlocked': [{ type: Input },],
};
var UxBlockContentComponentModule = (function () {
    function UxBlockContentComponentModule() {
    }
    return UxBlockContentComponentModule;
}());
UxBlockContentComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxBlockContentComponent],
                declarations: [UxBlockContentComponent]
            },] },
];
/**
 * @nocollapse
 */
UxBlockContentComponentModule.ctorParameters = function () { return []; };

var UxProgressCircleComponent = (function () {
    function UxProgressCircleComponent() {
        this.emptyLabel = 'N/A';
        this.isDefaultColorSteps = true;
        this.isSmall = false;
        this.isLarge = false;
        this.stateColorNumberClass = '';
        this.labelValue = '';
    }
    /**
     * @return {?}
     */
    UxProgressCircleComponent.prototype.ngAfterContentInit = function () {
        var /** @type {?} */ prefix = 'ux-progress-circle--';
        var /** @type {?} */ colorStepsArray = [];
        this.roundedValue = Math.round(this.value);
        if (this.colorSteps) {
            colorStepsArray = this.colorSteps.split(/[ ,]+/g);
        }
        else {
            colorStepsArray = ['33', '66'];
        }
        if (this.colorType) {
            this.stateColorNumberClass = prefix + this.colorType;
        }
        else {
            if (this.isDefaultColorSteps) {
                if (this.value <= Number.parseInt(colorStepsArray[0])) {
                    this.stateColorNumberClass = prefix + 'success';
                }
                if (this.value > Number.parseInt(colorStepsArray[0]) && this.value <= Number.parseInt(colorStepsArray[1])) {
                    this.stateColorNumberClass = prefix + 'warning';
                }
                if (this.value > Number.parseInt(colorStepsArray[1])) {
                    this.stateColorNumberClass = prefix + 'danger';
                }
            }
        }
        if (this.value === 0) {
            this.labelValue = this.emptyLabel;
        }
        else {
            this.labelValue = this.value + '%';
        }
    };
    return UxProgressCircleComponent;
}());
UxProgressCircleComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-progress-circle',
                template: "\n        <div class=\"ux-progress-circle-wrapper\">\n            <div class=\"ux-progress-circle p{{roundedValue}} {{stateColorNumberClass}}\"\n                [class.ux-progress-circle--over50]=\"roundedValue > 50\"\n                [class.ux-progress-circle--small]=\"isSmall\"\n                [class.ux-progress-circle--large]=\"isLarge\">\n\n                <span class=\"ux-progress-circle__label\">{{labelValue}}</span>\n                <div class=\"ux-progress-circle__left-half-clipper\">\n                    <div class=\"ux-progress-circle__first50-bar\"></div>\n                    <div class=\"ux-progress-circle__value-bar\"></div>\n                </div>\n            </div>\n            <div *ngIf=\"iconLabelClass\"\n                 [class.ux-progress-circle__icon-label]=\"!isLarge && !isSmall\"\n                 [class.ux-progress-circle__icon-label--small]=\"isSmall\"\n                 [class.ux-progress-circle__icon-label--large]=\"isLarge\">\n                <span class=\"fa {{iconLabelClass}} {{iconLabelStyleClass}}\"></span>\n            </div>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxProgressCircleComponent.ctorParameters = function () { return []; };
UxProgressCircleComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'value': [{ type: Input },],
    'emptyLabel': [{ type: Input },],
    'isDefaultColorSteps': [{ type: Input },],
    'colorSteps': [{ type: Input },],
    'colorType': [{ type: Input },],
    'isSmall': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'iconLabelClass': [{ type: Input },],
    'iconLabelStyleClass': [{ type: Input },],
};
var UxProgressCircleComponentModule = (function () {
    function UxProgressCircleComponentModule() {
    }
    return UxProgressCircleComponentModule;
}());
UxProgressCircleComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxProgressCircleComponent],
                declarations: [UxProgressCircleComponent]
            },] },
];
/**
 * @nocollapse
 */
UxProgressCircleComponentModule.ctorParameters = function () { return []; };

var UxLink = (function () {
    /**
     * @param {?=} values
     */
    function UxLink(values) {
        if (values === void 0) { values = {}; }
        this.disabled = false;
        this.hasIconBg = false;
        this.active = false;
        this.visible = true;
        this.expanded = false;
        this.hasMarker = false;
        this.hasTag = false;
        Object.assign(this, values);
    }
    Object.defineProperty(UxLink.prototype, "hasChildren", {
        /**
         * @return {?}
         */
        get: function () {
            return this.children != null;
        },
        enumerable: true,
        configurable: true
    });
    return UxLink;
}());

var UxDropdownButtonItemComponent = (function () {
    function UxDropdownButtonItemComponent() {
    }
    return UxDropdownButtonItemComponent;
}());
UxDropdownButtonItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-dropdown-button-item',
                template: "<div></div>"
            },] },
];
/**
 * @nocollapse
 */
UxDropdownButtonItemComponent.ctorParameters = function () { return []; };
UxDropdownButtonItemComponent.propDecorators = {
    'id': [{ type: Input },],
    'label': [{ type: Input },],
    'iconClass': [{ type: Input },],
};
var UxDropdownButtonItemComponentModule = (function () {
    function UxDropdownButtonItemComponentModule() {
    }
    return UxDropdownButtonItemComponentModule;
}());
UxDropdownButtonItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxDropdownButtonItemComponent],
                declarations: [UxDropdownButtonItemComponent]
            },] },
];
/**
 * @nocollapse
 */
UxDropdownButtonItemComponentModule.ctorParameters = function () { return []; };

var UxDropdownButtonComponent = (function () {
    /**
     * @param {?} uxService
     * @param {?} shadowDomRoot
     */
    function UxDropdownButtonComponent(uxService, shadowDomRoot) {
        this.uxService = uxService;
        this.shadowDomRoot = shadowDomRoot;
        this.links = [];
        this.typeClass = 'secondary';
        this.isSplitButtonToggle = false;
        this.isDropDownRightAligned = false;
        this.isLinkToggle = false;
        this.isUpdateLabelFromSelectedItem = false;
        this.isOutline = false;
        this.isShowDropdownToggle = true;
        this.linkSelected = new EventEmitter();
        this.isShown = true;
        this.dropDownIsOpen = false;
    }
    /**
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.ngOnInit = function () {
        if (!this.label && !this.iconClass && !this.isSplitButtonToggle) {
            this.iconClass = 'fa-bars';
        }
    };
    /**
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        if (this.items && this.links.length === 0) {
            this.links = [];
            this.items.forEach(function (item) {
                _this.links.push(new UxLink({
                    id: item.id,
                    label: item.label,
                    iconClass: item.iconClass
                }));
            });
        }
    };
    Object.defineProperty(UxDropdownButtonComponent.prototype, "toggleClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isLinkToggle) {
                return 'ux-dropdown-button__toggle--link-toggle';
            }
            else {
                if (this.label || this.isSplitButtonToggle) {
                    if (!this.isSplitButtonToggle) {
                        if (this.isOutline) {
                            return 'btn btn-outline-' + this.typeClass;
                        }
                        else {
                            return 'btn btn-' + this.typeClass;
                        }
                    }
                    else {
                        if (this.isOutline) {
                            return 'btn btn-outline-' + this.typeClass + ' ux-dropdown-button__toggle--split-toggle-outline';
                        }
                        else {
                            return 'btn btn-' + this.typeClass + ' ux-dropdown-button__toggle--split-toggle';
                        }
                    }
                }
                else if (this.iconClass && !this.label && !this.isSplitButtonToggle) {
                    return 'ux-dropdown-button__toggle--icon-toggle';
                }
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.toggleOutside = function () {
        this.dropDownIsOpen = false;
    };
    /**
     * @param {?} querySelector
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.shadowDomQuery = function (querySelector) {
        return this.shadowDomRoot.nativeElement.querySelector(querySelector);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.toggleDropDownKeyDown = function (event) {
        if (event.keyCode === 13) {
            this.toggleDropDown(event);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.toggleDropDown = function (event) {
        this.dropDownIsOpen = !this.dropDownIsOpen;
        var /** @type {?} */ selectedItem;
        if (this.dropDownIsOpen) {
            if (this.activeLink) {
                selectedItem = this.shadowDomQuery('.dropdown-item[data-link-id=' + this.activeLink.id + ']');
            }
            else {
                selectedItem = this.shadowDomQuery('.dropdown-item');
            }
            if (selectedItem) {
                setTimeout(function () {
                    if (selectedItem != null) {
                        selectedItem.focus();
                    }
                }, 0);
            }
        }
        this.uxService.consumeEvent(event);
    };
    /**
     * @param {?} event
     * @param {?} link
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.selectDropdownItem = function (event, link) {
        this.uxService.consumeEvent(event);
        this.linkSelected.emit(link);
        if (link && link.command) {
            link.command();
        }
        if (this.isUpdateLabelFromSelectedItem && link) {
            this.label = link.label;
        }
        this.dropDownIsOpen = false;
    };
    /**
     * @param {?} keyCode
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.closeDropDownOnEscape = function (keyCode) {
        if (keyCode === 27) {
            this.dropDownIsOpen = false;
        }
    };
    /**
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.closeOnBlur = function () {
        var _this = this;
        setTimeout(function () {
            if (_this.shadowDomQuery('.dropdown-item:focus') == null) {
                _this.dropDownIsOpen = false;
            }
        }, 0);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxDropdownButtonComponent.prototype.navigateDropDownSelection = function (event) {
        var /** @type {?} */ keyCode = event.keyCode;
        /*
         38 = arrow up
         40 = arrow down
         */
        var /** @type {?} */ focusedItem = this.shadowDomQuery('.dropdown-item:focus');
        if (focusedItem != null) {
            var /** @type {?} */ next = void 0;
            if (keyCode === 40) {
                next = ((focusedItem)).nextElementSibling;
                if (!next) {
                    next = ((((focusedItem)).parentNode)).firstElementChild;
                }
            }
            else if (keyCode === 38) {
                next = ((focusedItem)).previousElementSibling;
                if (!next) {
                    next = ((((focusedItem)).parentNode)).lastElementChild;
                }
            }
            if (next) {
                ((next)).focus();
                this.uxService.consumeEvent(event);
            }
        }
    };
    return UxDropdownButtonComponent;
}());
UxDropdownButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-dropdown-button',
                template: "\n        <div class=\"ux-dropdown-button {{styleClass}}\">\n            <div class=\"btn-group\">\n                <a class=\"ux-dropdown-button__toggle {{toggleClass}}\" tabindex=\"0\"\n                   [class.dropdown-toggle]=\"isShowDropdownToggle\"\n                   aria-haspopup=\"true\" data-toggle=\"dropdown\"\n                   [attr.aria-expanded]=\"!dropDownIsOpen\"\n                   (click)=\"toggleDropDown($event)\" (keydown)=\"toggleDropDownKeyDown($event)\">\n                    <span *ngIf=\"iconClass\" class=\"ux-dropdown-button__toggle-icon fa {{iconClass}}\"></span>\n                    <span *ngIf=\"label\" class=\"ux-dropdown-button__toggle-label\"\n                          [class.ux-dropdown-button__toggle-label--with-icon]=\"iconClass\">\n                           {{label}}\n                    </span>\n                </a>\n                <div class=\"ux-dropdown-button__menu dropdown-menu\"\n                     [style.display]=\"dropDownIsOpen ? 'block' : 'none'\"\n                     [class.ux-dropdown-button__menu--right-aligned]=\"isDropDownRightAligned\"\n                     [class.ux-dropdown-button__menu--link-toggle]=\"isLinkToggle\"\n                     (keydown)=\"closeDropDownOnEscape($event.keyCode)\">\n                    <a *ngFor=\"let link of links\" href=\"javascript:void(0);\"\n                        class=\"ux-dropdown-button__menu-item dropdown-item\" [attr.data-link-id]=\"link.id\"\n                        (click)=\"selectDropdownItem($event, link)\"\n                        (blur)=\"closeOnBlur()\"\n                        (keydown)=\"navigateDropDownSelection($event)\" >\n                            <span *ngIf=\"link.iconClass\" class=\"ux-dropdown-button__menu-item-icon fa fa-fw {{link.iconClass}}\"></span>\n                            <span class=\"ux-dropdown-button__menu-item-label\">{{link.label}}</span>\n                    </a>\n                </div>\n            </div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxDropdownButtonComponent.ctorParameters = function () { return [
    { type: UxService, },
    { type: ElementRef, },
]; };
UxDropdownButtonComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'label': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'links': [{ type: Input },],
    'activeLink': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'isSplitButtonToggle': [{ type: Input },],
    'isDropDownRightAligned': [{ type: Input },],
    'isLinkToggle': [{ type: Input },],
    'isUpdateLabelFromSelectedItem': [{ type: Input },],
    'isOutline': [{ type: Input },],
    'isShowDropdownToggle': [{ type: Input },],
    'linkSelected': [{ type: Output },],
    'items': [{ type: ContentChildren, args: [forwardRef(function () { return UxDropdownButtonItemComponent; }),] },],
    'toggleOutside': [{ type: HostListener, args: ['body:click',] },],
};
var UxDropdownButtonComponentModule = (function () {
    function UxDropdownButtonComponentModule() {
    }
    return UxDropdownButtonComponentModule;
}());
UxDropdownButtonComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxDropdownButtonComponent],
                declarations: [UxDropdownButtonComponent]
            },] },
];
/**
 * @nocollapse
 */
UxDropdownButtonComponentModule.ctorParameters = function () { return []; };

var UxPanelsComponent = (function () {
    function UxPanelsComponent() {
        this.isShowToolbar = false;
        this.isShowToolbarExtendedPanelsToggle = false;
        this.openAllPanelsLabel = 'Open all panels';
        this.closeAllPanelsLabel = 'Close all panels';
        this.filterLabel = 'Filter panels';
        this.goToPanelsLabel = 'Go to panel';
        this.isMultipleExpanded = true;
        this.isAllPanelsExpandable = false;
        this.isDebug = false;
        this.panelsFilter = '';
        this.extendedPanelsLinks = [];
    }
    /**
     * @return {?}
     */
    UxPanelsComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        if (this.isAllPanelsExpandable) {
            if (this.isDebug) {
                console.log(this.panels);
            }
            this.panels.forEach(function (i) {
                i.isExpandable = true;
                i.isExpanded = false;
            });
        }
        this.panels.forEach(function (panel, i) {
            _this.extendedPanelsLinks.push(new UxLink({
                label: panel.label, iconClass: panel.iconClass,
                command: function () { return _this.selectPanel(i); }
            }));
        });
    };
    /**
     * @param {?} index
     * @return {?}
     */
    UxPanelsComponent.prototype.selectPanel = function (index) {
        var /** @type {?} */ selectedPanel = this.panels.toArray()[index];
        this.closeAllPanels();
        selectedPanel.isExpanded = true;
        window.scrollTo(0, selectedPanel.HTMLElement.getBoundingClientRect().top - 150);
    };
    /**
     * @param {?} item
     * @return {?}
     */
    UxPanelsComponent.prototype.collapseAll = function (item) {
        this.panels.forEach(function (i) {
            if (i !== item) {
                i.isExpanded = false;
            }
        });
    };
    /**
     * @return {?}
     */
    UxPanelsComponent.prototype.openAllPanels = function () {
        this.panels.forEach(function (i) {
            i.isExpanded = true;
        });
    };
    /**
     * @return {?}
     */
    UxPanelsComponent.prototype.closeAllPanels = function () {
        this.panels.forEach(function (i) {
            i.isExpanded = false;
        });
    };
    /**
     * @return {?}
     */
    UxPanelsComponent.prototype.filterPanels = function () {
        var _this = this;
        if (this.panelsFilter !== '') {
            this.panels.forEach(function (i) {
                i.isVisible = false;
            });
            this.panels.forEach(function (i) {
                if (i.label.toUpperCase().indexOf(_this.panelsFilter.toUpperCase()) !== -1) {
                    i.isVisible = true;
                }
            });
        }
        else {
            this.panels.forEach(function (i) {
                i.isVisible = true;
            });
        }
    };
    return UxPanelsComponent;
}());
UxPanelsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-panels',
                template: "\n        <div *ngIf=\"isShowToolbar\" class=\"ux-panel-toolbar\">\n            <button class=\"btn btn-secondary ux-panel-toolbar__trigger\" (click)=\"openAllPanels()\" tabindex=\"1\">\n                <span class=\"fa fa-level-down ux-panel-toolbar__trigger-icon\"></span>\n                {{openAllPanelsLabel}}\n            </button>\n            <button class=\"btn btn-secondary ux-panel-toolbar__trigger\" (click)=\"closeAllPanels()\" tabindex=\"1\">\n                <span class=\"fa fa-level-up ux-panel-toolbar__trigger-icon\"></span>\n                {{closeAllPanelsLabel}}\n            </button>\n\n            <ux-dropdown-button *ngIf=\"isShowToolbarExtendedPanelsToggle\"\n                                label=\"{{goToPanelsLabel}}\" iconClass=\"fa-bars\"\n                                [links]=\"extendedPanelsLinks\">\n            </ux-dropdown-button>\n\n            <div class=\"ux-panel-toolbar__filter\">\n                <input type=\"text\" class=\"ux-panel-toolbar__filter-input\"\n                       placeholder=\"{{filterLabel}}\" [(ngModel)]=\"panelsFilter\" (keyup)=\"filterPanels()\"/>\n                <div class=\"ux-panel-toolbar__filter-input-search-icon\">\n                    <span class=\"fa fa-search\"></span>\n                </div>\n            </div>\n        </div>\n\n        <ul class=\"panels\">\n            <ng-content></ng-content>\n        </ul>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxPanelsComponent.ctorParameters = function () { return []; };
UxPanelsComponent.propDecorators = {
    'isShowToolbar': [{ type: Input },],
    'isShowToolbarExtendedPanelsToggle': [{ type: Input },],
    'openAllPanelsLabel': [{ type: Input },],
    'closeAllPanelsLabel': [{ type: Input },],
    'filterLabel': [{ type: Input },],
    'goToPanelsLabel': [{ type: Input },],
    'isMultipleExpanded': [{ type: Input },],
    'isAllPanelsExpandable': [{ type: Input },],
    'isDebug': [{ type: Input },],
    'panels': [{ type: ContentChildren, args: [forwardRef(function () { return UxPanelComponent; }), { descendants: true },] },],
};
var UxPanelsComponentModule = (function () {
    function UxPanelsComponentModule() {
    }
    return UxPanelsComponentModule;
}());
UxPanelsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule, UxDropdownButtonComponentModule],
                exports: [UxPanelsComponent],
                declarations: [UxPanelsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxPanelsComponentModule.ctorParameters = function () { return []; };

var UxPanelComponent = (function () {
    /**
     * @param {?} uxPanelsComponent
     * @param {?} el
     */
    function UxPanelComponent(uxPanelsComponent, el) {
        this.isIconRounded = false;
        this.isExpandable = false;
        this.isExpanded = false;
        this.isVisible = true;
        this.isSelected = false;
        this.badgeTypeClass = 'primary';
        this.hasTabs = false;
        this.hasNoHeader = false;
        this.hasNoContentPadding = false;
        this.isBlocked = false;
        this.maxHeight = 'none';
        this.isFlat = false;
        this.isLargeHeader = false;
        this.isLargeFooter = false;
        this.hasStatusIndicator = false;
        this.statusIndicatorTypeClass = 'warning';
        this.statusIndicatorMessageLabel = 'Status indicator default message';
        this.isTogglableStatusIndicatorContent = true;
        this.isStatusIndicatorContentExpanded = false;
        this.hasStatusIndicatorHeaderBg = false;
        this.hasStatusIndicatorHeaderIcon = false;
        this.isEditModeActive = false;
        this.isContainerOnly = false;
        this.isClickable = false;
        this.statusIndicatorIconClass = '';
        // panel states
        this.hasMaxHeight = false;
        this.stateExpandableClass = '';
        this.stateTabsClass = '';
        this.statePanelItemClasses = '';
        this.statePanelHeaderClasses = '';
        this.statePanelContentClasses = '';
        this.uxPanelsComponent = uxPanelsComponent;
        this.HTMLElement = el.nativeElement;
    }
    /**
     * @return {?}
     */
    UxPanelComponent.prototype.ngAfterContentInit = function () {
        if (!this.isExpandable) {
            this.isExpanded = true;
        }
        this.actualMaxHeight = this.maxHeight;
        // setting panel states
        if (this.maxHeight !== 'none') {
            this.hasMaxHeight = true;
        }
        if (this.isExpandable) {
            this.stateExpandableClass = 'ux-panel-header--expandable';
        }
        if (this.hasTabs) {
            this.stateTabsClass = 'ux-panel-item--has-tabs';
        }
        if (this.isContainerOnly) {
            this.hasNoHeader = true;
            this.hasNoContentPadding = true;
        }
        if (this.hasNoHeader) {
            this.statePanelItemClasses += 'ux-panel-item--no-header ';
        }
        if (this.hasTabs) {
            this.statePanelItemClasses += 'ux-panel-item--has-tabs ';
        }
        if (this.isFlat) {
            this.statePanelItemClasses += 'ux-panel-item--flat';
        }
        if (this.isClickable) {
            this.statePanelItemClasses += 'ux-panel-item--clickable';
        }
        if (this.isExpandable) {
            this.statePanelHeaderClasses += 'ux-panel-header--expandable ';
        }
        if (this.isLargeHeader) {
            this.statePanelHeaderClasses += 'ux-panel-header--large ';
        }
        if (this.hasStatusIndicator && this.hasStatusIndicatorHeaderBg && !this.hasStatusIndicatorHeaderIcon) {
            this.statePanelItemClasses += 'ux-panel-item--' + this.statusIndicatorTypeClass;
        }
        if (this.hasStatusIndicator && !this.hasStatusIndicatorHeaderBg && !this.hasStatusIndicatorHeaderIcon) {
            this.statePanelHeaderClasses += 'ux-u-border-l-' + this.statusIndicatorTypeClass;
        }
        if (this.hasNoContentPadding) {
            this.statePanelContentClasses += 'ux-panel-content--no-padding ';
        }
        if (this.isBlocked) {
            this.statePanelContentClasses += 'ux-panel-content--blocked';
        }
        // status indicator icon class
        if (this.statusIndicatorTypeClass === 'warning') {
            this.statusIndicatorIconClass = 'warning';
        }
        else if (this.statusIndicatorTypeClass === 'danger') {
            this.statusIndicatorIconClass = 'exclamation';
        }
        else if (this.statusIndicatorTypeClass === 'info') {
            this.statusIndicatorIconClass = 'info';
        }
        else if (this.statusIndicatorTypeClass === 'success') {
            this.statusIndicatorIconClass = 'check';
        }
    };
    Object.defineProperty(UxPanelComponent.prototype, "statePanelItemClassesDynamic", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isSelected) {
                return 'ux-panel-item--selected';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxPanelComponent.prototype, "statePanelContentClassesDynamic", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isEditModeActive) {
                return 'ux-panel-content--edit-mode-active';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxPanelComponent.prototype, "hasCustomPanelHeader", {
        /**
         * @return {?}
         */
        get: function () {
            return this.customPanelHeader.length !== 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxPanelComponent.prototype, "hasCustomPanelFooter", {
        /**
         * @return {?}
         */
        get: function () {
            return this.customPanelFooter.length !== 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxPanelComponent.prototype, "hasCustomPanelHeaderRightContent", {
        /**
         * @return {?}
         */
        get: function () {
            return this.customPanelHeaderRightContent.length !== 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxPanelComponent.prototype, "hasCustomPanelHeaderWithDescedants", {
        /**
         * @return {?}
         */
        get: function () {
            return this.customPanelHeaderWithDescendants.length !== 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxPanelComponent.prototype, "hasCustomPanelFooterWithDescedants", {
        /**
         * @return {?}
         */
        get: function () {
            return this.customPanelFooterWithDescendants.length !== 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxPanelComponent.prototype, "hasCustomPanelHeaderRightContentWithDescedants", {
        /**
         * @return {?}
         */
        get: function () {
            return this.customPanelHeaderRightContentWithDescendants.length !== 0;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} event
     * @return {?}
     */
    UxPanelComponent.prototype.toggle = function (event) {
        if (this.isExpandable) {
            if (this.uxPanelsComponent) {
                if (!this.uxPanelsComponent.isMultipleExpanded) {
                    this.uxPanelsComponent.collapseAll(this);
                }
            }
            this.isExpanded = !this.isExpanded;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxPanelComponent.prototype.toggleMaxHeight = function (event) {
        if (this.maxHeight !== this.actualMaxHeight) {
            this.actualMaxHeight = this.maxHeight;
        }
        else {
            this.actualMaxHeight = 'none';
        }
    };
    Object.defineProperty(UxPanelComponent.prototype, "isMaxHeightExpanded", {
        /**
         * @return {?}
         */
        get: function () {
            return this.actualMaxHeight === 'none';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxPanelComponent.prototype.onToggleStatusIndicatorContent = function () {
        this.isStatusIndicatorContentExpanded = !this.isStatusIndicatorContentExpanded;
    };
    return UxPanelComponent;
}());
UxPanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-panel',
                template: "\n        <div *ngIf=\"isVisible\" class=\"ux-panel-item {{styleClass}} {{statePanelItemClasses}} {{statePanelItemClassesDynamic}}\">\n            <span *ngIf=\"hasStatusIndicator && hasStatusIndicatorHeaderIcon\"\n                 class=\"ux-panel-item__status-indicator-icon-wrapper ux-u-bg-color-{{statusIndicatorTypeClass}}-dark\">\n                <span class=\"ux-panel-item__status-indicator-icon-wrapper-icon fa fa-{{statusIndicatorIconClass}}\"></span>\n            </span>\n\n            <template [ngIf]=\"hasCustomPanelHeader\">\n                <div class=\"ux-panel-header {{headerStyleClass}} {{statePanelHeaderClasses}}\" (click)=\"toggle($event)\">\n                    <ng-content select=\"uxPanelHeader\"></ng-content>\n                </div>\n            </template>\n\n            <template [ngIf]=\"hasCustomPanelHeaderWithDescendants\">\n                <div class=\"ux-panel-header {{headerStyleClass}} {{statePanelHeaderClasses}}\" (click)=\"toggle($event)\">\n                    <ng-content select=\"uxPanelHeaderWithDescendants\"></ng-content>\n                </div>\n            </template>\n\n            <template [ngIf]=\"!hasCustomPanelHeader && !hasCustomPanelHeaderWithDescendants\">\n                <div class=\"ux-panel-header {{headerStyleClass}} {{statePanelHeaderClasses}}\" (click)=\"toggle($event)\">\n                    <ux-a-icon iconClass=\"{{iconClass}}\" typeClass=\"{{iconTypeClass}}\" [isRounded]=\"isIconRounded\"></ux-a-icon>\n\n                    <ux-a-label label=\"{{label}}\" subLabel=\"{{subLabel}}\"\n                                badgeLabel=\"{{badgeLabel}}\" badgeTypeClass=\"{{badgeTypeClass}}\" infos=\"{{labelInfos}}\"></ux-a-label>\n\n                    <div class=\"ux-panel-header__right-content\">\n                        <template [ngIf]=\"hasCustomPanelHeaderRightContent\">\n                            <ng-content select=\"uxPanelHeaderRightContent\"></ng-content>\n                        </template>\n\n                        <template [ngIf]=\"hasCustomPanelHeaderRightContentWithDescendants\">\n                            <ng-content select=\"uxPanelHeaderRightContentWithDescendants\"></ng-content>\n                        </template>\n\n                        <template [ngIf]=\"!hasCustomPanelHeaderRightContent && !hasCustomPanelHeaderRightContentWithDescendants\">\n                            <ux-a-tag label=\"{{tagLabel}}\" subLabel=\"{{tagCount}}\" typeClass=\"{{tagTypeClass}}\"></ux-a-tag>\n                        </template>\n\n                        <span *ngIf=\"isExpandable\" class=\"ux-panel-header__expand-toggle fa\"\n                                [ngClass]=\"{'fa-angle-right': !isExpanded, 'fa-angle-down': isExpanded}\"></span>\n                    </div>\n                </div>\n            </template>\n\n            <template [ngIf]=\"hasStatusIndicator && customStatusIndicatorContent\">\n                <div class=\"ux-panel-header__status-indicator-wrapper\">\n                    <span class=\"ux-panel-header__status-indicator-wrapper-label ux-u-color-{{statusIndicatorTypeClass}}-darkest\">\n                        {{statusIndicatorMessageLabel}}\n                    </span>\n                    <a *ngIf=\"isTogglableStatusIndicatorContent\"\n                       (click)=\"onToggleStatusIndicatorContent()\" class=\"ux-panel-header__status-indicator-wrapper-toggle\" tabindex=\"1\">\n                        <span class=\"fa\" [class.fa-angle-down]=\"isStatusIndicatorContentExpanded\"\n                                         [class.fa-angle-right]=\"!isStatusIndicatorContentExpanded\">\n                        </span>\n                    </a>\n                </div>\n                <div *ngIf=\"isStatusIndicatorContentExpanded\" class=\"ux-panel-header__status-indicator-content\">\n                    <ng-content select=\"uxPanelHeaderStatusIndicatorContent\"></ng-content>\n                </div>\n            </template>\n\n            <div class=\"ux-panel-content {{statePanelContentClasses}} {{contentStyleClass}} {{statePanelContentClassesDynamic}}\"\n                 [class.ux-panel-content--fixed-height]=\"hasMaxHeight\"\n                 [style.max-height]=\"actualMaxHeight\" [hidden]=\"!isExpanded\"\n                 [style.height]=\"contentHeight\">\n                <ng-content></ng-content>\n                <div *ngIf=\"isBlocked\" class=\"ux-panel-content__blocked-overlay\"></div>\n            </div>\n\n            <div *ngIf=\"hasCustomPanelFooter\" class=\"ux-panel-footer\" [class.ux-panel-footer--large]=\"isLargeFooter\">\n                <ng-content select=\"uxPanelFooter\"></ng-content>\n            </div>\n\n            <div *ngIf=\"hasCustomPanelFooterWithDescendants\" class=\"ux-panel-footer\" [class.ux-panel-footer--large]=\"isLargeFooter\">\n                <ng-content select=\"uxPanelFooterWithDescendants\"></ng-content>\n            </div>\n\n            <div *ngIf=\"hasMaxHeight\" class=\"ux-panel-footer text-center\">\n                <button class=\"btn btn-sm btn-secondary\" (click)=\"toggleMaxHeight($event)\">\n                    <span *ngIf=\"isMaxHeightExpanded\">show less...</span>\n                    <span *ngIf=\"!isMaxHeightExpanded\">show more...</span>\n                </button>\n            </div>\n\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxPanelComponent.ctorParameters = function () { return [
    { type: UxPanelsComponent, decorators: [{ type: Optional }, { type: Host }, { type: Inject, args: [forwardRef(function () { return UxPanelsComponent; }),] },] },
    { type: ElementRef, },
]; };
UxPanelComponent.propDecorators = {
    'id': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'contentStyleClass': [{ type: Input },],
    'headerStyleClass': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'labelInfos': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'iconTypeClass': [{ type: Input },],
    'isIconRounded': [{ type: Input },],
    'isExpandable': [{ type: Input },],
    'isExpanded': [{ type: Input },],
    'isVisible': [{ type: Input },],
    'isSelected': [{ type: Input },],
    'tagLabel': [{ type: Input },],
    'tagTypeClass': [{ type: Input },],
    'tagCount': [{ type: Input },],
    'badgeLabel': [{ type: Input },],
    'badgeTypeClass': [{ type: Input },],
    'hasTabs': [{ type: Input },],
    'hasNoHeader': [{ type: Input },],
    'hasNoContentPadding': [{ type: Input },],
    'isBlocked': [{ type: Input },],
    'maxHeight': [{ type: Input },],
    'contentHeight': [{ type: Input },],
    'isFlat': [{ type: Input },],
    'isLargeHeader': [{ type: Input },],
    'isLargeFooter': [{ type: Input },],
    'hasStatusIndicator': [{ type: Input },],
    'statusIndicatorTypeClass': [{ type: Input },],
    'statusIndicatorMessageLabel': [{ type: Input },],
    'isTogglableStatusIndicatorContent': [{ type: Input },],
    'isStatusIndicatorContentExpanded': [{ type: Input },],
    'hasStatusIndicatorHeaderBg': [{ type: Input },],
    'hasStatusIndicatorHeaderIcon': [{ type: Input },],
    'isEditModeActive': [{ type: Input },],
    'isContainerOnly': [{ type: Input },],
    'isClickable': [{ type: Input },],
    'customPanelHeader': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxPanelHeaderTagDirective;
                }), { descendants: false },] },],
    'customPanelFooter': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxPanelFooterTagDirective;
                }), { descendants: false },] },],
    'customPanelHeaderRightContent': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxPanelHeaderRightContentTagDirective;
                }), { descendants: false },] },],
    'customPanelHeaderWithDescendants': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxPanelHeaderWithDescendantsTagDirective;
                }), { descendants: true },] },],
    'customPanelFooterWithDescendants': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxPanelFooterWithDescendantsTagDirective;
                }), { descendants: true },] },],
    'customPanelHeaderRightContentWithDescendants': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxPanelHeaderRightContentWithDescendantsTagDirective;
                }), { descendants: true },] },],
    'customStatusIndicatorContent': [{ type: ContentChild, args: [forwardRef(function () {
                    return UxPanelHeaderStatusIndicatorContentTagDirective;
                }),] },],
};
var UxPanelHeaderTagDirective = (function () {
    function UxPanelHeaderTagDirective() {
    }
    return UxPanelHeaderTagDirective;
}());
UxPanelHeaderTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxPanelHeader' },] },
];
/**
 * @nocollapse
 */
UxPanelHeaderTagDirective.ctorParameters = function () { return []; };
var UxPanelFooterTagDirective = (function () {
    function UxPanelFooterTagDirective() {
    }
    return UxPanelFooterTagDirective;
}());
UxPanelFooterTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxPanelFooter' },] },
];
/**
 * @nocollapse
 */
UxPanelFooterTagDirective.ctorParameters = function () { return []; };
var UxPanelHeaderRightContentTagDirective = (function () {
    function UxPanelHeaderRightContentTagDirective() {
    }
    return UxPanelHeaderRightContentTagDirective;
}());
UxPanelHeaderRightContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxPanelHeaderRightContent' },] },
];
/**
 * @nocollapse
 */
UxPanelHeaderRightContentTagDirective.ctorParameters = function () { return []; };
var UxPanelHeaderWithDescendantsTagDirective = (function () {
    function UxPanelHeaderWithDescendantsTagDirective() {
    }
    return UxPanelHeaderWithDescendantsTagDirective;
}());
UxPanelHeaderWithDescendantsTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxPanelHeaderWithDescendants' },] },
];
/**
 * @nocollapse
 */
UxPanelHeaderWithDescendantsTagDirective.ctorParameters = function () { return []; };
var UxPanelFooterWithDescendantsTagDirective = (function () {
    function UxPanelFooterWithDescendantsTagDirective() {
    }
    return UxPanelFooterWithDescendantsTagDirective;
}());
UxPanelFooterWithDescendantsTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxPanelFooterWithDescendants' },] },
];
/**
 * @nocollapse
 */
UxPanelFooterWithDescendantsTagDirective.ctorParameters = function () { return []; };
var UxPanelHeaderRightContentWithDescendantsTagDirective = (function () {
    function UxPanelHeaderRightContentWithDescendantsTagDirective() {
    }
    return UxPanelHeaderRightContentWithDescendantsTagDirective;
}());
UxPanelHeaderRightContentWithDescendantsTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxPanelHeaderRightContentWithDescendants' },] },
];
/**
 * @nocollapse
 */
UxPanelHeaderRightContentWithDescendantsTagDirective.ctorParameters = function () { return []; };
var UxPanelHeaderStatusIndicatorContentTagDirective = (function () {
    function UxPanelHeaderStatusIndicatorContentTagDirective() {
    }
    return UxPanelHeaderStatusIndicatorContentTagDirective;
}());
UxPanelHeaderStatusIndicatorContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxPanelHeaderStatusIndicatorContent' },] },
];
/**
 * @nocollapse
 */
UxPanelHeaderStatusIndicatorContentTagDirective.ctorParameters = function () { return []; };
var UxPanelComponentModule = (function () {
    function UxPanelComponentModule() {
    }
    return UxPanelComponentModule;
}());
UxPanelComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxTagComponentModule, UxIconComponentModule, UxLabelComponentModule],
                exports: [
                    UxPanelComponent, UxPanelHeaderTagDirective, UxPanelFooterTagDirective,
                    UxPanelHeaderRightContentTagDirective, UxPanelHeaderStatusIndicatorContentTagDirective,
                    UxPanelHeaderWithDescendantsTagDirective,
                    UxPanelFooterWithDescendantsTagDirective,
                    UxPanelHeaderRightContentWithDescendantsTagDirective
                ],
                declarations: [
                    UxPanelComponent, UxPanelHeaderTagDirective, UxPanelFooterTagDirective,
                    UxPanelHeaderRightContentTagDirective, UxPanelHeaderStatusIndicatorContentTagDirective,
                    UxPanelHeaderWithDescendantsTagDirective,
                    UxPanelFooterWithDescendantsTagDirective,
                    UxPanelHeaderRightContentWithDescendantsTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxPanelComponentModule.ctorParameters = function () { return []; };

var UxListItemComponent = (function () {
    function UxListItemComponent() {
        this.hasIconBg = false;
        this.hasMarker = false;
        this.isTagRounded = false;
        this.isLarge = false;
        this.isSmall = false;
        this.isExpanded = true;
        this.listItemClick = new EventEmitter();
        this.isHovered = false;
    }
    /**
     * @return {?}
     */
    UxListItemComponent.prototype.ngOnInit = function () {
        if (this.item) {
            this.id = this.item.id;
            this.label = this.item.label;
            this.subLabel = this.item.subLabel;
            this.iconClass = this.item.iconClass;
            this.iconTypeClass = this.item.iconTypeClass;
            this.hasIconBg = this.item.hasIconBg;
            this.typeClass = this.item.typeClass;
        }
    };
    Object.defineProperty(UxListItemComponent.prototype, "hasTag", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.tagLabel || this.tagCount) {
                return true;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxListItemComponent.prototype.onClick = function () {
        this.listItemClick.next(this.item);
    };
    /**
     * @return {?}
     */
    UxListItemComponent.prototype.onMouseenter = function () {
        this.isHovered = true;
    };
    /**
     * @return {?}
     */
    UxListItemComponent.prototype.onMouseleave = function () {
        this.isHovered = false;
    };
    return UxListItemComponent;
}());
UxListItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-list-item',
                template: "\n    <div class=\"ux-list-item-wrapper\"\n            (click)=\"onClick()\"\n            (mouseenter)=\"onMouseenter()\"\n            (mouseleave)=\"onMouseleave()\">\n        <div class=\"ux-list-item-hover-content\" *ngIf=\"isHovered && !isExpanded\">\n            <ux-a-label label=\"{{label}}\" subLabel=\"{{subLabel}}\"></ux-a-label>\n        </div>\n        <div class=\"ux-list-item ux-list-item--{{typeClass}} {{styleClass}}\"\n            [class.ux-list-item--small]=\"isSmall\"\n            [class.ux-list-item--large]=\"isLarge\"\n            [class.ux-list-item--hovered]=\"isHovered && !isExpanded\"\n            [class.ux-list-item--collapsed]=\"!isExpanded\">\n\n            <template [ngIf]=\"customContent\">\n                <ng-content select=\"uxListItemContent\"></ng-content>\n            </template>\n\n            <template [ngIf]=\"!customContent\">\n                <div *ngIf=\"iconClass\" class=\"ux-a-icon ux-a-icon--{{iconTypeClass}}\"\n                     [class.ux-a-icon--rounded]=\"hasIconBg\">\n                    <span class=\"fa {{iconClass}} ux-a-icon__icon\"></span>\n                </div>\n\n                <ux-a-marker *ngIf=\"hasMarker\" typeClass=\"{{markerTypeClass}}\"></ux-a-marker>\n\n                <template [ngIf]=\"customSubLabel\">\n                    <ux-a-label label=\"{{label}}\">\n                        <uxLabelSubLabel>\n                            <ng-content select=\"uxListItemSubLabel\"></ng-content>\n                        </uxLabelSubLabel>\n                    </ux-a-label>\n                </template>\n\n                <template [ngIf]=\"!customSubLabel\">\n                    <ux-a-label label=\"{{label}}\" subLabel=\"{{subLabel}}\"></ux-a-label>\n                </template>\n\n                <div *ngIf=\"hasTag\" class=\"ux-list-item__right-content\">\n                    <ux-a-tag label=\"{{tagLabel}}\" subLabel=\"{{tagCount}}\"\n                              typeClass=\"{{tagTypeClass}}\" [isRounded]=\"isTagRounded\"></ux-a-tag>\n                </div>\n\n            </template>\n        </div>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxListItemComponent.ctorParameters = function () { return []; };
UxListItemComponent.propDecorators = {
    'item': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'id': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'iconTypeClass': [{ type: Input },],
    'hasIconBg': [{ type: Input },],
    'hasMarker': [{ type: Input },],
    'markerTypeClass': [{ type: Input },],
    'tagTypeClass': [{ type: Input },],
    'tagLabel': [{ type: Input },],
    'tagCount': [{ type: Input },],
    'isTagRounded': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'isSmall': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'isExpanded': [{ type: Input },],
    'listItemClick': [{ type: Output },],
    'customContent': [{ type: ContentChild, args: [forwardRef(function () { return UxListItemContentTagDirective; }),] },],
    'customSubLabel': [{ type: ContentChild, args: [forwardRef(function () { return UxListItemSubLabelTagDirective; }),] },],
};
var UxListItemSubLabelTagDirective = (function () {
    function UxListItemSubLabelTagDirective() {
    }
    return UxListItemSubLabelTagDirective;
}());
UxListItemSubLabelTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxListItemSubLabel' },] },
];
/**
 * @nocollapse
 */
UxListItemSubLabelTagDirective.ctorParameters = function () { return []; };
var UxListItemContentTagDirective = (function () {
    function UxListItemContentTagDirective() {
    }
    return UxListItemContentTagDirective;
}());
UxListItemContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxListItemContent' },] },
];
/**
 * @nocollapse
 */
UxListItemContentTagDirective.ctorParameters = function () { return []; };
var UxListItemComponentModule = (function () {
    function UxListItemComponentModule() {
    }
    return UxListItemComponentModule;
}());
UxListItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxTagComponentModule, UxIconComponentModule, UxMarkerComponentModule, UxLabelComponentModule],
                exports: [UxListItemComponent, UxListItemSubLabelTagDirective, UxListItemContentTagDirective],
                declarations: [UxListItemComponent, UxListItemSubLabelTagDirective, UxListItemContentTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxListItemComponentModule.ctorParameters = function () { return []; };

var UxListItemsComponent = (function () {
    function UxListItemsComponent() {
        this.expanded = false;
        this.isSmall = false;
        this.isLarge = false;
        this.listItemClick = new EventEmitter();
    }
    /**
     * @param {?} item
     * @return {?}
     */
    UxListItemsComponent.prototype.onListItemClick = function (item) {
        this.listItemClick.emit(item);
    };
    return UxListItemsComponent;
}());
UxListItemsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-list-items',
                template: "\n    <div *ngIf=\"title\" class=\"ux-list-items-title {{titleClass}}\">{{title}}</div>\n    <ul class=\"ux-list-items\">\n        <template [ngIf]=\"links\">\n            <ux-list-item *ngFor=\"let link of links\"\n                           [item]=\"link\"\n                           (listItemClick)=\"onListItemClick($event)\"\n                           [isExpanded]=\"expanded\"\n                           [isSmall]=\"isSmall\"\n                           [isLarge]=\"isLarge\">\n            </ux-list-item>\n        </template>\n        <template [ngIf]=\"!links\">\n            <ng-content></ng-content>\n        </template>\n    </ul>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxListItemsComponent.ctorParameters = function () { return []; };
UxListItemsComponent.propDecorators = {
    'title': [{ type: Input },],
    'titleClass': [{ type: Input },],
    'expanded': [{ type: Input },],
    'links': [{ type: Input },],
    'isSmall': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'listItemClick': [{ type: Output },],
};
var UxListItemsComponentModule = (function () {
    function UxListItemsComponentModule() {
    }
    return UxListItemsComponentModule;
}());
UxListItemsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxListItemComponentModule],
                exports: [UxListItemsComponent],
                declarations: [UxListItemsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxListItemsComponentModule.ctorParameters = function () { return []; };

var UxActionBoxComponent = (function () {
    function UxActionBoxComponent() {
        this.items = [];
        this.isCollapsible = true;
        this.isExpanded = true;
    }
    Object.defineProperty(UxActionBoxComponent.prototype, "stateCollapsedClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (!this.isExpanded) {
                return 'ux-action-box--collapsed';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} link
     * @return {?}
     */
    UxActionBoxComponent.prototype.onItemClick = function (link) {
        if (link && link.command) {
            link.command();
        }
    };
    /**
     * @return {?}
     */
    UxActionBoxComponent.prototype.onToggle = function () {
        if (this.isCollapsible) {
            this.isExpanded = !this.isExpanded;
        }
    };
    return UxActionBoxComponent;
}());
UxActionBoxComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-action-box',
                template: "\n        <div class=\"flex-container flex-container--align-top\">\n            <div class=\"ux-action-box__content\">\n                <ng-content></ng-content>\n            </div>\n            <div class=\"ux-action-box__actions\" [class.ux-action-box__actions--collapsed]=\"!isExpanded\">\n                <ux-panel styleClass=\"ux-action-box {{stateCollapsedClass}}\">\n                    <uxPanelHeader class=\"ux-panel-header\">\n                        <ux-a-label label=\"{{title}}\" styleClass=\"ux-action-box__label\"></ux-a-label>\n                        <div class=\"ux-panel-header__right-content\">\n                        <span *ngIf=\"isCollapsible\"\n                              class=\"fa ux-action-box__icon-toggle\"\n                              [class.fa-indent]=\"isExpanded\"\n                              [class.fa-dedent]=\"!isExpanded\"\n                              (click)=\"onToggle()\"></span>\n                        </div>\n                    </uxPanelHeader>\n\n                    <template ngFor let-item [ngForOf]=\"items\">\n                        <ux-list-items *ngIf=\"item.actions\"\n                                        title=\"{{item.label}}\"\n                                        [isSmall]=\"true\"\n                                        [links]=\"item.actions\"\n                                        [expanded]=\"isExpanded\"\n                                        (listItemClick)=\"onItemClick($event)\">\n                        </ux-list-items>\n                    </template>\n                </ux-panel>\n            </div>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxActionBoxComponent.ctorParameters = function () { return []; };
UxActionBoxComponent.propDecorators = {
    'title': [{ type: Input },],
    'items': [{ type: Input },],
    'isCollapsible': [{ type: Input },],
    'isExpanded': [{ type: Input },],
};
var UxActionBoxComponentModule = (function () {
    function UxActionBoxComponentModule() {
    }
    return UxActionBoxComponentModule;
}());
UxActionBoxComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxListItemsComponentModule, UxPanelComponentModule, UxLabelComponentModule],
                exports: [UxActionBoxComponent],
                declarations: [UxActionBoxComponent]
            },] },
];
/**
 * @nocollapse
 */
UxActionBoxComponentModule.ctorParameters = function () { return []; };

var UxActionBoxItem = (function () {
    /**
     * @param {?=} values
     */
    function UxActionBoxItem(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return UxActionBoxItem;
}());

var UxPanel = (function () {
    /**
     * @param {?=} values
     */
    function UxPanel(values) {
        if (values === void 0) { values = {}; }
        this.isExpandable = false;
        this.isExpanded = false;
        this.hasTag = false;
        Object.assign(this, values);
    }
    return UxPanel;
}());

var UxAccordionPanelsComponent = (function () {
    function UxAccordionPanelsComponent() {
        this.selectPanel = new EventEmitter();
    }
    /**
     * @param {?} item
     * @return {?}
     */
    UxAccordionPanelsComponent.prototype.collapseAll = function (item) {
        this.items.toArray().forEach(function (i) {
            if (i !== item) {
                i.isExpanded = false;
            }
        });
        this.selectPanel.emit(new UxPanel({ id: item.id, label: item.label }));
    };
    return UxAccordionPanelsComponent;
}());
UxAccordionPanelsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-accordion-panels',
                template: "\n     <ux-panels>\n        <ng-content></ng-content>\n     </ux-panels>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxAccordionPanelsComponent.ctorParameters = function () { return []; };
UxAccordionPanelsComponent.propDecorators = {
    'items': [{ type: ContentChildren, args: [forwardRef(function () { return UxAccordionPanelComponent; }),] },],
    'selectPanel': [{ type: Output },],
};
var UxAccordionPanelsComponentModule = (function () {
    function UxAccordionPanelsComponentModule() {
    }
    return UxAccordionPanelsComponentModule;
}());
UxAccordionPanelsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxPanelsComponentModule],
                exports: [UxAccordionPanelsComponent],
                declarations: [UxAccordionPanelsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxAccordionPanelsComponentModule.ctorParameters = function () { return []; };

var UxAccordionPanelComponent = (function () {
    /**
     * @param {?} uxAccordionPanelsComponent
     */
    function UxAccordionPanelComponent(uxAccordionPanelsComponent) {
        this.isExpanded = false;
        this.uxAccordionPanelsComponent = uxAccordionPanelsComponent;
    }
    /**
     * @return {?}
     */
    UxAccordionPanelComponent.prototype.toggle = function () {
        if (!this.isExpanded) {
            this.uxAccordionPanelsComponent.collapseAll(this);
            this.isExpanded = !this.isExpanded;
        }
    };
    Object.defineProperty(UxAccordionPanelComponent.prototype, "stateAccordionExpandedClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isExpanded) {
                return 'ux-panel-header--accordion-expanded';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    return UxAccordionPanelComponent;
}());
UxAccordionPanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-accordion-panel',
                template: "\n    <ux-panel id=\"{{id}}\" label=\"{{label}}\" [isExpandable]=\"true\"\n              [isExpanded]=\"isExpanded\"\n              headerStyleClass=\"{{stateAccordionExpandedClass}}\"\n              (click)=\"toggle()\">\n        <ng-content></ng-content>\n    </ux-panel>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxAccordionPanelComponent.ctorParameters = function () { return [
    { type: UxAccordionPanelsComponent, decorators: [{ type: Host }, { type: Inject, args: [forwardRef(function () { return UxAccordionPanelsComponent; }),] },] },
]; };
UxAccordionPanelComponent.propDecorators = {
    'id': [{ type: Input },],
    'label': [{ type: Input },],
    'isExpanded': [{ type: Input },],
};
var UxAccordionPanelComponentModule = (function () {
    function UxAccordionPanelComponentModule() {
    }
    return UxAccordionPanelComponentModule;
}());
UxAccordionPanelComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxPanelComponentModule],
                exports: [UxAccordionPanelComponent],
                declarations: [UxAccordionPanelComponent]
            },] },
];
/**
 * @nocollapse
 */
UxAccordionPanelComponentModule.ctorParameters = function () { return []; };

var UxAlertComponent = (function () {
    function UxAlertComponent() {
        this.typeClass = 'info';
        this.isMuted = false;
        this.contentClass = '';
    }
    /**
     * @return {?}
     */
    UxAlertComponent.prototype.ngAfterContentInit = function () {
        if (this.iconClass) {
            this.isMuted = true;
            this.contentClass = 'text with-icon';
        }
    };
    return UxAlertComponent;
}());
UxAlertComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-alert',
                template: "\n    <div class=\"alert alert-{{typeClass}} {{styleClass}}\" [class.muted]=\"isMuted\">\n      <div *ngIf=\"iconClass\" class=\"fa {{iconClass}} icon\"></div>\n      <div class=\"{{contentClass}}\">\n        <ng-content></ng-content>\n      </div>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxAlertComponent.ctorParameters = function () { return []; };
UxAlertComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'isMuted': [{ type: Input },],
};
var UxAlertComponentModule = (function () {
    function UxAlertComponentModule() {
    }
    return UxAlertComponentModule;
}());
UxAlertComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxAlertComponent],
                declarations: [UxAlertComponent]
            },] },
];
/**
 * @nocollapse
 */
UxAlertComponentModule.ctorParameters = function () { return []; };

var UxDashboardCardComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxDashboardCardComponent(uxService) {
        this.uxService = uxService;
        this.imageWidth = '64';
        this.imageHeight = '64';
        this.isIconCompact = true;
        this.buttonStyleClass = 'btn-info';
        this.isClickable = false;
        this.isHorizontalLayout = false;
    }
    /**
     * @return {?}
     */
    UxDashboardCardComponent.prototype.ngOnInit = function () {
    };
    Object.defineProperty(UxDashboardCardComponent.prototype, "stateTypeClass", {
        /**
         * @return {?}
         */
        get: function () {
            var /** @type {?} */ classes = '';
            if (this.typeClass) {
                classes = 'ux-dashboard-card--' + this.typeClass;
            }
            return classes;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} keyCode
     * @return {?}
     */
    UxDashboardCardComponent.prototype.onKeyDown = function (keyCode) {
        if (this.isClickable && keyCode === 13) {
            this.openLink();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxDashboardCardComponent.prototype.onClick = function (event) {
        if (this.isClickable) {
            this.openLink();
        }
    };
    /**
     * @return {?}
     */
    UxDashboardCardComponent.prototype.openLink = function () {
        if (this.url) {
            this.uxService.appRouter.navigate([this.url]);
        }
        if (this.urlExternal) {
            window.open(this.urlExternal, '_blank');
        }
    };
    return UxDashboardCardComponent;
}());
UxDashboardCardComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-dashboard-card',
                template: "\n      <div class=\"ux-dashboard-card {{styleClass}} {{stateTypeClass}}\" tabindex=\"1\" [class.row]=\"isHorizontalLayout\"\n                 [class.ux-dashboard-card--clickable]=\"isClickable\"\n                 [class.ux-dashboard-card--icon-compact]=\"isIconCompact\"\n                 [class.ux-dashboard-card--horizontal]=\"isHorizontalLayout\"\n                 [class.ux-dashboard-card--horizontal--icon-compact]=\"isIconCompact && isHorizontalLayout\"\n                 (click)=\"onClick($event)\"\n                 (keydown)=\"onKeyDown($event.keyCode)\">\n\n          <div *ngIf=\"iconClass\" [class.col-4]=\"isHorizontalLayout\">\n              <div class=\"ux-dashboard-card__icon-wrapper\">\n                  <span class=\"fa {{iconClass}} fa-fw ux-dashboard-card__icon-wrapper-icon\"></span>\n              </div>\n          </div>\n\n          <div *ngIf=\"imageUrl\" [class.col-4]=\"isHorizontalLayout\">\n              <div class=\"ux-dashboard-card__image-wrapper\">\n                  <img [style.height.px]=\"imageHeight\" [style.width.px]=\"imageWidth\"\n                       src=\"{{imageUrl}}\" alt=\"{{imageUrl}}\"/>\n              </div>\n          </div>\n\n          <div class=\"ux-dashboard-card__content-wrapper\" [class.col-8]=\"isHorizontalLayout\">\n              <div class=\"ux-dashboard-card__content\">\n                  <div class=\"ux-dashboard-card__content-label\">{{label}}</div>\n                  <div class=\"ux-dashboard-card__content-sub-label\">{{subLabel}}</div>\n                  <div *ngIf=\"!isClickable\" class=\"ux-dashboard-card__content-link\">\n                    <a *ngIf=\"urlExternal\" href=\"{{urlExternal}}\" target=\"_blank\"\n                       class=\"btn {{buttonStyleClass}}\" tabindex=\"1\">{{buttonLinkLabel}}</a>\n                    <a *ngIf=\"url\" [routerLink]=\"[url]\" class=\"btn {{buttonStyleClass}}\" tabindex=\"1\">{{buttonLinkLabel}}</a>\n                  </div>\n              </div>\n          </div>\n      </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxDashboardCardComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxDashboardCardComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'imageUrl': [{ type: Input },],
    'imageWidth': [{ type: Input },],
    'imageHeight': [{ type: Input },],
    'isIconCompact': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'urlExternal': [{ type: Input },],
    'url': [{ type: Input },],
    'buttonLinkLabel': [{ type: Input },],
    'buttonStyleClass': [{ type: Input },],
    'isClickable': [{ type: Input },],
    'isHorizontalLayout': [{ type: Input },],
};
var UxDashboardCardComponentModule = (function () {
    function UxDashboardCardComponentModule() {
    }
    return UxDashboardCardComponentModule;
}());
UxDashboardCardComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, RouterModule],
                exports: [UxDashboardCardComponent],
                declarations: [UxDashboardCardComponent]
            },] },
];
/**
 * @nocollapse
 */
UxDashboardCardComponentModule.ctorParameters = function () { return []; };

var UxControlFeedbackComponent = (function () {
    function UxControlFeedbackComponent() {
        this.stateClass = '';
    }
    /**
     * @return {?}
     */
    UxControlFeedbackComponent.prototype.ngAfterContentInit = function () {
        if (this.typeClass) {
            this.stateClass = 'ux-u-color-' + this.typeClass;
        }
    };
    return UxControlFeedbackComponent;
}());
UxControlFeedbackComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-control-feedback',
                template: "\n        <div class=\"ux-c-control-feedback form-control-feedback {{stateClass}}\">\n            <small class=\"ux-c-control-feedback__message\"><ng-content></ng-content></small>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxControlFeedbackComponent.ctorParameters = function () { return []; };
UxControlFeedbackComponent.propDecorators = {
    'typeClass': [{ type: Input },],
};
var UxControlFeedbackComponentModule = (function () {
    function UxControlFeedbackComponentModule() {
    }
    return UxControlFeedbackComponentModule;
}());
UxControlFeedbackComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxControlFeedbackComponent],
                declarations: [UxControlFeedbackComponent]
            },] },
];
/**
 * @nocollapse
 */
UxControlFeedbackComponentModule.ctorParameters = function () { return []; };

var UxFieldsetComponent = (function () {
    function UxFieldsetComponent() {
        this.isFirst = false;
    }
    Object.defineProperty(UxFieldsetComponent.prototype, "stateClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isFirst) {
                return 'ux-fieldset--first';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    return UxFieldsetComponent;
}());
UxFieldsetComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-fieldset',
                template: "\n        <div class=\"ux-fieldset {{styleClass}} {{stateClass}}\">\n            <div *ngIf=\"label\" class=\"ux-fieldset__label\">{{label}}</div>\n            <div class=\"ux-fieldset__content\">\n                <ng-content></ng-content>\n            </div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxFieldsetComponent.ctorParameters = function () { return []; };
UxFieldsetComponent.propDecorators = {
    'label': [{ type: Input },],
    'labelWidthClass': [{ type: Input },],
    'inputWidthClass': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'isFirst': [{ type: Input },],
};
var UxFieldsetComponentModule = (function () {
    function UxFieldsetComponentModule() {
    }
    return UxFieldsetComponentModule;
}());
UxFieldsetComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxFieldsetComponent],
                declarations: [UxFieldsetComponent]
            },] },
];
/**
 * @nocollapse
 */
UxFieldsetComponentModule.ctorParameters = function () { return []; };

var UxFieldComponent = (function () {
    /**
     * @param {?} uxFieldsetComponent
     */
    function UxFieldComponent(uxFieldsetComponent) {
        this.isVertical = false;
        this.isReadOnly = false;
        this.stateClass = '';
        this.propagateChange = function () { };
        this.validateFn = function () { };
        this.onModelChange = function () { };
        this.onModelTouched = function () { };
        this.uxFieldsetComponent = uxFieldsetComponent;
    }
    /**
     * @return {?}
     */
    UxFieldComponent.prototype.hasCustomContent = function () {
        return this.uxContentPlaceholder.nativeElement.childNodes.length > 0;
    };
    /**
     * @return {?}
     */
    UxFieldComponent.prototype.ngAfterContentInit = function () {
        if (this.isVertical) {
            this.labelWidthClass = '';
            this.inputWidthClass = '';
        }
        else {
            if (this.uxFieldsetComponent) {
                if (this.uxFieldsetComponent.labelWidthClass && !this.labelWidthClass) {
                    this.labelWidthClass = this.uxFieldsetComponent.labelWidthClass;
                }
                if (this.uxFieldsetComponent.inputWidthClass && !this.inputWidthClass) {
                    this.inputWidthClass = this.uxFieldsetComponent.inputWidthClass;
                }
            }
            if (!this.labelWidthClass) {
                this.labelWidthClass = 'col-md-4';
            }
            if (!this.inputWidthClass) {
                this.inputWidthClass = 'col-md-8';
            }
        }
        if (this.feedbackTypeClass) {
            this.stateClass = 'has-' + this.feedbackTypeClass;
        }
    };
    Object.defineProperty(UxFieldComponent.prototype, "isFieldInvalid", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.formControl) {
                if (this.formControl.touched && this.formControl.invalid) {
                    return true;
                }
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxFieldComponent.prototype, "inputValue", {
        /**
         * @return {?}
         */
        get: function () {
            return this._inputValue;
        },
        /**
         * @param {?} value
         * @return {?}
         */
        set: function (value) {
            this._inputValue = value;
            this.propagateChange(value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} value
     * @return {?}
     */
    UxFieldComponent.prototype.onChange = function (value) {
        this.propagateChange(value);
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    UxFieldComponent.prototype.registerOnChange = function (fn) {
        this.onModelChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    UxFieldComponent.prototype.registerOnTouched = function (fn) {
        this.onModelTouched = fn;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    UxFieldComponent.prototype.writeValue = function (value) {
        this._inputValue = value;
    };
    /**
     * @param {?} c
     * @return {?}
     */
    UxFieldComponent.prototype.validate = function (c) {
        return this.validateFn(c);
    };
    return UxFieldComponent;
}());
UxFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-field',
                template: "\n        <div class=\"ux-field form-group {{styleClass}} {{stateClass}}\"\n                [class.row]=\"!isVertical\"\n                [class.ux-field--vertical]=\"isVertical\">\n            <label class=\"ux-field__label-wrapper {{labelWidthClass}}\"\n                   [class.col-form-label]=\"!isVertical\" [class.form-control-label]=\"isVertical\">\n                <span class=\"ux-field__label\">{{label}}</span>\n                <span *ngIf=\"isRequired\" class=\"ux-field__required-indicator ux-u-color-danger\">*</span>\n            </label>\n            <div class=\"{{inputWidthClass}}\">\n                <div #uxContentPlaceholder [class.d-none]=\"!hasCustomContent()\"\n                        [class.ux-field__read-only]=\"isReadOnly\">\n                        <ng-content></ng-content>\n                        <ux-control-feedback *ngIf=\"isFieldInvalid\" typeClass=\"danger\">{{validationErrorMessage}}</ux-control-feedback>\n                </div>\n                <div *ngIf=\"!hasCustomContent()\">\n                    <!-- TODO <input type=\"text\" class=\"form-control\" (onChange)=\"onChange($event)\" [value]=\"inputValue\"/>-->\n                </div>\n            </div>\n        </div>\n        ",
                providers: [
                    { provide: NG_VALUE_ACCESSOR, useExisting: forwardRef(function () { return UxFieldComponent; }), multi: true },
                    { provide: NG_VALIDATORS, useExisting: forwardRef(function () { return UxFieldComponent; }), multi: true }
                ]
            },] },
];
/**
 * @nocollapse
 */
UxFieldComponent.ctorParameters = function () { return [
    { type: UxFieldsetComponent, decorators: [{ type: Optional }, { type: Host }, { type: Inject, args: [forwardRef(function () { return UxFieldsetComponent; }),] },] },
]; };
UxFieldComponent.propDecorators = {
    'uxContentPlaceholder': [{ type: ViewChild, args: ['uxContentPlaceholder',] },],
    'label': [{ type: Input },],
    'isVertical': [{ type: Input },],
    'isRequired': [{ type: Input },],
    'feedbackTypeClass': [{ type: Input },],
    'labelWidthClass': [{ type: Input },],
    'inputWidthClass': [{ type: Input },],
    'isReadOnly': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'formControl': [{ type: Input },],
    'validationErrorMessage': [{ type: Input },],
};
var UxFieldComponentModule = (function () {
    function UxFieldComponentModule() {
    }
    return UxFieldComponentModule;
}());
UxFieldComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxControlFeedbackComponentModule],
                exports: [UxFieldComponent],
                declarations: [UxFieldComponent]
            },] },
];
/**
 * @nocollapse
 */
UxFieldComponentModule.ctorParameters = function () { return []; };

var __extends = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var UxFieldCalendarComponent = (function (_super) {
    __extends(UxFieldCalendarComponent, _super);
    /**
     * @param {?} uxFieldsetComponent
     * @param {?} el
     */
    function UxFieldCalendarComponent(uxFieldsetComponent, el) {
        var _this = _super.call(this, uxFieldsetComponent) || this;
        _this.el = el;
        _this.isVertical = false;
        _this.isReadOnly = false;
        return _this;
    }
    /**
     * @return {?}
     */
    UxFieldCalendarComponent.prototype.ngAfterContentInit = function () {
        var /** @type {?} */ input = this.el.nativeElement.querySelector('p-calendar input');
        console.log(input);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxFieldCalendarComponent.prototype.onCalendarClick = function (event) {
    };
    return UxFieldCalendarComponent;
}(UxFieldComponent));
UxFieldCalendarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-field-calendar',
                template: "\n        <ux-field label=\"{{label}}\">\n          <div class=\"input-group\">\n            <ng-content></ng-content>\n            <div class=\"input-group-addon\"><span class=\"fa fa-calendar\" (click)=\"onCalendarClick($event)\"></span></div>\n          </div>\n        </ux-field>\n        "
            },] },
];
/**
 * @nocollapse
 */
UxFieldCalendarComponent.ctorParameters = function () { return [
    { type: UxFieldsetComponent, decorators: [{ type: Optional }, { type: Host }, { type: Inject, args: [forwardRef(function () { return UxFieldsetComponent; }),] },] },
    { type: ElementRef, },
]; };
UxFieldCalendarComponent.propDecorators = {
    'label': [{ type: Input },],
    'isVertical': [{ type: Input },],
    'isRequired': [{ type: Input },],
    'feedbackType': [{ type: Input },],
    'labelWidthClass': [{ type: Input },],
    'inputWidthClass': [{ type: Input },],
    'isReadOnly': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'formControl': [{ type: Input },],
    'validationErrorMessage': [{ type: Input },],
};
var UxFieldCalendarComponentModule = (function () {
    function UxFieldCalendarComponentModule() {
    }
    return UxFieldCalendarComponentModule;
}());
UxFieldCalendarComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxFieldComponentModule],
                exports: [UxFieldCalendarComponent],
                declarations: [UxFieldCalendarComponent]
            },] },
];
/**
 * @nocollapse
 */
UxFieldCalendarComponentModule.ctorParameters = function () { return []; };

var UxSwitchComponent = (function () {
    function UxSwitchComponent() {
        this.isChecked = false;
        this.isDisabled = false;
        this.isSmall = false;
        this.isLarge = false;
        this.toggle = new EventEmitter();
    }
    /**
     * @return {?}
     */
    UxSwitchComponent.prototype.handleClicked = function () {
        if (this.isDisabled) {
            return;
        }
        this.isChecked = !this.isChecked;
        this.toggle.emit(this.isChecked);
    };
    return UxSwitchComponent;
}());
UxSwitchComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-switch',
                template: "\n        <span\n          class=\"ux-c-switch {{styleClass}}\"\n          [class.ux-c-switch--small]=\"isSmall\"\n          [class.ux-c-switch--large]=\"isLarge\"\n          [class.ux-c-switch--checked]=\"isChecked\"\n          [class.ux-c-switch--disabled]=\"isDisabled\">\n          <small class=\"ux-c-switch__handle\"></small>\n        </span>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxSwitchComponent.ctorParameters = function () { return []; };
UxSwitchComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'isChecked': [{ type: Input },],
    'isDisabled': [{ type: Input },],
    'isSmall': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'label': [{ type: Input },],
    'toggle': [{ type: Output },],
    'handleClicked': [{ type: HostListener, args: ['click',] },],
};
var UxSwitchComponentModule = (function () {
    function UxSwitchComponentModule() {
    }
    return UxSwitchComponentModule;
}());
UxSwitchComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxSwitchComponent],
                declarations: [UxSwitchComponent]
            },] },
];
/**
 * @nocollapse
 */
UxSwitchComponentModule.ctorParameters = function () { return []; };

var UxBadgeGroupComponent = (function () {
    function UxBadgeGroupComponent() {
        this.typeClass = 'default';
        this.isSmall = false;
        this.isLarge = false;
        this.isPill = false;
    }
    return UxBadgeGroupComponent;
}());
UxBadgeGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-badge-group',
                template: "\n        <span class=\"ux-badge-group ux-badge-group-{{typeClass}} {{styleClass}}\">\n            <span class=\"badge badge-{{typeClass}} ux-badge-group__badge-left\"\n                  [class.badge-pill]=\"isPill\"\n                  [class.badge--small]=\"isSmall\"\n                  [class.badge--large]=\"isLarge\">\n                  {{label}}\n            </span>\n            <span class=\"badge badge-secondary ux-badge-group__badge-right ux-badge-group__badge-right--{{typeClass}}\"\n                  [class.badge-pill]=\"isPill\"\n                  [class.badge--small]=\"isSmall\"\n                  [class.badge--large]=\"isLarge\">\n                  {{subLabel}}\n            </span>\n        </span>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxBadgeGroupComponent.ctorParameters = function () { return []; };
UxBadgeGroupComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'isSmall': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'isPill': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
};
var UxBadgeGroupComponentModule = (function () {
    function UxBadgeGroupComponentModule() {
    }
    return UxBadgeGroupComponentModule;
}());
UxBadgeGroupComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxBadgeGroupComponent],
                declarations: [UxBadgeGroupComponent]
            },] },
];
/**
 * @nocollapse
 */
UxBadgeGroupComponentModule.ctorParameters = function () { return []; };

var UxButtonComponent = (function () {
    function UxButtonComponent() {
        this.type = 'button';
        this.typeClass = 'primary';
        this.isOutline = false;
        this.isSecondary = false;
        this.isSmall = false;
        this.isLarge = false;
        this.isBlock = false;
        this.isDisabled = false;
    }
    /**
     * @return {?}
     */
    UxButtonComponent.prototype.ngAfterContentInit = function () {
        this.btnTypeClass = '';
        var /** @type {?} */ prefix = 'btn-';
        if (this.isSecondary) {
            this.typeClass = 'secondary';
        }
        if (this.isOutline) {
            prefix += 'outline-';
        }
        this.btnTypeClass = prefix + this.typeClass;
        this.btnSizeClass = '';
        if (this.isSmall) {
            this.btnSizeClass = 'btn-sm';
        }
        if (this.isLarge) {
            this.btnSizeClass = 'btn-lg';
        }
    };
    return UxButtonComponent;
}());
UxButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-button',
                template: "\n        <button type=\"{{type}}\" class=\"btn {{btnTypeClass}} {{btnSizeClass}} {{styleClass}}\"\n                [class.btn-block]=\"isBlock\" [disabled]=\"isDisabled\">\n            <i *ngIf=\"iconClass\" class=\"fa {{ iconClass }} ux-button__icon\"></i><ng-content></ng-content>\n        </button>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxButtonComponent.ctorParameters = function () { return []; };
UxButtonComponent.propDecorators = {
    'type': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'isOutline': [{ type: Input },],
    'isSecondary': [{ type: Input },],
    'isSmall': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'isBlock': [{ type: Input },],
    'isDisabled': [{ type: Input },],
};
var UxButtonComponentModule = (function () {
    function UxButtonComponentModule() {
    }
    return UxButtonComponentModule;
}());
UxButtonComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxButtonComponent],
                declarations: [UxButtonComponent]
            },] },
];
/**
 * @nocollapse
 */
UxButtonComponentModule.ctorParameters = function () { return []; };

var UxGrowlComponent = (function () {
    /**
     * @param {?} el
     * @param {?} differs
     */
    function UxGrowlComponent(el, differs) {
        this.el = el;
        this.sticky = false;
        this.life = 3000;
        this.differ = differs.find([]).create(undefined);
    }
    /**
     * @return {?}
     */
    UxGrowlComponent.prototype.ngAfterViewInit = function () {
        this.container = (this.containerViewChild.nativeElement);
    };
    /**
     * @return {?}
     */
    UxGrowlComponent.prototype.ngDoCheck = function () {
        var _this = this;
        var /** @type {?} */ changes = this.differ.diff(this.value);
        if (changes && this.container) {
            if (this.stopDoCheckPropagation) {
                this.stopDoCheckPropagation = false;
            }
            else if (this.value && this.value.length) {
                if (!this.sticky) {
                    if (this.timeout) {
                        clearTimeout(this.timeout);
                    }
                    this.timeout = setTimeout(function () {
                        _this.removeAll();
                    }, this.life);
                }
            }
        }
    };
    /**
     * @param {?} msg
     * @param {?} msgel
     * @return {?}
     */
    UxGrowlComponent.prototype.remove = function (msg, msgel) {
        var _this = this;
        this.stopDoCheckPropagation = true;
        setTimeout(function () {
            _this.value.splice(_this.findMessageIndex(msg), 1);
        }, 250);
    };
    /**
     * @return {?}
     */
    UxGrowlComponent.prototype.removeAll = function () {
        var _this = this;
        if (this.value && this.value.length) {
            this.stopDoCheckPropagation = true;
            setTimeout(function () {
                _this.value.splice(0, _this.value.length);
            }, 250);
        }
    };
    /**
     * @param {?} msg
     * @return {?}
     */
    UxGrowlComponent.prototype.findMessageIndex = function (msg) {
        var /** @type {?} */ index = -1;
        if (this.value && this.value.length) {
            for (var /** @type {?} */ i = 0; i < this.value.length; i++) {
                if (this.value[i] === msg) {
                    index = i;
                    break;
                }
            }
        }
        return index;
    };
    /**
     * @return {?}
     */
    UxGrowlComponent.prototype.ngOnDestroy = function () {
        if (!this.sticky) {
            clearTimeout(this.timeout);
        }
    };
    return UxGrowlComponent;
}());
UxGrowlComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-growl',
                template: "\n        <div #container class=\"ux-growl {{styleClass}}\">\n            <div #msgel *ngFor=\"let msg of value\"\n                 class=\"ux-growl-item-container\" aria-live=\"polite\"\n                 [ngClass]=\"{'ux-growl-item-container--info':msg.severity == 'info',\n                             'ux-growl-item-container--warning':msg.severity == 'warning',\n                             'ux-growl-item-container--danger':msg.severity == 'danger',\n                             'ux-growl-item-container--success':msg.severity == 'success'}\">\n                <div class=\"ux-growl-item\">\n                     <div class=\"ux-growl-item-close fa fa-close\" (click)=\"remove(msg,msgel)\"></div>\n                     <span class=\"ux-growl-item-image fa fa-2x\"\n                        [ngClass]=\"{'fa-info-circle':msg.severity == 'info',\n                                    'fa-exclamation-circle':msg.severity == 'warning',\n                                    'fa-times-circle':msg.severity == 'danger',\n                                    'fa-check-circle':msg.severity == 'success'}\"></span>\n                     <div class=\"ux-growl-item-message\">\n                        <span class=\"ux-growl-item-message-title\">{{msg.summary}}</span>\n                        <p class=\"ux-growl-item-message-detail\">{{msg.detail}}</p>\n                     </div>\n                </div>\n            </div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxGrowlComponent.ctorParameters = function () { return [
    { type: ElementRef, },
    { type: IterableDiffers, },
]; };
UxGrowlComponent.propDecorators = {
    'sticky': [{ type: Input },],
    'life': [{ type: Input },],
    'value': [{ type: Input },],
    'style': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'containerViewChild': [{ type: ViewChild, args: ['container',] },],
};
var UxGrowlComponentModule = (function () {
    function UxGrowlComponentModule() {
    }
    return UxGrowlComponentModule;
}());
UxGrowlComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxGrowlComponent],
                declarations: [UxGrowlComponent]
            },] },
];
/**
 * @nocollapse
 */
UxGrowlComponentModule.ctorParameters = function () { return []; };

var UxLayoutAppComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxLayoutAppComponent(uxService) {
        this.uxService = uxService;
        this.styleClass = '';
        this.uxService.resetSidebarClasses();
        this.uxService.setActiveBreakpoint(window.innerWidth);
        this.uxService.setWindowHeight(window.innerHeight);
        Observable.fromEvent(window, 'resize')
            .debounceTime(300)
            .subscribe(function (event) {
            uxService.setActiveBreakpoint(window.innerWidth);
            uxService.setWindowHeight(window.innerHeight);
        });
    }
    /**
     * @return {?}
     */
    UxLayoutAppComponent.prototype.ngOnInit = function () {
        // find a better way to inject the class into the app-wrapper instead
        // currently not working cause of the dynamic classes set
        if (this.styleClass !== '') {
            var /** @type {?} */ styles = this.styleClass.split(' ');
            if (styles != null) {
                styles.forEach(function (styleClass) {
                    var /** @type {?} */ wrapper = document.getElementById('app-wrapper');
                    if (wrapper != null) {
                        wrapper.classList.add(styleClass);
                    }
                });
            }
        }
    };
    return UxLayoutAppComponent;
}());
UxLayoutAppComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-app',
                template: "\n        <div id=\"app-wrapper\" [ngClass]=\"uxService.activeBreakpoint | async\"\n             [class.sidebar-active]=\"uxService.isSidebarActive\"\n             [class.sidebar-state-open]=\"uxService.isSidebarStateOpen\"\n             [class.sidebar-state-close]=\"!uxService.isSidebarStateOpen\"\n             [class.sidebar-state-close-with-icons]=\"uxService.isSidebarStateCloseWithIcons\"\n             [class.sidebar-always-visible]=\"uxService.isSidebarAlwaysVisible\"\n             [class.sidebar-collapsible]=\"uxService.isSidebarCollapsible\"\n             [class.sidebar-inner-active]=\"uxService.isSidebarInnerActive\"\n             [class.sidebar-mobile-visible]=\"uxService.isSidebarMobileVisible\">\n\n            <ng-content select=\"uxLayoutAppSidebarContainer\"></ng-content>\n            <ng-content select=\"uxLayoutAppMainContainer\"></ng-content>\n\n            <ux-growl [value]=\"uxService.growlMessages\"\n                      [sticky]=\"uxService.isGrowlSticky\"\n                      [life]=\"uxService.growlLife\"></ux-growl>\n\n            <ux-block-document [isBlocked]=\"uxService.isBlockDocumentActive\"></ux-block-document>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutAppComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxLayoutAppComponent.propDecorators = {
    'styleClass': [{ type: Input },],
};
var UxLayoutAppSidebarContainerTagDirective = (function () {
    function UxLayoutAppSidebarContainerTagDirective() {
    }
    return UxLayoutAppSidebarContainerTagDirective;
}());
UxLayoutAppSidebarContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppSidebarContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppSidebarContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainContainerTagDirective = (function () {
    function UxLayoutAppMainContainerTagDirective() {
    }
    return UxLayoutAppMainContainerTagDirective;
}());
UxLayoutAppMainContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppComponentModule = (function () {
    function UxLayoutAppComponentModule() {
    }
    return UxLayoutAppComponentModule;
}());
UxLayoutAppComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxGrowlComponentModule, UxBlockDocumentComponentModule],
                exports: [UxLayoutAppComponent, UxLayoutAppMainContainerTagDirective, UxLayoutAppSidebarContainerTagDirective],
                declarations: [UxLayoutAppComponent, UxLayoutAppMainContainerTagDirective, UxLayoutAppSidebarContainerTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutAppComponentModule.ctorParameters = function () { return []; };

var UxLayoutAppMainComponent = (function () {
    function UxLayoutAppMainComponent() {
    }
    return UxLayoutAppMainComponent;
}());
UxLayoutAppMainComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-app-main',
                template: "\n        <div id=\"main\" [ngClass]=\"styleClass\">\n            <ng-content select=\"uxLayoutAppMainHeaderContainer\"></ng-content>\n            <ng-content select=\"uxLayoutAppMainNavBarContainer\"></ng-content>\n            <ng-content select=\"uxLayoutAppMainTopMessageContainer\"></ng-content>\n            <ng-content select=\"uxLayoutAppMainBreadcrumbsContainer\"></ng-content>\n\n            <div class=\"ux-layout-banner-top-wrapper\">\n                <ng-content select=\"uxLayoutAppMainBannerTopContainer\"></ng-content>\n            </div>\n\n            <div id=\"main-content\">\n                <ng-content select=\"uxLayoutAppMainContentContainer\"></ng-content>\n            </div>\n\n            <div class=\"ux-layout-banner-bottom-wrapper\">\n                <ng-content select=\"uxLayoutAppMainBannerBottomContainer\"></ng-content>\n            </div>\n            <ng-content select=\"uxLayoutAppMainFooterContainer\"></ng-content>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainComponent.ctorParameters = function () { return []; };
UxLayoutAppMainComponent.propDecorators = {
    'styleClass': [{ type: Input },],
};
var UxLayoutAppMainHeaderContainerTagDirective = (function () {
    function UxLayoutAppMainHeaderContainerTagDirective() {
    }
    return UxLayoutAppMainHeaderContainerTagDirective;
}());
UxLayoutAppMainHeaderContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainHeaderContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainHeaderContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainBannerTopContainerTagDirective = (function () {
    function UxLayoutAppMainBannerTopContainerTagDirective() {
    }
    return UxLayoutAppMainBannerTopContainerTagDirective;
}());
UxLayoutAppMainBannerTopContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainBannerTopContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainBannerTopContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainNavBarContainerTagDirective = (function () {
    function UxLayoutAppMainNavBarContainerTagDirective() {
    }
    return UxLayoutAppMainNavBarContainerTagDirective;
}());
UxLayoutAppMainNavBarContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainNavBarContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainNavBarContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainTopMessageContainerTagDirective = (function () {
    function UxLayoutAppMainTopMessageContainerTagDirective() {
    }
    return UxLayoutAppMainTopMessageContainerTagDirective;
}());
UxLayoutAppMainTopMessageContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainTopMessageContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainTopMessageContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainBreadcrumbsContainerTagDirective = (function () {
    function UxLayoutAppMainBreadcrumbsContainerTagDirective() {
    }
    return UxLayoutAppMainBreadcrumbsContainerTagDirective;
}());
UxLayoutAppMainBreadcrumbsContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainBreadcrumbsContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainBreadcrumbsContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainContentContainerTagDirective = (function () {
    function UxLayoutAppMainContentContainerTagDirective() {
    }
    return UxLayoutAppMainContentContainerTagDirective;
}());
UxLayoutAppMainContentContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainContentContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainContentContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainBannerBottomContainerTagDirective = (function () {
    function UxLayoutAppMainBannerBottomContainerTagDirective() {
    }
    return UxLayoutAppMainBannerBottomContainerTagDirective;
}());
UxLayoutAppMainBannerBottomContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainBannerBottomContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainBannerBottomContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainFooterContainerTagDirective = (function () {
    function UxLayoutAppMainFooterContainerTagDirective() {
    }
    return UxLayoutAppMainFooterContainerTagDirective;
}());
UxLayoutAppMainFooterContainerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutAppMainFooterContainer' },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainFooterContainerTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppMainComponentModule = (function () {
    function UxLayoutAppMainComponentModule() {
    }
    return UxLayoutAppMainComponentModule;
}());
UxLayoutAppMainComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [
                    UxLayoutAppMainComponent,
                    UxLayoutAppMainHeaderContainerTagDirective,
                    UxLayoutAppMainNavBarContainerTagDirective,
                    UxLayoutAppMainBannerTopContainerTagDirective,
                    UxLayoutAppMainTopMessageContainerTagDirective,
                    UxLayoutAppMainBreadcrumbsContainerTagDirective,
                    UxLayoutAppMainContentContainerTagDirective,
                    UxLayoutAppMainBannerBottomContainerTagDirective,
                    UxLayoutAppMainFooterContainerTagDirective
                ],
                declarations: [
                    UxLayoutAppMainComponent,
                    UxLayoutAppMainHeaderContainerTagDirective,
                    UxLayoutAppMainNavBarContainerTagDirective,
                    UxLayoutAppMainBannerTopContainerTagDirective,
                    UxLayoutAppMainTopMessageContainerTagDirective,
                    UxLayoutAppMainBreadcrumbsContainerTagDirective,
                    UxLayoutAppMainContentContainerTagDirective,
                    UxLayoutAppMainBannerBottomContainerTagDirective,
                    UxLayoutAppMainFooterContainerTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutAppMainComponentModule.ctorParameters = function () { return []; };

var UxLayoutHeaderComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxLayoutHeaderComponent(uxService) {
        this.uxService = uxService;
        this.appFullName = '';
        this.appShortName = '';
        this.appSubtitle = '';
        this.envLabel = '';
        this.languageCodes = '';
        this.isCustomRightContent = false;
        this.isCustomTitleContent = false;
        this.isShowExtraButtonAction = false;
        this.isShowLanguageSelector = true;
        this.homeUrl = '/screen/home';
        this.isHideLogo = false;
        this.languageChanged = new EventEmitter();
    }
    Object.defineProperty(UxLayoutHeaderComponent.prototype, "hasSubtitle", {
        /**
         * @return {?}
         */
        get: function () {
            return this.appSubtitle !== '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxLayoutHeaderComponent.prototype, "activeLanguage", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.selectedLanguage) {
                return this.selectedLanguage;
            }
            return this.uxService.activeLanguage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxLayoutHeaderComponent.prototype, "appShortNameGen", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.appShortName === '') {
                return this.appFullName;
            }
            else {
                return this.appShortName;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} language
     * @return {?}
     */
    UxLayoutHeaderComponent.prototype.onLanguageChanged = function (language) {
        this.uxService.activeLanguage = language;
        this.languageChanged.emit(language);
    };
    return UxLayoutHeaderComponent;
}());
UxLayoutHeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-header',
                template: "\n        <div id=\"app-header\">\n            <a [routerLink]=\"homeUrl\" *ngIf=\"!isHideLogo\">\n                <div class=\"logo logo--{{activeLanguageCode}}\">\n                    <div *ngIf=\"envLabel\" class=\"env\">{{envLabel}}</div>\n                </div>\n            </a>\n\n            <div *ngIf=\"isCustomTitleContent\" class=\"title\">\n                <ng-content select=\"uxLayoutHeaderTitleContent\"></ng-content>\n            </div>\n\n            <div *ngIf=\"!isCustomTitleContent\" class=\"title\">\n                <h1 [class.with-subtitle]=\"hasSubtitle\"\n                    [class.no-margin-left]=\"isHideLogo\" class=\"full\"\n                    [ngClass]=\"appFullNameStyleClass\">{{appFullName}}</h1>\n                <h1 class=\"short\">{{appShortNameGen}}</h1>\n                <h2 *ngIf=\"appSubtitle\">\n                    {{appSubtitle}}\n                </h2>\n            </div>\n\n            <div *ngIf=\"!isCustomRightContent\" class=\"right-links\">\n                <div *ngIf=\"isShowLanguageSelector\" class=\"links\">\n                    <ul>\n                        <li>\n                            <ux-language-selector *ngIf=\"languageCodes\"\n                                    (languageChanged)=\"onLanguageChanged($event)\"\n                                    languageCodes=\"{{languageCodes}}\"\n                                    [selectedLanguage]=\"activeLanguage\">\n                            </ux-language-selector>\n                            <ux-language-selector *ngIf=\"!languageCodes\"\n                                    (languageChanged)=\"onLanguageChanged($event)\"\n                                    [selectedLanguage]=\"activeLanguage\">\n                            </ux-language-selector>\n                        </li>\n                    </ul>\n                </div>\n                <div class=\"user-infos\" *ngIf=\"userInfos\">\n                    <template [ngIf]=\"userProfileLinks\">\n                        <ux-dropdown-button label=\"{{userInfos}}\"\n                                            [isLinkToggle]=\"true\"\n                                            [isDropDownRightAligned]=\"true\"\n                                            [links]=\"userProfileLinks\">\n                        </ux-dropdown-button>\n                    </template>\n                    <template [ngIf]=\"!userProfileLinks\">\n                        {{userInfos}}\n                    </template>\n                </div>\n            </div>\n\n            <div *ngIf=\"isCustomRightContent\" class=\"right-content\">\n                <ng-content select=\"uxLayoutHeaderRightContent\"></ng-content>\n            </div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxLayoutHeaderComponent.propDecorators = {
    'appFullName': [{ type: Input },],
    'appFullNameStyleClass': [{ type: Input },],
    'appShortName': [{ type: Input },],
    'appSubtitle': [{ type: Input },],
    'envLabel': [{ type: Input },],
    'userInfos': [{ type: Input },],
    'languageCodes': [{ type: Input },],
    'isCustomRightContent': [{ type: Input },],
    'isCustomTitleContent': [{ type: Input },],
    'isShowExtraButtonAction': [{ type: Input },],
    'isShowLanguageSelector': [{ type: Input },],
    'homeUrl': [{ type: Input },],
    'isHideLogo': [{ type: Input },],
    'userProfileLinks': [{ type: Input },],
    'selectedLanguage': [{ type: Input },],
    'languageChanged': [{ type: Output },],
};
var UxLayoutHeaderRightContentTagDirective = (function () {
    function UxLayoutHeaderRightContentTagDirective() {
    }
    return UxLayoutHeaderRightContentTagDirective;
}());
UxLayoutHeaderRightContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutHeaderRightContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderRightContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutHeaderUserProfileLinksTagDirective = (function () {
    function UxLayoutHeaderUserProfileLinksTagDirective() {
    }
    return UxLayoutHeaderUserProfileLinksTagDirective;
}());
UxLayoutHeaderUserProfileLinksTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutHeaderUserProfileLinks' },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderUserProfileLinksTagDirective.ctorParameters = function () { return []; };
var UxLayoutHeaderTitleContentTagDirective = (function () {
    function UxLayoutHeaderTitleContentTagDirective() {
    }
    return UxLayoutHeaderTitleContentTagDirective;
}());
UxLayoutHeaderTitleContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutHeaderTitleContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderTitleContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutHeaderComponentModule = (function () {
    function UxLayoutHeaderComponentModule() {
    }
    return UxLayoutHeaderComponentModule;
}());
UxLayoutHeaderComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, RouterModule, UxLanguageSelectorComponentModule,
                    UxDropdownButtonComponentModule, UxDropdownButtonItemComponentModule],
                exports: [UxLayoutHeaderComponent, UxLayoutHeaderRightContentTagDirective, UxLayoutHeaderTitleContentTagDirective,
                    UxLayoutHeaderUserProfileLinksTagDirective],
                declarations: [UxLayoutHeaderComponent, UxLayoutHeaderRightContentTagDirective, UxLayoutHeaderTitleContentTagDirective,
                    UxLayoutHeaderUserProfileLinksTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderComponentModule.ctorParameters = function () { return []; };

var UxLayoutFooterComponent = (function () {
    function UxLayoutFooterComponent() {
        this.isStyleInverse = false;
        this.isCompact = false;
    }
    return UxLayoutFooterComponent;
}());
UxLayoutFooterComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-footer',
                template: "\n        <template [ngIf]=\"isCompact\">\n            <div id=\"app-footer\" class=\"compact\" [class.inverse]=\"isStyleInverse\">\n                <ng-content></ng-content>\n            </div>\n        </template>\n\n        <template [ngIf]=\"!isCompact\">\n            <div id=\"app-footer\" [class.inverse]=\"isStyleInverse\">\n                <div class=\"links\">\n                    <ng-content select=\"uxLayoutFooterLinks\"></ng-content>\n                </div>\n                <div class=\"app-infos\">\n                    <ng-content select=\"uxLayoutFooterAppInfos\"></ng-content>\n                </div>\n            </div>\n        </template>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutFooterComponent.ctorParameters = function () { return []; };
UxLayoutFooterComponent.propDecorators = {
    'isStyleInverse': [{ type: Input },],
    'isCompact': [{ type: Input },],
};
var UxLayoutFooterAppInfosTagDirective = (function () {
    function UxLayoutFooterAppInfosTagDirective() {
    }
    return UxLayoutFooterAppInfosTagDirective;
}());
UxLayoutFooterAppInfosTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutFooterAppInfos' },] },
];
/**
 * @nocollapse
 */
UxLayoutFooterAppInfosTagDirective.ctorParameters = function () { return []; };
var UxLayoutFooterLinksTagDirective = (function () {
    function UxLayoutFooterLinksTagDirective() {
    }
    return UxLayoutFooterLinksTagDirective;
}());
UxLayoutFooterLinksTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutFooterLinks' },] },
];
/**
 * @nocollapse
 */
UxLayoutFooterLinksTagDirective.ctorParameters = function () { return []; };
var UxLayoutFooterComponentModule = (function () {
    function UxLayoutFooterComponentModule() {
    }
    return UxLayoutFooterComponentModule;
}());
UxLayoutFooterComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [
                    UxLayoutFooterComponent, UxLayoutFooterAppInfosTagDirective,
                    UxLayoutFooterLinksTagDirective
                ],
                declarations: [
                    UxLayoutFooterComponent, UxLayoutFooterAppInfosTagDirective,
                    UxLayoutFooterLinksTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutFooterComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarTopMenuComponent = (function () {
    function UxLayoutNavBarTopMenuComponent() {
        this.links = [];
        this.homeUrl = '/screen/home';
        this.menuItemClicked = new EventEmitter();
    }
    Object.defineProperty(UxLayoutNavBarTopMenuComponent.prototype, "hasLinks", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.links) {
                return this.links.length !== 0;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxLayoutNavBarTopMenuComponent.prototype.onHomeClick = function () {
        var /** @type {?} */ link = new UxLink({ id: 'home', url: this.homeUrl });
        this.onLinkClick(link);
    };
    /**
     * @param {?} link
     * @return {?}
     */
    UxLayoutNavBarTopMenuComponent.prototype.onLinkClick = function (link) {
        if (link.url) {
            window.scrollTo(0, 0);
        }
        if (!link.disabled) {
            this.menuItemClicked.emit(link);
        }
    };
    return UxLayoutNavBarTopMenuComponent;
}());
UxLayoutNavBarTopMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar-top-menu',
                template: "\n        <nav id=\"top-menu\">\n            <ul>\n                <li class=\"icon-menu-item\"\n                    routerLinkActive=\"selected\" [routerLinkActiveOptions]=\"{exact:true}\">\n                    <a [routerLink]=\"homeUrl\" (click)=\"onHomeClick()\" class=\"icon-home-link\">\n                        <span class=\"fa fa-home\" ></span>\n                    </a>\n                </li>\n\n                <template [ngIf]=\"hasLinks\">\n                    <li [class.has-sub]=\"link.hasChildren\" *ngFor=\"let link of links\" routerLinkActive=\"selected\">\n                        <template [ngIf]=\"link.visible && !link.isHome\">\n                            <a *ngIf=\"!link.url\" (click)=\"onLinkClick(link)\">{{link.label}}</a>\n                            <a *ngIf=\"link.url\" [routerLink]=\"link.url\" (click)=\"onLinkClick(link)\">{{link.label}}</a>\n                            <ul *ngIf=\"link.hasChildren\">\n                                <li class=\"child\" *ngFor=\"let childLink of link.children\" [class.has-sub]=\"childLink.hasChildren\">\n                                    <template [ngIf]=\"childLink.visible\">\n                                        <a *ngIf=\"!childLink.url\"\n                                           [class.disabled]=\"childLink.disabled\"\n                                           (click)=\"onLinkClick(childLink)\">{{childLink.label}}</a>\n                                        <a *ngIf=\"childLink.url\"\n                                           [class.disabled]=\"childLink.disabled\"\n                                           [routerLink]=\"childLink.url\"\n                                           (click)=\"onLinkClick(childLink)\">{{childLink.label}}</a>\n                                        <ul *ngIf=\"childLink.hasChildren\">\n                                            <li class=\"child\" *ngFor=\"let childSubLink of childLink.children\">\n                                                <template [ngIf]=\"childSubLink.visible\">\n                                                    <a *ngIf=\"childSubLink.url\"\n                                                       [class.disabled]=\"childSubLink.disabled\"\n                                                       [routerLink]=\"childSubLink.url\"\n                                                       (click)=\"onLinkClick(childSubLink)\">{{childSubLink.label}}</a>\n                                                    <a *ngIf=\"!childSubLink.url\"\n                                                       [class.disabled]=\"childSubLink.disabled\"\n                                                       (click)=\"onLinkClick(childSubLink)\">{{childSubLink.label}}</a>\n                                                </template>\n                                            </li>\n                                        </ul>\n                                    </template>\n                                </li>\n                            </ul>\n                        </template>\n                    </li>\n                </template>\n            </ul>\n        </nav>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarTopMenuComponent.ctorParameters = function () { return []; };
UxLayoutNavBarTopMenuComponent.propDecorators = {
    'links': [{ type: Input },],
    'homeUrl': [{ type: Input },],
    'menuItemClicked': [{ type: Output },],
};
var UxLayoutNavBarTopMenuComponentModule = (function () {
    function UxLayoutNavBarTopMenuComponentModule() {
    }
    return UxLayoutNavBarTopMenuComponentModule;
}());
UxLayoutNavBarTopMenuComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, RouterModule],
                exports: [UxLayoutNavBarTopMenuComponent],
                declarations: [UxLayoutNavBarTopMenuComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarTopMenuComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarLeftActionItemSidebarToggleComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxLayoutNavBarLeftActionItemSidebarToggleComponent(uxService) {
        this.uxService = uxService;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutNavBarLeftActionItemSidebarToggleComponent.prototype.onToggleSidebar = function (event) {
        this.uxService.isSidebarStateOpen = !this.uxService.isSidebarStateOpen;
    };
    return UxLayoutNavBarLeftActionItemSidebarToggleComponent;
}());
UxLayoutNavBarLeftActionItemSidebarToggleComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar-left-action-item-sidebar-toggle',
                template: "\n        <li class=\"left-action-item hidden-sm-down\" (click)=\"onToggleSidebar($event)\">\n            <a class=\"left-action-item-link-block\">\n                <span class=\"icon-toggle ion ion-navicon\"></span>\n                <span class=\"left-action-item-label\">\n                    {{label}}\n                </span>\n            </a>\n        </li>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarLeftActionItemSidebarToggleComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxLayoutNavBarLeftActionItemSidebarToggleComponent.propDecorators = {
    'label': [{ type: Input },],
};
var UxLayoutNavBarLeftActionItemSidebarToggleComponentModule = (function () {
    function UxLayoutNavBarLeftActionItemSidebarToggleComponentModule() {
    }
    return UxLayoutNavBarLeftActionItemSidebarToggleComponentModule;
}());
UxLayoutNavBarLeftActionItemSidebarToggleComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutNavBarLeftActionItemSidebarToggleComponent],
                declarations: [UxLayoutNavBarLeftActionItemSidebarToggleComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarLeftActionItemSidebarToggleComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarComponent = (function () {
    function UxLayoutNavBarComponent() {
        this.isMuted = false;
    }
    return UxLayoutNavBarComponent;
}());
UxLayoutNavBarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar',
                template: "\n        <div id=\"nav-bar\"\n             [class.muted]=\"isMuted\"\n             [ngClass]=\"styleClass\"\n             [class.sidebar-toggle-with-top-menu]=\"sidebarToggle && topMenu\">\n            <ng-content select=\"uxLayoutNavBarLeftActionsContent\"></ng-content>\n            <ng-content select=\"uxLayoutNavBarElementsContent\"></ng-content>\n            <ng-content></ng-content>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarComponent.ctorParameters = function () { return []; };
UxLayoutNavBarComponent.propDecorators = {
    'isMuted': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'topMenu': [{ type: ContentChild, args: [forwardRef(function () { return UxLayoutNavBarTopMenuComponent; }),] },],
    'sidebarToggle': [{ type: ContentChild, args: [forwardRef(function () { return UxLayoutNavBarLeftActionItemSidebarToggleComponent; }),] },],
};
var UxLayoutNavBarElementsContentTagDirective = (function () {
    function UxLayoutNavBarElementsContentTagDirective() {
    }
    return UxLayoutNavBarElementsContentTagDirective;
}());
UxLayoutNavBarElementsContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutNavBarElementsContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarElementsContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutNavBarLeftActionsContentTagDirective = (function () {
    function UxLayoutNavBarLeftActionsContentTagDirective() {
    }
    return UxLayoutNavBarLeftActionsContentTagDirective;
}());
UxLayoutNavBarLeftActionsContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutNavBarLeftActionsContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarLeftActionsContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutNavBarComponentModule = (function () {
    function UxLayoutNavBarComponentModule() {
    }
    return UxLayoutNavBarComponentModule;
}());
UxLayoutNavBarComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutNavBarComponent, UxLayoutNavBarElementsContentTagDirective, UxLayoutNavBarLeftActionsContentTagDirective],
                declarations: [UxLayoutNavBarComponent, UxLayoutNavBarElementsContentTagDirective, UxLayoutNavBarLeftActionsContentTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarComponentModule.ctorParameters = function () { return []; };

var UxLayoutSidebarItemComponent = (function () {
    /**
     * @param {?} uxService
     * @param {?} router
     * @param {?} activatedRoute
     */
    function UxLayoutSidebarItemComponent(uxService, router, activatedRoute) {
        this.uxService = uxService;
        this.router = router;
        this.activatedRoute = activatedRoute;
        this.isLarge = false;
        this.isActive = false;
        this.isExpanded = false;
        this.isHome = false;
        this.clicked = new EventEmitter();
    }
    Object.defineProperty(UxLayoutSidebarItemComponent.prototype, "hasSub", {
        /**
         * @return {?}
         */
        get: function () {
            //  to prevent self inclusion : https://github.com/angular/angular/issues/10098#issuecomment-235157642
            if (this.subItems.length > 1) {
                return true;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxLayoutSidebarItemComponent.prototype, "isCurrentRouteActive", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.url) {
                if (this.router.isActive(this.router.createUrlTree([this.url], { relativeTo: this.activatedRoute }), this.isHome)) {
                    return true;
                }
            }
            else if (this.id && this.isActive) {
                return true;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutSidebarItemComponent.prototype.onKeyDown = function (event) {
        if (event.keyCode === 13) {
            this.navigateTo();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutSidebarItemComponent.prototype.toggle = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.isExpanded = !this.isExpanded;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutSidebarItemComponent.prototype.onClick = function (event) {
        this.clicked.emit(this.id);
        this.navigateTo();
        event.preventDefault();
        event.stopPropagation();
    };
    /**
     * @return {?}
     */
    UxLayoutSidebarItemComponent.prototype.navigateTo = function () {
        if (this.url) {
            this.router.navigate([this.url], { relativeTo: this.activatedRoute });
            window.scrollTo(0, 0);
        }
    };
    return UxLayoutSidebarItemComponent;
}());
UxLayoutSidebarItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-sidebar-item',
                template: "\n    <div class=\"sidebar-item\" (keydown)=\"onKeyDown($event)\"\n                              (click)=\"onClick($event)\">\n\n        <div class=\"sidebar-item__header\" [class.sidebar-item__header--title]=\"!(url || id)\"\n                                          [class.sidebar-item__header--large]=\"isLarge\"\n                                          [class.sidebar-item__header--active]=\"isCurrentRouteActive\" tabindex=\"0\">\n            <div *ngIf=\"iconClass\" class=\"sidebar-item__header-icon-wrapper\">\n                <span class=\"sidebar-item__header-icon-wrapper-icon fa fa-fw {{iconClass}}\"></span>\n            </div>\n            <div class=\"sidebar-item__header-label-wrapper\">\n                <div class=\"sidebar-item__header-label-wrapper-label\">\n                    {{label}}\n                </div>\n                <div *ngIf=\"subLabel\" class=\"sidebar-item__header-label-wrapper-sub-label\">\n                    {{subLabel}}\n                </div>\n            </div>\n            <span *ngIf=\"hasSub\" class=\"sidebar-item__header-expand-toggle\" (click)=\"toggle($event)\">\n                <span class=\"fa\" [ngClass]=\"{'fa-angle-right': !isExpanded, 'fa-angle-down': isExpanded}\"></span>\n            </span>\n        </div>\n\n        <template [ngIf]=\"isExpanded\">\n            <div class=\"sidebar-item__sub\">\n                <ng-content></ng-content>\n            </div>\n        </template>\n\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutSidebarItemComponent.ctorParameters = function () { return [
    { type: UxService, },
    { type: Router, },
    { type: ActivatedRoute, },
]; };
UxLayoutSidebarItemComponent.propDecorators = {
    'id': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'url': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'isActive': [{ type: Input },],
    'isExpanded': [{ type: Input },],
    'isHome': [{ type: Input },],
    'clicked': [{ type: Output },],
    'subItems': [{ type: ContentChildren, args: [forwardRef(function () { return UxLayoutSidebarItemComponent; }),] },],
};
var UxLayoutSidebarItemComponentModule = (function () {
    function UxLayoutSidebarItemComponentModule() {
    }
    return UxLayoutSidebarItemComponentModule;
}());
UxLayoutSidebarItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, RouterModule],
                exports: [UxLayoutSidebarItemComponent],
                declarations: [UxLayoutSidebarItemComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutSidebarItemComponentModule.ctorParameters = function () { return []; };

var UxLayoutSidebarItemsComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxLayoutSidebarItemsComponent(uxService) {
        this.uxService = uxService;
        this.links = [];
        this.isLargeItems = false;
        this.isInnerNavigationSidebar = false;
        this.clicked = new EventEmitter();
    }
    /**
     * @return {?}
     */
    UxLayoutSidebarItemsComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        if (this.items.length !== 0) {
            this.items.forEach(function (item) {
                if (_this.isLargeItems) {
                    item.isLarge = _this.isLargeItems;
                }
            });
        }
    };
    Object.defineProperty(UxLayoutSidebarItemsComponent.prototype, "hasLinks", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.links) {
                return this.links.length !== 0;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} id
     * @return {?}
     */
    UxLayoutSidebarItemsComponent.prototype.onClick = function (id) {
        this.clicked.emit(id);
    };
    return UxLayoutSidebarItemsComponent;
}());
UxLayoutSidebarItemsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-sidebar-items',
                template: "\n        <div class=\"sidebar-items\"\n             [class.sidebar-items--inner-navigation-sidebar]=\"isInnerNavigationSidebar\">\n\n            <template [ngIf]=\"!hasLinks\">\n                <ng-content></ng-content>\n            </template>\n\n            <template [ngIf]=\"hasLinks\">\n                <ux-layout-sidebar-item *ngFor=\"let link of links\"\n                                        id=\"{{link.id}}\"\n                                        label=\"{{link.label}}\"\n                                        subLabel=\"{{link.subLabel}}\"\n                                        url=\"{{link.url}}\"\n                                        [isActive]=\"link.active\"\n                                        [isLarge]=\"isLargeItems\"\n                                        iconClass=\"{{link.iconClass}}\"\n                                        [isHome]=\"link.isHome\"\n                                        (clicked)=\"onClick($event)\">\n                    <ux-layout-sidebar-item *ngFor=\"let childLink of link.children\"\n                                            id=\"{{childLink.id}}\"\n                                            label=\"{{childLink.label}}\"\n                                            subLabel=\"{{childLink.subLabel}}\"\n                                            url=\"{{childLink.url}}\"\n                                            [isActive]=\"childLink.active\"\n                                            [isLarge]=\"isLargeItems\"\n                                            iconClass=\"{{childLink.iconClass}}\"\n                                            (clicked)=\"onClick($event)\">\n                        <ux-layout-sidebar-item *ngFor=\"let childLinkSub of childLink.children\"\n                                                id=\"{{childLinkSub.id}}\"\n                                                label=\"{{childLinkSub.label}}\"\n                                                subLabel=\"{{childLinkSub.subLabel}}\"\n                                                url=\"{{childLinkSub.url}}\"\n                                                [isActive]=\"childLinkSub.active\"\n                                                [isLarge]=\"isLargeItems\"\n                                                iconClass=\"{{childLinkSub.iconClass}}\"\n                                                (clicked)=\"onClick($event)\">\n                        </ux-layout-sidebar-item>\n                    </ux-layout-sidebar-item>\n                </ux-layout-sidebar-item>\n            </template>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutSidebarItemsComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxLayoutSidebarItemsComponent.propDecorators = {
    'links': [{ type: Input },],
    'isLargeItems': [{ type: Input },],
    'isInnerNavigationSidebar': [{ type: Input },],
    'clicked': [{ type: Output },],
    'items': [{ type: ContentChildren, args: [forwardRef(function () { return UxLayoutSidebarItemComponent; }),] },],
};
var UxLayoutSidebarItemsComponentModule = (function () {
    function UxLayoutSidebarItemsComponentModule() {
    }
    return UxLayoutSidebarItemsComponentModule;
}());
UxLayoutSidebarItemsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule, UxLayoutSidebarItemComponentModule
                ],
                exports: [
                    UxLayoutSidebarItemsComponent,
                ],
                declarations: [
                    UxLayoutSidebarItemsComponent,
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutSidebarItemsComponentModule.ctorParameters = function () { return []; };

var UxLayoutOverlayPanelComponent = (function () {
    function UxLayoutOverlayPanelComponent() {
        this.isActive = false;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutOverlayPanelComponent.prototype.onClick = function (event) {
        this.isActive = !this.isActive;
    };
    return UxLayoutOverlayPanelComponent;
}());
UxLayoutOverlayPanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-overlay-panel',
                template: "\n        <div class=\"overlay-panel\" [class.active]=\"isActive\" (click)=\"onClick($event)\">\n            <!--\n            <div class=\"overlay-panel__header overlay-panel__header-toolbar\">\n                <ng-content select=\"uxLayoutOverlayPanelHeader\"></ng-content>\n            </div>\n            <div class=\"overlay-panel__inner\">\n                <ng-content select=\"uxLayoutOverlayPanelInner\"></ng-content>\n            </div>\n            <div class=\"overlay-panel__footer\">\n                <ng-content select=\"uxLayoutOverlayPanelFooter\"></ng-content>\n            </div>\n            -->\n            <ng-content></ng-content>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutOverlayPanelComponent.ctorParameters = function () { return []; };
UxLayoutOverlayPanelComponent.propDecorators = {
    'isActive': [{ type: Input },],
};
var UxLayoutOverlayPanelHeaderTagDirective = (function () {
    function UxLayoutOverlayPanelHeaderTagDirective() {
    }
    return UxLayoutOverlayPanelHeaderTagDirective;
}());
UxLayoutOverlayPanelHeaderTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutOverlayPanelHeader' },] },
];
/**
 * @nocollapse
 */
UxLayoutOverlayPanelHeaderTagDirective.ctorParameters = function () { return []; };
var UxLayoutOverlayPanelInnerTagDirective = (function () {
    function UxLayoutOverlayPanelInnerTagDirective() {
    }
    return UxLayoutOverlayPanelInnerTagDirective;
}());
UxLayoutOverlayPanelInnerTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutOverlayPanelInner' },] },
];
/**
 * @nocollapse
 */
UxLayoutOverlayPanelInnerTagDirective.ctorParameters = function () { return []; };
var UxLayoutOverlayPanelFooterTagDirective = (function () {
    function UxLayoutOverlayPanelFooterTagDirective() {
    }
    return UxLayoutOverlayPanelFooterTagDirective;
}());
UxLayoutOverlayPanelFooterTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutOverlayPanelFooter' },] },
];
/**
 * @nocollapse
 */
UxLayoutOverlayPanelFooterTagDirective.ctorParameters = function () { return []; };
var UxLayoutOverlayPanelComponentModule = (function () {
    function UxLayoutOverlayPanelComponentModule() {
    }
    return UxLayoutOverlayPanelComponentModule;
}());
UxLayoutOverlayPanelComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [
                    UxLayoutOverlayPanelComponent,
                    UxLayoutOverlayPanelHeaderTagDirective,
                    UxLayoutOverlayPanelInnerTagDirective,
                    UxLayoutOverlayPanelFooterTagDirective
                ],
                declarations: [
                    UxLayoutOverlayPanelComponent,
                    UxLayoutOverlayPanelHeaderTagDirective,
                    UxLayoutOverlayPanelInnerTagDirective,
                    UxLayoutOverlayPanelFooterTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutOverlayPanelComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarActionItemComponent = (function () {
    /**
     * @param {?} uxLayoutNavBarActionsComponent
     * @param {?} uxService
     */
    function UxLayoutNavBarActionItemComponent(uxLayoutNavBarActionsComponent, uxService) {
        this.uxService = uxService;
        this.isToggleContent = true;
        this.isOverlayPanel = false;
        this.isOverlayPanelCustomContent = false;
        this.isOverlayPanelBg = false;
        this.links = [];
        this.isShowHome = true;
        this.homeUrl = '/screen/home';
        this.itemClass = '';
        this.contentClass = '';
        this.tagCount = 0;
        this.isHiddenMobile = false;
        this.isMobileOnly = false;
        this.isUseSidebarTemplateAsLinks = false;
        this.isActive = false;
        this.isContentFixedHeight = false;
        this.languageChanged = new EventEmitter();
        this.clicked = new EventEmitter();
        this.uxLayoutNavBarActionsComponent = uxLayoutNavBarActionsComponent;
    }
    /**
     * @return {?}
     */
    UxLayoutNavBarActionItemComponent.prototype.ngAfterContentInit = function () {
        if (this.isUseSidebarTemplateAsLinks) {
            this.links = this.uxService.sidebarLinks;
        }
    };
    /**
     * @param {?} language
     * @return {?}
     */
    UxLayoutNavBarActionItemComponent.prototype.onLanguageChanged = function (language) {
        this.uxService.activeLanguage = language;
        this.languageChanged.emit(language);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutNavBarActionItemComponent.prototype.toggle = function (event) {
        if (this.isToggleContent) {
            this.uxLayoutNavBarActionsComponent.hideAll(this);
            this.isActive = !this.isActive;
            event.stopImmediatePropagation();
            event.preventDefault();
        }
        else {
            this.clicked.emit(this);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutNavBarActionItemComponent.prototype.onClick = function (event) {
        event.stopImmediatePropagation();
        event.preventDefault();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutNavBarActionItemComponent.prototype.onSidebarItemClick = function (event) {
        this.isActive = !this.isActive;
    };
    return UxLayoutNavBarActionItemComponent;
}());
UxLayoutNavBarActionItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar-action-item',
                template: "\n        <li class=\"action-item {{itemClass}}\" (click)=\"toggle($event)\"\n            [class.selected]=\"isActive\" [class.hidden-sm-down]=\"isHiddenMobile\" [class.hidden-sm-up]=\"isMobileOnly\">\n            <span class=\"action-item-icon fa {{iconClass}}\">\n                <span *ngIf=\"tagCount && tagCount != 0\" class=\"tag\">{{tagCount}}</span>\n            </span>\n        </li>\n\n        <template [ngIf]=\"isOverlayPanel\">\n            <template [ngIf]=\"isOverlayPanelCustomContent\">\n                <ux-layout-overlay-panel [isActive]=\"isActive\">\n                    <ng-content select=\"uxLayoutNavBarOverlayPanelContent\"></ng-content>\n                </ux-layout-overlay-panel>\n            </template>\n\n            <template [ngIf]=\"!isOverlayPanelCustomContent\">\n                <div class=\"overlay-panel\" [class.active]=\"isActive\" (click)=\"onClick($event)\">\n                    <div class=\"overlay-panel__header\">\n                        <div class=\"overlay-panel__header-profile\">\n                            <span><ux-language-selector (languageChanged)=\"onLanguageChanged($event)\"\n                                    languageCodes=\"en,fr\" [selectedLanguage]=\"uxService.activeLanguage\"></ux-language-selector></span>\n                            <span class=\"overlay-panel__header-profile-infos\">\n                                {{userInfos}}\n                            </span>\n                        </div>\n                    </div>\n\n                    <div class=\"overlay-panel__inner\">\n                        <ux-layout-sidebar-items [links]=\"links\" (clicked)=\"onSidebarItemClick($event)\"></ux-layout-sidebar-items>\n                    </div>\n                </div>\n            </template>\n        </template>\n\n        <template [ngIf]=\"!isOverlayPanel\">\n        <aside class=\"inner-content {{contentClass}}\"\n               [class.hidden]=\"!isActive\"\n               [class.fixed-height]=\"isContentFixedHeight\" (click)=\"onClick($event)\">\n            <ng-content></ng-content>\n        </aside>\n        </template>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarActionItemComponent.ctorParameters = function () { return [
    { type: UxLayoutNavBarActionsComponent, decorators: [{ type: Host }, { type: Inject, args: [forwardRef(function () { return UxLayoutNavBarActionsComponent; }),] },] },
    { type: UxService, },
]; };
UxLayoutNavBarActionItemComponent.propDecorators = {
    'id': [{ type: Input },],
    'isToggleContent': [{ type: Input },],
    'isOverlayPanel': [{ type: Input },],
    'isOverlayPanelCustomContent': [{ type: Input },],
    'isOverlayPanelBg': [{ type: Input },],
    'links': [{ type: Input },],
    'isShowHome': [{ type: Input },],
    'homeUrl': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'itemClass': [{ type: Input },],
    'contentClass': [{ type: Input },],
    'tagCount': [{ type: Input },],
    'userInfos': [{ type: Input },],
    'isHiddenMobile': [{ type: Input },],
    'isMobileOnly': [{ type: Input },],
    'isUseSidebarTemplateAsLinks': [{ type: Input },],
    'isActive': [{ type: Input },],
    'isContentFixedHeight': [{ type: Input },],
    'languageChanged': [{ type: Output },],
    'clicked': [{ type: Output },],
};
var UxLayoutNavBarOverlayPanelContentTagDirective = (function () {
    function UxLayoutNavBarOverlayPanelContentTagDirective() {
    }
    return UxLayoutNavBarOverlayPanelContentTagDirective;
}());
UxLayoutNavBarOverlayPanelContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutNavBarOverlayPanelContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarOverlayPanelContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutNavBarActionItemComponentModule = (function () {
    function UxLayoutNavBarActionItemComponentModule() {
    }
    return UxLayoutNavBarActionItemComponentModule;
}());
UxLayoutNavBarActionItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    RouterModule,
                    UxLanguageSelectorComponentModule,
                    UxLayoutSidebarItemsComponentModule,
                    UxLayoutOverlayPanelComponentModule
                ],
                exports: [UxLayoutNavBarActionItemComponent, UxLayoutNavBarOverlayPanelContentTagDirective],
                declarations: [
                    UxLayoutNavBarActionItemComponent, UxLayoutNavBarOverlayPanelContentTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarActionItemComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarActionsComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxLayoutNavBarActionsComponent(uxService) {
        this.uxService = uxService;
    }
    /**
     * @return {?}
     */
    UxLayoutNavBarActionsComponent.prototype.unToggleAll = function () {
        this.items.toArray().forEach(function (i) {
            i.isActive = false;
        });
    };
    /**
     * @param {?} item
     * @return {?}
     */
    UxLayoutNavBarActionsComponent.prototype.hideAll = function (item) {
        this.items.toArray().forEach(function (i) {
            if (i !== item) {
                i.isActive = false;
            }
        });
    };
    return UxLayoutNavBarActionsComponent;
}());
UxLayoutNavBarActionsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar-actions',
                template: "\n        <div class=\"right-actions\">\n            <ul>\n                <ng-content></ng-content>\n            </ul>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarActionsComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxLayoutNavBarActionsComponent.propDecorators = {
    'items': [{ type: ContentChildren, args: [forwardRef(function () { return UxLayoutNavBarActionItemComponent; }),] },],
    'unToggleAll': [{ type: HostListener, args: ['body:click',] },],
};
var UxLayoutNavBarActionsComponentModule = (function () {
    function UxLayoutNavBarActionsComponentModule() {
    }
    return UxLayoutNavBarActionsComponentModule;
}());
UxLayoutNavBarActionsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutNavBarActionsComponent],
                declarations: [UxLayoutNavBarActionsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarActionsComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarLeftActionItemComponent = (function () {
    function UxLayoutNavBarLeftActionItemComponent() {
        this.itemClass = '';
        this.isStatic = false;
    }
    return UxLayoutNavBarLeftActionItemComponent;
}());
UxLayoutNavBarLeftActionItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar-left-action-item',
                template: "\n        <li class=\"left-action-item {{itemClass}}\" [class.hidden-sm-down]=\"isHiddenMobile\" [class.left-action-item--static]=\"isStatic\">\n            <ng-content></ng-content>\n        </li>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarLeftActionItemComponent.ctorParameters = function () { return []; };
UxLayoutNavBarLeftActionItemComponent.propDecorators = {
    'itemClass': [{ type: Input },],
    'isHiddenMobile': [{ type: Input },],
    'isStatic': [{ type: Input },],
};
var UxLayoutNavBarLeftActionItemComponentModule = (function () {
    function UxLayoutNavBarLeftActionItemComponentModule() {
    }
    return UxLayoutNavBarLeftActionItemComponentModule;
}());
UxLayoutNavBarLeftActionItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutNavBarLeftActionItemComponent],
                declarations: [UxLayoutNavBarLeftActionItemComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarLeftActionItemComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarLeftActionsComponent = (function () {
    function UxLayoutNavBarLeftActionsComponent() {
    }
    return UxLayoutNavBarLeftActionsComponent;
}());
UxLayoutNavBarLeftActionsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar-left-actions',
                template: "\n        <div class=\"left-actions\">\n            <ul>\n                <ng-content></ng-content>\n            </ul>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarLeftActionsComponent.ctorParameters = function () { return []; };
var UxLayoutNavBarLeftActionsComponentModule = (function () {
    function UxLayoutNavBarLeftActionsComponentModule() {
    }
    return UxLayoutNavBarLeftActionsComponentModule;
}());
UxLayoutNavBarLeftActionsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutNavBarLeftActionsComponent],
                declarations: [UxLayoutNavBarLeftActionsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarLeftActionsComponentModule.ctorParameters = function () { return []; };

var UxNotificationItemComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxNotificationItemComponent(uxService) {
        this.uxService = uxService;
        this.dateFormat = 'dd/MM/yyyy';
        this.itemClick = new EventEmitter();
        this.itemMarkAsRead = new EventEmitter();
        this.itemMarkAsUnRead = new EventEmitter();
        this.markAsReadLabel = uxService.translate('notificationMarkAsRead');
        this.markAsUnReadLabel = uxService.translate('notificationMarkAsUnRead');
    }
    /**
     * @param {?} event
     * @return {?}
     */
    UxNotificationItemComponent.prototype.onItemClick = function (event) {
        if (this.notificationLink.metadata) {
            this.notificationLink.metadata.read = true;
        }
        this.itemClick.emit(this.notificationLink);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxNotificationItemComponent.prototype.onItemMarkAsRead = function (event) {
        if (this.notificationLink.metadata) {
            this.notificationLink.metadata.read = true;
        }
        this.itemMarkAsRead.emit(this.notificationLink);
        this.uxService.consumeEvent(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxNotificationItemComponent.prototype.onItemMarkAsUnRead = function (event) {
        if (this.notificationLink.metadata) {
            this.notificationLink.metadata.read = false;
        }
        this.itemMarkAsUnRead.emit(this.notificationLink);
        this.uxService.consumeEvent(event);
    };
    Object.defineProperty(UxNotificationItemComponent.prototype, "notificationTypeClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (!this.notificationLink.typeClass) {
                return 'primary';
            }
            else {
                return this.notificationLink.typeClass;
            }
        },
        enumerable: true,
        configurable: true
    });
    return UxNotificationItemComponent;
}());
UxNotificationItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-notification-item',
                styles: [":host { width: 100%; }"],
                template: "\n    <div class=\"ux-notification-item\" (click)=\"onItemClick($event)\">\n        <template [ngIf]=\"notificationLink.metadata\">\n            <ux-badge [isPill]=\"true\" typeClass=\"{{notificationTypeClass}}\" styleClass=\"ux-u-width-3\">\n                <template [ngIf]=\"notificationLink.metadata.type\">\n                    {{notificationLink.metadata.type}}\n                </template>\n                <template [ngIf]=\"!notificationLink.metadata.type\">\n                    <span class=\"fa fa-envelope-o\"></span>\n                </template>\n            </ux-badge>\n        </template>\n\n        <ux-a-label label=\"{{notificationLink.label}}\"\n                    subLabel=\"{{notificationLink.subLabel}}\">\n        </ux-a-label>\n\n        <div class=\"ux-notification-item__right-content\">\n            <ux-a-date-tag *ngIf=\"notificationLink.metadata && notificationLink.metadata.date\"\n                            [date]=\"notificationLink.metadata.date\"\n                            [isMuted]=\"true\" [isDisplayDaysLeft]=\"true\"\n                            dateFormat=\"{{dateFormat}}\">\n            </ux-a-date-tag>\n\n            <div *ngIf=\"notificationLink.metadata && !notificationLink.metadata.read\"\n                    class=\"ux-notification-item__read-toggle\"\n                    (click)=\"onItemMarkAsRead($event)\"\n                    uxTooltip=\"{{markAsReadLabel}}\" [size]=\"small\">\n                    <span class=\"fa fa-circle ux-u-font-size-h8\"></span>\n            </div>\n            <div *ngIf=\"notificationLink.metadata && notificationLink.metadata.read\"\n                    class=\"ux-notification-item__read-toggle\"\n                    (click)=\"onItemMarkAsUnRead($event)\"\n                    uxTooltip=\"{{markAsUnReadLabel}}\" [size]=\"small\">\n                    <span class=\"fa fa-circle-thin ux-u-font-size-h8\"></span>\n            </div>\n        </div>\n    </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxNotificationItemComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxNotificationItemComponent.propDecorators = {
    'notificationLink': [{ type: Input },],
    'dateFormat': [{ type: Input },],
    'itemClick': [{ type: Output },],
    'itemMarkAsRead': [{ type: Output },],
    'itemMarkAsUnRead': [{ type: Output },],
};
var UxNotificationItemComponentModule = (function () {
    function UxNotificationItemComponentModule() {
    }
    return UxNotificationItemComponentModule;
}());
UxNotificationItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    UxIconComponentModule,
                    UxLabelComponentModule,
                    UxBadgeComponentModule,
                    UxDateTagComponentModule,
                    UxTooltipModule
                ],
                exports: [UxNotificationItemComponent],
                declarations: [UxNotificationItemComponent]
            },] },
];
/**
 * @nocollapse
 */
UxNotificationItemComponentModule.ctorParameters = function () { return []; };

var UxNotificationsPanelComponent = (function () {
    /**
     * @param {?} uxLayoutNavBarActionsComponent
     * @param {?} uxLayoutOverlayPanelComponent
     * @param {?} uxService
     */
    function UxNotificationsPanelComponent(uxLayoutNavBarActionsComponent, uxLayoutOverlayPanelComponent, uxService) {
        this.uxService = uxService;
        this.notificationLinks = [];
        this.isShowViewAllAction = true;
        this.isSeparateCurrentDayNotifications = false;
        this.isHidePanelOnViewAllAction = true;
        this.refreshClick = new EventEmitter();
        this.viewAllClick = new EventEmitter();
        this.itemClick = new EventEmitter();
        this.itemMarkAsRead = new EventEmitter();
        this.itemMarkAsUnRead = new EventEmitter();
        this.currentDayNotifications = [];
        this.oldestNotifications = [];
        this.unreadNotifications = [];
        this.today = new Date();
        this.uxLayoutNavBarActionsComponent = uxLayoutNavBarActionsComponent;
        this.uxLayoutOverlayPanelComponent = uxLayoutOverlayPanelComponent;
        console.log(this.uxLayoutOverlayPanelComponent);
        if (!this.viewAllNotificationsLabel) {
            this.viewAllNotificationsLabel = uxService.translate('viewAllNotifications');
        }
        if (!this.headerTitleLabel) {
            this.headerTitleLabel = uxService.translate('myNotifications');
        }
        if (!this.noNotificationFoundLabel) {
            this.noNotificationFoundLabel = uxService.translate('noNotificationFound');
        }
        if (!this.todaysSeparatorLabel) {
            this.todaysSeparatorLabel = uxService.translate('notificationsToday');
        }
        if (!this.oldestSeparatorLabel) {
            this.oldestSeparatorLabel = uxService.translate('notificationsOldest');
        }
        this.unreadLabel = uxService.translate('notificationsUnread');
    }
    Object.defineProperty(UxNotificationsPanelComponent.prototype, "unreadCount", {
        /**
         * @return {?}
         */
        get: function () {
            var /** @type {?} */ unreadNotifications = this.notificationLinks.filter(function (link) {
                if (link.metadata) {
                    return link.metadata.read === false;
                }
                return false;
            });
            return unreadNotifications.length;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} changes
     * @return {?}
     */
    UxNotificationsPanelComponent.prototype.ngOnChanges = function (changes) {
        if (this.isSeparateCurrentDayNotifications) {
            this.currentDayNotifications = this.notificationLinks.filter(function (link) {
                if (link.metadata) {
                    return new Date(link.metadata.date).toDateString() === new Date().toDateString();
                }
                return false;
            });
            this.oldestNotifications = this.notificationLinks.filter(function (link) {
                if (link.metadata) {
                    return new Date(link.metadata.date).toDateString() !== new Date().toDateString();
                }
                return false;
            });
        }
    };
    /**
     * @return {?}
     */
    UxNotificationsPanelComponent.prototype.ngOnInit = function () {
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxNotificationsPanelComponent.prototype.onRefresh = function (event) {
        this.refreshClick.emit();
        this.uxService.consumeEvent(event);
    };
    /**
     * @param {?} link
     * @return {?}
     */
    UxNotificationsPanelComponent.prototype.onItemClick = function (link) {
        this.itemClick.emit(link);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxNotificationsPanelComponent.prototype.onViewAllClick = function (event) {
        this.viewAllClick.emit();
        if (!this.isHidePanelOnViewAllAction) {
            this.uxService.consumeEvent(event);
        }
        else {
            if (this.uxLayoutNavBarActionsComponent && !this.uxLayoutOverlayPanelComponent) {
                this.uxLayoutNavBarActionsComponent.hideAll(null);
            }
        }
    };
    /**
     * @param {?} link
     * @return {?}
     */
    UxNotificationsPanelComponent.prototype.onItemMarkAsRead = function (link) {
        this.itemMarkAsRead.emit(link);
    };
    /**
     * @param {?} link
     * @return {?}
     */
    UxNotificationsPanelComponent.prototype.onItemMarkAsUnRead = function (link) {
        this.itemMarkAsUnRead.emit(link);
    };
    /**
     * @param {?} link
     * @return {?}
     */
    UxNotificationsPanelComponent.prototype.getNotificationStateClass = function (link) {
        if (link.metadata) {
            if (link.metadata.read) {
                return 'ux-list-item--muted';
            }
        }
        return '';
    };
    return UxNotificationsPanelComponent;
}());
UxNotificationsPanelComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-notifications-panel',
                template: "\n    <div class=\"ux-notifications-panel\">\n        <div class=\"ux-notifications-panel__header\">\n            <div class=\"ux-notifications-panel__header-title\">\n                <div class=\"ux-notifications-panel__header-title-label\">\n                    {{headerTitleLabel}}\n                </div>\n                <div class=\"ux-notifications-panel__header-title-subLabel\">\n                    <span *ngIf=\"notificationLinks.length !== 0\">\n                        total <ux-badge [isSmall]=\"true\">{{notificationLinks.length}}</ux-badge>\n                    </span>\n                    <span *ngIf=\"unreadCount != 0\">\n                        - unread <ux-badge [isSmall]=\"true\" typeClass=\"danger\">{{unreadCount}}</ux-badge>\n                    </span>\n                </div>\n            </div>\n            <div class=\"ux-notifications-panel__header-actions\">\n                <button class=\"btn btn-sm btn-secondary\" (click)=\"onRefresh($event)\">\n                    <span class=\"fa fa-refresh\"></span>\n                </button>\n            </div>\n        </div>\n        <div class=\"ux-notifications-panel__content\">\n            <template [ngIf]=\"notificationLinks.length == 0\">\n                <div class=\"p-3\">\n                    {{noNotificationFoundLabel}}\n                </div>\n            </template>\n            <template [ngIf]=\"notificationLinks.length != 0\">\n                <template [ngIf]=\"isSeparateCurrentDayNotifications && currentDayNotifications.length != 0\">\n\n                    <div class=\"ux-notifications-panel__section-title\">\n                        {{todaysSeparatorLabel}}\n                        <div class=\"ml-auto\">\n                            {{today | date:'dd/MM/yyyy'}}\n                        </div>\n                    </div>\n\n                    <ux-list-items>\n                        <ux-list-item *ngFor=\"let link of currentDayNotifications\"\n                                    [styleClass]=\"getNotificationStateClass(link)\">\n                            <uxListItemContent>\n                                <ux-notification-item [notificationLink]=\"link\"\n                                                    dateFormat=\"hh:mm\"\n                                                    (itemClick)=\"onItemClick($event)\"\n                                                    (itemMarkAsRead)=\"onItemMarkAsRead($event)\"\n                                                    (itemMarkAsUnRead)=\"onItemMarkAsUnRead($event)\">\n                                </ux-notification-item>\n                            </uxListItemContent>\n                        </ux-list-item>\n                    </ux-list-items>\n\n\n                    <div class=\"ux-notifications-panel__section-title\">\n                        {{oldestSeparatorLabel}}\n                    </div>\n\n                    <ux-list-items>\n                        <ux-list-item *ngFor=\"let link of oldestNotifications\"\n                                    [styleClass]=\"getNotificationStateClass(link)\">\n                            <uxListItemContent>\n                                <ux-notification-item [notificationLink]=\"link\"\n                                                    (itemClick)=\"onItemClick($event)\"\n                                                    (itemMarkAsRead)=\"onItemMarkAsRead($event)\"\n                                                    (itemMarkAsUnRead)=\"onItemMarkAsUnRead($event)\">\n                                </ux-notification-item>\n                            </uxListItemContent>\n                        </ux-list-item>\n                    </ux-list-items>\n\n                </template>\n\n\n\n                <template [ngIf]=\"!isSeparateCurrentDayNotifications || currentDayNotifications.length == 0\">\n\n                    <ux-list-items>\n                        <ux-list-item *ngFor=\"let link of notificationLinks\"\n                                    [styleClass]=\"getNotificationStateClass(link)\">\n                            <uxListItemContent>\n                                <ux-notification-item [notificationLink]=\"link\"\n                                                    (itemClick)=\"onItemClick($event)\"\n                                                    (itemMarkAsRead)=\"onItemMarkAsRead($event)\"\n                                                    (itemMarkAsUnRead)=\"onItemMarkAsUnRead($event)\">\n                                </ux-notification-item>\n                            </uxListItemContent>\n                        </ux-list-item>\n                    </ux-list-items>\n\n                </template>\n\n            </template>\n        </div>\n        <div *ngIf=\"notificationLinks.length != 0 && isShowViewAllAction\" class=\"ux-notifications-panel__footer\">\n            <a (click)=\"onViewAllClick($event)\">{{viewAllNotificationsLabel}}</a>\n        </div>\n    </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxNotificationsPanelComponent.ctorParameters = function () { return [
    { type: UxLayoutNavBarActionsComponent, decorators: [{ type: Host }, { type: Optional }, { type: Inject, args: [forwardRef(function () { return UxLayoutNavBarActionsComponent; }),] },] },
    { type: UxLayoutOverlayPanelComponent, decorators: [{ type: Host }, { type: Optional }, { type: Inject, args: [forwardRef(function () { return UxLayoutOverlayPanelComponent; }),] },] },
    { type: UxService, },
]; };
UxNotificationsPanelComponent.propDecorators = {
    'notificationLinks': [{ type: Input },],
    'viewAllNotificationsLabel': [{ type: Input },],
    'headerTitleLabel': [{ type: Input },],
    'noNotificationFoundLabel': [{ type: Input },],
    'todaysSeparatorLabel': [{ type: Input },],
    'oldestSeparatorLabel': [{ type: Input },],
    'isShowViewAllAction': [{ type: Input },],
    'isSeparateCurrentDayNotifications': [{ type: Input },],
    'isHidePanelOnViewAllAction': [{ type: Input },],
    'refreshClick': [{ type: Output },],
    'viewAllClick': [{ type: Output },],
    'itemClick': [{ type: Output },],
    'itemMarkAsRead': [{ type: Output },],
    'itemMarkAsUnRead': [{ type: Output },],
};
var UxNotificationsPanelComponentModule = (function () {
    function UxNotificationsPanelComponentModule() {
    }
    return UxNotificationsPanelComponentModule;
}());
UxNotificationsPanelComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    UxListItemsComponentModule,
                    UxListItemComponentModule,
                    UxNotificationItemComponentModule,
                    UxBadgeComponentModule
                ],
                exports: [UxNotificationsPanelComponent],
                declarations: [UxNotificationsPanelComponent]
            },] },
];
/**
 * @nocollapse
 */
UxNotificationsPanelComponentModule.ctorParameters = function () { return []; };

var UxLayoutSidebarComponent = (function () {
    /**
     * @param {?} uxService
     * @param {?} document
     */
    function UxLayoutSidebarComponent(uxService, document) {
        this.uxService = uxService;
        this.document = document;
        this.homeUrl = '/screen/home';
        this.title = '';
        this.isShowLogo = false;
        this.isStateOpen = true;
        this.isAlwaysVisible = false;
        this.isCollapsible = false;
        this.links = [];
        this.isStateCloseWithIcons = false;
        this.isLargeItems = false;
        this.toggleOpenIconClass = 'fa-chevron-right';
        this.toggleCloseIconClass = 'fa-times';
        this.hasFilter = false;
        this.filterPlaceholderLabel = 'Filter';
        this.isInnerSidebar = false;
        this.isMobileVisible = false;
        this.clicked = new EventEmitter();
        this.topPosition = 139;
    }
    /**
     * @return {?}
     */
    UxLayoutSidebarComponent.prototype.onWindowScroll = function () {
        this.topPosition = this.document.getElementById('nav-bar').getBoundingClientRect().bottom;
    };
    Object.defineProperty(UxLayoutSidebarComponent.prototype, "innerSidebarTopPosition", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isInnerSidebar) {
                if (this.topPosition >= 0) {
                    return this.topPosition;
                }
            }
            return 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxLayoutSidebarComponent.prototype, "hasLinks", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.links.length !== 0) {
                return true;
            }
            return false;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    UxLayoutSidebarComponent.prototype.ngAfterContentInit = function () {
        this.uxService.isSidebarActive = true;
        if (this.isAlwaysVisible) {
            this.uxService.isSidebarAlwaysVisible = true;
        }
        else {
            this.uxService.isSidebarAlwaysVisible = false;
            this.isCollapsible = true;
        }
        if (this.isCollapsible) {
            this.uxService.isSidebarCollapsible = true;
            this.isAlwaysVisible = false;
        }
        if (this.isStateOpen) {
            this.uxService.isSidebarStateOpen = true;
        }
        else {
            this.uxService.isSidebarStateOpen = false;
        }
        if (this.isInnerSidebar) {
            this.uxService.isSidebarInnerActive = true;
        }
        if (this.isMobileVisible) {
            this.uxService.isSidebarMobileVisible = true;
        }
        this.uxService.isSidebarStateCloseWithIcons = this.isStateCloseWithIcons;
        var /** @type {?} */ itemsLinks = [];
        if (this.items.length !== 0 && !this.hasLinks) {
            this.items.forEach(function (item) {
                itemsLinks.push(new UxLink({
                    id: item.id,
                    label: item.label,
                    url: item.url
                }));
            });
            this.uxService.setSidebarLinks(itemsLinks);
        }
        if (this.theme) {
            this.themeClass = 'sidebar--t-' + this.theme;
        }
    };
    /**
     * @return {?}
     */
    UxLayoutSidebarComponent.prototype.toggleSidebar = function () {
        if (!this.isAlwaysVisible && this.isCollapsible) {
            this.isStateOpen = !this.isStateOpen;
        }
        this.uxService.isSidebarStateOpen = this.isStateOpen;
        if (!this.isStateOpen && this.isStateCloseWithIcons) {
            this.uxService.isSidebarStateCloseWithIcons = true;
        }
        else {
            this.uxService.isSidebarStateCloseWithIcons = false;
        }
    };
    /**
     * @param {?} id
     * @return {?}
     */
    UxLayoutSidebarComponent.prototype.onClick = function (id) {
        if (id) {
            this.clicked.emit(id);
        }
    };
    return UxLayoutSidebarComponent;
}());
UxLayoutSidebarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-sidebar',
                template: "\n        <div class=\"sidebar {{themeClass}}\"\n            [class.state-open]=\"uxService.isSidebarStateOpen\"\n            [class.state-close]=\"!uxService.isSidebarStateOpen\"\n            [class.state-close-with-icons]=\"!uxService.isSidebarStateOpen && isStateCloseWithIcons\"\n            [class.inner-sidebar]=\"isInnerSidebar\"\n            [class.sidebar--large-items]=\"isLargeItems\"\n            [class.mobile-visible]=\"isMobileVisible\"\n            [style.top.px]=\"innerSidebarTopPosition\">\n\n            <div *ngIf=\"isCollapsible && !isInnerSidebar\" class=\"sidebar-toggle\">\n                <a (click)=\"toggleSidebar()\">\n                    <span *ngIf=\"!uxService.isSidebarStateOpen\" class=\"label-open\">\n                        <span class=\"fa {{toggleOpenIconClass}}\"></span>\n                    </span>\n                    <span *ngIf=\"uxService.isSidebarStateOpen\" class=\"label-close\">\n                        <span class=\"fa {{toggleCloseIconClass}}\"></span>\n                    </span>\n                </a>\n            </div>\n\n            <div class=\"logo-wrapper\" *ngIf=\"isShowLogo\">\n                <a [routerLink]=\"homeUrl\">\n                    <div class=\"logo\"></div>\n                </a>\n            </div>\n\n            <div class=\"sidebar-inner\">\n\n                <template [ngIf]=\"customContent\">\n                    <ng-content select=\"uxLayoutSidebarContent\"></ng-content>\n                </template>\n\n                <template [ngIf]=\"!customContent\">\n\n                    <div *ngIf=\"title\" class=\"title\">{{title}}</div>\n\n                    <div class=\"header\" *ngIf=\"customHeaderContent\">\n                        <ng-content select=\"uxLayoutSidebarHeader\"></ng-content>\n                    </div>\n\n                    <div class=\"filter\" *ngIf=\"hasFilter\">\n                        <input type=\"text\" #sidebarFilter placeholder=\"{{filterPlaceholderLabel}}\"/>\n                        <span class=\"fa fa-search\"></span>\n                    </div>\n\n                    <template [ngIf]=\"!hasLinks\">\n                        <ng-content></ng-content>\n                    </template>\n\n                    <template [ngIf]=\"hasLinks\">\n                        <ux-layout-sidebar-items [links]=\"links\" [isLargeItems]=\"isLargeItems\"></ux-layout-sidebar-items>\n                    </template>\n\n                </template>\n            </div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutSidebarComponent.ctorParameters = function () { return [
    { type: UxService, },
    { type: undefined, decorators: [{ type: Inject, args: [DOCUMENT,] },] },
]; };
UxLayoutSidebarComponent.propDecorators = {
    'homeUrl': [{ type: Input },],
    'title': [{ type: Input },],
    'isShowLogo': [{ type: Input },],
    'isStateOpen': [{ type: Input },],
    'isAlwaysVisible': [{ type: Input },],
    'isCollapsible': [{ type: Input },],
    'links': [{ type: Input },],
    'isStateCloseWithIcons': [{ type: Input },],
    'isLargeItems': [{ type: Input },],
    'toggleOpenIconClass': [{ type: Input },],
    'toggleCloseIconClass': [{ type: Input },],
    'hasFilter': [{ type: Input },],
    'filterPlaceholderLabel': [{ type: Input },],
    'isInnerSidebar': [{ type: Input },],
    'isMobileVisible': [{ type: Input },],
    'theme': [{ type: Input },],
    'clicked': [{ type: Output },],
    'customHeaderContent': [{ type: ContentChild, args: [forwardRef(function () { return UxLayoutSidebarHeaderTagDirective; }),] },],
    'customContent': [{ type: ContentChild, args: [forwardRef(function () { return UxLayoutSidebarContentTagDirective; }),] },],
    'items': [{ type: ContentChildren, args: [forwardRef(function () { return UxLayoutSidebarItemComponent; }),] },],
    'onWindowScroll': [{ type: HostListener, args: ['window:scroll', [],] },],
};
var UxLayoutSidebarHeaderTagDirective = (function () {
    function UxLayoutSidebarHeaderTagDirective() {
    }
    return UxLayoutSidebarHeaderTagDirective;
}());
UxLayoutSidebarHeaderTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutSidebarHeader' },] },
];
/**
 * @nocollapse
 */
UxLayoutSidebarHeaderTagDirective.ctorParameters = function () { return []; };
var UxLayoutSidebarContentTagDirective = (function () {
    function UxLayoutSidebarContentTagDirective() {
    }
    return UxLayoutSidebarContentTagDirective;
}());
UxLayoutSidebarContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutSidebarContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutSidebarContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutSidebarComponentModule = (function () {
    function UxLayoutSidebarComponentModule() {
    }
    return UxLayoutSidebarComponentModule;
}());
UxLayoutSidebarComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule, RouterModule, UxLayoutSidebarItemComponentModule, UxLayoutSidebarItemsComponentModule
                ],
                exports: [
                    UxLayoutSidebarComponent,
                    UxLayoutSidebarHeaderTagDirective,
                    UxLayoutSidebarContentTagDirective
                ],
                declarations: [
                    UxLayoutSidebarComponent,
                    UxLayoutSidebarHeaderTagDirective,
                    UxLayoutSidebarContentTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutSidebarComponentModule.ctorParameters = function () { return []; };

var UxLayoutAppShellComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxLayoutAppShellComponent(uxService) {
        this.uxService = uxService;
        // GLOBAL
        this.appHomeUrl = '/screen/home';
        this.isContract = false;
        this.isContractMedium = false;
        this.isContractSmall = false;
        // HEADER
        this.hasHeader = true;
        this.hasHeaderUserProfile = false;
        this.hasHeaderProfileAvatar = false;
        this.isHeaderHideLogo = false;
        this.headerLanguageChanged = new EventEmitter();
        // NAV BAR
        this.navBarTopMenuLinks = [];
        this.navBarNotificationLinks = [];
        this.isNavBarMuted = false;
        this.navBarTopMenuItemClicked = new EventEmitter();
        this.navbarNotificationsRefreshClick = new EventEmitter();
        this.navbarNotificationsViewAllClick = new EventEmitter();
        this.navbarNotificationsItemClick = new EventEmitter();
        this.navbarNotificationsItemMarkAsRead = new EventEmitter();
        this.navbarNotificationsItemMarkAsUnRead = new EventEmitter();
        // SIDEBAR
        this.hasSidebar = false;
        this.isSidebarInner = false;
        this.sidebarLinks = [];
        this.isSidebarShowLogo = false;
        this.isSidebarAlwaysVisible = false;
        this.isSidebarCollapsible = false;
        this.isSidebarStateOpen = true;
        this.isSidebarLargeItems = false;
        this.isSidebarStateCloseWithIcons = false;
        this.isSidebarMobileVisible = false;
    }
    /**
     * @return {?}
     */
    UxLayoutAppShellComponent.prototype.ngAfterContentInit = function () {
        this.appClasses = '';
        if (this.styleClass) {
            this.appClasses += this.styleClass;
        }
        if (this.themeClass) {
            this.appClasses += this.themeClass;
        }
        if (this.isContract) {
            this.appClasses += 'contract';
        }
        if (this.isContractMedium) {
            this.appClasses += 'contract-half';
        }
        if (this.isContractSmall) {
            this.appClasses += 'contract-quarter';
        }
        if (this.isSidebarShowLogo) {
            this.isHeaderHideLogo = true;
        }
    };
    /**
     * @param {?} uxLanguage
     * @return {?}
     */
    UxLayoutAppShellComponent.prototype.onHeaderLanguageChanged = function (uxLanguage) {
        this.headerLanguageChanged.emit(uxLanguage);
    };
    /**
     * @param {?} uxLink
     * @return {?}
     */
    UxLayoutAppShellComponent.prototype.onTopMenuItemClicked = function (uxLink) {
        this.navBarTopMenuItemClicked.emit(uxLink);
    };
    return UxLayoutAppShellComponent;
}());
UxLayoutAppShellComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-app-shell',
                template: "\n<ux-layout-app styleClass=\"{{appClasses}}\">\n\n    <uxLayoutAppSidebarContainer *ngIf=\"hasSidebar && !isSidebarInner\">\n\n        <ux-layout-sidebar title=\"{{sidebarTitle}}\"\n                           [isAlwaysVisible]=\"isSidebarAlwaysVisible\"\n                           [isCollapsible]=\"isSidebarCollapsible\"\n                           [isStateOpen]=\"isSidebarStateOpen\"\n                           [isShowLogo]=\"isSidebarShowLogo\"\n                           [links]=\"sidebarLinks\"\n                           [isLargeItems]=\"isSidebarLargeItems\"\n                           [isStateCloseWithIcons]=\"isSidebarStateCloseWithIcons\"\n                           theme=\"{{sidebarTheme}}\">\n\n            <template [ngIf]=\"customSidebarContent.length !== 0\">\n                <ng-content select=\"uxAppShellSidebarContent\"></ng-content>\n            </template>\n\n            <uxLayoutSidebarHeader *ngIf=\"customSidebarHeaderContent.length !== 0\">\n                <ng-content select=\"uxAppShellSidebarHeaderContent\"></ng-content>\n            </uxLayoutSidebarHeader>\n\n        </ux-layout-sidebar>\n\n    </uxLayoutAppSidebarContainer>\n\n    <uxLayoutAppMainContainer>\n\n        <ux-layout-app-main>\n\n            <uxLayoutAppMainHeaderContainer *ngIf=\"hasHeader\">\n                    <ux-layout-header appFullName=\"{{headerAppFullName}}\"\n                                      appShortName=\"{{headerAppShortName}}\"\n                                      appSubtitle=\"{{headerAppSubtitle}}\"\n                                      [userInfos]=\"headerUserInfos\"\n                                      homeUrl=\"{{appHomeUrl}}\"\n                                      [userProfileLinks]=\"headerUserProfileLinks\"\n                                      [isHideLogo]=\"isHeaderHideLogo\"\n                                      [isCustomTitleContent]=\"customHeaderTitleContent.length !== 0\"\n                                      [isCustomRightContent]=\"customHeaderRightContent.length !== 0\"\n                                      envLabel=\"{{headerEnvLabel}}\"\n                                      languageCodes=\"{{headerLanguageCodes}}\"\n                                      (languageChanged)=\"onHeaderLanguageChanged($event)\">\n\n                        <uxLayoutHeaderTitleContent *ngIf=\"customHeaderTitleContent.length !== 0\">\n                            <ng-content select=\"uxAppShellHeaderTitleContent\"></ng-content>\n                        </uxLayoutHeaderTitleContent>\n\n                        <uxLayoutHeaderRightContent *ngIf=\"customHeaderRightContent.length !== 0\">\n                            <ng-content select=\"uxAppShellHeaderRightContent\"></ng-content>\n                        </uxLayoutHeaderRightContent>\n\n                    </ux-layout-header>\n            </uxLayoutAppMainHeaderContainer>\n\n            <uxLayoutAppMainNavBarContainer>\n                <template [ngIf]=\"isNavBarMuted\">\n                    <ux-layout-nav-bar [isMuted]=\"true\"></ux-layout-nav-bar>\n                </template>\n\n                <template [ngIf]=\"!isNavBarMuted\">\n                    <ux-layout-nav-bar>\n                        <uxLayoutNavBarLeftActionsContent *ngIf=\"isSidebarInner\">\n                            <ux-layout-nav-bar-left-actions>\n                                <ux-layout-nav-bar-left-action-item-sidebar-toggle label=\"{{navBarSidebarToggleLabel}}\">\n                                </ux-layout-nav-bar-left-action-item-sidebar-toggle>\n                            </ux-layout-nav-bar-left-actions>\n                        </uxLayoutNavBarLeftActionsContent>\n                        <ux-layout-nav-bar-top-menu *ngIf=\"navBarTopMenuLinks.length !== 0\"\n                                                    [links]=\"navBarTopMenuLinks\" homeUrl=\"{{appHomeUrl}}\"\n                                                    (menuItemClicked)=\"onTopMenuItemClicked($event)\">\n                        </ux-layout-nav-bar-top-menu>\n                        <ux-layout-nav-bar-actions>\n                            <ux-layout-nav-bar-action-item iconClass=\"fa-bell\" tagCount=\"{{navBarNotificationLinks.length}}\">\n                                <ux-notifications-panel [notificationLinks]=\"navBarNotificationLinks\">\n                                </ux-notifications-panel>\n                            </ux-layout-nav-bar-action-item>\n\n\n                            <!-- WITHIN OVERLAY\n                            <ux-layout-nav-bar-action-item *ngIf=\"isMobile\"\n                                                            iconClass=\"fa-bell\"\n                                                            tagCount=\"{{notificationLinks.length}}\"\n                                                            [isOverlayPanel]=\"true\"\n                                                            [isOverlayPanelCustomContent]=\"true\">\n                                <uxLayoutNavBarOverlayPanelContent>\n                                    <ux-notifications-panel [notificationLinks]=\"notificationLinks\"\n                                                            [isSeparateCurrentDayNotifications]=\"true\"\n                                                            [isHidePanelOnViewAllAction]=\"false\"\n                                                            (refreshClick)=\"onNotificationsRefreshClick($event)\"\n                                                            (viewAllClick)=\"onNotificationsViewAllClick($event)\"\n                                                            (itemMarkAsRead)=\"onNotificationsItemMarkAsRead($event)\"\n                                                            (itemMarkAsUnRead)=\"onNotificationsItemMarkAsUnRead($event)\"\n                                                            (itemClick)=\"onNotificationsItemClick($event)\">\n                                    </ux-notifications-panel>\n                                </uxLayoutNavBarOverlayPanelContent>\n                            </ux-layout-nav-bar-action-item>\n                            -->\n\n                            <!-- WITHIN INNER CONTENT\n                            <ux-layout-nav-bar-action-item *ngIf=\"!isMobile\"\n                                                            iconClass=\"fa-bell\"\n                                                            tagCount=\"{{notificationLinks.length}}\"\n                                                            [isContentFixedHeight]=\"true\">\n                                <ux-notifications-panel [notificationLinks]=\"notificationLinks\"\n                                                        [isHidePanelOnViewAllAction]=\"false\"\n                                                        [isSeparateCurrentDayNotifications]=\"true\"\n                                                        (refreshClick)=\"onNotificationsRefreshClick($event)\"\n                                                        (viewAllClick)=\"onNotificationsViewAllClick($event)\"\n                                                        (itemMarkAsRead)=\"onNotificationsItemMarkAsRead($event)\"\n                                                        (itemMarkAsUnRead)=\"onNotificationsItemMarkAsUnRead($event)\"\n                                                        (itemClick)=\"onNotificationsItemClick($event)\">\n                                </ux-notifications-panel>\n                            </ux-layout-nav-bar-action-item>\n                            -->\n\n\n\n\n                            <ux-layout-nav-bar-action-item iconClass=\"fa-bars\"\n                                                        itemClass=\"app-menu\" isOverlayPanel=\"true\"\n                                                        [userInfos]=\"headerUserInfos\" [links]=\"navBarTopMenuLinks\">\n                            </ux-layout-nav-bar-action-item>\n                            <template [ngIf]=\"customNavBarItemsContent.length !== 0\">\n                                <ng-content select=\"customNavBarItemsContent\"></ng-content>\n                            </template>\n                        </ux-layout-nav-bar-actions>\n                    </ux-layout-nav-bar>\n                </template>\n            </uxLayoutAppMainNavBarContainer>\n\n            <uxLayoutAppMainContentContainer>\n\n                <ux-layout-sidebar *ngIf=\"hasSidebar && isSidebarInner\"\n                            title=\"{{sidebarTitle}}\"\n                            [isInnerSidebar]=\"isSidebarInner\"\n                            [isAlwaysVisible]=\"isSidebarAlwaysVisible\"\n                            [isCollapsible]=\"isSidebarCollapsible\"\n                            [isShowLogo]=\"isSidebarShowLogo\"\n                            [links]=\"sidebarLinks\"\n                            [isLargeItems]=\"isSidebarLargeItems\"\n                            [isStateCloseWithIcons]=\"isSidebarStateCloseWithIcons\"\n                            theme=\"{{sidebarTheme}}\">\n\n                    <template [ngIf]=\"customSidebarContent.length !== 0\">\n                        <ng-content select=\"uxAppShellSidebarContent\"></ng-content>\n                    </template>\n\n                    <template [ngIf]=\"customSidebarHeaderContent.length !== 0\">\n                        <uxLayoutSidebarHeader>\n                            <ng-content select=\"uxAppShellSidebarHeaderContent\"></ng-content>\n                        </uxLayoutSidebarHeader>\n                    </template>\n\n                </ux-layout-sidebar>\n\n                <template [ngIf]=\"customMainContent.length === 0\">\n                    <router-outlet></router-outlet>\n                </template>\n\n                <template [ngIf]=\"customMainContent.length !== 0\">\n                    <ng-content select=\"uxAppShellMainContent\"></ng-content>\n                </template>\n\n            </uxLayoutAppMainContentContainer>\n\n            <uxLayoutAppMainFooterContainer>\n                <ux-layout-footer isCompact=\"true\">\n                    compact footer example : version <strong class=\"color-primary\">{{footerAppVersion}}</strong> - {{footerAppReleaseDate}}\n                </ux-layout-footer>\n            </uxLayoutAppMainFooterContainer>\n\n        </ux-layout-app-main>\n\n    </uxLayoutAppMainContainer>\n\n</ux-layout-app>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutAppShellComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxLayoutAppShellComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'appHomeUrl': [{ type: Input },],
    'themeClass': [{ type: Input },],
    'isContract': [{ type: Input },],
    'isContractMedium': [{ type: Input },],
    'isContractSmall': [{ type: Input },],
    'hasHeader': [{ type: Input },],
    'headerAppFullName': [{ type: Input },],
    'headerAppShortName': [{ type: Input },],
    'headerAppSubtitle': [{ type: Input },],
    'headerUserInfos': [{ type: Input },],
    'hasHeaderUserProfile': [{ type: Input },],
    'hasHeaderProfileAvatar': [{ type: Input },],
    'headerUserProfileLinks': [{ type: Input },],
    'isHeaderHideLogo': [{ type: Input },],
    'languageCodes': [{ type: Input },],
    'headerEnvLabel': [{ type: Input },],
    'headerLanguageCodes': [{ type: Input },],
    'headerLanguageChanged': [{ type: Output },],
    'navBarTopMenuLinks': [{ type: Input },],
    'navBarNotificationLinks': [{ type: Input },],
    'isNavBarMuted': [{ type: Input },],
    'navBarSidebarToggleLabel': [{ type: Input },],
    'navBarTopMenuItemClicked': [{ type: Output },],
    'navbarNotificationsRefreshClick': [{ type: Output },],
    'navbarNotificationsViewAllClick': [{ type: Output },],
    'navbarNotificationsItemClick': [{ type: Output },],
    'navbarNotificationsItemMarkAsRead': [{ type: Output },],
    'navbarNotificationsItemMarkAsUnRead': [{ type: Output },],
    'footerAppVersion': [{ type: Input },],
    'footerAppReleaseDate': [{ type: Input },],
    'hasSidebar': [{ type: Input },],
    'isSidebarInner': [{ type: Input },],
    'sidebarLinks': [{ type: Input },],
    'sidebarTitle': [{ type: Input },],
    'isSidebarShowLogo': [{ type: Input },],
    'isSidebarAlwaysVisible': [{ type: Input },],
    'isSidebarCollapsible': [{ type: Input },],
    'isSidebarStateOpen': [{ type: Input },],
    'isSidebarLargeItems': [{ type: Input },],
    'isSidebarStateCloseWithIcons': [{ type: Input },],
    'isSidebarMobileVisible': [{ type: Input },],
    'sidebarTheme': [{ type: Input },],
    'customSidebarContent': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxAppShellSidebarContentTagDirective;
                }),] },],
    'customSidebarHeaderContent': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxAppShellSidebarHeaderContentTagDirective;
                }),] },],
    'customHeaderRightContent': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxAppShellHeaderRightContentTagDirective;
                }),] },],
    'customHeaderTitleContent': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxAppShellHeaderTitleContentTagDirective;
                }),] },],
    'customMainContent': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxAppShellMainContentTagDirective;
                }),] },],
    'customNavBarItemsContent': [{ type: ContentChildren, args: [forwardRef(function () {
                    return UxAppShellNavBarItemsContentTagDirective;
                }),] },],
};
var UxAppShellSidebarContentTagDirective = (function () {
    function UxAppShellSidebarContentTagDirective() {
    }
    return UxAppShellSidebarContentTagDirective;
}());
UxAppShellSidebarContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxAppShellSidebarContent' },] },
];
/**
 * @nocollapse
 */
UxAppShellSidebarContentTagDirective.ctorParameters = function () { return []; };
var UxAppShellSidebarHeaderContentTagDirective = (function () {
    function UxAppShellSidebarHeaderContentTagDirective() {
    }
    return UxAppShellSidebarHeaderContentTagDirective;
}());
UxAppShellSidebarHeaderContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxAppShellSidebarHeaderContent' },] },
];
/**
 * @nocollapse
 */
UxAppShellSidebarHeaderContentTagDirective.ctorParameters = function () { return []; };
var UxAppShellHeaderRightContentTagDirective = (function () {
    function UxAppShellHeaderRightContentTagDirective() {
    }
    return UxAppShellHeaderRightContentTagDirective;
}());
UxAppShellHeaderRightContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxAppShellHeaderRightContent' },] },
];
/**
 * @nocollapse
 */
UxAppShellHeaderRightContentTagDirective.ctorParameters = function () { return []; };
var UxAppShellHeaderTitleContentTagDirective = (function () {
    function UxAppShellHeaderTitleContentTagDirective() {
    }
    return UxAppShellHeaderTitleContentTagDirective;
}());
UxAppShellHeaderTitleContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxAppShellHeaderTitleContent' },] },
];
/**
 * @nocollapse
 */
UxAppShellHeaderTitleContentTagDirective.ctorParameters = function () { return []; };
var UxAppShellMainContentTagDirective = (function () {
    function UxAppShellMainContentTagDirective() {
    }
    return UxAppShellMainContentTagDirective;
}());
UxAppShellMainContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxAppShellMainContent' },] },
];
/**
 * @nocollapse
 */
UxAppShellMainContentTagDirective.ctorParameters = function () { return []; };
var UxAppShellNavBarItemsContentTagDirective = (function () {
    function UxAppShellNavBarItemsContentTagDirective() {
    }
    return UxAppShellNavBarItemsContentTagDirective;
}());
UxAppShellNavBarItemsContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxAppShellNavBarItemsContent' },] },
];
/**
 * @nocollapse
 */
UxAppShellNavBarItemsContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutAppShellComponentModule = (function () {
    function UxLayoutAppShellComponentModule() {
    }
    return UxLayoutAppShellComponentModule;
}());
UxLayoutAppShellComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    RouterModule,
                    UxLayoutAppComponentModule,
                    UxLayoutAppMainComponentModule,
                    UxLayoutHeaderComponentModule,
                    UxLayoutFooterComponentModule,
                    UxLayoutNavBarComponentModule,
                    UxLayoutNavBarActionsComponentModule,
                    UxLayoutNavBarActionItemComponentModule,
                    UxLayoutNavBarLeftActionItemComponentModule,
                    UxLayoutNavBarLeftActionItemSidebarToggleComponentModule,
                    UxLayoutNavBarLeftActionsComponentModule,
                    UxNotificationsPanelComponentModule,
                    UxLayoutNavBarTopMenuComponentModule,
                    UxLayoutSidebarComponentModule
                ],
                exports: [
                    UxLayoutAppShellComponent,
                    UxAppShellSidebarContentTagDirective,
                    UxAppShellSidebarHeaderContentTagDirective,
                    UxAppShellHeaderRightContentTagDirective,
                    UxAppShellHeaderTitleContentTagDirective,
                    UxAppShellMainContentTagDirective,
                    UxAppShellNavBarItemsContentTagDirective
                ],
                declarations: [
                    UxLayoutAppShellComponent,
                    UxAppShellSidebarContentTagDirective,
                    UxAppShellSidebarHeaderContentTagDirective,
                    UxAppShellHeaderRightContentTagDirective,
                    UxAppShellHeaderTitleContentTagDirective,
                    UxAppShellMainContentTagDirective,
                    UxAppShellNavBarItemsContentTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutAppShellComponentModule.ctorParameters = function () { return []; };

var UxLayoutHeaderProfileComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxLayoutHeaderProfileComponent(uxService) {
        this.uxService = uxService;
        this.languageCodes = '';
        this.isConnected = false;
        this.isOnline = false;
        this.isShowTimeWrapper = false;
        this.isUserLoggedIn = true;
        this.languageChanged = new EventEmitter();
        this.isProfileDropdownVisible = false;
        this.localTime = new Date();
    }
    /**
     * @return {?}
     */
    UxLayoutHeaderProfileComponent.prototype.closeProfileDropdown = function () {
        this.isProfileDropdownVisible = false;
    };
    /**
     * @return {?}
     */
    UxLayoutHeaderProfileComponent.prototype.ngOnInit = function () {
        var _this = this;
        // Refresh Local Timer every 1 secs
        setInterval(function () {
            _this.localTime = new Date();
        }, 1000);
        this.refreshUtcTime();
        setInterval(function () {
            _this.refreshUtcTime();
        }, 1000 * 60);
    };
    Object.defineProperty(UxLayoutHeaderProfileComponent.prototype, "activeLanguage", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.selectedLanguage) {
                return this.selectedLanguage;
            }
            return this.uxService.activeLanguage;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} language
     * @return {?}
     */
    UxLayoutHeaderProfileComponent.prototype.onLanguageChanged = function (language) {
        this.uxService.activeLanguage = language;
        this.languageChanged.emit(language);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxLayoutHeaderProfileComponent.prototype.onToggleProfileDropdown = function (event) {
        this.isProfileDropdownVisible = !this.isProfileDropdownVisible;
        this.uxService.consumeEvent(event);
    };
    Object.defineProperty(UxLayoutHeaderProfileComponent.prototype, "localTimeString", {
        /**
         * @return {?}
         */
        get: function () {
            return this.formatDate(this.localTime);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxLayoutHeaderProfileComponent.prototype, "utcTimeString", {
        /**
         * @return {?}
         */
        get: function () {
            return this.formatDate(this.utcTime);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} d
     * @return {?}
     */
    UxLayoutHeaderProfileComponent.prototype.formatDate = function (d) {
        var /** @type {?} */ date = new Date(d);
        var /** @type {?} */ dateStr = date.toISOString().substr(0, 19);
        var /** @type {?} */ dmy = dateStr.substr(8, 2) + '/' + dateStr.substr(5, 2) + '/' + dateStr.substr(0, 4);
        return dmy + ' ' + dateStr.substr(11, 8);
    };
    /**
     * @return {?}
     */
    UxLayoutHeaderProfileComponent.prototype.refreshUtcTime = function () {
        this.utcTime = new Date();
        this.utcOffset = (this.utcTime.getTimezoneOffset() / 60);
        this.utcTime = this.utcTime.setHours(this.utcTime.getHours() + this.utcOffset); // Add or substract UTC hours
    };
    return UxLayoutHeaderProfileComponent;
}());
UxLayoutHeaderProfileComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-header-profile',
                template: "\n        <div class=\"profile-wrapper\">\n            <div class=\"left-panel\">\n                <div class=\"user-infos\">\n                    <template [ngIf]=\"isUserLoggedIn\">\n                        Welcome <strong class=\"ux-u-color-primary\">{{userInfos}}</strong>\n                    </template>\n\n                    <ux-language-selector *ngIf=\"languageCodes\"\n                            (languageChanged)=\"onLanguageChanged($event)\"\n                            languageCodes=\"{{languageCodes}}\"\n                            [isShowLabel]=\"!isUserLoggedIn\"\n                            [selectedLanguage]=\"activeLanguage\">\n                    </ux-language-selector>\n                    <ux-language-selector *ngIf=\"!languageCodes\"\n                            (languageChanged)=\"onLanguageChanged($event)\"\n                            [isShowLabel]=\"!isUserLoggedIn\"\n                            [selectedLanguage]=\"activeLanguage\">\n                    </ux-language-selector>\n\n                </div>\n\n                <template [ngIf]=\"!isUserLoggedIn\">\n                    <div class=\"mt-2\">\n                        <ng-content select=\"uxLayoutHeaderProfileNotLoggedInContent\"></ng-content>\n                    </div>\n                </template>\n\n                <div *ngIf=\"isShowTimeWrapper\" class=\"time-wrapper\">\n                    <div class=\"current-time\">\n                        <span class=\"fa ion-ios-clock-outline\"></span>\n                        <span>{{localTimeString}}</span>\n                    </div>\n                    <div class=\"utc-time\">\n                        <span class=\"fa ion-location\"></span>&nbsp;Addis-Abeba&nbsp;\n                        <span>{{utcTimeString}} UTC</span>\n                        <span *ngIf=\"utcOffset>=0\">+</span>{{utcOffset}}\n                    </div>\n                </div>\n            </div>\n            <div *ngIf=\"isUserLoggedIn\" class=\"right-panel\">\n                <div class=\"picture\" (click)=\"onToggleProfileDropdown($event)\">\n                    <div class=\"connected-state\">\n                        <span class=\"pulse-ring connect red\"></span>\n                        <span *ngIf=\"isConnected\"\n                              class=\"icon ion ion-social-rss ux-u-color-success\"\n                              title=\"Sucessfully connected to remote Opsys Server\">\n                        </span>\n                        <span *ngIf=\"!isConnected\"\n                              class=\"icon ion ion-social-rss ux-u-color-danger\"\n                              title=\"Connection to remote Opsys Server failed!\">\n                        </span>\n                    </div>\n                    <div class=\"available-state\">\n                        <span *ngIf=\"isOnline\"\n                              class=\"icon fa fa-circle ux-u-color-success-light\"\n                              title=\"Online - Available for chat discussion\">\n                        </span>\n                        <span *ngIf=\"!isOnline\"\n                              class=\"icon fa fa-circle ux-u-color-danger-light\"\n                              title=\"Offline - Unavailable for chat discussion\">\n                        </span>\n                    </div>\n                </div>\n                <div class=\"profile-toggle\">\n                    <span class=\"fa\" [ngClass]=\"isProfileDropdownVisible ? 'fa-angle-up' : 'fa-angle-down'\"></span>\n                </div>\n            </div>\n\n            <div *ngIf=\"isProfileDropdownVisible && customProfileMenuContent\" class=\"profile-dropdown fx flipInY\">\n                <ng-content select=\"uxLayoutHeaderProfileMenuContent\"></ng-content>\n            </div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderProfileComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxLayoutHeaderProfileComponent.propDecorators = {
    'userInfos': [{ type: Input },],
    'languageCodes': [{ type: Input },],
    'isConnected': [{ type: Input },],
    'isOnline': [{ type: Input },],
    'isShowTimeWrapper': [{ type: Input },],
    'isUserLoggedIn': [{ type: Input },],
    'selectedLanguage': [{ type: Input },],
    'languageChanged': [{ type: Output },],
    'customProfileMenuContent': [{ type: ContentChild, args: [forwardRef(function () {
                    return UxLayoutHeaderProfileMenuContentTagDirective;
                }),] },],
    'closeProfileDropdown': [{ type: HostListener, args: ['body:click',] },],
};
var UxLayoutHeaderProfileMenuContentTagDirective = (function () {
    function UxLayoutHeaderProfileMenuContentTagDirective() {
    }
    return UxLayoutHeaderProfileMenuContentTagDirective;
}());
UxLayoutHeaderProfileMenuContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutHeaderProfileMenuContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderProfileMenuContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutHeaderProfileNotLoggedInContentTagDirective = (function () {
    function UxLayoutHeaderProfileNotLoggedInContentTagDirective() {
    }
    return UxLayoutHeaderProfileNotLoggedInContentTagDirective;
}());
UxLayoutHeaderProfileNotLoggedInContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutHeaderProfileNotLoggedInContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderProfileNotLoggedInContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutHeaderProfileComponentModule = (function () {
    function UxLayoutHeaderProfileComponentModule() {
    }
    return UxLayoutHeaderProfileComponentModule;
}());
UxLayoutHeaderProfileComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    RouterModule,
                    UxLanguageSelectorComponentModule,
                    UxDropdownButtonComponentModule,
                    UxDropdownButtonItemComponentModule
                ],
                exports: [
                    UxLayoutHeaderProfileComponent,
                    UxLayoutHeaderProfileMenuContentTagDirective,
                    UxLayoutHeaderProfileNotLoggedInContentTagDirective,
                ],
                declarations: [
                    UxLayoutHeaderProfileComponent,
                    UxLayoutHeaderProfileMenuContentTagDirective,
                    UxLayoutHeaderProfileNotLoggedInContentTagDirective,
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutHeaderProfileComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarElementsComponent = (function () {
    function UxLayoutNavBarElementsComponent() {
    }
    return UxLayoutNavBarElementsComponent;
}());
UxLayoutNavBarElementsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar-elements',
                template: "\n        <div class=\"right-elements\">\n            <ul>\n                <ng-content></ng-content>\n            </ul>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarElementsComponent.ctorParameters = function () { return []; };
var UxLayoutNavBarElementsComponentModule = (function () {
    function UxLayoutNavBarElementsComponentModule() {
    }
    return UxLayoutNavBarElementsComponentModule;
}());
UxLayoutNavBarElementsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutNavBarElementsComponent],
                declarations: [UxLayoutNavBarElementsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarElementsComponentModule.ctorParameters = function () { return []; };

var UxLayoutNavBarElementItemComponent = (function () {
    function UxLayoutNavBarElementItemComponent() {
        this.itemClass = '';
    }
    return UxLayoutNavBarElementItemComponent;
}());
UxLayoutNavBarElementItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-nav-bar-element-item',
                template: "\n        <li class=\"element-item {{itemClass}}\" [class.hidden-sm-down]=\"isHiddenMobile\">\n            <ng-content></ng-content>\n        </li>  \n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarElementItemComponent.ctorParameters = function () { return []; };
UxLayoutNavBarElementItemComponent.propDecorators = {
    'itemClass': [{ type: Input },],
    'isHiddenMobile': [{ type: Input },],
};
var UxLayoutNavBarElementItemComponentModule = (function () {
    function UxLayoutNavBarElementItemComponentModule() {
    }
    return UxLayoutNavBarElementItemComponentModule;
}());
UxLayoutNavBarElementItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutNavBarElementItemComponent],
                declarations: [UxLayoutNavBarElementItemComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutNavBarElementItemComponentModule.ctorParameters = function () { return []; };

var UxLayoutBreadcrumbsComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxLayoutBreadcrumbsComponent(uxService) {
        this.uxService = uxService;
        this.homeUrl = '/screen/home';
    }
    return UxLayoutBreadcrumbsComponent;
}());
UxLayoutBreadcrumbsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-breadcrumbs',
                template: "\n    <div id=\"breadcrumbs\" *ngIf=\"uxService.hasBreadcrumbLinks\">\n        <ul>\n            <li routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact:true}\">\n                <a [routerLink]=\"homeUrl\"><span class=\"fa fa-home\"></span></a>\n            </li>\n            <li *ngFor=\"let link of uxService.breadcrumbLinks\" routerLinkActive=\"active\" [routerLinkActiveOptions]=\"{exact:true}\">\n                <span class=\"fa fa-angle-right\"></span>\n                <a [routerLink]=\"link.url\" >{{link.label}}</a>\n            </li>\n        </ul>\n    </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutBreadcrumbsComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxLayoutBreadcrumbsComponent.propDecorators = {
    'homeUrl': [{ type: Input },],
};
var UxLayoutBreadcrumbsComponentModule = (function () {
    function UxLayoutBreadcrumbsComponentModule() {
    }
    return UxLayoutBreadcrumbsComponentModule;
}());
UxLayoutBreadcrumbsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, RouterModule],
                exports: [UxLayoutBreadcrumbsComponent],
                declarations: [UxLayoutBreadcrumbsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutBreadcrumbsComponentModule.ctorParameters = function () { return []; };

var UxLayoutTopMessageComponent = (function () {
    function UxLayoutTopMessageComponent() {
    }
    return UxLayoutTopMessageComponent;
}());
UxLayoutTopMessageComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-top-message',
                template: "\n        <div class=\"ux-top-message {{styleClass}} ux-top-message--{{typeClass}}\">\n            <ng-content></ng-content>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutTopMessageComponent.ctorParameters = function () { return []; };
UxLayoutTopMessageComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
};
var UxLayoutTopMessageComponentModule = (function () {
    function UxLayoutTopMessageComponentModule() {
    }
    return UxLayoutTopMessageComponentModule;
}());
UxLayoutTopMessageComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutTopMessageComponent],
                declarations: [UxLayoutTopMessageComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutTopMessageComponentModule.ctorParameters = function () { return []; };

var UxLayoutBannerComponent = (function () {
    function UxLayoutBannerComponent() {
    }
    return UxLayoutBannerComponent;
}());
UxLayoutBannerComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-banner',
                template: "\n        <div class=\"ux-layout-banner {{styleClass}}\">\n            <div class=\"ux-layout-banner__content\">\n                <ng-content></ng-content>\n             </div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutBannerComponent.ctorParameters = function () { return []; };
UxLayoutBannerComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
};
var UxLayoutBannerComponentModule = (function () {
    function UxLayoutBannerComponentModule() {
    }
    return UxLayoutBannerComponentModule;
}());
UxLayoutBannerComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxLayoutBannerComponent],
                declarations: [UxLayoutBannerComponent]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutBannerComponentModule.ctorParameters = function () { return []; };

var UxLayoutHorizontalComponent = (function () {
    function UxLayoutHorizontalComponent() {
    }
    return UxLayoutHorizontalComponent;
}());
UxLayoutHorizontalComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-horizontal',
                template: "\n        <div class=\"ux-o-layout-horizontal {{styleClass}}\">\n            <div class=\"ux-o-layout-horizontal__left\"><ng-content select=\"uxLayoutHorizontalLeft\"></ng-content></div>\n            <div class=\"ux-o-layout-horizontal__center\"><ng-content select=\"uxLayoutHorizontalCenter\"></ng-content></div>\n            <div class=\"ux-o-layout-horizontal__right\"><ng-content select=\"uxLayoutHorizontalRight\"></ng-content></div>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutHorizontalComponent.ctorParameters = function () { return []; };
UxLayoutHorizontalComponent.propDecorators = {
    'styleClass': [{ type: Input },],
};
var UxLayoutHorizontalLeftTagDirective = (function () {
    function UxLayoutHorizontalLeftTagDirective() {
    }
    return UxLayoutHorizontalLeftTagDirective;
}());
UxLayoutHorizontalLeftTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutHorizontalLeft' },] },
];
/**
 * @nocollapse
 */
UxLayoutHorizontalLeftTagDirective.ctorParameters = function () { return []; };
var UxLayoutHorizontalRightTagDirective = (function () {
    function UxLayoutHorizontalRightTagDirective() {
    }
    return UxLayoutHorizontalRightTagDirective;
}());
UxLayoutHorizontalRightTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutHorizontalRight' },] },
];
/**
 * @nocollapse
 */
UxLayoutHorizontalRightTagDirective.ctorParameters = function () { return []; };
var UxLayoutHorizontalCenterTagDirective = (function () {
    function UxLayoutHorizontalCenterTagDirective() {
    }
    return UxLayoutHorizontalCenterTagDirective;
}());
UxLayoutHorizontalCenterTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutHorizontalCenter' },] },
];
/**
 * @nocollapse
 */
UxLayoutHorizontalCenterTagDirective.ctorParameters = function () { return []; };
var UxLayoutHorizontalModule = (function () {
    function UxLayoutHorizontalModule() {
    }
    return UxLayoutHorizontalModule;
}());
UxLayoutHorizontalModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [
                    UxLayoutHorizontalComponent,
                    UxLayoutHorizontalLeftTagDirective,
                    UxLayoutHorizontalRightTagDirective,
                    UxLayoutHorizontalCenterTagDirective
                ],
                declarations: [
                    UxLayoutHorizontalComponent,
                    UxLayoutHorizontalLeftTagDirective,
                    UxLayoutHorizontalRightTagDirective,
                    UxLayoutHorizontalCenterTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutHorizontalModule.ctorParameters = function () { return []; };

var UxLayoutPageHeaderComponent = (function () {
    function UxLayoutPageHeaderComponent() {
    }
    return UxLayoutPageHeaderComponent;
}());
UxLayoutPageHeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-layout-page-header',
                template: "\n        <div class=\"flex-container\">\n            <div class=\"page-title\" [class.pb-2]=\"pageSubtitle\">\n                {{pageTitle}}\n            </div>\n            <div *ngIf=\"customActionsContent\" class=\"ml-auto\">\n                <ng-content select=\"uxLayoutPageHeaderActionsContent\"></ng-content>\n            </div>\n            <ul *ngIf=\"customActionIconsContent\" class=\"page-action-icons ml-auto\">\n                <ng-content select=\"uxLayoutPageHeaderActionIconsContent\"></ng-content>\n            </ul>\n        </div>\n\n        <div *ngIf=\"pageSubtitle\" class=\"page-sub-title\">\n            {{pageSubtitle}}\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxLayoutPageHeaderComponent.ctorParameters = function () { return []; };
UxLayoutPageHeaderComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'pageTitle': [{ type: Input },],
    'pageSubtitle': [{ type: Input },],
    'customActionsContent': [{ type: ContentChild, args: [forwardRef(function () {
                    return UxLayoutPageHeaderActionsContentTagDirective;
                }),] },],
    'customActionIconsContent': [{ type: ContentChild, args: [forwardRef(function () {
                    return UxLayoutPageHeaderActionIconsContentTagDirective;
                }),] },],
};
var UxLayoutPageHeaderActionsContentTagDirective = (function () {
    function UxLayoutPageHeaderActionsContentTagDirective() {
    }
    return UxLayoutPageHeaderActionsContentTagDirective;
}());
UxLayoutPageHeaderActionsContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutPageHeaderActionsContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutPageHeaderActionsContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutPageHeaderActionIconsContentTagDirective = (function () {
    function UxLayoutPageHeaderActionIconsContentTagDirective() {
    }
    return UxLayoutPageHeaderActionIconsContentTagDirective;
}());
UxLayoutPageHeaderActionIconsContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxLayoutPageHeaderActionIconsContent' },] },
];
/**
 * @nocollapse
 */
UxLayoutPageHeaderActionIconsContentTagDirective.ctorParameters = function () { return []; };
var UxLayoutPageHeaderComponentModule = (function () {
    function UxLayoutPageHeaderComponentModule() {
    }
    return UxLayoutPageHeaderComponentModule;
}());
UxLayoutPageHeaderComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [
                    UxLayoutPageHeaderComponent,
                    UxLayoutPageHeaderActionsContentTagDirective,
                    UxLayoutPageHeaderActionIconsContentTagDirective
                ],
                declarations: [
                    UxLayoutPageHeaderComponent,
                    UxLayoutPageHeaderActionsContentTagDirective,
                    UxLayoutPageHeaderActionIconsContentTagDirective
                ]
            },] },
];
/**
 * @nocollapse
 */
UxLayoutPageHeaderComponentModule.ctorParameters = function () { return []; };

var UxDashboardButtonComponent = (function () {
    function UxDashboardButtonComponent() {
        this.isRounded = true;
        this.isLarge = false;
        this.isFullWidth = false;
        this.cmpPrefix = 'ux-dashboard-button';
    }
    Object.defineProperty(UxDashboardButtonComponent.prototype, "stateRoundedClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isRounded) {
                return this.cmpPrefix + '--rounded';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxDashboardButtonComponent.prototype, "stateTypeClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.typeClass) {
                return this.cmpPrefix + '--' + this.typeClass;
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxDashboardButtonComponent.prototype, "stateLargeClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isLarge) {
                return this.cmpPrefix + '--large';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxDashboardButtonComponent.prototype, "stateFullWidthClass", {
        /**
         * @return {?}
         */
        get: function () {
            if (this.isFullWidth) {
                return this.cmpPrefix + '--full-width';
            }
            return '';
        },
        enumerable: true,
        configurable: true
    });
    return UxDashboardButtonComponent;
}());
UxDashboardButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-dashboard-button',
                template: "\n        <div class=\"ux-dashboard-button {{stateRoundedClass}} {{stateTypeClass}} {{stateLargeClass}} {{stateFullWidthClass}}\" tabindex=\"1\">\n            <div class=\"ux-dashboard-button__content\">\n                <div *ngIf=\"iconClass\" class=\"ux-dashboard-button__content-main-icon\">\n                    <span class=\"fa {{iconClass}}\"></span>\n                </div>\n                <div *ngIf=\"!iconClass\" class=\"ux-dashboard-button__content-main-label\">\n                    {{mainLabel}}\n                </div>\n                <div class=\"ux-dashboard-button__content-title\">\n                    {{label}}\n                </div>\n                <div class=\"ux-dashboard-button__content-sub-title\">\n                    {{subLabel}}\n                </div>\n            </div>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxDashboardButtonComponent.ctorParameters = function () { return []; };
UxDashboardButtonComponent.propDecorators = {
    'mainLabel': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'url': [{ type: Input },],
    'isRounded': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'isFullWidth': [{ type: Input },],
};
var UxDashboardButtonComponentModule = (function () {
    function UxDashboardButtonComponentModule() {
    }
    return UxDashboardButtonComponentModule;
}());
UxDashboardButtonComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, RouterModule],
                exports: [UxDashboardButtonComponent],
                declarations: [UxDashboardButtonComponent]
            },] },
];
/**
 * @nocollapse
 */
UxDashboardButtonComponentModule.ctorParameters = function () { return []; };

var UxDashboardButtonsComponent = (function () {
    function UxDashboardButtonsComponent() {
        this.isRounded = true;
        this.isLarge = false;
        this.isExtendedPanelsViewActive = false;
    }
    /**
     * @return {?}
     */
    UxDashboardButtonsComponent.prototype.ngAfterContentInit = function () {
        if (this.isRounded) {
            this.buttons.forEach(function (i) {
                i.isRounded = true;
            });
        }
        else {
            this.buttons.forEach(function (i) {
                i.isRounded = false;
            });
        }
        if (this.isLarge) {
            this.buttons.forEach(function (i) {
                i.isLarge = true;
            });
        }
        else {
            this.buttons.forEach(function (i) {
                i.isLarge = false;
            });
        }
    };
    return UxDashboardButtonsComponent;
}());
UxDashboardButtonsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-dashboard-buttons',
                template: "\n    <ul class=\"ux-dashboard-buttons {{styleClass}}\">\n        <ng-content></ng-content>\n    </ul>        \n  "
            },] },
];
/**
 * @nocollapse
 */
UxDashboardButtonsComponent.ctorParameters = function () { return []; };
UxDashboardButtonsComponent.propDecorators = {
    'isRounded': [{ type: Input },],
    'isLarge': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'buttons': [{ type: ContentChildren, args: [forwardRef(function () { return UxDashboardButtonComponent; }),] },],
};
var UxDashboardButtonsComponentModule = (function () {
    function UxDashboardButtonsComponentModule() {
    }
    return UxDashboardButtonsComponentModule;
}());
UxDashboardButtonsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxDashboardButtonsComponent],
                declarations: [UxDashboardButtonsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxDashboardButtonsComponentModule.ctorParameters = function () { return []; };

var UxTabComponent = (function () {
    function UxTabComponent() {
        this.isClosable = false;
        this.isDisabled = false;
        this.isActive = false;
        this.isVisible = true;
        this.isVisibleOnScreen = true;
    }
    return UxTabComponent;
}());
UxTabComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-tab',
                template: "\n    <div [hidden]=\"!isActive\">\n      <ng-content></ng-content>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTabComponent.ctorParameters = function () { return []; };
UxTabComponent.propDecorators = {
    'id': [{ type: Input },],
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'iconTypeClass': [{ type: Input },],
    'isClosable': [{ type: Input },],
    'isDisabled': [{ type: Input },],
    'tag': [{ type: Input },],
    'tagTypeClass': [{ type: Input },],
    'hasMarker': [{ type: Input },],
    'markerTypeClass': [{ type: Input },],
    'isActive': [{ type: Input },],
    'url': [{ type: Input },],
    'isVisible': [{ type: Input },],
};
var UxTabComponentModule = (function () {
    function UxTabComponentModule() {
    }
    return UxTabComponentModule;
}());
UxTabComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxTabComponent],
                declarations: [UxTabComponent]
            },] },
];
/**
 * @nocollapse
 */
UxTabComponentModule.ctorParameters = function () { return []; };

var UxTabsComponent = (function () {
    /**
     * @param {?} uxService
     * @param {?} el
     */
    function UxTabsComponent(uxService, el) {
        var _this = this;
        this.uxService = uxService;
        this.el = el;
        this.tabs = [];
        this.isMainNavigation = false;
        this.isSubTabs = false;
        this.tabSelected = new EventEmitter();
        this.tabClosed = new EventEmitter();
        this.calculatedWidth = 'auto';
        console.log('UxTabs : initiated');
        this.subscription = this.uxService.activeBreakpoint.subscribe(function (bkp) {
            console.log('Breakpoint change detected : ' + bkp);
            _this.checkTabsWidthHandler();
        });
    }
    /**
     * @return {?}
     */
    UxTabsComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        this.childrenTabs.forEach(function (tab) {
            _this.tabs.push(tab);
        });
        var /** @type {?} */ activeTabs = this.tabs.filter(function (tab) { return tab.isActive; });
        if (activeTabs.length === 0 && this.tabs.length !== 0) {
            this.selectTab(this.tabs[0], true);
        }
        if (this.isMainNavigation) {
            this.calculatedWidth = 100 / this.tabs.length + '%';
        }
    };
    /**
     * @return {?}
     */
    UxTabsComponent.prototype.ngOnDestroy = function () {
        console.log('UxTabs : destroyed');
        this.subscription.unsubscribe();
    };
    /**
     * @return {?}
     */
    UxTabsComponent.prototype.checkTabsWidthHandler = function () {
        if (this.el.nativeElement.children[0]) {
            var /** @type {?} */ lastVisibleOnScreenTab = this.el.nativeElement.children[0].lastElementChild;
            if (lastVisibleOnScreenTab) {
                var /** @type {?} */ right = lastVisibleOnScreenTab.offsetLeft + lastVisibleOnScreenTab.clientWidth;
                var /** @type {?} */ triggerOffset = 30;
                var /** @type {?} */ position = this.uxService.position(lastVisibleOnScreenTab);
                // console.log("========> UxTabs : checkTabsWidthHandler : position : " + JSON.stringify(position));
                // console.log('========> UxTabs : checkTabsWidthHandler : end of last tab at : ' + right);
                // console.log('========> UxTabs : checkTabsWidthHandler : current window width : ' + this.uxService.windowWidth);
                if (right + triggerOffset > this.uxService.windowWidth) {
                    this.tabs[this.tabs.length - 1].isVisibleOnScreen = false;
                }
                else {
                    this.tabs[this.tabs.length - 1].isVisibleOnScreen = true;
                }
            }
        }
    };
    /**
     * @param {?} tab
     * @param {?=} init
     * @return {?}
     */
    UxTabsComponent.prototype.selectTab = function (tab, init) {
        if (!tab.isDisabled) {
            this.tabs.forEach(function (currentTab) { return currentTab.isActive = false; });
            tab.isActive = true;
            if (!init) {
                this.tabSelected.emit(tab);
            }
        }
    };
    /**
     * @param {?} event
     * @param {?} tab
     * @return {?}
     */
    UxTabsComponent.prototype.onCloseTab = function (event, tab) {
        event.stopPropagation();
        tab.isVisible = false;
        this.tabClosed.emit(tab);
    };
    return UxTabsComponent;
}());
UxTabsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-tabs',
                template: "\n    <div class=\"ux-tabs {{styleClass}}\" [class.ux-tabs--main-navigation]=\"isMainNavigation\">\n      <template *ngFor=\"let tab of tabs; let i = index\" [ngIf]=\"tab.isVisible\">\n        <div class=\"ux-tab-item\" id=\"{{tab.id}}\" (click)=\"selectTab(tab)\"\n                [class.ux-tab-item--active]=\"tab.isActive\"\n                [class.ux-tab-item--disabled]=\"tab.isDisabled\"\n                [class.ux-tab-item--sub-tabs]=\"isSubTabs\"\n                [style.width]=\"calculatedWidth\">\n            <ux-a-icon styleClass=\"ux-tab-item__icon\"\n                       iconClass=\"{{tab.iconClass}}\" typeClass=\"{{tab.iconTypeClass}}\"\n                       [isRounded]=\"false\">\n            </ux-a-icon>\n            <ux-a-label styleClass=\"ux-tab-item__label\"\n                        label=\"{{tab.label}}\"\n                        subLabel=\"{{tab.subLabel}}\">\n            </ux-a-label>\n            <ux-a-tag *ngIf=\"tab.tag\" styleClass=\"ux-tab-item__tag\"\n                      label=\"{{tab.tag}}\"\n                      [isSmall]=\"true\"\n                      typeClass=\"{{tab.tagTypeClass}}\">\n            </ux-a-tag>\n            <ux-a-marker *ngIf=\"tab.hasMarker\"\n                         styleClass=\"ux-tab-item__marker\"\n                         typeClass=\"{{tab.markerTypeClass}}\">\n            </ux-a-marker>\n            <span *ngIf=\"tab.isClosable\" class=\"ux-tab-item__close-toggle fa fa-times\" (click)=\"onCloseTab($event,tab)\"></span>\n        </div>\n      </template>\n    </div>\n    <div class=\"ux-tab-content\">\n        <ng-content></ng-content>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTabsComponent.ctorParameters = function () { return [
    { type: UxService, },
    { type: ElementRef, },
]; };
UxTabsComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'tabs': [{ type: Input },],
    'isMainNavigation': [{ type: Input },],
    'isSubTabs': [{ type: Input },],
    'tabSelected': [{ type: Output },],
    'tabClosed': [{ type: Output },],
    'childrenTabs': [{ type: ContentChildren, args: [UxTabComponent,] },],
};
var UxTabsComponentModule = (function () {
    function UxTabsComponentModule() {
    }
    return UxTabsComponentModule;
}());
UxTabsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxLabelComponentModule, UxIconComponentModule, UxMarkerComponentModule, UxTagComponentModule],
                exports: [UxTabsComponent],
                declarations: [UxTabsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxTabsComponentModule.ctorParameters = function () { return []; };

var UxTab = (function () {
    /**
     * @param {?=} values
     */
    function UxTab(values) {
        if (values === void 0) { values = {}; }
        this.isClosable = false;
        this.isDisabled = false;
        this.isActive = false;
        this.isVisible = true;
        this.isVisibleOnScreen = true;
        Object.assign(this, values);
    }
    return UxTab;
}());

var UxTimelineItemsComponent = (function () {
    function UxTimelineItemsComponent() {
        this.isLeftAligned = false;
    }
    /**
     * @return {?}
     */
    UxTimelineItemsComponent.prototype.ngAfterContentInit = function () { };
    return UxTimelineItemsComponent;
}());
UxTimelineItemsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-timeline-items',
                template: "\n     <ul class=\"timeline\" [class.left-aligned]=\"isLeftAligned\">\n        <ng-content></ng-content>\n     </ul>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTimelineItemsComponent.ctorParameters = function () { return []; };
UxTimelineItemsComponent.propDecorators = {
    'isLeftAligned': [{ type: Input },],
};
var UxTimelineItemsComponentModule = (function () {
    function UxTimelineItemsComponentModule() {
    }
    return UxTimelineItemsComponentModule;
}());
UxTimelineItemsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxTimelineItemsComponent],
                declarations: [UxTimelineItemsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxTimelineItemsComponentModule.ctorParameters = function () { return []; };

var UxTimelineItemComponent = (function () {
    /**
     * @param {?} uxTimelineItemsComponent
     */
    function UxTimelineItemComponent(uxTimelineItemsComponent) {
        this.isLeftAligned = false;
        this.uxTimelineItemsComponent = uxTimelineItemsComponent;
    }
    /**
     * @return {?}
     */
    UxTimelineItemComponent.prototype.ngAfterContentInit = function () {
        this.isLeftAligned = this.uxTimelineItemsComponent.isLeftAligned;
    };
    return UxTimelineItemComponent;
}());
UxTimelineItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-timeline-item',
                template: "\n        <li [ngClass]=\"styleClass\">\n            <a>\n                <div *ngIf=\"!isLeftAligned\" class=\"date\">{{date}}</div>\n                <span class=\"icon fa fa-circle-o\"></span>\n                <div class=\"content\">\n                    <div class=\"title\">{{label}}</div>\n                    <div class=\"sub\">{{subLabel}}</div>\n                    <div *ngIf=\"isLeftAligned\" class=\"date\">{{date}}</div>\n                </div>\n            </a>\n        </li>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTimelineItemComponent.ctorParameters = function () { return [
    { type: UxTimelineItemsComponent, decorators: [{ type: Host }, { type: Inject, args: [forwardRef(function () { return UxTimelineItemsComponent; }),] },] },
]; };
UxTimelineItemComponent.propDecorators = {
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'styleClass': [{ type: Input },],
    'date': [{ type: Input },],
};
var UxTimelineItemComponentModule = (function () {
    function UxTimelineItemComponentModule() {
    }
    return UxTimelineItemComponentModule;
}());
UxTimelineItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxTimelineItemComponent],
                declarations: [UxTimelineItemComponent]
            },] },
];
/**
 * @nocollapse
 */
UxTimelineItemComponentModule.ctorParameters = function () { return []; };

var UxWizardStepComponent = (function () {
    function UxWizardStepComponent() {
        this.isCompleted = false;
        this.isActive = false;
        this.isShowStepTitle = false;
        this.isDisabled = false;
    }
    return UxWizardStepComponent;
}());
UxWizardStepComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-wizard-step',
                template: "\n    <div [hidden]=\"!isActive\">\n        <template [ngIf]=\"!customContent\">\n            <ux-panel label=\"{{label}}\">\n                <ng-content></ng-content>\n            </ux-panel>\n        </template>\n        <template [ngIf]=\"customContent\">\n            <h5 *ngIf=\"isShowStepTitle\" class=\"section-title first\">\n               {{label}}\n            </h5>\n            <div class=\"content\">\n                <ng-content select=\"uxWizardStepContent\"></ng-content>\n            </div>\n        </template>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxWizardStepComponent.ctorParameters = function () { return []; };
UxWizardStepComponent.propDecorators = {
    'label': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'isCompleted': [{ type: Input },],
    'isActive': [{ type: Input },],
    'isShowStepTitle': [{ type: Input },],
    'isDisabled': [{ type: Input },],
    'customContent': [{ type: ContentChild, args: [forwardRef(function () { return UxWizardStepContentTagDirective; }),] },],
};
var UxWizardStepContentTagDirective = (function () {
    function UxWizardStepContentTagDirective() {
    }
    return UxWizardStepContentTagDirective;
}());
UxWizardStepContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxWizardStepContent' },] },
];
/**
 * @nocollapse
 */
UxWizardStepContentTagDirective.ctorParameters = function () { return []; };
var UxWizardStepComponentModule = (function () {
    function UxWizardStepComponentModule() {
    }
    return UxWizardStepComponentModule;
}());
UxWizardStepComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxPanelComponentModule],
                exports: [UxWizardStepComponent, UxWizardStepContentTagDirective],
                declarations: [UxWizardStepComponent, UxWizardStepContentTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxWizardStepComponentModule.ctorParameters = function () { return []; };

var UxWizardStepsComponent = (function () {
    function UxWizardStepsComponent() {
        this.isCustomContent = false;
        this.isShowStepTitle = false;
        this.selectStep = new EventEmitter();
    }
    /**
     * @return {?}
     */
    UxWizardStepsComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        var /** @type {?} */ activeTabs = this.steps.filter(function (step) { return step.isActive; });
        if (activeTabs.length === 0) {
            this.onSelectStep(this.steps.first);
        }
        this.steps.toArray().forEach(function (step) {
            step.isShowStepTitle = _this.isShowStepTitle;
        });
    };
    /**
     * @param {?} step
     * @return {?}
     */
    UxWizardStepsComponent.prototype.onSelectStep = function (step) {
        if (!step.isDisabled) {
            this.steps.toArray().forEach(function (currentStep) { return currentStep.isActive = false; });
            step.isActive = true;
            this.selectStep.emit(step);
        }
    };
    return UxWizardStepsComponent;
}());
UxWizardStepsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-wizard-steps',
                template: "\n    <div class=\"ux-wizard-steps\">\n        <div *ngFor=\"let step of steps\" class=\"ux-wizard-step\" tabindex=\"0\"\n             [class.ux-wizard-step--completed]=\"step.isCompleted\"\n             [class.ux-wizard-step--active]=\"step.isActive\"\n             [class.ux-wizard-step--disabled]=\"step.isDisabled\"\n             (click)=\"onSelectStep(step)\">\n\n             <div *ngIf=\"step.isActive\" class=\"ux-wizard-step__current-marker\">\n                <span class=\"ux-wizard-step__current-marker-icon fa fa-fw fa-map-marker\"></span>\n             </div>\n             <div class=\"ux-wizard-step__indicator-wrapper\">\n                <span class=\"ux-wizard-step__indicator-wrapper-item\"></span>\n             </div>\n             <div class=\"ux-wizard-step__label-wrapper\">\n                <div class=\"ux-wizard-step__label-wrapper-label\">\n                    {{step.label}}\n                </div>\n                <div class=\"ux-wizard-step__label-wrapper-sub-label\">\n                    {{step.subLabel}}\n                </div>\n             </div>\n\n        </div>\n    </div>\n    <div class=\"step-content\">\n        <ng-content></ng-content>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxWizardStepsComponent.ctorParameters = function () { return []; };
UxWizardStepsComponent.propDecorators = {
    'isCustomContent': [{ type: Input },],
    'isShowStepTitle': [{ type: Input },],
    'selectStep': [{ type: Output },],
    'steps': [{ type: ContentChildren, args: [UxWizardStepComponent,] },],
};
var UxWizardStepsComponentModule = (function () {
    function UxWizardStepsComponentModule() {
    }
    return UxWizardStepsComponentModule;
}());
UxWizardStepsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxWizardStepsComponent],
                declarations: [UxWizardStepsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxWizardStepsComponentModule.ctorParameters = function () { return []; };

var UxTreeListItemComponent = (function () {
    /**
     * @param {?} treeListComponent
     * @param {?} uxService
     */
    function UxTreeListItemComponent(treeListComponent, uxService) {
        this.uxService = uxService;
        this.active = false;
        this.expanded = false;
        this.alwaysExpanded = false;
        this.isDisplaySubLinksOnHover = false;
        this.subLinks = [];
        this.isVisible = true;
        this.isHovered = false;
        // tree states
        this.hasSub = false;
        // custom content
        this.hasCustomContent = false;
        this.treeListComponent = treeListComponent;
    }
    /**
     * @return {?}
     */
    UxTreeListItemComponent.prototype.ngAfterContentInit = function () {
        // setting tree states
        if (this.subTreeList.length !== 0) {
            this.hasSub = true;
        }
        // checking if customContent set
        if (this.customContent.length !== 0) {
            this.hasCustomContent = true;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxTreeListItemComponent.prototype.toggle = function (event) {
        event.preventDefault();
        event.stopPropagation();
        this.expanded = !this.expanded;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxTreeListItemComponent.prototype.navigateToLink = function (event) {
        if (this.linkUrl) {
            this.uxService.appRouter.navigate([this.linkUrl]);
        }
    };
    /**
     * @param {?} state
     * @return {?}
     */
    UxTreeListItemComponent.prototype.setVisibleState = function (state) {
        this.isVisible = state;
        if (this.subTreeList.length !== 0) {
            this.subTreeList.toArray().forEach(function (item) {
                item.setVisibleState(state);
            });
        }
    };
    /**
     * @param {?} state
     * @return {?}
     */
    UxTreeListItemComponent.prototype.setExpandedState = function (state) {
        this.expanded = state;
        if (this.subTreeList.length !== 0) {
            this.subTreeList.toArray().forEach(function (item) {
                item.setExpandedState(state);
            });
        }
    };
    return UxTreeListItemComponent;
}());
UxTreeListItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-tree-list-item',
                template: "\n      <div class=\"ux-tree-list-item {{styleClass}}\"\n          [class.ux-tree-list-item--active]=\"active\"\n          [hidden]=\"!isVisible\">\n\n        <div class=\"ux-tree-list-item-header ux-tree-list-item-header--{{typeClass}} {{headerStyleClass}}\" (click)=\"navigateToLink($event)\">\n            <div class=\"ux-tree-list-item-header__content\">\n                <template [ngIf]=\"hasCustomContent\">\n                    <ng-content select=\"uxTreeListItemCustomContent\"></ng-content>\n                </template>\n\n                <template [ngIf]=\"!hasCustomContent\">\n                    <span *ngIf=\"iconClass\" class=\"ux-tree-list-item-header__content-icon fa fa-fw\"\n                                            [ngClass]=\"iconClass\" [class.with-sub-label]=\"subLabel\"></span>\n\n                    <template [ngIf]=\"!customSubLabel\">\n                        <ux-a-label label=\"{{label}}\" subLabel=\"{{subLabel}}\"\n                                    [hasMarker]=\"markerTypeClass\" markerTypeClass=\"{{markerTypeClass}}\"\n                                    (click)=\"navigateToLink($event)\"></ux-a-label>\n                    </template>\n\n                    <template [ngIf]=\"customSubLabel\">\n                        <ux-a-label label=\"{{label}}\"\n                                    [hasMarker]=\"markerTypeClass\" markerTypeClass=\"{{markerTypeClass}}\"\n                                    (click)=\"navigateToLink($event)\">\n                            <uxLabelSubLabel>\n                                <ng-content select=\"uxTreeListItemSubLabel\"></ng-content>\n                            </uxLabelSubLabel>\n                        </ux-a-label>\n                    </template>\n\n                    <div class=\"ux-tree-list-item-header__content-right-content\">\n                        <div class=\"ux-tree-list-item-header__content-right-content-sub\">\n                            <ng-content select=\"uxTreeListItemRightContent\"></ng-content>\n                        </div>\n                        <span *ngIf=\"hasSub && !alwaysExpanded\"\n                              class=\"ux-tree-list-item-header__content-expand-toggle\" (click)=\"toggle($event)\">\n                            <span class=\"fa\" [ngClass]=\"{'fa-angle-right': !expanded, 'fa-angle-down': expanded}\"></span>\n                        </span>\n                    </div>\n                </template>\n            </div>\n\n            <template [ngIf]=\"customDetailContent\">\n                <div class=\"ux-tree-list-item-header__details-content\">\n                    <ng-content select=\"uxTreeListItemDetailsContent\"></ng-content>\n                </div>\n            </template>\n\n            <template [ngIf]=\"customSubContainerContent\">\n                <ng-content select=\"uxTreeListItemSubContainerContent\"></ng-content>\n            </template>\n        </div>\n\n        <template [ngIf]=\"expanded || alwaysExpanded\">\n            <ng-content></ng-content>\n        </template>\n      </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTreeListItemComponent.ctorParameters = function () { return [
    { type: UxTreeListComponent, decorators: [{ type: Host }, { type: Inject, args: [forwardRef(function () { return UxTreeListComponent; }),] },] },
    { type: UxService, },
]; };
UxTreeListItemComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'headerStyleClass': [{ type: Input },],
    'id': [{ type: Input },],
    'label': [{ type: Input },],
    'linkUrl': [{ type: Input },],
    'subLabel': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'active': [{ type: Input },],
    'expanded': [{ type: Input },],
    'alwaysExpanded': [{ type: Input },],
    'url': [{ type: Input },],
    'isDisplaySubLinksOnHover': [{ type: Input },],
    'subLinks': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'markerTypeClass': [{ type: Input },],
    'subTreeList': [{ type: ContentChildren, args: [forwardRef(function () { return UxTreeListComponent; }),] },],
    'customSubLabel': [{ type: ContentChild, args: [forwardRef(function () { return UxTreeListItemSubLabelTagDirective; }),] },],
    'customContent': [{ type: ContentChildren, args: [forwardRef(function () { return UxTreeListItemCustomContentTagDirective; }), { descendants: false },] },],
    'customDetailContent': [{ type: ContentChild, args: [forwardRef(function () { return UxTreeListItemDetailsContentTagDirective; }),] },],
    'customSubContainerContent': [{ type: ContentChild, args: [forwardRef(function () { return UxTreeListItemSubContainerContentTagDirective; }),] },],
};
var UxTreeListItemCustomContentTagDirective = (function () {
    function UxTreeListItemCustomContentTagDirective() {
    }
    return UxTreeListItemCustomContentTagDirective;
}());
UxTreeListItemCustomContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxTreeListItemCustomContent' },] },
];
/**
 * @nocollapse
 */
UxTreeListItemCustomContentTagDirective.ctorParameters = function () { return []; };
var UxTreeListItemRightContentTagDirective = (function () {
    function UxTreeListItemRightContentTagDirective() {
    }
    return UxTreeListItemRightContentTagDirective;
}());
UxTreeListItemRightContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxTreeListItemRightContent' },] },
];
/**
 * @nocollapse
 */
UxTreeListItemRightContentTagDirective.ctorParameters = function () { return []; };
var UxTreeListItemSubLabelTagDirective = (function () {
    function UxTreeListItemSubLabelTagDirective() {
    }
    return UxTreeListItemSubLabelTagDirective;
}());
UxTreeListItemSubLabelTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxTreeListItemSubLabel' },] },
];
/**
 * @nocollapse
 */
UxTreeListItemSubLabelTagDirective.ctorParameters = function () { return []; };
var UxTreeListItemDetailsContentTagDirective = (function () {
    function UxTreeListItemDetailsContentTagDirective() {
    }
    return UxTreeListItemDetailsContentTagDirective;
}());
UxTreeListItemDetailsContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxTreeListItemDetailsContent' },] },
];
/**
 * @nocollapse
 */
UxTreeListItemDetailsContentTagDirective.ctorParameters = function () { return []; };
var UxTreeListItemSubContainerContentTagDirective = (function () {
    function UxTreeListItemSubContainerContentTagDirective() {
    }
    return UxTreeListItemSubContainerContentTagDirective;
}());
UxTreeListItemSubContainerContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxTreeListItemSubContainerContent' },] },
];
/**
 * @nocollapse
 */
UxTreeListItemSubContainerContentTagDirective.ctorParameters = function () { return []; };
var UxTreeListItemComponentModule = (function () {
    function UxTreeListItemComponentModule() {
    }
    return UxTreeListItemComponentModule;
}());
UxTreeListItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, RouterModule, UxLabelComponentModule],
                exports: [UxTreeListItemComponent, UxTreeListItemSubLabelTagDirective, UxTreeListItemCustomContentTagDirective,
                    UxTreeListItemRightContentTagDirective, UxTreeListItemDetailsContentTagDirective, UxTreeListItemSubContainerContentTagDirective],
                declarations: [UxTreeListItemComponent, UxTreeListItemSubLabelTagDirective, UxTreeListItemCustomContentTagDirective,
                    UxTreeListItemRightContentTagDirective, UxTreeListItemDetailsContentTagDirective, UxTreeListItemSubContainerContentTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxTreeListItemComponentModule.ctorParameters = function () { return []; };

var UxTreeListComponent = (function () {
    function UxTreeListComponent() {
        this.isShowToolbar = false;
        this.isShowToolbarToggle = true;
        this.hasItemsUrl = false;
        this.hasItemsBullet = true;
        this.itemSelected = new EventEmitter();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    UxTreeListComponent.prototype.onExpandAll = function (event) {
        this.setExpandedState(true);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxTreeListComponent.prototype.onCollapseAll = function (event) {
        this.setExpandedState(false);
    };
    /**
     * @param {?} filterValue
     * @return {?}
     */
    UxTreeListComponent.prototype.onFilter = function (filterValue) {
        // TODO find a recursive way of doing the filtering througout the tree structure
        var _this = this;
        if (filterValue !== '') {
            this.setVisibleState(false);
            this.items.toArray().forEach(function (item1) {
                item1.isVisible = _this.filterMatched(item1.label, filterValue);
                if (item1.subTreeList.length !== 0) {
                    item1.subTreeList.toArray().forEach(function (item1SubTreeList) {
                        item1SubTreeList.items.toArray().forEach(function (item2) {
                            if (_this.filterMatched(item2.label, filterValue)) {
                                item2.isVisible = true;
                                item1.isVisible = true;
                                item1.expanded = true;
                                if (item2.subTreeList.length !== 0) {
                                    item2.subTreeList.toArray().forEach(function (item2SubTreeList) {
                                        item2SubTreeList.items.toArray().forEach(function (item3) {
                                            item3.isVisible = true;
                                        });
                                    });
                                }
                            }
                            if (item2.subTreeList.length !== 0) {
                                item2.subTreeList.toArray().forEach(function (item2SubTreeList) {
                                    item2SubTreeList.items.toArray().forEach(function (item3) {
                                        if (_this.filterMatched(item3.label, filterValue)) {
                                            item3.isVisible = true;
                                            item2.isVisible = true;
                                            item2.expanded = true;
                                            item1.isVisible = true;
                                            item1.expanded = true;
                                        }
                                    });
                                });
                            }
                        });
                    });
                }
            });
        }
        else {
            this.setVisibleState(true);
        }
    };
    /**
     * @param {?} state
     * @return {?}
     */
    UxTreeListComponent.prototype.setVisibleState = function (state) {
        this.items.toArray().forEach(function (item) {
            item.setVisibleState(state);
        });
    };
    /**
     * @param {?} state
     * @return {?}
     */
    UxTreeListComponent.prototype.setExpandedState = function (state) {
        this.items.toArray().forEach(function (item) {
            item.setExpandedState(state);
        });
    };
    /**
     * @param {?} label
     * @param {?} filterValue
     * @return {?}
     */
    UxTreeListComponent.prototype.filterMatched = function (label, filterValue) {
        if (label.toUpperCase().indexOf(filterValue.toUpperCase()) !== -1) {
            return true;
        }
        return false;
    };
    return UxTreeListComponent;
}());
UxTreeListComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-tree-list',
                template: "\n    <ux-a-toolbar-filter [isVisible]=\"isShowToolbar\" [isToggleVisible]=\"isShowToolbarToggle\"\n                         (filter)=\"onFilter($event)\" (expandAll)=\"onExpandAll($event)\"\n                         (collapseAll)=\"onCollapseAll($event)\"\n                         filterLabel=\"{{filterLabel}}\" expandAllLabel=\"{{expandAllLabel}}\"\n                         collapseAllLabel=\"{{collapseAllLabel}}\">\n            <ng-content select=\"uxTreeListToolbarContent\"></ng-content>\n    </ux-a-toolbar-filter>\n\n    <div class=\"ux-tree-list {{styleClass}}\"\n         [class.ux-tree-list--with-items-url]=\"hasItemsUrl\"\n         [class.ux-tree-list--no-bullets]=\"!hasItemsBullet\">\n       <ng-content></ng-content>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTreeListComponent.ctorParameters = function () { return []; };
UxTreeListComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'isShowToolbar': [{ type: Input },],
    'isShowToolbarToggle': [{ type: Input },],
    'filterLabel': [{ type: Input },],
    'expandAllLabel': [{ type: Input },],
    'collapseAllLabel': [{ type: Input },],
    'hasItemsUrl': [{ type: Input },],
    'hasItemsBullet': [{ type: Input },],
    'itemSelected': [{ type: Output },],
    'items': [{ type: ContentChildren, args: [forwardRef(function () { return UxTreeListItemComponent; }),] },],
};
var UxTreeListToolbarContentTagDirective = (function () {
    function UxTreeListToolbarContentTagDirective() {
    }
    return UxTreeListToolbarContentTagDirective;
}());
UxTreeListToolbarContentTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxTreeListToolbarContent' },] },
];
/**
 * @nocollapse
 */
UxTreeListToolbarContentTagDirective.ctorParameters = function () { return []; };
var UxTreeListComponentModule = (function () {
    function UxTreeListComponentModule() {
    }
    return UxTreeListComponentModule;
}());
UxTreeListComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule, UxToolbarFilterComponentModule],
                exports: [UxTreeListComponent, UxTreeListToolbarContentTagDirective],
                declarations: [UxTreeListComponent, UxTreeListToolbarContentTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxTreeListComponentModule.ctorParameters = function () { return []; };

var UxCarouselItemComponent = (function () {
    function UxCarouselItemComponent() {
        this.isActive = false;
    }
    return UxCarouselItemComponent;
}());
UxCarouselItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-carousel-item',
                template: "\n    <div [hidden]=\"!isActive\">\n        <ng-content></ng-content>         \n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxCarouselItemComponent.ctorParameters = function () { return []; };
UxCarouselItemComponent.propDecorators = {
    'isActive': [{ type: Input },],
};
var UxCarouselItemComponentModule = (function () {
    function UxCarouselItemComponentModule() {
    }
    return UxCarouselItemComponentModule;
}());
UxCarouselItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxCarouselItemComponent],
                declarations: [UxCarouselItemComponent]
            },] },
];
/**
 * @nocollapse
 */
UxCarouselItemComponentModule.ctorParameters = function () { return []; };

var UxCarouselItemsComponent = (function () {
    function UxCarouselItemsComponent() {
        this.isShowSummaryIndicators = true;
        this.itemsArray = [];
        this.itemsLength = 0;
        this.currentItem = 0;
    }
    /**
     * @return {?}
     */
    UxCarouselItemsComponent.prototype.ngAfterContentInit = function () {
        var /** @type {?} */ activeItems = this.items.filter(function (item) { return item.isActive; });
        if (activeItems.length === 0) {
            this.selectItem(this.items.first);
        }
        this.itemsArray = this.items.toArray();
        this.itemsLength = this.itemsArray.length;
    };
    /**
     * @param {?} item
     * @return {?}
     */
    UxCarouselItemsComponent.prototype.selectItem = function (item) {
        this.itemsArray.forEach(function (currentItem) { return currentItem.isActive = false; });
        item.isActive = true;
    };
    /**
     * @param {?} itemIndex
     * @return {?}
     */
    UxCarouselItemsComponent.prototype.gotoItem = function (itemIndex) {
        this.currentItem = itemIndex;
        this.selectItem(this.itemsArray[itemIndex]);
    };
    Object.defineProperty(UxCarouselItemsComponent.prototype, "isFirstItemActive", {
        /**
         * @return {?}
         */
        get: function () {
            return this.currentItem === 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UxCarouselItemsComponent.prototype, "isLastItemActive", {
        /**
         * @return {?}
         */
        get: function () {
            return this.currentItem === this.itemsArray.length - 1;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} event
     * @return {?}
     */
    UxCarouselItemsComponent.prototype.goPrevious = function (event) {
        if (!this.isFirstItemActive) {
            this.currentItem--;
            this.selectItem(this.itemsArray[this.currentItem]);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxCarouselItemsComponent.prototype.goNext = function (event) {
        if (!this.isLastItemActive) {
            this.currentItem++;
            this.selectItem(this.itemsArray[this.currentItem]);
        }
    };
    return UxCarouselItemsComponent;
}());
UxCarouselItemsComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-carousel-items',
                template: "\n    <div class=\"ux-carousel-items\">\n        <div class=\"row flex-container\">\n            <div class=\"col-2 text-center\">\n                <span class=\"fa fa-caret-left fa-3x ux-carousel-items__arrow\"\n                      [class.ux-carousel-items__arrow--inactive]=\"isFirstItemActive\"\n                      (click)=\"goPrevious($event)\"></span>\n            </div>\n\n            <div class=\"col-8\">\n                <ng-content></ng-content>\n            </div>\n\n            <div class=\"col-2 text-center\">\n                <span class=\"fa fa-caret-right fa-3x ux-carousel-items__arrow\"\n                      [class.ux-carousel-items__arrow--inactive]=\"isLastItemActive\"\n                      (click)=\"goNext($event)\"></span>\n            </div>\n        </div>\n        <div *ngIf=\"isShowSummaryIndicators\" class=\"row flex-container ux-carousel-items__summary-indicators\">\n            <div class=\"col-12 text-center\">\n                <span *ngFor=\"let item of itemsArray; let i = index\"\n                      class=\"fa ux-carousel-items__summary-indicators-item\"\n                      (click)=\"gotoItem(i)\"\n                      [ngClass]=\"i === currentItem ? 'fa-circle ux-u-color-primary' : 'fa-circle-o ux-u-color-grey-light'\"></span>\n                <!--\n                <button *ngFor=\"let item of itemsArray; let i = index\" (click)=\"gotoItem(i)\"\n                        class=\"btn btn-sm ux-carousel-items__summary-indicators-button\"\n                        [ngClass]=\"i === currentItem ? 'btn-primary' : 'btn-secondary'\">\n                    {{i+1}}\n                </button>\n                -->\n            </div>\n        </div>\n    </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxCarouselItemsComponent.ctorParameters = function () { return []; };
UxCarouselItemsComponent.propDecorators = {
    'isShowSummaryIndicators': [{ type: Input },],
    'items': [{ type: ContentChildren, args: [UxCarouselItemComponent,] },],
};
var UxCarouselItemsComponentModule = (function () {
    function UxCarouselItemsComponentModule() {
    }
    return UxCarouselItemsComponentModule;
}());
UxCarouselItemsComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxCarouselItemComponentModule],
                exports: [UxCarouselItemsComponent],
                declarations: [UxCarouselItemsComponent]
            },] },
];
/**
 * @nocollapse
 */
UxCarouselItemsComponentModule.ctorParameters = function () { return []; };

var UxTreeNodeComponent = (function () {
    /**
     * @param {?} router
     * @param {?} uxService
     */
    function UxTreeNodeComponent(router, uxService) {
        this.router = router;
        this.uxService = uxService;
        this.isRootNode = false;
        this.nodeClick = new EventEmitter();
    }
    /**
     * @param {?} event
     * @param {?} uxLink
     * @return {?}
     */
    UxTreeNodeComponent.prototype.onNodeKeydown = function (event, uxLink) {
        if (event.keyCode === 13) {
            this.onNodeClick(event, uxLink);
        }
    };
    /**
     * @param {?} event
     * @param {?} uxLink
     * @return {?}
     */
    UxTreeNodeComponent.prototype.onNodeClick = function (event, uxLink) {
        if (!this.node.metadata) {
            this.node.expanded = !this.node.expanded;
        }
        else {
            if (this.node.url) {
                this.router.navigate([this.node.url]);
            }
            if (this.node.urlExternal) {
                window.open(this.node.urlExternal, '_blank');
            }
        }
        this.nodeClick.emit(uxLink);
        this.uxService.consumeEvent(event);
    };
    /**
     * @param {?} event
     * @param {?} uxLink
     * @return {?}
     */
    UxTreeNodeComponent.prototype.onMetadataNodeToggle = function (event, uxLink) {
        this.node.expanded = !this.node.expanded;
        this.uxService.consumeEvent(event);
    };
    /**
     * @param {?} uxLink
     * @return {?}
     */
    UxTreeNodeComponent.prototype.onSubNodeClick = function (uxLink) {
        this.nodeClick.emit(uxLink);
    };
    return UxTreeNodeComponent;
}());
UxTreeNodeComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-tree-node',
                template: "\n     <a *ngIf=\"node.visible\" class=\"ux-tree__node\" tabindex=\"1\" (click)=\"onNodeClick($event, node)\" (keydown)=\"onNodeKeydown($event, node)\"\n          [class.ux-tree__node-parent]=\"node.children\"\n          [class.ux-tree__node-child]=\"!node.children && !isRootNode\"\n          [class.ux-tree__node-child-root]=\"!node.children && isRootNode\" title=\"{{node.label}}\">\n        <span *ngIf=\"node.children\"\n              class=\"ux-tree__node-toggle-icon fa\"\n              [ngClass]=\"node.expanded ? expandedIconClass : collapsedIconClass\">\n        </span>\n        <span *ngIf=\"!node.children\" class=\"ux-tree__node-child-icon fa fa-circle\"></span>\n        <div *ngIf=\"node.typeLabel || node.iconClass\" class=\"ux-tree__node-item-wrapper\">\n            <div *ngIf=\"node.typeLabel\" class=\"ux-tree__node-item-type ux-tree__node-item-type--{{node.typeClass}}\">\n                {{node.typeLabel}}\n            </div>\n            <div *ngIf=\"node.iconClass\" class=\"ux-tree__node-item-icon ux-tree__node-item-type--{{node.iconTypeClass}}\">\n                <span class=\"fa {{node.iconClass}}\"></span>\n            </div>\n            <div class=\"ux-tree__node-item-label-wrapped\">\n                {{node.label}}\n            </div>\n        </div>\n        <span *ngIf=\"!node.typeLabel\" class=\"ux-tree__node-item-label\">\n            {{node.label}}\n        </span>\n        <span *ngIf=\"node.metadata\" class=\"ux-tree__node-metacontent-toggle\">\n            <span (click)=\"onMetadataNodeToggle($event, node)\"\n                  class=\"fa\" [ngClass]=\"node.expanded ? 'fa-angle-down' : 'fa-angle-right'\">\n            </span>\n        </span>\n     </a>\n     <div *ngIf=\"node.expanded\">\n        <ul *ngIf=\"node.children\" class=\"ux-tree__nodes\">\n            <div class=\"ux-tree__node-sub-node\" [class.with-type]=\"subNode.typeLabel\" *ngFor=\"let subNode of node.children\">\n                <ux-tree-node [node]=\"subNode\"\n                              (nodeClick)=\"onSubNodeClick($event)\"\n                              expandedIconClass=\"{{expandedIconClass}}\"\n                              collapsedIconClass=\"{{collapsedIconClass}}\"\n                              [templateVariable]=\"templateVariable\">\n                </ux-tree-node>\n            </div>\n        </ul>\n        <div *ngIf=\"node.metadata\" class=\"ux-tree__node-metacontent\">\n            <template [ngTemplateOutlet]=\"templateVariable\" [ngOutletContext]=\"{metadata: node.metadata}\"></template>\n        </div>\n     </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTreeNodeComponent.ctorParameters = function () { return [
    { type: Router, },
    { type: UxService, },
]; };
UxTreeNodeComponent.propDecorators = {
    'node': [{ type: Input },],
    'isRootNode': [{ type: Input },],
    'expandedIconClass': [{ type: Input },],
    'collapsedIconClass': [{ type: Input },],
    'templateVariable': [{ type: Input },],
    'nodeClick': [{ type: Output },],
};
var UxTreeComponent = (function () {
    function UxTreeComponent() {
        this.isShowToolbar = false;
        this.isExpanded = false;
        this.expandedIconClass = 'ion ion-ios-minus-outline';
        this.collapsedIconClass = 'ion ion-ios-plus';
        this.nodeClick = new EventEmitter();
    }
    /**
     * @return {?}
     */
    UxTreeComponent.prototype.ngAfterContentInit = function () {
        if (this.isExpanded) {
            this.setNodesExpandedState(true);
        }
    };
    /**
     * @param {?} uxLink
     * @return {?}
     */
    UxTreeComponent.prototype.onNodeClick = function (uxLink) {
        this.nodeClick.emit(uxLink);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxTreeComponent.prototype.onExpandAll = function (event) {
        this.setNodesExpandedState(true);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxTreeComponent.prototype.onCollapseAll = function (event) {
        this.setNodesExpandedState(false);
    };
    /**
     * @param {?} filterValue
     * @return {?}
     */
    UxTreeComponent.prototype.onFilter = function (filterValue) {
        var _this = this;
        if (filterValue !== '') {
            this.setNodesVisibleState(false);
            this.nodes.forEach(function (node) {
                node.visible = _this.isFilterMatched(node, filterValue);
                if (node.children) {
                    node.children.forEach(function (node2) {
                        if (_this.isFilterMatched(node2, filterValue)) {
                            node2.visible = true;
                            node.visible = true;
                            node.expanded = true;
                        }
                        if (node2.children) {
                            node2.children.forEach(function (node3) {
                                if (_this.isFilterMatched(node3, filterValue)) {
                                    node3.visible = true;
                                    node2.visible = true;
                                    node2.expanded = true;
                                    node.visible = true;
                                    node.expanded = true;
                                }
                            });
                        }
                    });
                }
            });
        }
        else {
            this.setNodesVisibleState(true);
        }
    };
    /**
     * @param {?} node
     * @param {?} filterValue
     * @return {?}
     */
    UxTreeComponent.prototype.isFilterMatched = function (node, filterValue) {
        if (node.label.toUpperCase().indexOf(filterValue.toUpperCase()) !== -1) {
            return true;
        }
        return false;
    };
    /**
     * @param {?} expanded
     * @return {?}
     */
    UxTreeComponent.prototype.setNodesExpandedState = function (expanded) {
        this.nodes.forEach(function (node) {
            node.expanded = expanded;
            if (node.children) {
                node.children.forEach(function (node2) {
                    node2.expanded = expanded;
                    if (node2.children) {
                        node2.children.forEach(function (node3) {
                            node3.expanded = expanded;
                        });
                    }
                });
            }
        });
    };
    /**
     * @param {?} visible
     * @return {?}
     */
    UxTreeComponent.prototype.setNodesVisibleState = function (visible) {
        this.nodes.forEach(function (node) {
            node.visible = visible;
            if (node.children) {
                node.children.forEach(function (node2) {
                    node2.visible = visible;
                    if (node2.children) {
                        node2.children.forEach(function (node3) {
                            node3.visible = visible;
                        });
                    }
                });
            }
        });
    };
    return UxTreeComponent;
}());
UxTreeComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-tree',
                template: "\n    <ux-a-toolbar-filter [isVisible]=\"isShowToolbar\"\n                         (filter)=\"onFilter($event)\" (expandAll)=\"onExpandAll($event)\"\n                         (collapseAll)=\"onCollapseAll($event)\"\n                         filterLabel=\"{{filterLabel}}\" expandAllLabel=\"{{expandAllLabel}}\"\n                         collapseAllLabel=\"{{collapseAllLabel}}\">\n    </ux-a-toolbar-filter>\n\n     <ul class=\"ux-tree\">\n        <div *ngFor=\"let node of nodes\">\n            <ux-tree-node [node]=\"node\" (nodeClick)=\"onNodeClick($event)\"\n                          [isRootNode]=\"true\"\n                          expandedIconClass=\"{{expandedIconClass}}\"\n                          collapsedIconClass=\"{{collapsedIconClass}}\"\n                          [templateVariable]=\"templateVariable\">\n            </ux-tree-node>\n        </div>\n     </ul>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxTreeComponent.ctorParameters = function () { return []; };
UxTreeComponent.propDecorators = {
    'nodes': [{ type: Input },],
    'isShowToolbar': [{ type: Input },],
    'collapseAllLabel': [{ type: Input },],
    'expandAllLabel': [{ type: Input },],
    'filterLabel': [{ type: Input },],
    'isExpanded': [{ type: Input },],
    'expandedIconClass': [{ type: Input },],
    'collapsedIconClass': [{ type: Input },],
    'nodeClick': [{ type: Output },],
    'templateVariable': [{ type: ContentChild, args: [TemplateRef,] },],
};
var UxTreeComponentModule = (function () {
    function UxTreeComponentModule() {
    }
    return UxTreeComponentModule;
}());
UxTreeComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule, UxToolbarFilterComponentModule],
                exports: [UxTreeComponent, UxTreeNodeComponent],
                declarations: [UxTreeComponent, UxTreeNodeComponent]
            },] },
];
/**
 * @nocollapse
 */
UxTreeComponentModule.ctorParameters = function () { return []; };

var UxTimebarItemUI = (function () {
    function UxTimebarItemUI() {
    }
    return UxTimebarItemUI;
}());
var UxTimebarComponent = (function () {
    /**
     * @param {?} uxService
     * @param {?} renderer2
     */
    function UxTimebarComponent(uxService, renderer2) {
        var _this = this;
        this.uxService = uxService;
        this.renderer2 = renderer2;
        this.dateFormat = 'yyyy-MM-dd';
        this.isShowLegend = false;
        this.isShowLegendAsIndex = true;
        this.isShowCurrentDateMarker = false;
        this.isGroupOverlappingLabels = false;
        this.itemsUI = [];
        this.currentDate = new Date();
        this.timebarColumnClass = 'col-12';
        this.isMobile = false;
        this.isSomeStepsAreGrouped = false;
        this.extraTimelineLabelSpace = 0;
        this.maxStepWidth = 112;
        // subscribing to breakpoint changes to adapt the display dynamically
        this.subscription = this.uxService.activeBreakpoint.subscribe(function (bkp) {
            _this.onBreakpointChange(bkp);
        });
    }
    /**
     * @return {?}
     */
    UxTimebarComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        this.removeNullItems();
        this.sortItems();
        var /** @type {?} */ startDate = this.items[0].date;
        var /** @type {?} */ endDate = this.items[this.items.length - 1].date;
        this.items.forEach(function (item) {
            var /** @type {?} */ stepTypeClass = '';
            var /** @type {?} */ tooltipColor = 'none';
            if (item.stepType) {
                stepTypeClass = 'ux-timebar__step--' + item.stepType;
                tooltipColor = item.stepType;
            }
            _this.itemsUI.push({
                perc: ((item.date.getTime() - startDate.getTime()) / (endDate.getTime() - startDate.getTime())) * 100,
                item: item,
                stepTypeClass: stepTypeClass,
                tooltipColor: tooltipColor,
            });
        });
        this.currentPerc = ((this.currentDate.getTime() - startDate.getTime()) / (endDate.getTime() - startDate.getTime())) * 100;
        // if the current date is over the end date, simulate its positioning to not mess up the timebar global width
        if (this.currentPerc > 100) {
            this.currentPerc = 105;
        }
        if (this.startLabel && this.endLabel) {
            this.timebarColumnClass = 'col-8';
        }
        else {
            if (this.startLabel || this.endLabel) {
                this.timebarColumnClass = 'col-10';
            }
        }
        // initialize attributes
        this.isShowLegendGenerated = this.isShowLegend;
        this.isShowLegendAsIndexGenerated = this.isShowLegendAsIndex;
        this.isGroupOverlappingLabelsGenerated = this.isGroupOverlappingLabels;
        this.isMobile = false;
    };
    /**
     * @return {?}
     */
    UxTimebarComponent.prototype.ngAfterViewInit = function () {
        this.groupOverlappingLabels();
    };
    /**
     * @param {?} bkp
     * @return {?}
     */
    UxTimebarComponent.prototype.onBreakpointChange = function (bkp) {
        if (bkp === 'sm' || bkp === 'xs') {
            this.isShowLegendGenerated = true;
            this.isShowLegendAsIndexGenerated = true;
            this.isMobile = true;
        }
        else {
            this.isShowLegendGenerated = this.isShowLegend;
            this.isShowLegendAsIndexGenerated = this.isShowLegendAsIndex;
            this.isMobile = false;
        }
        this.groupOverlappingLabels();
    };
    /**
     * @return {?}
     */
    UxTimebarComponent.prototype.removeNullItems = function () {
        if (this.items) {
            for (var /** @type {?} */ i = 0; i < this.items.length; i++) {
                if (!this.items[i]) {
                    this.items.splice(i, 1);
                    i--;
                }
            }
        }
    };
    /**
     * @return {?}
     */
    UxTimebarComponent.prototype.sortItems = function () {
        if (this.items) {
            // Sort by ascending date:
            this.items = this.items.sort(function (a, b) {
                if (a && b) {
                    return (a.date) - (b.date);
                }
                else {
                    return 0;
                }
            });
        }
    };
    /**
     * @return {?}
     */
    UxTimebarComponent.prototype.groupOverlappingLabels = function () {
        var _this = this;
        if (this.isGroupOverlappingLabelsGenerated) {
            this.clearGrouping();
            if (this.container && this.itemsUI && this.isShowLegendAsIndexGenerated) {
                var /** @type {?} */ containerElement_1 = this.container.nativeElement;
                setTimeout(function () {
                    var /** @type {?} */ containerWidth = containerElement_1.clientWidth;
                    // const groupingThreshold = this.maxStepWidth / 2;
                    var /** @type {?} */ groupingThreshold = (_this.maxStepWidth * 100) / containerWidth;
                    var /** @type {?} */ groupingCounter = 0;
                    var /** @type {?} */ mobileIndexDisplacement = 0;
                    var /** @type {?} */ previousUiItem = null;
                    for (var /** @type {?} */ i = 0; i < _this.itemsUI.length - 1; i++) {
                        var /** @type {?} */ uiItem = _this.itemsUI[i];
                        if (previousUiItem) {
                            var /** @type {?} */ distance = Math.abs(uiItem.perc - previousUiItem.perc);
                            if (distance <= groupingThreshold) {
                                // Group the 2 items:
                                groupingCounter++;
                                if (_this.isShowLegendGenerated && mobileIndexDisplacement <= 0) {
                                    mobileIndexDisplacement = i - 1;
                                }
                                if (!previousUiItem.groupLabel) {
                                    previousUiItem.groupIndex = groupingCounter;
                                    if (!_this.isShowLegendGenerated) {
                                        previousUiItem.groupLabel = '' + groupingCounter;
                                    }
                                    else {
                                        previousUiItem.groupLabel = '' + (mobileIndexDisplacement + groupingCounter);
                                    }
                                    previousUiItem.groupLabelMobile = '' + (mobileIndexDisplacement + groupingCounter);
                                    groupingCounter++;
                                }
                                if (!_this.isShowLegendGenerated) {
                                    previousUiItem.groupLabel += ', ' + groupingCounter;
                                }
                                else {
                                    previousUiItem.groupLabel += ', ' + (mobileIndexDisplacement + groupingCounter);
                                }
                                previousUiItem.groupLabelMobile += ', ' + (mobileIndexDisplacement + groupingCounter);
                                previousUiItem.groupEndDate = uiItem.item.date;
                                uiItem.isGrouped = true;
                                uiItem.groupIndex = groupingCounter;
                                _this.isSomeStepsAreGrouped = true;
                            }
                            else {
                                previousUiItem = uiItem;
                            }
                        }
                        else {
                            previousUiItem = uiItem;
                        }
                    }
                    _this.calculateExtraTimelineLabelSpace();
                }, 0);
            }
        }
    };
    /**
     * @return {?}
     */
    UxTimebarComponent.prototype.clearGrouping = function () {
        this.isSomeStepsAreGrouped = false;
        if (this.itemsUI) {
            for (var _i = 0, _a = this.itemsUI; _i < _a.length; _i++) {
                var item = _a[_i];
                delete item.isGrouped;
                delete item.groupIndex;
                delete item.groupLabel;
                delete item.groupLabelMobile;
                delete item.groupEndDate;
            }
        }
        this.extraTimelineLabelSpace = 0;
    };
    /**
     * @return {?}
     */
    UxTimebarComponent.prototype.calculateExtraTimelineLabelSpace = function () {
        if (this.itemsUI) {
            var /** @type {?} */ maxLabelLength = 0;
            for (var _i = 0, _a = this.itemsUI; _i < _a.length; _i++) {
                var item = _a[_i];
                if (item.item && item.item.label) {
                    maxLabelLength = Math.max(maxLabelLength, item.item.label.length);
                }
            }
            if (maxLabelLength > 56) {
                this.extraTimelineLabelSpace = maxLabelLength + 28; // 28px = 2rem = 1 line of text in height + margins
            }
        }
    };
    return UxTimebarComponent;
}());
UxTimebarComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-timebar',
                template: "\n        <div class=\"row flex-container\" #container>\n            <div *ngIf=\"startLabel\" class=\"col-2 ux-timebar__start-label\">\n                {{startLabel}}\n            </div>\n            <div class=\"{{timebarColumnClass}}\">\n                <div class=\"ux-timebar\" [style.marginBottom.px]=\"extraTimelineLabelSpace\">\n                    <div *ngIf=\"isShowCurrentDateMarker\" class=\"ux-timebar__current-progress\" [style.width.%]=\"currentPerc\"></div>\n                    <template [ngIf]=\"!isMobile\">\n                        <ng-container *ngFor=\"let item of itemsUI; let i = index;\">\n                            <ng-container *ngIf=\"! item.isGrouped\">\n                                <div class=\"ux-timebar__step\" [ngClass]=\"item.stepTypeClass\" [style.left.%]=\"item.perc\"\n                                    [class.ux-timebar__step--with-current-date-marker]=\"isShowCurrentDateMarker\"\n              uxTooltip=\"{{(item.item.date | date: dateFormat) + (item.groupEndDate ? ' ' + (item.groupEndDate | date: dateFormat) : '')}}\"\n                                    [always]=\"true\" [color]=\"item.tooltipColor\">\n                                    <div *ngIf=\"!isShowLegendGenerated\" [style.left.%]=\"item.perc\" class=\"ux-timebar__step-label\">\n                                        <ng-container *ngIf=\"! item.groupLabel\">\n                                            <span title=\"{{item.item.label}}\">{{item.item.label}}</span>\n                                        </ng-container>\n                                        <ng-container *ngIf=\"item.groupLabel\">\n                                            <span title=\"{{item.groupLabel}}\">{{item.groupLabel}}</span>\n                                        </ng-container>\n                                    </div>\n                                    <div *ngIf=\"isShowLegendGenerated && isShowLegendAsIndexGenerated\"\n                                        [style.left.%]=\"item.perc\" class=\"ux-timebar__step-label\">\n                                        <ng-container *ngIf=\"! item.groupLabelMobile\">\n                                            {{i+1}}\n                                        </ng-container>\n                                        <ng-container *ngIf=\"item.groupLabelMobile\">\n                                            {{item.groupLabelMobile}}\n                                        </ng-container>\n                                    </div>\n                                </div>\n                            </ng-container>\n                        </ng-container>\n                    </template>\n\n                    <template [ngIf]=\"isMobile\">\n                        <ng-container *ngFor=\"let item of itemsUI; let i = index;\">\n                            <ng-container *ngIf=\"! item.isGrouped\">\n                                <div class=\"ux-timebar__step\" [ngClass]=\"item.stepTypeClass\" [style.left.%]=\"item.perc\"\n                                   [class.ux-timebar__step--with-current-date-marker]=\"isShowCurrentDateMarker\">\n                                    <div *ngIf=\"!isShowLegendGenerated\" class=\"ux-timebar__step-label\" title=\"{{item.item.label}}\">\n                                        <ng-container *ngIf=\"! item.groupLabelMobile\">\n                                            <span title=\"{{item.item.label}}\">{{item.item.label}}</span>\n                                        </ng-container>\n                                        <ng-container *ngIf=\"item.groupLabelMobile\">\n                                            {{item.groupLabelMobile}}\n                                        </ng-container>\n                                    </div>\n                                    <div *ngIf=\"isShowLegendGenerated && isShowLegendAsIndexGenerated\" class=\"ux-timebar__step-label\">\n                                        <ng-container *ngIf=\"! item.groupLabelMobile\">\n                                            {{i+1}}\n                                        </ng-container>\n                                        <ng-container *ngIf=\"item.groupLabelMobile\">\n                                            {{item.groupLabelMobile}}\n                                        </ng-container>\n                                    </div>\n                                </div>\n                            </ng-container>\n                        </ng-container>\n                    </template>\n\n                    <div *ngIf=\"isShowCurrentDateMarker\" class=\"ux-timebar__current-date-marker\"\n                         [style.left.%]=\"currentPerc\" uxTooltip=\"{{currentDate | date: dateFormat}}\">\n                        <span class=\"fa fa-map-marker\"></span>\n                    </div>\n                    <div *ngIf=\"isShowCurrentDateMarker\" class=\"ux-timebar__current-date-marker-step\" [style.left.%]=\"currentPerc\">\n                        <span class=\"fa fa-circle\"></span>\n                    </div>\n                </div>\n            </div>\n            <div *ngIf=\"endLabel\" class=\"col-2 ux-timebar__end-label\">\n                {{endLabel}}\n            </div>\n        </div>\n        <div class=\"row flex-container\">\n            <div *ngIf=\"startLabel\" class=\"col-2\"></div>\n            <div class=\"{{timebarColumnClass}}\">\n                <div *ngIf=\"isShowLegendGenerated || isSomeStepsAreGrouped\" class=\"ux-timebar__legend\">\n                    <ng-container *ngIf=\"isShowLegendGenerated\">\n                        <div class=\"ux-timebar__legend-item\" *ngFor=\"let item of itemsUI; let i = index\">\n                            <div *ngIf=\"!isShowLegendAsIndexGenerated\" class=\"ux-timebar__legend-item-icon\">\n                                <span class=\"fa fa-fw fa-circle ux-timebar__legend-item-icon--{{item.item.stepType}}\"></span>\n                            </div>\n                            <div *ngIf=\"isShowLegendAsIndexGenerated\" class=\"ux-timebar__legend-item-index-wrapper\">\n                                {{i+1}}\n                            </div>\n                            <div class=\"ux-timebar__legend-item-label\">\n                                <template [ngIf]=\"isMobile\">\n                                    <strong>{{item.item.date | date: dateFormat}}</strong> - {{item.item.label}}\n                                </template>\n                                <template [ngIf]=\"!isMobile\">\n                                    {{item.item.label}}\n                                </template>\n                            </div>\n                        </div>\n                    </ng-container>\n                    <ng-container *ngIf=\"! isShowLegendGenerated\">\n                        <ng-container *ngFor=\"let item of itemsUI\">\n                            <div *ngIf=\"item.groupLabel || item.isGrouped\" class=\"ux-timebar__legend-item\">\n                                <div class=\"ux-timebar__legend-item-index-wrapper\">\n                                    {{item.groupIndex}}\n                                </div>\n                                <div class=\"ux-timebar__legend-item-label\">\n                                    <strong>{{item.item.date | date: dateFormat}}</strong> - {{item.item.label}}\n                                </div>\n                            </div>\n                        </ng-container>\n                    </ng-container>\n                </div>\n            </div>\n            <div *ngIf=\"endLabel\" class=\"col-2\"></div>\n        </div>\n  ",
            },] },
];
/**
 * @nocollapse
 */
UxTimebarComponent.ctorParameters = function () { return [
    { type: UxService, },
    { type: Renderer2, },
]; };
UxTimebarComponent.propDecorators = {
    'items': [{ type: Input },],
    'startLabel': [{ type: Input },],
    'endLabel': [{ type: Input },],
    'dateFormat': [{ type: Input },],
    'isShowLegend': [{ type: Input },],
    'isShowLegendAsIndex': [{ type: Input },],
    'isShowCurrentDateMarker': [{ type: Input },],
    'isGroupOverlappingLabels': [{ type: Input },],
    'container': [{ type: ViewChild, args: ['container',] },],
};
var UxTimebarComponentModule = (function () {
    function UxTimebarComponentModule() {
    }
    return UxTimebarComponentModule;
}());
UxTimebarComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxTooltipModule],
                exports: [UxTimebarComponent],
                declarations: [UxTimebarComponent]
            },] },
];
/**
 * @nocollapse
 */
UxTimebarComponentModule.ctorParameters = function () { return []; };

var UxTimebarItem = (function () {
    /**
     * @param {?=} values
     */
    function UxTimebarItem(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return UxTimebarItem;
}());

var UxAutocompleteTagItem = (function () {
    /**
     * @param {?=} values
     */
    function UxAutocompleteTagItem(values) {
        if (values === void 0) { values = {}; }
        Object.assign(this, values);
    }
    return UxAutocompleteTagItem;
}());

var UxAutocompleteTagComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxAutocompleteTagComponent(uxService) {
        this.uxService = uxService;
        this.isReadOnly = false;
        this.items = [];
        this.selectedItems = [];
        this.isFreeTagsAllowed = false;
        this.selectionChanged = new EventEmitter();
        this.itemAdded = new EventEmitter();
        this.itemRemoved = new EventEmitter();
        this.query = '';
        this.filteredList = [];
        this.selectedIdx = -1;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.handleBodyClick = function (event) {
        this.reset();
    };
    /**
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.ngAfterContentInit = function () {
        if (this.items.length === 0) {
            this.isFreeTagsAllowed = true;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.onInputKeyup = function (event) {
        var _this = this;
        if (event.code === 'ArrowDown' && this.filteredList.length === 0) {
            this.updateFilteredList();
        }
        else if (event.code === 'ArrowDown' && this.selectedIdx < this.filteredList.length - 1) {
            this.selectedIdx++;
            this.query = this.filteredList[this.selectedIdx].label;
        }
        else if (event.code === 'ArrowUp' && this.selectedIdx > 0) {
            this.selectedIdx--;
            this.query = this.filteredList[this.selectedIdx].label;
        }
        else if (event.keyCode === 13 && this.query !== '') {
            var /** @type {?} */ itemInFilteredList = this.filteredList.find(function (item) { return item.label === _this.query; });
            var /** @type {?} */ itemInSelectedItems = this.selectedItems.find(function (item) { return item.label === _this.query; });
            if (!itemInFilteredList) {
                if (!itemInSelectedItems && this.isFreeTagsAllowed) {
                    var /** @type {?} */ newItem = new UxAutocompleteTagItem({ label: this.query });
                    this.selectedItems.push(newItem);
                    this.query = '';
                    this.itemAdded.emit(newItem);
                    this.selectionChanged.emit(this.selectedItems);
                }
            }
            else {
                this.selectItem(this.filteredList[this.selectedIdx]);
            }
        }
        else {
            if (this.query === '' && event.keyCode !== 9 && event.keyCode !== 16) {
                this.filteredList = [];
            }
            else {
                if (event.code !== 'ArrowUp' && event.code !== 'ArrowDown') {
                    this.updateFilteredList();
                }
            }
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.onInputClick = function (event) {
        this.updateFilteredList();
        this.uxService.consumeEvent(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.onToggle = function (event) {
        this.updateFilteredList();
        this.uxService.consumeEvent(event);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.onInputBlur = function (event) {
        var _this = this;
        setTimeout(function () {
            _this.reset();
        }, 250);
    };
    /**
     * @param {?} item
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.selectItem = function (item) {
        this.selectedItems.push(item);
        this.reset();
        this.itemAdded.emit(item);
        this.selectionChanged.emit(this.selectedItems);
    };
    /**
     * @param {?} item
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.removeItem = function (item) {
        this.selectedItems.splice(this.selectedItems.indexOf(item), 1);
        this.itemRemoved.emit(item);
        this.selectionChanged.emit(this.selectedItems);
    };
    /**
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.reset = function () {
        this.query = '';
        this.filteredList = [];
        this.selectedIdx = -1;
    };
    /**
     * @return {?}
     */
    UxAutocompleteTagComponent.prototype.updateFilteredList = function () {
        var _this = this;
        this.filteredList = this.items.filter(function (item) {
            var /** @type {?} */ foundItem = _this.selectedItems.find(function (selectedItem) {
                return selectedItem.label === item.label;
            });
            if (foundItem) {
                return false;
            }
            return item.label.toLowerCase().indexOf(_this.query.toLowerCase()) > -1;
        });
    };
    return UxAutocompleteTagComponent;
}());
UxAutocompleteTagComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-autocomplete-tag',
                template: "\n        <div class=\"ux-autocomplete-tag\">\n            <div class=\"form-group\">\n                <div class=\"ux-autocomplete-tag__tags\">\n                    <div *ngFor=\"let item of selectedItems\" class=\"ux-autocomplete-tag__tags-item\">\n                        <div class=\"badge badge-primary badge-{{item.typeClass}}\">\n                            <span>{{item.label}}</span>\n                            <span *ngIf=\"!isReadOnly\"\n                                  class=\"ux-autocomplete-tag__tags-item-close fa fa-times\" (click)=\"removeItem(item)\"></span>\n                        </div>\n                    </div>\n                </div>\n                <div *ngIf=\"!isReadOnly\" class=\"ux-autocomplete-tag__input\">\n                    <input class=\"form-control\" type=\"text\"\n                           [(ngModel)]=\"query\"\n                           placeholder=\"{{placeholder}}\"\n                           (keyup)=\"onInputKeyup($event)\"\n                           (click)=\"onInputClick($event)\"\n                           (blur)=\"onInputBlur($event)\"/>\n                    <span class=\"fa fa-angle-down ux-autocomplete-tag__input-icon\" (click)=\"onToggle($event)\"></span>\n                </div>\n            </div>\n            <div class=\"ux-autocomplete-tag__dropdown-list dropdown-menu\"\n                 [style.display]=\"filteredList.length > 0 ? 'block' : 'none'\">\n                <div *ngFor=\"let item of filteredList; let idx = index\"\n                class=\"ux-autocomplete-tag__dropdown-list-item dropdown-item ux-autocomplete-tag__dropdown-list-item--{{item.typeClass}}\"\n                     [class.ux-autocomplete-tag__dropdown-list-item--selected]=\"idx == selectedIdx\"\n                     (click)=\"selectItem(item)\">\n                    {{item.label}}\n                </div>\n            </div>\n        </div>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxAutocompleteTagComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxAutocompleteTagComponent.propDecorators = {
    'isReadOnly': [{ type: Input },],
    'items': [{ type: Input },],
    'selectedItems': [{ type: Input },],
    'placeholder': [{ type: Input },],
    'isFreeTagsAllowed': [{ type: Input },],
    'selectionChanged': [{ type: Output },],
    'itemAdded': [{ type: Output },],
    'itemRemoved': [{ type: Output },],
    'handleBodyClick': [{ type: HostListener, args: ['body:click', ['$event'],] },],
};
var UxAutocompleteTagComponentModule = (function () {
    function UxAutocompleteTagComponentModule() {
    }
    return UxAutocompleteTagComponentModule;
}());
UxAutocompleteTagComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, FormsModule],
                exports: [UxAutocompleteTagComponent],
                declarations: [UxAutocompleteTagComponent]
            },] },
];
/**
 * @nocollapse
 */
UxAutocompleteTagComponentModule.ctorParameters = function () { return []; };

var UxButtonGroupItemComponent = (function () {
    function UxButtonGroupItemComponent() {
        this.isActive = false;
    }
    return UxButtonGroupItemComponent;
}());
UxButtonGroupItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-button-group-item',
                template: "<div></div>"
            },] },
];
/**
 * @nocollapse
 */
UxButtonGroupItemComponent.ctorParameters = function () { return []; };
UxButtonGroupItemComponent.propDecorators = {
    'id': [{ type: Input },],
    'label': [{ type: Input },],
    'iconClass': [{ type: Input },],
    'isActive': [{ type: Input },],
    'typeClass': [{ type: Input },],
};
var UxButtonGroupItemComponentModule = (function () {
    function UxButtonGroupItemComponentModule() {
    }
    return UxButtonGroupItemComponentModule;
}());
UxButtonGroupItemComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxButtonGroupItemComponent],
                declarations: [UxButtonGroupItemComponent]
            },] },
];
/**
 * @nocollapse
 */
UxButtonGroupItemComponentModule.ctorParameters = function () { return []; };

var UxButtonGroupComponent = (function () {
    function UxButtonGroupComponent() {
        this.typeClass = 'primary';
        this.links = [];
        this.isCheckboxButtons = false;
        this.isRadioButtons = false;
        this.hasPairedIcon = false;
        this.clicked = new EventEmitter();
    }
    /**
     * @return {?}
     */
    UxButtonGroupComponent.prototype.ngAfterContentInit = function () {
        var _this = this;
        if (this.items && this.links.length === 0) {
            this.links = [];
            this.items.forEach(function (item) {
                _this.links.push(new UxLink({
                    id: item.id,
                    label: item.label,
                    iconClass: item.iconClass,
                    active: item.isActive
                }));
            });
        }
    };
    /**
     * @param {?} link
     * @param {?} event
     * @return {?}
     */
    UxButtonGroupComponent.prototype.onClick = function (link, event) {
        if (this.isCheckboxButtons) {
            this.links.forEach(function (currentLink) {
                if (JSON.stringify(currentLink) === JSON.stringify(link)) {
                    currentLink.active = !currentLink.active;
                }
            });
        }
        else if (this.isRadioButtons) {
            this.links.forEach(function (currentLink) {
                currentLink.active = false;
                if (JSON.stringify(currentLink) === JSON.stringify(link)) {
                    currentLink.active = !currentLink.active;
                }
            });
        }
        event.preventDefault();
        event.stopPropagation();
        this.clicked.emit(link);
    };
    return UxButtonGroupComponent;
}());
UxButtonGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-button-group',
                template: "\n        <div class=\"btn-group {{styleClass}}\" data-toggle=\"buttons\">\n            <template [ngIf]=\"isCheckboxButtons\">\n                <label *ngFor=\"let link of links\"\n                       (click)=\"onClick(link, $event)\"\n                       class=\"btn btn-{{typeClass}}\" [class.active]=\"link.active\">\n                    <input type=\"checkbox\" autocomplete=\"off\" [attr.checked]=\"link.active\">\n                    {{link.label}}\n                    <span *ngIf=\"link.iconClass\" class=\"fa {{link.iconClass}}\"></span>\n                </label>\n            </template>\n            <template [ngIf]=\"isRadioButtons\">\n                <label *ngFor=\"let link of links\"\n                       (click)=\"onClick(link, $event)\"\n                       class=\"btn btn-{{typeClass}}\" [class.active]=\"link.active\">\n                    <input type=\"radio\" name=\"options\" id=\"option1\" autocomplete=\"off\" [attr.checked]=\"link.active\">\n                    {{link.label}}\n                    <span *ngIf=\"link.iconClass\" class=\"fa {{link.iconClass}}\"></span>\n                </label>\n            </template>\n            <template [ngIf]=\"!isCheckboxButtons && !isRadioButtons\">\n                <template [ngIf]=\"hasPairedIcon\">\n                    <button (click)=\"onClick(links[0], $event)\"\n                            class=\"btn btn-{{typeClass}}\">\n                            {{links[0].label}}\n                    </button>\n                    <button (click)=\"onClick(links[1], $event)\"\n                            class=\"btn btn-{{typeClass}}-dark\">\n                            <span class=\"fa {{links[1].iconClass}}\"></span>\n                    </button>\n                </template>\n\n                <template [ngIf]=\"!hasPairedIcon\">\n                    <button *ngFor=\"let link of links; let i = index\"\n                            (click)=\"onClick(link, $event)\"\n                            class=\"btn btn-{{typeClass}} mr-2\">\n                            {{link.label}}\n                            <span *ngIf=\"link.iconClass\" class=\"fa {{link.iconClass}}\"></span>\n                    </button>\n                </template>\n            </template>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxButtonGroupComponent.ctorParameters = function () { return []; };
UxButtonGroupComponent.propDecorators = {
    'styleClass': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'links': [{ type: Input },],
    'isCheckboxButtons': [{ type: Input },],
    'isRadioButtons': [{ type: Input },],
    'hasPairedIcon': [{ type: Input },],
    'clicked': [{ type: Output },],
    'items': [{ type: ContentChildren, args: [forwardRef(function () { return UxButtonGroupItemComponent; }),] },],
};
var UxButtonGroupComponentModule = (function () {
    function UxButtonGroupComponentModule() {
    }
    return UxButtonGroupComponentModule;
}());
UxButtonGroupComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxButtonGroupComponent],
                declarations: [UxButtonGroupComponent]
            },] },
];
/**
 * @nocollapse
 */
UxButtonGroupComponentModule.ctorParameters = function () { return []; };

var UxSplitButtonComponent = (function () {
    function UxSplitButtonComponent() {
        this.typeClass = 'secondary';
        this.isOutline = false;
        this.links = [];
        this.isDropDownRightAligned = false;
        this.linkSelected = new EventEmitter();
        this.buttonClicked = new EventEmitter();
    }
    /**
     * @return {?}
     */
    UxSplitButtonComponent.prototype.ngAfterContentInit = function () {
        this.btnTypeClass = 'btn-' + this.typeClass;
        if (this.isOutline) {
            this.btnTypeClass = 'btn-outline-' + this.typeClass;
        }
    };
    /**
     * @param {?} link
     * @return {?}
     */
    UxSplitButtonComponent.prototype.onLinkSelected = function (link) {
        this.linkSelected.emit(link);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UxSplitButtonComponent.prototype.onButtonClicked = function (event) {
        this.buttonClicked.emit(event);
    };
    return UxSplitButtonComponent;
}());
UxSplitButtonComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-split-button',
                template: "\n        <div class=\"btn-group\">\n            <button class=\"btn {{btnTypeClass}}\" (click)=\"onButtonClicked($event)\">{{label}}</button>\n            <ux-dropdown-button [links]=\"links\"\n                                typeClass=\"{{typeClass}}\" [isSplitButtonToggle]=\"true\"\n                                [isOutline]=\"isOutline\"\n                                (linkSelected)=\"onLinkSelected($event)\"\n                                [isDropDownRightAligned]=\"isDropDownRightAligned\">\n                <ng-content></ng-content>\n            </ux-dropdown-button>\n        </div>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxSplitButtonComponent.ctorParameters = function () { return []; };
UxSplitButtonComponent.propDecorators = {
    'typeClass': [{ type: Input },],
    'isOutline': [{ type: Input },],
    'label': [{ type: Input },],
    'links': [{ type: Input },],
    'isDropDownRightAligned': [{ type: Input },],
    'linkSelected': [{ type: Output },],
    'buttonClicked': [{ type: Output },],
};
var UxSplitButtonComponentModule = (function () {
    function UxSplitButtonComponentModule() {
    }
    return UxSplitButtonComponentModule;
}());
UxSplitButtonComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxDropdownButtonComponentModule, UxDropdownButtonItemComponentModule],
                exports: [UxSplitButtonComponent],
                declarations: [UxSplitButtonComponent]
            },] },
];
/**
 * @nocollapse
 */
UxSplitButtonComponentModule.ctorParameters = function () { return []; };

var UxMessageBoxComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxMessageBoxComponent(uxService) {
        this.uxService = uxService;
        this.id = 'messagebox_modal';
        this.typeClass = 'warning';
        this.messageBoxType = 'confirmation';
        this.isFooterCustomAlignment = false;
        this.clicked = new EventEmitter();
    }
    /**
     * @return {?}
     */
    UxMessageBoxComponent.prototype.ngAfterContentInit = function () {
        if (!this.acceptLabel) {
            this.acceptLabel = this.uxService.translate('YES');
        }
        if (!this.dismissLabel) {
            this.dismissLabel = this.uxService.translate('NO');
        }
        this.stateClasses = '';
        if (this.typeClass) {
            this.stateClasses += 'ux-message-box--' + this.typeClass + ' ';
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    UxMessageBoxComponent.prototype.onClick = function (value) {
        this.uxService.closeModal(this.id);
        this.clicked.emit(value);
    };
    return UxMessageBoxComponent;
}());
UxMessageBoxComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-message-box',
                template: "\n        <ux-modal id=\"{{id}}\" title=\"{{titleLabel}}\"\n                  styleClass=\"ux-message-box {{stateClasses}}\"\n                  [isSizeSmall]=\"true\" [isFooterCustomAlignment]=\"isFooterCustomAlignment\">\n            <uxModalBody>\n                <ng-content></ng-content>\n            </uxModalBody>\n            <uxModalFooter>\n                <template [ngIf]=\"customFooterContent\">\n                    <ng-content select=\"uxMessageBoxFooter\"></ng-content>\n                </template>\n                <template [ngIf]=\"!customFooterContent\">\n                    <template [ngIf]=\"messageBoxType === 'confirmation'\">\n                        <button class=\"btn btn-secondary\" (click)=\"onClick(false)\">{{dismissLabel}}</button>\n                        <button class=\"btn btn-primary\" (click)=\"onClick(true)\">{{acceptLabel}}</button>\n                    </template>\n                    <template [ngIf]=\"messageBoxType === 'ok'\">\n                        <button class=\"btn btn-secondary\" (click)=\"onClick(true)\">OK</button>\n                    </template>\n                </template>\n            </uxModalFooter>\n        </ux-modal>\n  "
            },] },
];
/**
 * @nocollapse
 */
UxMessageBoxComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxMessageBoxComponent.propDecorators = {
    'id': [{ type: Input },],
    'titleLabel': [{ type: Input },],
    'message': [{ type: Input },],
    'typeClass': [{ type: Input },],
    'messageBoxType': [{ type: Input },],
    'acceptLabel': [{ type: Input },],
    'dismissLabel': [{ type: Input },],
    'isFooterCustomAlignment': [{ type: Input },],
    'clicked': [{ type: Output },],
    'customFooterContent': [{ type: ContentChild, args: [forwardRef(function () { return UxMessageBoxFooterTagDirective; }),] },],
};
var UxMessageBoxFooterTagDirective = (function () {
    function UxMessageBoxFooterTagDirective() {
    }
    return UxMessageBoxFooterTagDirective;
}());
UxMessageBoxFooterTagDirective.decorators = [
    { type: Directive, args: [{ selector: 'uxMessageBoxFooter' },] },
];
/**
 * @nocollapse
 */
UxMessageBoxFooterTagDirective.ctorParameters = function () { return []; };
var UxMessageBoxComponentModule = (function () {
    function UxMessageBoxComponentModule() {
    }
    return UxMessageBoxComponentModule;
}());
UxMessageBoxComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, UxModalComponentModule],
                exports: [UxMessageBoxComponent, UxMessageBoxFooterTagDirective],
                declarations: [UxMessageBoxComponent, UxMessageBoxFooterTagDirective]
            },] },
];
/**
 * @nocollapse
 */
UxMessageBoxComponentModule.ctorParameters = function () { return []; };

// LICENSE MIT : https://github.com/makseo/ng2-sticky
var UxStickyComponent = (function () {
    /**
     * @param {?} element
     */
    function UxStickyComponent(element) {
        this.element = element;
        this.zIndex = 10;
        this.width = 'auto';
        this.offsetTop = 0;
        this.offsetBottom = 0;
        this.start = 0;
        this.stickClass = 'ux-sticky';
        this.endStickClass = 'ux-sticky-end';
        this.mediaQuery = '';
        this.parentMode = true;
        this.activated = new EventEmitter();
        this.deactivated = new EventEmitter();
        this.onScrollBind = this.onScroll.bind(this);
        this.onResizeBind = this.onResize.bind(this);
        this.isStuck = false;
        this.elem = element.nativeElement;
    }
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.ngOnInit = function () {
        window.addEventListener('scroll', this.onScrollBind);
        window.addEventListener('resize', this.onResizeBind);
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.ngAfterViewInit = function () {
        // define scroll container as parent element
        this.container = this.elem.parentNode;
        this.originalCss = {
            zIndex: this.getCssValue(this.elem, 'zIndex'),
            position: this.getCssValue(this.elem, 'position'),
            top: this.getCssValue(this.elem, 'top'),
            right: this.getCssValue(this.elem, 'right'),
            left: this.getCssValue(this.elem, 'left'),
            bottom: this.getCssValue(this.elem, 'bottom'),
            width: this.getCssValue(this.elem, 'width'),
        };
        if (this.width === 'auto') {
            this.width = this.originalCss.width;
        }
        this.defineDimensions();
        this.sticker();
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.ngOnDestroy = function () {
        window.removeEventListener('scroll', this.onScrollBind);
        window.removeEventListener('resize', this.onResizeBind);
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.onScroll = function () {
        this.defineDimensions();
        this.sticker();
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.onResize = function () {
        this.defineDimensions();
        this.sticker();
        if (this.isStuck) {
            this.unstuckElement();
            this.stuckElement();
        }
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.defineDimensions = function () {
        var /** @type {?} */ containerTop = this.getBoundingClientRectValue(this.container, 'top');
        this.windowHeight = window.innerHeight;
        this.elemHeight = this.getCssNumber(this.elem, 'height');
        this.containerHeight = this.getCssNumber(this.container, 'height');
        this.containerStart = containerTop + this.scrollbarYPos() - this.offsetTop + this.start;
        if (this.parentMode) {
            this.scrollFinish = this.containerStart - this.start - this.offsetBottom + (this.containerHeight - this.elemHeight);
        }
        else {
            this.scrollFinish = document.body.offsetHeight;
        }
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.resetElement = function () {
        this.elem.classList.remove(this.stickClass);
        Object.assign(this.elem.style, this.originalCss);
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.stuckElement = function () {
        this.isStuck = true;
        this.elem.classList.remove(this.endStickClass);
        this.elem.classList.add(this.stickClass);
        var /** @type {?} */ elementLeft = this.getBoundingClientRectValue(this.elem, 'left');
        this.elem.style.zIndex = this.zIndex;
        this.elem.style.position = 'fixed';
        this.elem.style.top = this.offsetTop + 'px';
        this.elem.style.right = 'auto';
        this.elem.style.left = elementLeft + 'px';
        this.elem.style.bottom = 'auto';
        this.elem.style.width = this.width;
        this.activated.next(this.elem);
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.unstuckElement = function () {
        this.isStuck = false;
        this.elem.classList.add(this.endStickClass);
        this.container.style.position = 'relative';
        this.elem.style.position = 'absolute';
        this.elem.style.top = 'auto';
        this.elem.style.right = 0;
        this.elem.style.left = 'auto';
        this.elem.style.bottom = this.offsetBottom + 'px';
        this.elem.style.width = this.width;
        this.deactivated.next(this.elem);
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.matchMediaQuery = function () {
        if (!this.mediaQuery) {
            return true;
        }
        return (window.matchMedia('(' + this.mediaQuery + ')').matches ||
            window.matchMedia(this.mediaQuery).matches);
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.sticker = function () {
        // check media query
        if (this.isStuck && !this.matchMediaQuery()) {
            this.resetElement();
            return;
        }
        // detecting when a container's height changes
        var /** @type {?} */ currentContainerHeight = this.getCssNumber(this.container, 'height');
        if (currentContainerHeight !== this.containerHeight) {
            this.defineDimensions();
        }
        var /** @type {?} */ position = this.scrollbarYPos();
        // unstick
        if (this.isStuck && (position < this.containerStart || position > this.scrollFinish) || position > this.scrollFinish) {
            this.resetElement();
            if (position > this.scrollFinish) {
                this.unstuckElement();
            }
            this.isStuck = false;
            // stick
        }
        else if (this.isStuck === false && position > this.containerStart && position < this.scrollFinish) {
            this.stuckElement();
        }
    };
    /**
     * @return {?}
     */
    UxStickyComponent.prototype.scrollbarYPos = function () {
        return window.pageYOffset || document.documentElement.scrollTop;
    };
    /**
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    UxStickyComponent.prototype.getBoundingClientRectValue = function (element, property) {
        var /** @type {?} */ result = 0;
        if (element.getBoundingClientRect) {
            var /** @type {?} */ rect = element.getBoundingClientRect();
            result = (typeof rect[property] !== 'undefined') ? rect[property] : 0;
        }
        return result;
    };
    /**
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    UxStickyComponent.prototype.getCssValue = function (element, property) {
        var /** @type {?} */ result = '';
        if (typeof window.getComputedStyle !== 'undefined') {
            result = window.getComputedStyle(element, null).getPropertyValue(property);
        }
        else if (typeof element.currentStyle !== 'undefined') {
            result = element.currentStyle[property];
        }
        return result;
    };
    /**
     * @param {?} element
     * @param {?} property
     * @return {?}
     */
    UxStickyComponent.prototype.getCssNumber = function (element, property) {
        return parseInt(this.getCssValue(element, property), 10) || 0;
    };
    return UxStickyComponent;
}());
UxStickyComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-sticky',
                template: '<ng-content></ng-content>'
            },] },
];
/**
 * @nocollapse
 */
UxStickyComponent.ctorParameters = function () { return [
    { type: ElementRef, },
]; };
UxStickyComponent.propDecorators = {
    'zIndex': [{ type: Input },],
    'width': [{ type: Input },],
    'offsetTop': [{ type: Input },],
    'offsetBottom': [{ type: Input },],
    'start': [{ type: Input },],
    'stickClass': [{ type: Input },],
    'endStickClass': [{ type: Input },],
    'mediaQuery': [{ type: Input },],
    'parentMode': [{ type: Input },],
    'activated': [{ type: Output },],
    'deactivated': [{ type: Output },],
};
var UxStickyComponentModule = (function () {
    function UxStickyComponentModule() {
    }
    return UxStickyComponentModule;
}());
UxStickyComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule],
                exports: [UxStickyComponent],
                declarations: [UxStickyComponent]
            },] },
];
/**
 * @nocollapse
 */
UxStickyComponentModule.ctorParameters = function () { return []; };

var DEFAULT_OPTIONS = {
    animation: {
        animateRotate: false,
    },
    legend: {
        display: false,
    },
    tooltips: { enabled: false },
};
var innerDougnutData = function (data) {
    if (data.length === 0) {
        return;
    }
    var /** @type {?} */ totalAmount = data[0].amount;
    if (totalAmount === 0) {
        return;
    }
    var /** @type {?} */ innerData = data.reduce(function (acc, item, index) {
        if (index > 0) {
            acc.data.push(Math.max(0, Math.round(item.amount / totalAmount * 100)));
            acc.backgroundColor.push(item.color);
        }
        return acc;
    }, { data: [], backgroundColor: [] });
    // Add white "space" left
    var /** @type {?} */ pctLeft = 100 - innerData.data.reduce(function (acc, pct) { return acc + pct; }, 0);
    innerData.data.push(pctLeft);
    innerData.backgroundColor.push('white');
    return { datasets: [innerData], labels: [] };
};
var pieData = function (data) {
    if (data.length === 0) {
        return;
    }
    var /** @type {?} */ pieDataset = data.reduce(function (acc, item, index) {
        acc.data.push(item.amount);
        acc.backgroundColor.push(item.color);
        return acc;
    }, { data: [], backgroundColor: [] });
    return { datasets: [pieDataset], labels: [] };
};
var UxPieChartComponent$$1 = (function () {
    function UxPieChartComponent$$1() {
        this.data = [];
        this.chartType = 'doughnut';
        this.isShowLabel = true;
    }
    /**
     * @return {?}
     */
    UxPieChartComponent$$1.prototype.ngOnInit = function () {
        this.pieOptions = this.options ? Object.assign({}, DEFAULT_OPTIONS, this.options) : Object.assign({}, DEFAULT_OPTIONS);
        this.innerData = innerDougnutData(this.data.values);
        this.pieData = pieData(this.data.values);
        if (this.chartType === 'doughnut') {
            this.pieOptions.cutoutPercentage = 75;
        }
    };
    return UxPieChartComponent$$1;
}());
UxPieChartComponent$$1.decorators = [
    { type: Component, args: [{
                selector: 'ux-pie-chart',
                template: "\n    <div class=\"chart-title\">\n        {{chartTitle}}\n    </div>\n\n    <div class=\"chart-container\">\n\n        <ng-container *ngIf=\"chartType === 'doughnut'\">\n            <!--Outer doughnut-->\n            <div>\n                <p-chart type=\"doughnut\"\n                            [data]=\"{datasets: [{data: [100], backgroundColor: [data.values[0].color || 'blue']}]}\"\n                            [options]=\"pieOptions\"></p-chart>\n            </div>\n\n            <!--Inner doughnut-->\n            <div class=\"inner-chart\" *ngIf=\"innerData\">\n                <p-chart type=\"doughnut\" [data]=\"innerData\" [options]=\"pieOptions\"></p-chart>\n            </div>\n        </ng-container>\n\n        <ng-container *ngIf=\"chartType === 'pie'\">\n            <div><p-chart type=\"pie\" [data]=\"pieData\" [options]=\"pieOptions\"></p-chart></div>\n        </ng-container>\n\n        <!--Label center-->\n        <div class=\"chart-inner-labels small\" *ngIf=\"isShowLabel\">\n            <span class=\"amount\">{{data.values[0].amount | uxCurrency: 0}}</span>\n        </div>\n    </div>\n\n<!--Legend-->\n<ul class=\"chart-legend\">\n    <li class=\"chart-legend-item\" *ngFor=\"let item of data.values\">\n        <span class=\"chart-legend-item-marker fa fa-fw fa-circle\" [style.color]=\"item.color\"></span>\n        <span class=\"chart-legend-item-label\">{{item.label}}</span>\n        <span class=\"chart-legend-item-counter\" *ngIf=\"chartType === 'doughnut'\">{{item.amount | uxCurrency:0}}</span>\n        <span class=\"chart-legend-item-counter\" *ngIf=\"chartType === 'pie'\">{{item.amount}}</span>\n    </li>\n</ul>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxPieChartComponent$$1.ctorParameters = function () { return []; };
UxPieChartComponent$$1.propDecorators = {
    'data': [{ type: Input },],
    'options': [{ type: Input },],
    'chartTitle': [{ type: Input },],
    'chartType': [{ type: Input },],
    'isShowLabel': [{ type: Input },],
};
var UxPieChartComponentModule$$1 = (function () {
    function UxPieChartComponentModule$$1() {
    }
    return UxPieChartComponentModule$$1;
}());
UxPieChartComponentModule$$1.decorators = [
    { type: NgModule, args: [{
                imports: [CommonModule, ChartModule, UxCurrencyPipeModule],
                exports: [UxPieChartComponent$$1],
                declarations: [UxPieChartComponent$$1]
            },] },
];
/**
 * @nocollapse
 */
UxPieChartComponentModule$$1.ctorParameters = function () { return []; };

var UxPieChartGroupComponent = (function () {
    /**
     * @param {?} uxService
     */
    function UxPieChartGroupComponent(uxService) {
        var _this = this;
        this.uxService = uxService;
        this.itemsArray = [];
        this.isMobile = false;
        this.uxService.activeBreakpoint.subscribe(function (bkp) {
            if (bkp === 'xs' || bkp === 'sm') {
                _this.isMobile = true;
            }
            else {
                _this.isMobile = false;
            }
        });
    }
    /**
     * @return {?}
     */
    UxPieChartGroupComponent.prototype.ngAfterContentInit = function () {
        this.itemsArray = this.items.toArray();
    };
    return UxPieChartGroupComponent;
}());
UxPieChartGroupComponent.decorators = [
    { type: Component, args: [{
                selector: 'ux-pie-chart-group',
                template: "\n        <template [ngIf]=\"!isMobile\">\n            <div class=\"row\">\n                <div *ngFor=\"let item of itemsArray\" class=\"col-{{12 / itemsArray.length}}\">\n                   <ux-pie-chart chartTitle=\"{{item.chartTitle}}\"\n                                 [data]=\"item.data\"\n                                 [isShowLabel]=\"item.isShowLabel\"\n                                 chartType=\"{{item.chartType}}\">\n                   </ux-pie-chart>\n                </div>\n            </div>\n        </template>\n\n        <ng-container *ngIf=\"isMobile\">\n            <ux-carousel-items>\n                <ux-carousel-item *ngFor=\"let item of itemsArray\">\n                    <ux-pie-chart chartTitle=\"{{item.chartTitle}}\"\n                                    [data]=\"item.data\"\n                                    [isShowLabel]=\"item.isShowLabel\"\n                                    chartType=\"{{item.chartType}}\">\n                    </ux-pie-chart>\n                </ux-carousel-item>\n            </ux-carousel-items>\n        </ng-container>\n    "
            },] },
];
/**
 * @nocollapse
 */
UxPieChartGroupComponent.ctorParameters = function () { return [
    { type: UxService, },
]; };
UxPieChartGroupComponent.propDecorators = {
    'items': [{ type: ContentChildren, args: [forwardRef(function () { return UxPieChartComponent$$1; }),] },],
};
var UxPieChartGroupComponentModule = (function () {
    function UxPieChartGroupComponentModule() {
    }
    return UxPieChartGroupComponentModule;
}());
UxPieChartGroupComponentModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    UxPieChartComponentModule$$1,
                    UxCarouselItemComponentModule, UxCarouselItemsComponentModule
                ],
                exports: [UxPieChartGroupComponent],
                declarations: [UxPieChartGroupComponent]
            },] },
];
/**
 * @nocollapse
 */
UxPieChartGroupComponentModule.ctorParameters = function () { return []; };

var UxMessage = (function () {
    function UxMessage() {
    }
    return UxMessage;
}());

var UxModule$$1 = (function () {
    function UxModule$$1() {
    }
    return UxModule$$1;
}());
UxModule$$1.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule, RouterModule, ReactiveFormsModule, FormsModule,
                    UxLayoutAppShellComponentModule,
                    UxLayoutAppComponentModule,
                    UxLayoutAppMainComponentModule,
                    UxLayoutHeaderComponentModule,
                    UxLayoutHeaderProfileComponentModule,
                    UxLayoutNavBarComponentModule,
                    UxLayoutNavBarTopMenuComponentModule,
                    UxLayoutNavBarActionsComponentModule,
                    UxLayoutNavBarActionItemComponentModule,
                    UxLayoutNavBarLeftActionsComponentModule,
                    UxLayoutNavBarLeftActionItemComponentModule,
                    UxLayoutNavBarLeftActionItemSidebarToggleComponentModule,
                    UxLayoutNavBarElementsComponentModule,
                    UxLayoutNavBarElementItemComponentModule,
                    UxLayoutOverlayPanelComponentModule,
                    UxLayoutBreadcrumbsComponentModule,
                    UxLayoutTopMessageComponentModule,
                    UxLayoutSidebarComponentModule,
                    UxLayoutSidebarItemComponentModule,
                    UxLayoutSidebarItemsComponentModule,
                    UxLayoutFooterComponentModule,
                    UxLayoutBannerComponentModule,
                    UxLayoutPageHeaderComponentModule,
                    UxGrowlComponentModule,
                    UxBlockDocumentComponentModule,
                    UxPanelComponentModule,
                    UxPanelsComponentModule,
                    UxLanguageSelectorComponentModule,
                    UxSearchInputComponentModule
                ],
                declarations: [],
                exports: [
                    CommonModule, RouterModule, ReactiveFormsModule, FormsModule,
                    UxLayoutAppShellComponentModule,
                    UxLayoutAppComponentModule,
                    UxLayoutAppMainComponentModule,
                    UxLayoutHeaderComponentModule,
                    UxLayoutHeaderProfileComponentModule,
                    UxLayoutNavBarComponentModule,
                    UxLayoutNavBarTopMenuComponentModule,
                    UxLayoutNavBarActionsComponentModule,
                    UxLayoutNavBarActionItemComponentModule,
                    UxLayoutNavBarLeftActionsComponentModule,
                    UxLayoutNavBarLeftActionItemComponentModule,
                    UxLayoutNavBarLeftActionItemSidebarToggleComponentModule,
                    UxLayoutNavBarElementsComponentModule,
                    UxLayoutNavBarElementItemComponentModule,
                    UxLayoutOverlayPanelComponentModule,
                    UxLayoutBreadcrumbsComponentModule,
                    UxLayoutTopMessageComponentModule,
                    UxLayoutSidebarComponentModule,
                    UxLayoutSidebarItemComponentModule,
                    UxLayoutSidebarItemsComponentModule,
                    UxLayoutFooterComponentModule,
                    UxLayoutBannerComponentModule,
                    UxLayoutPageHeaderComponentModule,
                    UxGrowlComponentModule,
                    UxBlockDocumentComponentModule,
                    UxPanelComponentModule,
                    UxPanelsComponentModule,
                    UxLanguageSelectorComponentModule,
                    UxSearchInputComponentModule
                ]
            },] },
];
/**
 * @nocollapse
 */
UxModule$$1.ctorParameters = function () { return []; };

var UxAllModule$$1 = (function () {
    function UxAllModule$$1() {
    }
    return UxAllModule$$1;
}());
UxAllModule$$1.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule, RouterModule, ReactiveFormsModule, FormsModule,
                    UxLayoutAppShellComponentModule,
                    UxLayoutAppComponentModule,
                    UxLayoutAppMainComponentModule,
                    UxLayoutHeaderComponentModule,
                    UxLayoutHeaderProfileComponentModule,
                    UxLayoutNavBarComponentModule,
                    UxLayoutNavBarTopMenuComponentModule,
                    UxLayoutNavBarActionsComponentModule,
                    UxLayoutNavBarActionItemComponentModule,
                    UxLayoutNavBarLeftActionsComponentModule,
                    UxLayoutNavBarLeftActionItemComponentModule,
                    UxLayoutNavBarLeftActionItemSidebarToggleComponentModule,
                    UxLayoutNavBarElementsComponentModule,
                    UxLayoutNavBarElementItemComponentModule,
                    UxLayoutOverlayPanelComponentModule,
                    UxLayoutBreadcrumbsComponentModule,
                    UxLayoutTopMessageComponentModule,
                    UxLayoutSidebarComponentModule,
                    UxLayoutSidebarItemComponentModule,
                    UxLayoutSidebarItemsComponentModule,
                    UxLayoutFooterComponentModule,
                    UxLayoutBannerComponentModule,
                    UxLayoutHorizontalModule,
                    UxLayoutPageHeaderComponentModule,
                    UxOrderByPipeModule,
                    UxFilterPipeModule,
                    UxTruncatePipeModule,
                    UxCurrencyPipeModule,
                    UxTrackScrollDirectiveModule,
                    UxMaxlengthModule,
                    UxTooltipModule,
                    UxAutoResizeModule,
                    UxCollapsibleDirectiveModule,
                    UxAlertComponentModule,
                    UxDashboardCardComponentModule,
                    UxDashboardButtonsComponentModule,
                    UxDashboardButtonComponentModule,
                    UxAccordionPanelComponentModule,
                    UxAccordionPanelsComponentModule,
                    UxActionBoxComponentModule,
                    UxListItemComponentModule,
                    UxListItemsComponentModule,
                    UxPanelComponentModule,
                    UxPanelsComponentModule,
                    UxTabComponentModule,
                    UxTabsComponentModule,
                    UxTimelineItemsComponentModule,
                    UxTimelineItemComponentModule,
                    UxWizardStepComponentModule,
                    UxWizardStepsComponentModule,
                    UxTreeListComponentModule,
                    UxTreeListItemComponentModule,
                    UxSwitchComponentModule,
                    UxButtonComponentModule,
                    UxControlFeedbackComponentModule,
                    UxDropdownButtonComponentModule,
                    UxDropdownButtonItemComponentModule,
                    UxFieldComponentModule,
                    UxFieldCalendarComponentModule,
                    UxFieldsetComponentModule,
                    UxModalComponentModule,
                    UxLanguageSelectorComponentModule,
                    UxSearchInputComponentModule,
                    UxBlockContentComponentModule,
                    UxTagComponentModule,
                    UxIconComponentModule,
                    UxIconToggleComponentModule,
                    UxIconBoxComponentModule,
                    UxMarkerComponentModule,
                    UxLabelComponentModule,
                    UxDateTagComponentModule,
                    UxProgressCircleComponentModule,
                    UxCarouselItemsComponentModule,
                    UxCarouselItemComponentModule,
                    UxGrowlComponentModule,
                    UxBlockDocumentComponentModule,
                    UxTreeComponentModule,
                    UxTimebarComponentModule,
                    UxBadgeComponentModule,
                    UxAutocompleteTagComponentModule,
                    UxToolbarFilterComponentModule,
                    UxButtonGroupComponentModule,
                    UxButtonGroupItemComponentModule,
                    UxSplitButtonComponentModule,
                    UxMessageBoxComponentModule,
                    UxBadgeGroupComponentModule,
                    UxNotificationsPanelComponentModule,
                    UxNotificationItemComponentModule,
                    UxStickyComponentModule,
                    UxPieChartComponentModule$$1,
                    UxPieChartGroupComponentModule
                ],
                declarations: [],
                exports: [
                    CommonModule, RouterModule, ReactiveFormsModule, FormsModule,
                    UxLayoutAppShellComponentModule,
                    UxLayoutAppComponentModule,
                    UxLayoutAppMainComponentModule,
                    UxLayoutHeaderComponentModule,
                    UxLayoutHeaderProfileComponentModule,
                    UxLayoutNavBarComponentModule,
                    UxLayoutNavBarTopMenuComponentModule,
                    UxLayoutNavBarActionsComponentModule,
                    UxLayoutNavBarActionItemComponentModule,
                    UxLayoutNavBarLeftActionsComponentModule,
                    UxLayoutNavBarLeftActionItemComponentModule,
                    UxLayoutNavBarLeftActionItemSidebarToggleComponentModule,
                    UxLayoutNavBarElementsComponentModule,
                    UxLayoutNavBarElementItemComponentModule,
                    UxLayoutOverlayPanelComponentModule,
                    UxLayoutBreadcrumbsComponentModule,
                    UxLayoutTopMessageComponentModule,
                    UxLayoutSidebarComponentModule,
                    UxLayoutSidebarItemComponentModule,
                    UxLayoutSidebarItemsComponentModule,
                    UxLayoutFooterComponentModule,
                    UxLayoutBannerComponentModule,
                    UxLayoutHorizontalModule,
                    UxLayoutPageHeaderComponentModule,
                    UxOrderByPipeModule,
                    UxFilterPipeModule,
                    UxTruncatePipeModule,
                    UxCurrencyPipeModule,
                    UxTrackScrollDirectiveModule,
                    UxMaxlengthModule,
                    UxTooltipModule,
                    UxAutoResizeModule,
                    UxCollapsibleDirectiveModule,
                    UxAlertComponentModule,
                    UxDashboardCardComponentModule,
                    UxDashboardButtonsComponentModule,
                    UxDashboardButtonComponentModule,
                    UxAccordionPanelComponentModule,
                    UxAccordionPanelsComponentModule,
                    UxActionBoxComponentModule,
                    UxListItemComponentModule,
                    UxListItemsComponentModule,
                    UxPanelComponentModule,
                    UxPanelsComponentModule,
                    UxTabComponentModule,
                    UxTabsComponentModule,
                    UxTimelineItemsComponentModule,
                    UxTimelineItemComponentModule,
                    UxWizardStepComponentModule,
                    UxWizardStepsComponentModule,
                    UxTreeListComponentModule,
                    UxTreeListItemComponentModule,
                    UxSwitchComponentModule,
                    UxButtonComponentModule,
                    UxControlFeedbackComponentModule,
                    UxDropdownButtonComponentModule,
                    UxDropdownButtonItemComponentModule,
                    UxFieldComponentModule,
                    UxFieldCalendarComponentModule,
                    UxFieldsetComponentModule,
                    UxModalComponentModule,
                    UxLanguageSelectorComponentModule,
                    UxSearchInputComponentModule,
                    UxBlockContentComponentModule,
                    UxTagComponentModule,
                    UxIconComponentModule,
                    UxIconToggleComponentModule,
                    UxIconBoxComponentModule,
                    UxMarkerComponentModule,
                    UxLabelComponentModule,
                    UxDateTagComponentModule,
                    UxProgressCircleComponentModule,
                    UxCarouselItemsComponentModule,
                    UxCarouselItemComponentModule,
                    UxGrowlComponentModule,
                    UxBlockDocumentComponentModule,
                    UxTreeComponentModule,
                    UxTimebarComponentModule,
                    UxBadgeComponentModule,
                    UxAutocompleteTagComponentModule,
                    UxToolbarFilterComponentModule,
                    UxButtonGroupComponentModule,
                    UxButtonGroupItemComponentModule,
                    UxSplitButtonComponentModule,
                    UxMessageBoxComponentModule,
                    UxBadgeGroupComponentModule,
                    UxNotificationsPanelComponentModule,
                    UxNotificationItemComponentModule,
                    UxStickyComponentModule,
                    UxPieChartComponentModule$$1,
                    UxPieChartGroupComponentModule
                ]
            },] },
];
/**
 * @nocollapse
 */
UxAllModule$$1.ctorParameters = function () { return []; };

var HttpCall = (function () {
    function HttpCall() {
    }
    return HttpCall;
}());

var __extends$1 = (undefined && undefined.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var UxHttp$$1 = (function (_super) {
    __extends$1(UxHttp$$1, _super);
    /**
     * @param {?} backend
     * @param {?} defaultOptions
     * @param {?=} requestInterceptors
     * @param {?=} responseInterceptors
     */
    function UxHttp$$1(backend, defaultOptions, requestInterceptors, responseInterceptors) {
        var _this = _super.call(this, backend, defaultOptions) || this;
        if (requestInterceptors == null) {
            _this.requestInterceptors = [new CachePreventionHttpInterceptor(), new CsrfPreventionHttpInterceptor()];
        }
        else {
            _this.requestInterceptors = requestInterceptors;
        }
        if (responseInterceptors == null) {
            _this.responseInterceptors = [new EcasSessionTimeoutHttpInterceptor()];
        }
        else {
            _this.responseInterceptors = responseInterceptors;
        }
        return _this;
    }
    /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    UxHttp$$1.prototype.get = function (url, options) {
        return this.interceptCall('get', url, options);
    };
    /**
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UxHttp$$1.prototype.post = function (url, body, options) {
        return this.interceptCall('post', url, options, body);
    };
    /**
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UxHttp$$1.prototype.put = function (url, body, options) {
        return this.interceptCall('put', url, options, body);
    };
    /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    UxHttp$$1.prototype.delete = function (url, options) {
        return this.interceptCall('delete', url, options);
    };
    /**
     * @param {?} url
     * @param {?} body
     * @param {?=} options
     * @return {?}
     */
    UxHttp$$1.prototype.patch = function (url, body, options) {
        return this.interceptCall('patch', url, options, body);
    };
    /**
     * @param {?} url
     * @param {?=} options
     * @return {?}
     */
    UxHttp$$1.prototype.head = function (url, options) {
        return this.interceptCall('head', url, options);
    };
    /**
     * @param {?} interceptor
     * @return {?}
     */
    UxHttp$$1.prototype.addRequestInterceptor = function (interceptor) {
        this.requestInterceptors.push(interceptor);
    };
    /**
     * @param {?} interceptor
     * @return {?}
     */
    UxHttp$$1.prototype.addResponseInterceptor = function (interceptor) {
        this.responseInterceptors.push(interceptor);
    };
    /**
     * @param {?} methodName
     * @param {?} url
     * @param {?=} options
     * @param {?=} body
     * @return {?}
     */
    UxHttp$$1.prototype.interceptCall = function (methodName, url, options, body) {
        var /** @type {?} */ httpCall = new HttpCall();
        httpCall.method = methodName;
        httpCall.url = url;
        if (body != null) {
            httpCall.body = body;
        }
        if (options != null) {
            httpCall.options = options;
        }
        httpCall = this.interceptRequest(httpCall);
        if (!httpCall.isCancelled) {
            if (methodName === 'get') {
                httpCall.response = _super.prototype.get.call(this, httpCall.url, httpCall.options);
            }
            else if (methodName === 'delete') {
                httpCall.response = _super.prototype.delete.call(this, httpCall.url, httpCall.options);
            }
            else if (methodName === 'head') {
                httpCall.response = _super.prototype.head.call(this, httpCall.url, httpCall.options);
            }
            else if (methodName === 'post') {
                httpCall.response = _super.prototype.post.call(this, httpCall.url, httpCall.body, httpCall.options);
            }
            else if (methodName === 'put') {
                httpCall.response = _super.prototype.put.call(this, httpCall.url, httpCall.body, httpCall.options);
            }
            else if (methodName === 'patch') {
                httpCall.response = _super.prototype.patch.call(this, httpCall.url, httpCall.body, httpCall.options);
            }
            return this.interceptResponse(httpCall);
        }
        else {
            return httpCall.response;
        }
    };
    /**
     * @param {?} httpCall
     * @return {?}
     */
    UxHttp$$1.prototype.interceptRequest = function (httpCall) {
        for (var _i = 0, _a = this.requestInterceptors; _i < _a.length; _i++) {
            var interceptor = _a[_i];
            httpCall = interceptor.interceptRequest(httpCall);
            if (httpCall.isCancelled) {
                break;
            }
        }
        return httpCall;
    };
    /**
     * @param {?} httpCall
     * @return {?}
     */
    UxHttp$$1.prototype.interceptResponse = function (httpCall) {
        for (var _i = 0, _a = this.responseInterceptors; _i < _a.length; _i++) {
            var interceptor = _a[_i];
            httpCall = interceptor.interceptResponse(httpCall);
            if (httpCall.isCancelled) {
                break;
            }
        }
        return httpCall.response;
    };
    return UxHttp$$1;
}(Http));
UxHttp$$1.decorators = [
    { type: Injectable },
];
/**
 * @nocollapse
 */
UxHttp$$1.ctorParameters = function () { return [
    { type: ConnectionBackend, },
    { type: RequestOptions, },
    { type: Array, },
    { type: Array, },
]; };

var CachePreventionHttpInterceptor = (function () {
    function CachePreventionHttpInterceptor() {
    }
    /**
     * @param {?} httpCall
     * @return {?}
     */
    CachePreventionHttpInterceptor.prototype.interceptRequest = function (httpCall) {
        var /** @type {?} */ options = httpCall.options;
        if (options == null) {
            options = {};
        }
        options.headers = options.headers || new Headers();
        options.headers.append('Cache-Control', 'No-Cache');
        httpCall.options = options;
        return httpCall;
    };
    return CachePreventionHttpInterceptor;
}());

var CsrfPreventionHttpInterceptor = (function () {
    function CsrfPreventionHttpInterceptor() {
    }
    /**
     * @param {?} httpCall
     * @return {?}
     */
    CsrfPreventionHttpInterceptor.prototype.interceptRequest = function (httpCall) {
        var /** @type {?} */ options = httpCall.options;
        if (options == null) {
            options = {};
        }
        options.headers = options.headers || new Headers();
        options.headers.append('X-Requested-With', 'XMLHttpRequest');
        httpCall.options = options;
        return httpCall;
    };
    return CsrfPreventionHttpInterceptor;
}());

var EcasSessionTimeoutHttpInterceptor = (function () {
    function EcasSessionTimeoutHttpInterceptor() {
        this.domainPattern = /https?:\/\/((?:[\w\d-\.])+[:\w\d\.]{2,})/i;
        this.ecasDomainPattern = /https?:\/\/ecas\..+\.eu.*/i;
    }
    /**
     * @param {?} httpCall
     * @return {?}
     */
    EcasSessionTimeoutHttpInterceptor.prototype.interceptResponse = function (httpCall) {
        httpCall.response = this.checkForECASSessionTimeout(httpCall);
        return httpCall;
    };
    /**
     * @param {?} httpCall
     * @return {?}
     */
    EcasSessionTimeoutHttpInterceptor.prototype.checkForECASSessionTimeout = function (httpCall) {
        var _this = this;
        return httpCall.response.map(function (response) {
            return _this.checkRequestSuccessForECASSessionTimeout(response, httpCall);
        }).catch(function (error, source) {
            _this.checkRequestErrorForECASSessionTimeout(error, httpCall);
            throw error;
        });
    };
    /**
     * @param {?} response
     * @param {?} httpCall
     * @return {?}
     */
    EcasSessionTimeoutHttpInterceptor.prototype.checkRequestSuccessForECASSessionTimeout = function (response, httpCall) {
        if (response != null) {
            var /** @type {?} */ text = response.text();
            if (text != null) {
                if (this.isSsoResponse(text)) {
                    this.reauthenticate(httpCall);
                }
            }
        }
        return response;
    };
    /**
     * @param {?} response
     * @param {?} httpCall
     * @return {?}
     */
    EcasSessionTimeoutHttpInterceptor.prototype.checkRequestErrorForECASSessionTimeout = function (response, httpCall) {
        // For ECAS redirection, there is no way to intercept a redirect. The XMLHttpRequest itself internally follows same-domain µ
        // redirects. The redirect request to ECAS appears to be cancelled and no NETWORK_ERR is thrown.
        // Unexpected server errors produce a status 500. In case of a failed redirect, the status is 0 (or 12017 for IE)
        if (response == null ||
            (response.status === 0 && response.statusText !== 'abort' &&
                !this.isCorsRequest(httpCall)) || response.status === 12017) {
            this.reauthenticate(httpCall);
            return true;
        }
        else if (response != null && response.status === 200) {
            if (this.isSsoResponse(response.text())) {
                this.reauthenticate(httpCall);
                return true;
            }
        }
        return false;
    };
    /**
     * @param {?} html
     * @return {?}
     */
    EcasSessionTimeoutHttpInterceptor.prototype.isSsoResponse = function (html) {
        return html != null && ((html.indexOf('<meta name="Title" content="Redirection to requested service" />') >= 0 &&
            html.indexOf('<meta name="Keywords" content="EU Login, ECAS, Authentication, Security" />') >= 0 &&
            html.indexOf('<meta name="Description" content="EU Login" />') >= 0) ||
            html.indexOf('<meta name="Description" content="European Commission Authentication Service" />') >= 0 ||
            html.indexOf('<title>Mock Login Form</title>') >= 0 ||
            html.indexOf('<title>Redirecting To ECAS</title>') >= 0 ||
            html === '{ success : false, status : "ECAS_AUTHENTICATION_REQUIRED", code : 303, message : "session expired" }' ||
            html === '{ "success" : false, "status" : "ECAS_AUTHENTICATION_REQUIRED", "code" : 303, "message": "session expired" }');
    };
    /**
     * @param {?} httpCall
     * @return {?}
     */
    EcasSessionTimeoutHttpInterceptor.prototype.reauthenticate = function (httpCall) {
        document.location.reload(true);
    };
    /**
     * @param {?} url
     * @param {?=} defaultUrl
     * @return {?}
     */
    EcasSessionTimeoutHttpInterceptor.prototype.domain = function (url, defaultUrl) {
        var /** @type {?} */ parts = this.domainPattern.exec(url);
        if (parts != null) {
            return parts[1];
        }
        else {
            return defaultUrl;
        }
    };
    /**
     * @param {?} httpCall
     * @return {?}
     */
    EcasSessionTimeoutHttpInterceptor.prototype.isCorsRequest = function (httpCall) {
        var /** @type {?} */ url = httpCall.url;
        var /** @type {?} */ currentDomain = this.domain(document.location.href);
        var /** @type {?} */ requestedDomain = this.domain(url, currentDomain);
        return !this.ecasDomainPattern.test(url) && requestedDomain !== currentDomain;
    };
    return EcasSessionTimeoutHttpInterceptor;
}());

var KeepAliveHttpInterceptor = (function () {
    /**
     * @param {?} http
     * @param {?=} keepAliveRepeatTime
     * @param {?=} keepAliveUrl
     */
    function KeepAliveHttpInterceptor(http, keepAliveRepeatTime, keepAliveUrl) {
        var _this = this;
        this.keepAliveRepeatTime = 20 * 60 * 1000;
        this.keepAliveUrl = 'index.html';
        this.http = http;
        if (keepAliveRepeatTime != null) {
            this.keepAliveRepeatTime = keepAliveRepeatTime;
        }
        if (keepAliveUrl != null) {
            this.keepAliveUrl = keepAliveUrl;
        }
        setInterval(function () {
            _this.keepAlive();
        }, this.keepAliveRepeatTime);
    }
    /**
     * @param {?} httpCall
     * @return {?}
     */
    KeepAliveHttpInterceptor.prototype.interceptRequest = function (httpCall) {
        return httpCall;
    };
    /**
     * @return {?}
     */
    KeepAliveHttpInterceptor.prototype.keepAlive = function () {
        this.http.get(this.keepAliveUrl).toPromise().then();
    };
    return KeepAliveHttpInterceptor;
}());

/* UX-PIPES */
/* -------- */

export { UxOrderByPipe, UxOrderByPipeModule, UxFilterPipe, UxFilterPipeModule, UxTruncatePipe, UxTruncatePipeModule, UxCurrencyPipe, UxCurrencyPipeModule, UxTrackScrollDirective, UxTrackScrollDirectiveModule, UxMaxlengthDirective, UxMaxlengthModule, UxTooltip, UxTooltipModule, UxAutoResizeDirective, UxAutoResizeModule, UxCollapsibleDirective, UxCollapsibleDirectiveModule, UxLanguageSelectorComponent, UxLanguageSelectorComponentModule, UxLanguage, UxEuLanguages, UxSearchInputComponent, UxSearchInputComponentModule, UxTagComponent, UxTagComponentModule, UxIconComponent, UxIconComponentModule, UxIconToggleComponent, UxIconToggleComponentModule, UxIconBoxComponent, UxIconBoxComponentModule, UxMarkerComponent, UxMarkerComponentModule, UxLabelComponent, UxLabelSubLabelTagDirective, UxLabelComponentModule, UxDateTagComponent, UxDateTagComponentModule, UxToolbarFilterComponent, UxToolbarFilterComponentModule, UxBlockDocumentComponent, UxBlockDocumentComponentModule, UxBlockContentComponent, UxBlockContentComponentModule, UxProgressCircleComponent, UxProgressCircleComponentModule, UxPanelComponent, UxPanelHeaderTagDirective, UxPanelFooterTagDirective, UxPanelHeaderRightContentTagDirective, UxPanelHeaderWithDescendantsTagDirective, UxPanelFooterWithDescendantsTagDirective, UxPanelHeaderRightContentWithDescendantsTagDirective, UxPanelHeaderStatusIndicatorContentTagDirective, UxPanelComponentModule, UxPanelsComponent, UxPanelsComponentModule, UxActionBoxComponent, UxActionBoxComponentModule, UxActionBoxItem, UxAccordionPanelComponent, UxAccordionPanelComponentModule, UxAccordionPanelsComponent, UxAccordionPanelsComponentModule, UxAlertComponent, UxAlertComponentModule, UxDashboardCardComponent, UxDashboardCardComponentModule, UxControlFeedbackComponent, UxControlFeedbackComponentModule, UxFieldComponent, UxFieldComponentModule, UxFieldCalendarComponent, UxFieldCalendarComponentModule, UxFieldsetComponent, UxFieldsetComponentModule, UxSwitchComponent, UxSwitchComponentModule, UxBadgeComponent, UxBadgeComponentModule, UxBadgeGroupComponent, UxBadgeGroupComponentModule, UxButtonComponent, UxButtonComponentModule, UxDropdownButtonComponent, UxDropdownButtonComponentModule, UxDropdownButtonItemComponent, UxDropdownButtonItemComponentModule, UxGrowlComponent, UxGrowlComponentModule, UxLayoutAppShellComponent, UxAppShellSidebarContentTagDirective, UxAppShellSidebarHeaderContentTagDirective, UxAppShellHeaderRightContentTagDirective, UxAppShellHeaderTitleContentTagDirective, UxAppShellMainContentTagDirective, UxAppShellNavBarItemsContentTagDirective, UxLayoutAppShellComponentModule, UxLayoutAppComponent, UxLayoutAppSidebarContainerTagDirective, UxLayoutAppMainContainerTagDirective, UxLayoutAppComponentModule, UxLayoutAppMainComponent, UxLayoutAppMainHeaderContainerTagDirective, UxLayoutAppMainBannerTopContainerTagDirective, UxLayoutAppMainNavBarContainerTagDirective, UxLayoutAppMainTopMessageContainerTagDirective, UxLayoutAppMainBreadcrumbsContainerTagDirective, UxLayoutAppMainContentContainerTagDirective, UxLayoutAppMainBannerBottomContainerTagDirective, UxLayoutAppMainFooterContainerTagDirective, UxLayoutAppMainComponentModule, UxLayoutHeaderComponent, UxLayoutHeaderRightContentTagDirective, UxLayoutHeaderUserProfileLinksTagDirective, UxLayoutHeaderTitleContentTagDirective, UxLayoutHeaderComponentModule, UxLayoutHeaderProfileComponent, UxLayoutHeaderProfileMenuContentTagDirective, UxLayoutHeaderProfileNotLoggedInContentTagDirective, UxLayoutHeaderProfileComponentModule, UxLayoutNavBarComponent, UxLayoutNavBarElementsContentTagDirective, UxLayoutNavBarLeftActionsContentTagDirective, UxLayoutNavBarComponentModule, UxLayoutNavBarTopMenuComponent, UxLayoutNavBarTopMenuComponentModule, UxLayoutNavBarActionsComponent, UxLayoutNavBarActionsComponentModule, UxLayoutNavBarActionItemComponent, UxLayoutNavBarOverlayPanelContentTagDirective, UxLayoutNavBarActionItemComponentModule, UxLayoutNavBarLeftActionsComponent, UxLayoutNavBarLeftActionsComponentModule, UxLayoutNavBarLeftActionItemComponent, UxLayoutNavBarLeftActionItemComponentModule, UxLayoutNavBarLeftActionItemSidebarToggleComponent, UxLayoutNavBarLeftActionItemSidebarToggleComponentModule, UxLayoutNavBarElementsComponent, UxLayoutNavBarElementsComponentModule, UxLayoutNavBarElementItemComponent, UxLayoutNavBarElementItemComponentModule, UxLayoutBreadcrumbsComponent, UxLayoutBreadcrumbsComponentModule, UxLayoutOverlayPanelComponent, UxLayoutOverlayPanelHeaderTagDirective, UxLayoutOverlayPanelInnerTagDirective, UxLayoutOverlayPanelFooterTagDirective, UxLayoutOverlayPanelComponentModule, UxLayoutTopMessageComponent, UxLayoutTopMessageComponentModule, UxLayoutSidebarComponent, UxLayoutSidebarHeaderTagDirective, UxLayoutSidebarContentTagDirective, UxLayoutSidebarComponentModule, UxLayoutSidebarItemComponent, UxLayoutSidebarItemComponentModule, UxLayoutSidebarItemsComponent, UxLayoutSidebarItemsComponentModule, UxLayoutFooterComponent, UxLayoutFooterAppInfosTagDirective, UxLayoutFooterLinksTagDirective, UxLayoutFooterComponentModule, UxLayoutBannerComponent, UxLayoutBannerComponentModule, UxLayoutHorizontalComponent, UxLayoutHorizontalLeftTagDirective, UxLayoutHorizontalRightTagDirective, UxLayoutHorizontalCenterTagDirective, UxLayoutHorizontalModule, UxLayoutPageHeaderComponent, UxLayoutPageHeaderActionsContentTagDirective, UxLayoutPageHeaderActionIconsContentTagDirective, UxLayoutPageHeaderComponentModule, UxDashboardButtonsComponent, UxDashboardButtonsComponentModule, UxDashboardButtonComponent, UxDashboardButtonComponentModule, UxListItemComponent, UxListItemSubLabelTagDirective, UxListItemContentTagDirective, UxListItemComponentModule, UxListItemsComponent, UxListItemsComponentModule, UxTabComponent, UxTabComponentModule, UxTabsComponent, UxTabsComponentModule, UxTab, UxTimelineItemsComponent, UxTimelineItemsComponentModule, UxTimelineItemComponent, UxTimelineItemComponentModule, UxWizardStepComponent, UxWizardStepContentTagDirective, UxWizardStepComponentModule, UxWizardStepsComponent, UxWizardStepsComponentModule, UxTreeListComponent, UxTreeListToolbarContentTagDirective, UxTreeListComponentModule, UxTreeListItemComponent, UxTreeListItemCustomContentTagDirective, UxTreeListItemRightContentTagDirective, UxTreeListItemSubLabelTagDirective, UxTreeListItemDetailsContentTagDirective, UxTreeListItemSubContainerContentTagDirective, UxTreeListItemComponentModule, UxModalComponent, UxModalBodyTagDirective, UxModalFooterTagDirective, UxModalComponentModule, UxCarouselItemComponent, UxCarouselItemComponentModule, UxCarouselItemsComponent, UxCarouselItemsComponentModule, UxTreeNodeComponent, UxTreeComponent, UxTreeComponentModule, UxTimebarItemUI, UxTimebarComponent, UxTimebarComponentModule, UxTimebarItem, UxAutocompleteTagItem, UxAutocompleteTagComponent, UxAutocompleteTagComponentModule, UxButtonGroupComponent, UxButtonGroupComponentModule, UxButtonGroupItemComponent, UxButtonGroupItemComponentModule, UxSplitButtonComponent, UxSplitButtonComponentModule, UxMessageBoxComponent, UxMessageBoxFooterTagDirective, UxMessageBoxComponentModule, UxNotificationItemComponent, UxNotificationItemComponentModule, UxNotificationsPanelComponent, UxNotificationsPanelComponentModule, UxStickyComponent, UxStickyComponentModule, UxPieChartComponent$$1 as UxPieChartComponent, UxPieChartComponentModule$$1 as UxPieChartComponentModule, UxPieChartGroupComponent, UxPieChartGroupComponentModule, UxLink, UxMessage, UxModal, UxService, UxModule$$1 as UxModule, UxAllModule$$1 as UxAllModule, UX_COLORS, UX_I18N, HttpCall, UxHttp$$1 as UxHttp, CachePreventionHttpInterceptor, CsrfPreventionHttpInterceptor, EcasSessionTimeoutHttpInterceptor, KeepAliveHttpInterceptor };
