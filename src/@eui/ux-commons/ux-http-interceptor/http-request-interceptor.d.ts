import { HttpCall } from './http-call';
export interface HttpRequestInterceptor {
    interceptRequest(httpCall: HttpCall): HttpCall;
}
