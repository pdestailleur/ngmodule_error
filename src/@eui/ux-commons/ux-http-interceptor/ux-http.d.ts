import { Http, RequestOptionsArgs, Response, ConnectionBackend, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { HttpRequestInterceptor, HttpResponseInterceptor, HttpCall } from './index';
export declare class UxHttp extends Http {
    protected requestInterceptors: HttpRequestInterceptor[];
    protected responseInterceptors: HttpResponseInterceptor[];
    constructor(backend: ConnectionBackend, defaultOptions: RequestOptions, requestInterceptors?: HttpRequestInterceptor[], responseInterceptors?: HttpResponseInterceptor[]);
    get(url: string, options?: RequestOptionsArgs): Observable<Response>;
    post(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>;
    put(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>;
    delete(url: string, options?: RequestOptionsArgs): Observable<Response>;
    patch(url: string, body: string, options?: RequestOptionsArgs): Observable<Response>;
    head(url: string, options?: RequestOptionsArgs): Observable<Response>;
    addRequestInterceptor(interceptor: HttpRequestInterceptor): void;
    addResponseInterceptor(interceptor: HttpResponseInterceptor): void;
    protected interceptCall(methodName: string, url: string, options?: RequestOptionsArgs, body?: string): Observable<Response>;
    protected interceptRequest(httpCall: HttpCall): HttpCall;
    protected interceptResponse(httpCall: HttpCall): Observable<Response>;
}
