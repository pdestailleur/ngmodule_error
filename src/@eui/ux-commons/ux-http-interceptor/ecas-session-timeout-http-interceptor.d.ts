import { HttpResponseInterceptor, HttpCall } from './index';
import { Observable } from 'rxjs/Rx';
import { Response } from '@angular/http';
export declare class EcasSessionTimeoutHttpInterceptor implements HttpResponseInterceptor {
    protected domainPattern: RegExp;
    protected ecasDomainPattern: RegExp;
    interceptResponse(httpCall: HttpCall): HttpCall;
    protected checkForECASSessionTimeout(httpCall: HttpCall): Observable<Response>;
    protected checkRequestSuccessForECASSessionTimeout(response: Response, httpCall: HttpCall): Response;
    protected checkRequestErrorForECASSessionTimeout(response: Response, httpCall: HttpCall): boolean;
    protected isSsoResponse(html: string): boolean;
    protected reauthenticate(httpCall: HttpCall): void;
    protected domain(url: string, defaultUrl?: string): string;
    protected isCorsRequest(httpCall: HttpCall): boolean;
}
