import { RequestOptionsArgs, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
export declare class HttpCall {
    method: string;
    url: string;
    body: string;
    options: RequestOptionsArgs;
    response: Observable<Response>;
    isCancelled: boolean;
}
