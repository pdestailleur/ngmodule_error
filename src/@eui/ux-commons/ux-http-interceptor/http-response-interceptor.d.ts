import { HttpCall } from './http-call';
export interface HttpResponseInterceptor {
    interceptResponse(httpCall: HttpCall): HttpCall;
}
