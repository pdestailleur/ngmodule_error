import { HttpRequestInterceptor, HttpCall } from './index';
export declare class CsrfPreventionHttpInterceptor implements HttpRequestInterceptor {
    interceptRequest(httpCall: HttpCall): HttpCall;
}
