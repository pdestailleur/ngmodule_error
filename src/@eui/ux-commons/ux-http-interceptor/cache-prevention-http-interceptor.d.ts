import { HttpRequestInterceptor, HttpCall } from './index';
export declare class CachePreventionHttpInterceptor implements HttpRequestInterceptor {
    interceptRequest(httpCall: HttpCall): HttpCall;
}
