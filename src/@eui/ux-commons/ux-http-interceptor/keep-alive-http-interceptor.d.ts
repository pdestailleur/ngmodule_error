import { HttpRequestInterceptor, HttpCall } from './index';
import { Http } from '@angular/http';
export declare class KeepAliveHttpInterceptor implements HttpRequestInterceptor {
    protected http: Http;
    protected keepAliveRepeatTime: number;
    protected keepAliveUrl: string;
    constructor(http: Http, keepAliveRepeatTime?: number, keepAliveUrl?: string);
    interceptRequest(httpCall: HttpCall): HttpCall;
    protected keepAlive(): void;
}
