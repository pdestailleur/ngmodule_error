import { ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl } from '@angular/forms';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { UxLink } from '../shared/models/ux-link.model';
import { UxMessage } from '../shared/models/ux-message.model';
import { UxLanguage } from '../ux-components/ux-language-selector/ux-language';
export declare class UxModal {
    id: string;
    isOpen: boolean;
    isFullHeight: boolean;
    constructor(values?: Object);
}
export declare class UxService {
    private router;
    isNavigationBlockDocumentActive: boolean;
    isLoggingActive: boolean;
    activeLanguage: UxLanguage;
    shadowDomRoot: ElementRef;
    breadcrumbLinks: Array<UxLink>;
    growlMessages: UxMessage[];
    isGrowlSticky: boolean;
    growlLife: number;
    isBlockDocumentActive: boolean;
    isSidebarActive: boolean;
    isSidebarStateOpen: boolean;
    isSidebarAlwaysVisible: boolean;
    isSidebarCollapsible: boolean;
    isSidebarStateCloseWithIcons: boolean;
    isSidebarInnerActive: boolean;
    isSidebarMobileVisible: boolean;
    activeBreakpoint: BehaviorSubject<string>;
    windowWidth: number;
    windowHeight: BehaviorSubject<number>;
    activeModals: UxModal[];
    sidebarLinks: UxLink[];
    navigationStartCustomHandler: () => void;
    navigationEndCustomHandler: () => void;
    constructor(router: Router);
    readonly appRouter: Router;
    shadowDomQuery(querySelector: string): HTMLElement;
    setSidebarLinks(links: UxLink[]): void;
    readonly hasBreadcrumbLinks: boolean;
    clearBreadcrumbLinks(): void;
    addBreadcrumbLink(link: UxLink): void;
    growl(msg: UxMessage, isSticky?: boolean, isMultiple?: boolean, life?: number): void;
    clearGrowl(): void;
    blockDocument(): void;
    unblockDocument(): void;
    setActiveBreakpoint(width: number): void;
    isMobile(): boolean;
    isTablet(): boolean;
    isLtDesktop(): boolean;
    isDesktop(): boolean;
    isLargeDesktop(): boolean;
    setWindowHeight(height: number): void;
    translate(key: string, languageCode?: string): string;
    diffDays(dateStart: Date, dateEnd: Date): number;
    diffDaysFromToday(date: Date): number;
    resetSidebarClasses(): void;
    toggleInnerSidebar(): void;
    handleError(error: any): any;
    openModal(modalId?: string): void;
    openMessageBox(messageBoxId?: string): void;
    closeMessageBox(messageBoxId?: string): void;
    isModalOpen(modalId?: string): boolean;
    closeModal(modalId?: string): void;
    findModalIndex(modalId: string): number;
    /**
     * Provides read-only equivalent of jQuery's position function:
     * http://api.jquery.com/position/
     */
    position(nativeEl: any): {
        width: number;
        height: number;
        top: number;
        left: number;
    };
    /**
     * Provides read-only equivalent of jQuery's offset function:
     * http://api.jquery.com/offset/
     */
    offset(nativeEl: any): {
        width: number;
        height: number;
        top: number;
        left: number;
    };
    consumeEvent(event: Event): boolean;
    validateEmail(c: FormControl): {
        validateEmail: {
            valid: boolean;
        };
    };
    private navigationStartHandler(event);
    private navigationEndHandler(event);
    private readonly window;
    private readonly document;
    private getStyle(nativeEl, cssProp);
    /**
     * Checks if a given element is statically positioned
     */
    private isStaticPositioned(nativeEl);
    /**
     * returns the closest, non-statically positioned parentOffset of a given element
     * @param nativeEl
     */
    private parentOffsetEl(nativeEl);
}
