export declare class UxMessage {
    severity?: string;
    summary?: string;
    detail?: string;
}
