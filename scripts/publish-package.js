var fs = require('fs');
var root = require('./helpers').root;
const ncp = require('ncp').ncp;
const rimraf = require('rimraf');

var src = root('package.json');
var dst = root('dist/package.json');
var file = require(src);

var scripts = file.scripts;
file.scripts = undefined;
file.devDependencies = undefined;
file.peerDependencies = file.dependencies;
file.dependencies = undefined;
file.private = undefined;

var sourceDir = root('dist');
fs.readdir(sourceDir, function(err, files) {
    if (err) {
        console.log(JSON.stringify(err));
    } else {
        files.forEach(function(file) {
            fs.stat(sourceDir + '/' + file,
                function(err, stats) {
                    if (err) {
                        console.log(JSON.stringify(err));
                    } else {
                        const deleteFile = (file.indexOf('index') < 0 && file.indexOf('typings') < 0 && file.indexOf('styles.css') < 0) || file.indexOf('html') > 0
                        if (stats.isFile() && deleteFile) {
                            fs.unlink(sourceDir + '/' + file, function(err) {
                                if (err) {
                                    console.log(file, err);
                                }
                                console.log('Removed ' + sourceDir + '\\' + file);
                            });
                        } else if (stats.isDirectory()) {
                            if (file.indexOf('environments') >= 0) {
                                rimraf(sourceDir + '/' + file, function(err) {
                                    if (err) { console.log(err) }
                                });
                            }
                        }
                    }
                }
            );
        })
    }
});
writeFile(dst, file);
ncp(root('README.md'), root('dist/README.md'), {}, function(err) { if (err) { console.log(err) } });
ncp(root('CHANGELOG.md'), root('dist/CHANGELOG.md'), {}, function(err) { if (err) { console.log(err) } });



function writeFile(file, content) {
    fs.writeFile(file, JSON.stringify(content, null, 2), error => {
        if (error) {
            console.log(error)
        }
    });
}
