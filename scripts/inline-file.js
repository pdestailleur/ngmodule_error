const fs = require('fs');
const path = require('path');
const glob = require('glob');
const root = require('./helpers').root;
const rimraf = require('rimraf');
const ncp = require('ncp').ncp;

const stylesBlockRegExp = /styleUrls*:\s*(\[[^\]]+])/i;
const templateBlockRegExp = /templateUrl\s*:\s*('.*')/i;

const srcFolder = root('src');
const distFolder = root('dist');
const dtsFolder = root('dts');


function getJsFiles() {
    return glob.sync('**/*.ts', { cwd: distFolder, dot: true })
        .map(file => { return { path: path.dirname(file), name: path.basename(file) } });
}

function readFile(fullFileName) {
    return fs.readFileSync(fullFileName, { encoding: 'UTF-8' });
}

function writeFile(fullFileName, content) {
    fs.writeFileSync(fullFileName, content, { flag: 'w' });
}

function inlineStyles(file) {
    const fullFileName = distFolder + '/' + file.path + '/' + file.name;
    const fileString = readFile(fullFileName);
    var result = fileString;

    while (styleUrls = result.match(stylesBlockRegExp)) {
        const filename = styleUrls[1].replace(/\[/g, '').replace(/\]/g, '').replace(/\'/g, '').trim();
        const fullPath = srcFolder + '/' + file.path + '/' + filename;
        const distfullPath = distFolder + '/' + file.path + '/' + filename;

        if (fs.existsSync(fullPath)) {
            const cssString = readFile(fullPath);

            const strippedString = cssString.split('\n')
                .map(line => line.trim())
                .join('').split('\b')
                .map(line => line.trim())
                .join('')
                .replace(/\r?\n|\r/g, '')
                .replace(/\'/g, '\'');

            result = result.replace(styleUrls[0], 'styles: [\'' + strippedString + '\']');
            fs.unlinkSync(distfullPath);
            console.log('INF: including CSS', fullPath);
        } else {
            result = result.replace(styleUrls[0], 'styles: [\'\']');

            console.log('ERR: including CSS', fullPath, ': file not found');
        }
    }

    if (result !== fileString) {
        writeFile(fullFileName, result);
    }
}

function inlineHtml(file) {
    const fullFileName = distFolder + '/' + file.path + '/' + file.name;
    const fileString = readFile(fullFileName);
    var result = fileString;

    while (templateUrl = result.match(templateBlockRegExp)) {
        const filename = templateUrl[1].replace(/\[/g, '').replace(/\]/g, '').replace(/\'/g, '');
        const fullPath = srcFolder + '/' + file.path + '/' + filename;
        const distfullPath = distFolder + '/' + file.path + '/' + filename;

        if (fs.existsSync(fullPath)) {
            const htmlString = readFile(fullPath);

            const strippedString = htmlString.split('\n')
                .map(line => line.trim())
                .join('').split('\b')
                .map(line => line.trim())
                .join('')
                .replace(/\r?\n|\r/g, '')
                .replace(/\'/g, '\\\'');

            result = result.replace(templateUrl[0], 'template: \'' + strippedString + '\'');
            fs.unlinkSync(distfullPath);
            console.log('INF: including HTML', fullPath);
        } else {
            result = result.replace(templateUrl[0], 'template: \'\'');

            console.log('ERR: including HTML', fullPath, ': file not found');
        }
    }

    if (result !== fileString) {
        writeFile(fullFileName, result);
    }
}

// Main loop

rimraf(distFolder, {}, function(err) {
    fs.rename(dtsFolder, distFolder, function(err) {
        ncp(srcFolder, distFolder, {}, function(err) {
            rimraf(distFolder + '/app', {}, function(err) {
                rimraf(dtsFolder, {}, function(err) {
                    getJsFiles().map(file => {
                        inlineStyles(file);
                        inlineHtml(file);
                    });
                });
            });
        });
    });
});