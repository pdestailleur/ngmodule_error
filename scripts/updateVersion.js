var fs = require('fs');
var root = require('./helpers').root;

var src = root('package.json');
var buildInfo = root('src/environments/buildInfo.ts');
var file = require(src);


var version = file.version;

var semVer = version.split('-')[0].split('.');

var part = -1;
var rc = '';

process.argv.forEach(function(val, index, array) {
    if (val === '--major') {
        part = 0;
    } else if (val === '--minor') {
        part = 1;
    } else if (val === '--bugfix') {
        part = 2;
    } else if (val === '--rc') {
        rc = version.split('-');
        if (rc.length > 1) {
            rc = rc[1].split('.')[1];
            rc = '-rc.' + (Number(rc) + 1)
        } else {
            rc = '-rc.1'
        }
    }
});

if (part > -1 || rc != '') {
    if (part > -1){
        semVer.forEach((versionpart, index, semVer) => {
            if (part == index) {
                semVer[index] = Number(versionpart) + 1 + '';
            } else {
                if (index > part) {
                    semVer[index] = 0;
                }
            }
        });
    }
    file.version = semVer.join('.') + rc;
    console.log('new published version: ', file.version);

    writeFile(src, file);
    fs.writeFile(buildInfo,`export const version = '` + file.version + `';\r\nexport const lastMod = new Date();\r\n`);
}

function writeFile(file, content) {
    fs.writeFile(file, JSON.stringify(content, null, 2), error => {
        if (error) {
            console.log(error)
        }
    });
}
